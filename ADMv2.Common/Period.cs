﻿namespace ADMv2.CommonProj
{
    public class Period
    {
        public DateTime StartDate { get; set; }
        public DateTime EndtDate { get; set; }

        public Period() { }

        public Period(DateTime startdate, DateTime endDate)
        {
            StartDate = startdate;
            EndtDate = endDate;
        }
    }
}
