﻿namespace ADMv2.CommonProj
{
    public static class DateTimeExtensions
    {
        #region First
        public static DateTime GetFirstDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        public static string GetFirstDayOfMonthString(DateTime date)
        {
            return date.GetFirstDayOfMonth().ToStringDMY();
        }

        public static DateTime GetFirstDayOfMonth(int month, int year)
        {
            return new DateTime(year, month, 1);
        }

        public static string GetFirstDayOfMonthString(int month, int year)
        {
            return GetFirstDayOfMonth(month, year).ToStringDMY();
        }
        #endregion

        #region Tenth
        public static DateTime GetTenthDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 10);
        }

        public static string GetTenthDayOfMonthString(DateTime date)
        {
            return date.GetTenthDayOfMonth().ToStringDMY();
        }

        public static DateTime GetTenthDayOfMonth(int month, int year)
        {
            return new DateTime(year, month, 10);
        }

        public static string GetTenthDayOfMonthString(int month, int year)
        {
            return GetTenthDayOfMonth(year, month).ToStringDMY();
        }
        #endregion

        #region Last
        public static DateTime GetLastDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }

        public static string GetLastDayOfMonthString(DateTime date)
        {
            return date.GetLastDayOfMonth().ToStringDMY();
        }

        public static DateTime GetLastDayOfMonth(int month, int year)
        {
            return new DateTime(year, month, DateTime.DaysInMonth(year, month));
        }

        public static string GetLastDayOfMonthString(int month, int year)
        {
            return GetLastDayOfMonth(month, year).ToStringDMY();
        }
        #endregion

        public static string ToStringDMY(this DateTime dateTime)
        {
            return dateTime.ToString("dd.MM.yyyy");
        }

        public static bool IsSameMonthYear(this DateTime datetime, DateTime compare)
        {
            return datetime.Month == compare.Month && datetime.Year == compare.Year;
        }
    }
}