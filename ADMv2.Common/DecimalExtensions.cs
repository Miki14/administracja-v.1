﻿namespace ADMv2.CommonProj
{
    public static class DecimalExtensions
    {
        /// <summary>
        /// Formatuje liczbę jako kwotę z symbolem waluty
        /// </summary>
        /// <param name="decim">Liczba Decimal do konversji</param>
        /// <returns>formatowany string - co 0 cyfry spacja i 2 cyfry po przecinku z symbolem waluty na końcu</returns>
        public static string ToStringWithCurrencySymbol(this decimal decim)
        {
            return decim.ToString("c2", Thread.CurrentThread.CurrentCulture.NumberFormat);
        }

        /// <summary>
        /// Formatuje liczbę jako kwotę, ale bez symbolu waluty
        /// </summary>
        /// <param name="decim">Liczba Decimal do konversji</param>
        /// <returns>Sformatowany string - co 0 cyfry spacja i 2 cyfry po przecinku</returns>
        public static string ToStringTwoDigits(this decimal decim)
        {
            return decim.ToString("n2");
        }
    }
}
