using FluentAssertions;

namespace ADM.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            //arrang
            int a = 1;
            int b = 2;

            //act
            int c = a + b;

            //assert
            c.Should().Be(3);
        }
    }
}