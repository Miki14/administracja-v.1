﻿using System.Drawing;
using System.Globalization;

namespace Administracja
{
    static class Fonty
    {
        public static Font Header = new Font("Arial", 14, FontStyle.Bold);
        public static Font Period = new Font("Arial", 14);
        public static Font Normal = new Font("Arial", 10);
        public static Font Dane = new Font("Arial", 11, FontStyle.Bold);
        public static Font Goods = new Font("Arial", 11);
        public static Font Small = new Font("Arial", 8);

        public static Pen NormalPen = new Pen(Brushes.Black);
        public static Pen DashPen = new Pen(Brushes.Black);

        public static StringFormat Left = new StringFormat()
        {
            Alignment = StringAlignment.Far
        };

        //liczba.ToString i w nawiasie podajemy jaki ma być separator, przecinek czy kropka
        public static CultureInfo Kropka = CultureInfo.InvariantCulture;
        public static CultureInfo Przecinek = new CultureInfo("pl-PL");

        static Fonty()
        {
            const float tmpLength = 10;
            DashPen.DashPattern = new[] { tmpLength, tmpLength, tmpLength, tmpLength };
        }
    }
}
