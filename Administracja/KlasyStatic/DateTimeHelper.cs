﻿using System;

namespace Administracja.KlasyStatic
{
    public static class DateTimeHelper
    {
        #region First
        public static DateTime GetDateFirstDayOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        public static string GetDateFirstDayOfMonthString(DateTime date)
        {
            return GetDateFirstDayOfMonth(date).ToStringDMY();
        }

        public static DateTime GetDateFirstDayOfMonth(int month, int year)
        {
            return new DateTime(year, month, 1);
        }

        public static string GetDateFirstDayOfMonthString(int month, int year)
        {
            return GetDateFirstDayOfMonth(month, year).ToStringDMY();
        }
        #endregion

        #region Tenth
        public static DateTime GetDateTenthDayOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, 10);
        }

        public static string GetDateTenthDayOfMonthString(DateTime date)
        {
            return GetDateTenthDayOfMonth(date).ToStringDMY();
        }

        public static DateTime GetDateTenthDayOfMonth(int month, int year)
        {
            return new DateTime(year, month, 10);
        }

        public static string GetDateTenthDayOfMonthString(int month, int year)
        {
            return GetDateTenthDayOfMonth(year, month).ToStringDMY();
        }
        #endregion

        #region Last
        public static DateTime GetDateLastDayOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }

        public static string GetDateLastDayOfMonthString(DateTime date)
        {
            return GetDateLastDayOfMonth(date).ToStringDMY();
        }

        public static DateTime GetDateLastDayOfMonth(int month, int year)
        {
            return new DateTime(year, month, DateTime.DaysInMonth(year, month));
        }

        public static string GetDateLastDayOfMonthString(int month, int year)
        {
            return GetDateLastDayOfMonth(month, year).ToStringDMY();
        }
        #endregion

        public static string ToStringDMY(this DateTime dateTime)
        {
            return dateTime.ToString("dd.MM.yyyy");
        }
    }
}