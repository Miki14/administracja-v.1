﻿using System;
using System.Windows.Forms;

namespace Administracja
{
    class Wyjatki
    {
        public static void Error(Exception ex)
        {
            if (!Program.Zamykam)
            {
                string komunikat = ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace;
                MessageBox.Show(komunikat, @"Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Raport.Instance.Raportuj(komunikat);

                Program.Zamykam = true;
                Application.Exit();
            }
        }

        public static DialogResult Komunikat(string wiadomosc, string naglowek, MessageBoxButtons przyciski, MessageBoxIcon ikona)
        {
            if (!Program.Zamykam)
            {
                Raport.Instance.Raportuj("Kom: " + wiadomosc + "(" + naglowek + ")");
                return MessageBox.Show(wiadomosc, naglowek, przyciski, ikona);
            }
            return DialogResult.Abort;
        }
    }
}
