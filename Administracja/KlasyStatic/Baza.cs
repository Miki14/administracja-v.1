﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Administracja.Model;
using FirebirdSql.Data.FirebirdClient;
using Newtonsoft.Json;

namespace Administracja
{
    public static class Baza
    {
        public static Dictionary<int, string> GrupyOsob =
            new Dictionary<int, string> {
                { 1, "Mieszkańcy"},
                { 2, "Użytkowe"},
                { 3 , "Stałe Ap."},
                { 4 , "Krótko. Ap."},
                { 5 , "Parking"},
                { 100 , "Ukryci"}
            };

        public static string ConnectionString
        {
            get; private set;
        }

        public static DataModel ModelData;

        private static FbConnection _connection;
        private static FbDataReader _myreader;
        private static FbTransaction _transaction;
        private static bool _isConnected;
        private static readonly string JsonPath = Application.StartupPath + @"\Baza\Data.json";

        static Baza()
        {
            ModelData = JsonConvert.DeserializeObject<DataModel>(File.ReadAllText(JsonPath));
        }

        public static void OpenConnection()
        {
            if (!_isConnected)
            {
                try
                {
                    if (ConnectionString != null)
                    {
                        _connection = new FbConnection(ConnectionString);
                        _connection.Open();
                    }

                    _isConnected = true;
                }
                catch (Exception ex)
                {
                    Wyjatki.Error(ex);
                }
            }
        }

        public static void CloseConnection()
        {
            if (_isConnected)
            {
                try
                {
                    File.WriteAllText(JsonPath, JsonConvert.SerializeObject(ModelData, Formatting.Indented));

                    _connection.Close();

                    _isConnected = false;
                }
                catch (Exception ex)
                {
                    Wyjatki.Error(ex);
                }
            }
        }

        public static void ConnectionStringSet(string database, string user = "sysdba", string password = "masterkey", string server = "localhost")
        {
            ConnectionString = "User ID=" + user + ";Password=" + password + ";Database=" + server + ":" + database + ";Charset=WIN1250";
        }

        public static void Zapisz(string sqlCommandText)
        {
            try
            {
                Raport.Instance.Raportuj("Zapisz = " + sqlCommandText);

                _transaction = _connection.BeginTransaction();
                new FbCommand(sqlCommandText, _connection, _transaction).ExecuteNonQuery();
                _transaction.Commit();
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
                _transaction.Rollback();
            }
        }

        public static void Zapisz(params string[] sqlCommandText)
        {
            try
            {
                _transaction = _connection.BeginTransaction();

                foreach (string sqlCommand in sqlCommandText)
                {
                    Raport.Instance.Raportuj("ZapiszParams = " + sqlCommand);
                    new FbCommand(sqlCommand, _connection, _transaction).ExecuteNonQuery();
                }
                _transaction.Commit();
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
                _transaction.Rollback();
            }
        }

        /// <summary>
        /// Wczytuje z bazy pojedynczą wartość
        /// </summary>
        /// <param name="sqlCommandText"></param>
        /// <returns>Pojedynczy string</returns>
        public static string Wczytaj(string sqlCommandText)
        {
            string ret = "";

            try
            {
                Raport.Instance.Raportuj("Wczytaj = " + sqlCommandText);

                _myreader = new FbCommand(sqlCommandText, _connection).ExecuteReader();
                while (_myreader.Read())
                {
                    ret = _myreader[0].ToString();
                }
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
            finally
            {
                _myreader.Close();
            }

            if (string.IsNullOrWhiteSpace(ret)) ret = "0";

            return ret;
        }

        /// <summary>
        /// Wczytuje z bazy jadną encję
        /// </summary>
        /// <param name="sqlCommandText"></param>
        /// <returns></returns>
        public static List<string> Wczytaj1D(string sqlCommandText)
        {
            var ret = new List<string>();

            try
            {
                Raport.Instance.Raportuj("Wczytaj1D = " + sqlCommandText);

                _myreader = new FbCommand(sqlCommandText, _connection).ExecuteReader();
                while (_myreader.Read())
                {
                    ret.Clear();
                    for (int i = 0; i < _myreader.FieldCount; i++)
                    {
                        ret.Add(_myreader[i].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
            finally
            {
                _myreader.Close();
            }

            return ret;
        }

        /// <summary>
        /// Wczytuje z bazy całą tabelę
        /// </summary>
        /// <param name="sqlCommandText"></param>
        /// <returns></returns>
        public static List<List<string>> Wczytaj2D(string sqlCommandText)
        {
            var ret = new List<List<string>>();

            try
            {
                Raport.Instance.Raportuj("Wczytaj2D = " + sqlCommandText);

                _myreader = new FbCommand(sqlCommandText, _connection).ExecuteReader();
                while (_myreader.Read())
                {
                    ret.Add(new List<string>());
                    for (int i = 0; i < _myreader.FieldCount; i++)
                    {
                        ret[ret.Count - 1].Add(_myreader[i].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
            finally
            {
                _myreader.Close();
            }

            return ret;
        }

        /// <summary>
        /// Wczytuje z bazy jedną kolumną
        /// </summary>
        /// <param name="sqlCommandText"></param>
        /// <returns></returns>
        public static List<string> Wczytaj1Dp(string sqlCommandText)
        {
            var ret = new List<string>();

            try
            {
                Raport.Instance.Raportuj("Wczytaj1Dp = " + sqlCommandText);

                _myreader = new FbCommand(sqlCommandText, _connection).ExecuteReader();
                while (_myreader.Read())
                {
                    ret.Add(_myreader[0].ToString());
                }
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
            finally
            {
                _myreader.Close();
            }

            return ret;
        }

        public static int Ustaw(string tabela)
        {
            string maxId = "0";

            try
            {
                maxId = Wczytaj("SELECT MAX(ID) AS ID FROM " + tabela);

                string sql = "SET GENERATOR NUMERACJA_" + tabela + " TO " + maxId;

                if (tabela == "FAKTURY")
                {
                    var maxNr = Wczytaj("SELECT MAX(Numer) AS Numer FROM FAKTURY");

                    Zapisz(sql, "SET GENERATOR NUMER_FAKTURY TO " + maxNr);
                }
                else if (tabela == "WPLATY")
                {
                    var maxNr = Wczytaj("SELECT MAX(CAST(NUMER_DOK AS Int)) FROM WPLATY WHERE DOKUMENT = 'KP'");

                    Zapisz(sql, "SET GENERATOR NUMER_KP TO " + maxNr);
                }
                else
                {
                    Zapisz(sql);
                }
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }

            return int.Parse(maxId) + 1;
        }

        public static double LiczBilansOsob(int idOs)
        {
            double ret = 0;
            try
            {
                double oplata = Convert.ToDouble(Wczytaj("SELECT SUM(Kwota) AS Oplata FROM WPLATY WHERE ID_F IN ( SELECT ID FROM FAKTURY WHERE ID_N = '" + idOs + "')").Replace(".", ","));
                double sumaB = Convert.ToDouble(Wczytaj("SELECT SUM(Suma_B) AS Suma_B FROM FAKTURY WHERE ID_N = '" + idOs + "'").Replace(".", ","));

                ret = Math.Round(oplata - sumaB, 2);
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
            return ret;
        }

        public static int GetNextFvNumber(DateTime dataFv, bool checkDay)
        {
            string maxNumber = Wczytaj("SELECT MAX(NUMER) FROM FAKTURY WHERE EXTRACT(MONTH FROM DATA) = " +
                dataFv.Month + " AND EXTRACT(YEAR FROM DATA) = " + dataFv.Year);

            if (checkDay)
            {
                string dayOfmaxNumberStr = Wczytaj("SELECT EXTRACT(DAY FROM DATA) FROM FAKTURY WHERE EXTRACT(MONTH FROM DATA) = " +
                    dataFv.Month + " AND EXTRACT(YEAR FROM DATA) = " + dataFv.Year + "AND NUMER = " + maxNumber);

                int dayOfmaxNumber = Convert.ToInt32(dayOfmaxNumberStr);

                if (dayOfmaxNumber > dataFv.Day)
                {
                    throw new IndexOutOfRangeException("Day of new invoice is lover than existing invoice");
                }
            }

            return Convert.ToInt32(maxNumber) + 1;
        }

        public static int GetNextKpNumber(DateTime dataWplaty)
        {
            string numerStr = Wczytaj("SELECT MAX(CAST(NUMER_DOK as INT)) FROM WPLATY WHERE dokument = 'KP' AND DATA >= '01.01."
                + dataWplaty.Year + "' AND DATA <= '31.12." + dataWplaty.Year + "'");

            int numer = int.TryParse(numerStr, out numer) ? numer : 0;

            return numer + 1;
        }
    }
}
