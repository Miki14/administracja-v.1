﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Administracja
{
    static class OperacjeTekstowe
    {
        private static int _fCzyZero = 4;

        public static void SlowaToUpper(TextBox textBox)
        {
            int poz = textBox.SelectionStart;

            string[] slowa = textBox.Text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            string nazwaBoxText = "";

            for (int i = 0; i < slowa.Length; i++)
            {
                nazwaBoxText += slowa[i].Substring(0, 1).ToUpper() + slowa[i].Substring(1) + " ";
            }
            textBox.Text = nazwaBoxText;

            textBox.SelectionStart = poz;
        }

        public static void PierwszaToUpper(TextBox textBox)
        {
            int poz = textBox.SelectionStart;

            textBox.Text = textBox.Text.Substring(0, 1).ToUpper() + textBox.Text.Substring(1);

            textBox.SelectionStart = poz;
        }

        public static string TrymujDate(string data)
        {
            DateTime dataD = DateTime.Parse(data);

            return dataD.ToString("dd.MM.yyyy");
        }

        public static bool CheckKeys(char key)
        {
            return key == (char)Keys.Enter;
            //najlepiej zrobione:
            //if (e.KeyChar == (char)Keys.Enter)
            //{
            //    zaloguj_Click(this, EventArgs.Empty);
            //    e.KeyChar = (char)0;          //jak nie to 4 razy wywoła funkcje
            //    e.Handled = true;
            //}
        }

        public static string SlownieLiczba(double liczba)//liczby z przedziału 0 - 1kk-1 ze znakiem i z groszami
        {
            string liczbaS = string.Format("{0:N2}", liczba);

            string tysiace = "";
            string setki;
            string zlotych = "złotych";
            string groszy = "groszy";

            string[] tab = liczbaS.Split();
            string grosze = tab[tab.Length - 1].Split(',')[1];
            tab[tab.Length - 1] = tab[tab.Length - 1].Split(',')[0];

            if (liczbaS.Count() > 6)
            {
                tysiace = tab[tab.Length - 2];
                setki = tab[tab.Length - 1];

                int tysiaceI = int.Parse(tysiace);
                if (tysiaceI == 1) tysiace = "tysiąc ";
                else
                {
                    _fCzyZero = 0;

                    if (tysiaceI > 1 && tysiaceI < 5)
                    {
                        tysiace = Cyfry(tysiaceI) + "tysiące ";
                    }
                    else
                    {
                        tysiace = Cyfry(tysiaceI) + "tysięcy ";
                    }
                }
            }
            else
            {
                setki = tab[0];
            }


            if (setki.Substring(setki.Length - 1, 1) == "2" || setki.Substring(setki.Length - 1, 1) == "3" || setki.Substring(setki.Length - 1, 1) == "4")
            {
                try
                {
                    if (setki.Substring(setki.Length - 2, 1) != "1")
                    {
                        zlotych = "złote";
                    }
                }
                catch
                {
                    zlotych = "złote";
                }
            }

            if (grosze.Substring(grosze.Length - 1, 1) == "2" || grosze.Substring(grosze.Length - 1, 1) == "3" || grosze.Substring(grosze.Length - 1, 1) == "4")
            {
                try
                {
                    if (grosze.Substring(grosze.Length - 2, 1) != "1")
                    {
                        groszy = "grosze";
                    }
                }
                catch
                {
                    groszy = "grosze";
                }
            }

            _fCzyZero = 0;
            setki = Cyfry(int.Parse(setki));
            _fCzyZero = 0;
            grosze = Cyfry(int.Parse(grosze));

            return tysiace + setki + zlotych + " i " + grosze + groszy;
        }

        private static string Cyfry(int liczba) //tylko dla liczb max 3 cyfrowych
        {
            string liczbaS;
            string ret = "";

            if (liczba < 0)
            {
                ret = "minus ";
                liczba *= (-1);
            }

            if (liczba < 10)
            {
                liczbaS = liczba.ToString();

                switch (liczbaS)
                {
                    case "0":
                        if (_fCzyZero == 0) ret += "zero";
                        break;
                    case "1":
                        ret += "jeden";
                        break;
                    case "2":
                        ret += "dwa";
                        break;
                    case "3":
                        ret += "trzy";
                        break;
                    case "4":
                        ret += "cztery";
                        break;
                    case "5":
                        ret += "pięć";
                        break;
                    case "6":
                        ret += "sześć";
                        break;
                    case "7":
                        ret += "siedem";
                        break;
                    case "8":
                        ret += "osiem";
                        break;
                    case "9":
                        ret += "dziewięć";
                        break;
                }
                ret += " ";
            }

            _fCzyZero = 1;

            if (liczba > 9 && liczba < 20)
            {
                liczbaS = liczba.ToString();

                switch (liczbaS)
                {
                    case "10":
                        ret += "dziesięć";
                        liczba -= 10;
                        break;
                    case "11":
                        ret += "jedenaście";
                        liczba -= 11;
                        break;
                    case "12":
                        ret += "dwanaście";
                        liczba -= 12;
                        break;
                    case "13":
                        ret += "trzynaście";
                        liczba -= 13;
                        break;
                    case "14":
                        ret += "czternaście";
                        liczba -= 14;
                        break;
                    case "15":
                        ret += "piętnaście";
                        liczba -= 15;
                        break;
                    case "16":
                        ret += "szesnaście";
                        liczba -= 16;
                        break;
                    case "17":
                        ret += "siedemnaście";
                        liczba -= 17;
                        break;
                    case "18":
                        ret += "osiemnaście";
                        liczba -= 18;
                        break;
                    case "19":
                        ret += "dziewiętnaście";
                        liczba -= 19;
                        break;
                }
                ret += " ";
            }

            if (liczba > 19 && liczba < 100)
            {
                liczbaS = liczba.ToString();

                switch (liczbaS.Substring(0, 1))
                {
                    case "2":
                        ret += "dwadzieścia";
                        liczba -= 20;
                        break;
                    case "3":
                        ret += "trzydzieści";
                        liczba -= 30;
                        break;
                    case "4":
                        ret += "czterdzieści";
                        liczba -= 40;
                        break;
                    case "5":
                        ret += "pięćdziesiąt";
                        liczba -= 50;
                        break;
                    case "6":
                        ret += "sześćdziesiąt";
                        liczba -= 60;
                        break;
                    case "7":
                        ret += "siedemdziesiąt";
                        liczba -= 70;
                        break;
                    case "8":
                        ret += "osiemdziesiąt";
                        liczba -= 80;
                        break;
                    case "9":
                        ret += "dziewięćdziesiąt";
                        liczba -= 90;
                        break;
                }
                ret += " " + Cyfry(liczba);
            }

            if (liczba > 99 && liczba < 1000)
            {
                liczbaS = liczba.ToString();

                switch (liczbaS.Substring(0, 1))
                {
                    case "1":
                        ret += "sto";
                        liczba -= 100;
                        break;
                    case "2":
                        ret += "dwieście";
                        liczba -= 200;
                        break;
                    case "3":
                        ret += "trzysta";
                        liczba -= 300;
                        break;
                    case "4":
                        ret += "czterysta";
                        liczba -= 400;
                        break;
                    case "5":
                        ret += "pięćset";
                        liczba -= 500;
                        break;
                    case "6":
                        ret += "sześćset";
                        liczba -= 600;
                        break;
                    case "7":
                        ret += "siedemset";
                        liczba -= 700;
                        break;
                    case "8":
                        ret += "osiemset";
                        liczba -= 800;
                        break;
                    case "9":
                        ret += "dziewięćset";
                        liczba -= 900;
                        break;
                }
                ret += " " + Cyfry(liczba);
            }
            return ret;
        }
    }
}
