﻿using System;
using System.Windows.Forms;

namespace Administracja.KlasyStatic
{
    public class OdsetkiKlasa
    {
        public double ZaFv { get; }
        public double OdsetkiOp { get; }
        public double Pozostalo { get; }
        public int LiczbaDni { get; }

        private OdsetkiKlasa()
        { }

        /// <summary>
        /// Funkcja obliczająca odsetki od wplaty.
        /// </summary>
        /// <param name="dataOd">Data wystawienie FV.</param>
        /// <param name="dataDo">Data wplaty.</param>
        /// <param name="procent">Oprocentowanie odsetek w skali roku.</param>
        /// <param name="wplata">Kwota przyniesiona przez najemce.</param>
        /// <param name="nadplata">Czy możliwa nadpłata</param>
        /// <param name="odsetki">Czy uwzględniać odsetki</param>
        /// <param name="sumaB">Ile było na FV</param>
        /// <param name="oplacono">Ile klient już opłacił</param>
        /// <returns>Obiekt OdsetkiKlasa zaw. : ZaFV, Odsetki, Pozostalo, liczbaDni</returns>
        public OdsetkiKlasa(DateTime dataOd, DateTime dataDo, double procent, double wplata, bool nadplata, bool odsetki, double sumaB, double oplacono)
        {
            if (nadplata) //gdy nadplata
            {
                ZaFv = wplata;
            }
            else
            {
                double zaFvTmp = 0;

                if (oplacono < sumaB) //sprawdzenie, czy nie oplacono już więcej niż wynika to z faktur.
                {
                    zaFvTmp = sumaB - oplacono;
                }
                else
                {
                    Pozostalo = wplata;
                }

                if (!odsetki) //gdy bez nadplaty i bez odsetek
                {
                    if (wplata >= zaFvTmp)
                    {
                        ZaFv = zaFvTmp;
                        Pozostalo = wplata - zaFvTmp;
                    }
                    else
                    {
                        ZaFv = wplata;
                    }
                }
                else //gdy odsetki
                {
                    if (dataOd.CompareTo(dataDo) >= 0)
                    {
                        Wyjatki.Komunikat("Błąd dat przy obliczaniu odsetek.", "Błąd odsetek.", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }

                    LiczbaDni = (dataDo - dataOd).Days;
                    double odsetki2 = Math.Round(zaFvTmp * procent / 100 / 365 * LiczbaDni, 2);

                    if (zaFvTmp <= wplata - odsetki2)
                    {
                        ZaFv = zaFvTmp;
                        OdsetkiOp = odsetki2;
                    }
                    else
                    {
                        double tmpOproc = wplata / (zaFvTmp + odsetki2);

                        ZaFv = Math.Round(zaFvTmp * tmpOproc, 2);
                        OdsetkiOp = Math.Round(wplata - ZaFv, 2);
                    }

                    Pozostalo = wplata - ZaFv - OdsetkiOp;
                }
            }
        }
    }
}
