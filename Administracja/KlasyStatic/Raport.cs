﻿using System;
using System.IO;
using System.Linq;

namespace Administracja
{
    class Raport
    {
        public static string Folder;
        private static Raport _instance;
        private static readonly object Locked = new object();
        private static string _sciezka;

        private Raport() { }

        public static Raport Instance
        {
            get
            {
                if (_instance == null)
                {
                    Folder = Directory.GetCurrentDirectory();

                    _sciezka = Folder + "\\Logs";

                    if (!Directory.Exists(_sciezka))
                    {
                        Directory.CreateDirectory(_sciezka);
                    }

                    //czyszczenie folderu raportów
                    const int kasowanie = 500; //liczba plików która ma być zachowana po kasowaniu
                    var pliki = Directory.GetFiles(_sciezka);

                    for (int i = 0; i < pliki.Count() - kasowanie; i++)
                    {
                        File.Delete(pliki[i]);
                    }

                    //tutaj podajemy pełną ściezkę dla tej sesji 
                    _sciezka += @"\Log " + DateTime.Now.ToString("yyyy.MM.dd HH_mm_ss") + ".txt";

                    _instance = new Raport();
                }
                return _instance;
            }
        }

        public void Raportuj(string log)
        {
            lock (Locked)
            {
                using (StreamWriter sw = File.AppendText(_sciezka))
                {
                    sw.WriteLine(DateTime.Now + " : " + log);
                }
            }
        }
    }
}
