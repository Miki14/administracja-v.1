﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Windows.Forms;

namespace Administracja.KlasyStatic
{
    public static class Wydruki
    {
        private const int LewyMargX = 50;
        private static int _idW;

        public static void Kp(bool podglad, int idW)
        {
            _idW = idW;

            PrintDocument printDocument = new PrintDocument();
            printDocument.PrintPage += KpPrintPage;

            if (podglad)
            {
                PrintPreviewDialog podgladDialog = new PrintPreviewDialog { Document = printDocument };
                ((Form)podgladDialog).WindowState = FormWindowState.Maximized;
                podgladDialog.PrintPreviewControl.Zoom = 1.5;
                podgladDialog.ShowDialog();
            }
            else
            {
                PrintDialog printDialog = new PrintDialog { Document = printDocument };

                if (printDialog.ShowDialog() != DialogResult.OK) return;
                try
                {
                    printDocument.Print();
                }
                catch (Exception ex)
                {
                    Wyjatki.Error(ex);
                }
            }
        }

        public static void KpPrintPage(object sender, PrintPageEventArgs e)
        {
            int polowaY = 580;

            //Pierwsza część od 0
            Graphics g = KpSkladanie(e.Graphics, 0);
            //Linia połowy kartki
            g.DrawLine(Fonty.DashPen, new PointF(LewyMargX - 20, polowaY), new PointF(800, polowaY));
            //Druga część od połowy
            KpSkladanie(g, polowaY);
        }

        private static Graphics KpSkladanie(Graphics g, int przesuniecieY)
        {
            var wplata = Baza.Wczytaj1D("SELECT DATA, KWOTA, ODSETKI, NUMER_DOK, OPIS FROM WPLATY WHERE ID='" + _idW + "'");
            DateTime dataWp = DateTime.Parse(wplata[0]);

            int pozY = 50 + przesuniecieY;

            g.DrawString("Dowód wpłaty", Fonty.Header, Brushes.Black, new PointF(315, pozY));

            //DANE SPRZEDAWCY
            int pozSprzedY = 150 + przesuniecieY;
            var sprzedawca = Baza.Wczytaj1D("SELECT * FROM DANE_SPRZEDAWCY");

            g.DrawString("Sprzedawca:", Fonty.Normal, Brushes.Black, new PointF(LewyMargX - 10, pozSprzedY - 5));
            g.DrawString(sprzedawca[1], Fonty.Dane, Brushes.Black, new PointF(LewyMargX, pozSprzedY + 20));
            g.DrawString(sprzedawca[2], Fonty.Dane, Brushes.Black, new PointF(LewyMargX, pozSprzedY + 40));
            g.DrawString("ul. " + sprzedawca[3], Fonty.Dane, Brushes.Black, new PointF(LewyMargX, pozSprzedY + 60));
            g.DrawString(sprzedawca[4], Fonty.Dane, Brushes.Black, new PointF(LewyMargX, pozSprzedY + 80));

            if (!string.IsNullOrWhiteSpace(sprzedawca[9]))
            {
                g.DrawImage(Image.FromFile(Raport.Folder + @"\Loga\" + sprzedawca[9]), 50, pozSprzedY - 60, 150, 50);
            }

            //DATA
            int dataY = 80 + przesuniecieY;
            g.DrawString("Data: " + dataWp.ToString("dd / MM / yyyy", CultureInfo.InvariantCulture), Fonty.Normal, Brushes.Black, new PointF(630, dataY));

            //NUMER
            g.DrawString("KP " + wplata[3] + " / " + sprzedawca[10] + " / " + dataWp.Year, Fonty.Header, Brushes.Black, new PointF(310, pozY + 30));

            //DANE NABYWCY
            const int pozNabX = 480;
            int pozNabY = 150 + przesuniecieY;
            int idN = int.Parse(Baza.Wczytaj("SELECT ID_N FROM FAKTURY WHERE ID IN (SELECT ID_F FROM WPLATY WHERE ID='" + _idW + "')"));
            var nabywca = Baza.Wczytaj1D("SELECT * FROM OSOBY WHERE ID='" + idN + "'");
            g.DrawString("Nabywca:", Fonty.Normal, Brushes.Black, new PointF(pozNabX - 10, pozNabY - 5));
            g.DrawString(nabywca[1], Fonty.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 20));
            g.DrawString(nabywca[2], Fonty.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 40));

            string adres = nabywca[4] + " " + nabywca[5];
            if (!string.IsNullOrWhiteSpace(nabywca[6]))
            {
                g.DrawString(adres += " m." + nabywca[6], Fonty.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 60));
            }
            g.DrawString(adres, Fonty.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 60));

            g.DrawString(nabywca[7] + " " + nabywca[3], Fonty.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 80));

            int przesuniecieNip = 0;
            if (!string.IsNullOrWhiteSpace(nabywca[11]))
            {
                przesuniecieNip = 20;
                g.DrawString("NIP: " + nabywca[11], Fonty.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 100));
            }

            //OPIS
            int pozTowarY = 260 + przesuniecieNip + przesuniecieY;

            int PozXFv = 550;
            int PozXOdsetki = 630;
            int PozXFvIOdsetki = 750;

            g.DrawString("Kwota", Fonty.Normal, Brushes.Black, new PointF(PozXFv - 10, pozTowarY), Fonty.Left);
            g.DrawString("Odsetki", Fonty.Normal, Brushes.Black, new PointF(PozXOdsetki - 10, pozTowarY), Fonty.Left);
            g.DrawString("Razem", Fonty.Normal, Brushes.Black, new PointF(PozXFvIOdsetki - 10, pozTowarY), Fonty.Left);

            double sumaFv = 0;
            double sumaOds = 0;
            pozTowarY += 20;

            //Po wszystkich KP
            var reader2D = Baza.Wczytaj2D("SELECT KWOTA, ODSETKI, OPIS FROM WPLATY WHERE DOKUMENT = 'KP' AND NUMER_DOK = '" + wplata[3]
                + "' AND EXTRACT(YEAR FROM DATA) = '" + dataWp.Year + "'");
            foreach (List<string> skladniki in reader2D)
            {
                double zaFv = Convert.ToDouble(skladniki[0]);
                double odsetki = Convert.ToDouble(skladniki[1]);

                g.DrawString(skladniki[2], Fonty.Normal, Brushes.Black, new PointF(LewyMargX, pozTowarY));
                g.DrawString(string.Format("{0:N2}", zaFv), Fonty.Normal, Brushes.Black, new PointF(PozXFv, pozTowarY), Fonty.Left);
                g.DrawString(string.Format("{0:N2}", odsetki), Fonty.Normal, Brushes.Black, new PointF(PozXOdsetki, pozTowarY), Fonty.Left);
                g.DrawString(string.Format("{0:N2}", zaFv + odsetki), Fonty.Normal, Brushes.Black, new PointF(PozXFvIOdsetki, pozTowarY), Fonty.Left);

                sumaFv += zaFv;
                sumaOds += odsetki;
                pozTowarY += 15;
            }

            //KWOTA
            pozTowarY += 5;
            g.DrawLine(Fonty.NormalPen, LewyMargX + 400, pozTowarY, LewyMargX + 700, pozTowarY);
            pozTowarY += 5;
            g.DrawString(string.Format("{0:N2}", sumaFv), Fonty.Dane, Brushes.Black, new PointF(PozXFv + 5, pozTowarY), Fonty.Left);
            g.DrawString(string.Format("{0:N2}", sumaOds), Fonty.Dane, Brushes.Black, new PointF(PozXOdsetki + 5, pozTowarY), Fonty.Left);
            g.DrawString(string.Format("{0:N2}", sumaFv + sumaOds), Fonty.Dane, Brushes.Black, new PointF(PozXFvIOdsetki + 5, pozTowarY), Fonty.Left);
            g.DrawString("Słownie: " + OperacjeTekstowe.SlownieLiczba(sumaFv + sumaOds), Fonty.Normal, Brushes.Black, new PointF(LewyMargX, pozTowarY + 30));

            //PODPIS
            int pozPodpisX = 70;
            int pozPodpisY = 500 + przesuniecieY;
            g.DrawString(".........................................", Fonty.Normal, Brushes.Black, new PointF(pozPodpisX, pozPodpisY));
            g.DrawString("Podpis osoby upoważnionej", Fonty.Small, Brushes.Black, new PointF(pozPodpisX + 15, pozPodpisY + 20));
            g.DrawString("do wystawienia dokumentu", Fonty.Small, Brushes.Black, new PointF(pozPodpisX + 20, pozPodpisY + 40));

            return g;
        }
    }
}
