﻿using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace Administracja
{
    public partial class Logowanie : Form
    {
        public bool IsOk { get; private set; }

        public Logowanie()
        {
            InitializeComponent();

#if DEBUG
            login.Text = "ela";
            haslo.Text = "asia";
#endif
        }

        private void ZalogujClick(object sender, EventArgs e)
        {
            const string hashLoginu = "81f7eeab96c8eb939b02d06e97219a83469e4b9990702ec2380b61f1f1a014de337c8e5f231d449e96520a4d353f317affe2d75548d898414b603b72ff009bd4";
            const string hashHasla = "16a855c85ee6b1a01c56ba10e86f64473e55980c98510db26380b923ded1290da39825773b657a149408e4060cedab964c69342449c6a71ce2f5b98ad3de1243";

            if (LiczSha512(login.Text).Equals(hashLoginu) & LiczSha512(haslo.Text).Equals(hashHasla))
            {
                IsOk = true;
                Close();
            }
            else
            {
                IsOk = false;
                Wyjatki.Komunikat("Błędne hasło lub login.", "Błąd logowania.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AnulujClick(object sender, EventArgs e)
        {
            IsOk = false;
            Application.Exit();
        }

        private void CheckKeys(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                ZalogujClick(this, EventArgs.Empty);
                e.Handled = true;
            }
        }

        private static string LiczSha512(string input)
        {
            using (var sha = SHA512.Create())
            {
                var hashB = sha.ComputeHash(Encoding.UTF8.GetBytes(input));

                var sBuilder = new StringBuilder();

                foreach (byte b in hashB)
                {
                    sBuilder.Append(b.ToString("x2"));
                }

                return sBuilder.ToString();
            }
        }
    }
}
