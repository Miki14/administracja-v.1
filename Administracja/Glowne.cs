﻿using Administracja.InneOkna;
using Administracja.OsobyOkna;
using Administracja.WydrukiOkna;
using Administracja.ZakupyOkna;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Administracja
{
    public partial class Glowne : Form
    {
        public Glowne(int open)
        {
            InitializeComponent();

            Wyswietl();
            if (budynek.Items.Count > 0)
            {
                budynek.SelectedIndex = 0;
                UpdateConnectionString();
            }

            switch (open)
            {
                case 1:
                    OpenWindow(new Osoby(), Karty);
                    break;
                case 2:
                    OpenWindow(new Zakupy(), Zakupy);
                    break;
            }
        }

        private void Wyswietl()
        {
            budynek.Items.Clear();

            var listaBaz = Baza.ModelData.BazaJson.Where(x => x.Show == "T");

            budynek.Items.AddRange(listaBaz.Select(x => x.Name).ToArray());
        }

        private void UpdateConnectionString()
        {
            var listaBaz = (from x in Baza.ModelData.BazaJson
                            where x.Name == budynek.Text
                            select x).ToList();

            if (listaBaz.Count == 1)
            {
                Baza.CloseConnection();
                Baza.ConnectionStringSet(listaBaz[0].Path, server: listaBaz[0].Server);
                Baza.OpenConnection();
            }
        }

        public void Clean()
        {
            panel.Controls.Clear();
            Karty.BackColor = Color.ForestGreen;
            Kartoteki.BackColor = Color.ForestGreen;
            Zakupy.BackColor = Color.ForestGreen;
            Inne.BackColor = Color.ForestGreen;
        }

        private void OpenWindow(Control kontrolka, Button wybrany)
        {
            Clean();
            panel.Controls.Add(kontrolka);
            wybrany.BackColor = Color.Transparent;
        }

        private void KartyClick(object sender, EventArgs e)
        {
            OpenWindow(new Osoby(), Karty);
        }

        private void ZakupyClick(object sender, EventArgs e)
        {
            OpenWindow(new Zakupy(), Zakupy);
        }

        private void KartotekiClick(object sender, EventArgs e)
        {
            OpenWindow(new Wydruki(), Kartoteki);
        }

        private void InneClick(object sender, EventArgs e)
        {
            OpenWindow(new Inne(), Inne);
        }

        private void ZamknijClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BuildingSelectedIndexChanged(object sender, EventArgs e)
        {
            Clean();
            UpdateConnectionString();
        }

        private void BuildingMouseClick(object sender, MouseEventArgs e)
        {
            Wyswietl();
        }

        private void BackClick(object sender, EventArgs e)
        {
            Close();
        }
    }
}
