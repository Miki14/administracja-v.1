﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;

namespace Administracja.WydrukiOkna
{
    class WydrukZestawienia
    {
        private int _licznikStron;
        private int _licznikPozycji;
        private int _iloscPozycji;
        private bool _fPodsumowanie;
        private const int LewyMargX = 50;

        private readonly DateTime _dataOd;
        private readonly DateTime _dataDo;

        public WydrukZestawienia(DateTime dataOd, DateTime dataDo)
        {
            _dataOd = dataOd;
            _dataDo = dataDo;
        }

        private void OkresSprzedawcaZestawienie(ref Graphics g)
        {
            g.DrawString("Za okres od  " + _dataOd.ToString("dd.MM.yyyy") + " do  " + _dataDo.ToString("dd.MM.yyyy"), Fonty.Period, Brushes.Black, new PointF(LewyMargX, 80));

            var reader1D = Baza.Wczytaj1D("SELECT Nazwa_1, Nazwa_2, Adres_1, Adres_2 FROM DANE_SPRZEDAWCY");
            g.DrawString(reader1D[0] + " " + reader1D[1] + " " + reader1D[2] + " " + reader1D[3], Fonty.Normal, Brushes.Black, new PointF(LewyMargX, 100));
        }

        public void FakturyZestawieniePrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            int pozY = 50;
            const int pozycjiNaStronie = 11;

            const int xNrD = LewyMargX + 30; //80
            const int xDaneN = xNrD + 150; //230
            const int xNetto = xDaneN + 275; //425
            const int xPVat = xNetto + 80; //505
            const int xVat = xPVat + 50; //555
            const int xBrutto = xVat + 90; //645

            var stawkiVat = new List<int?>();

            e.HasMorePages = false;

            g.DrawString("Zestawienie faktur", Fonty.Header, Brushes.Black, new PointF(315, pozY));

            OkresSprzedawcaZestawienie(ref g);

            if (!_fPodsumowanie)
            {
                //NAGLOWEK
                pozY += 80;
                g.DrawString("Lp.", Fonty.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                g.DrawString("Nr/Data", Fonty.Normal, Brushes.Black, new PointF(xNrD, pozY));
                g.DrawString("Dane nabywcy", Fonty.Normal, Brushes.Black, new PointF(xDaneN, pozY));
                g.DrawString("Netto", Fonty.Normal, Brushes.Black, new PointF(xNetto + 10, pozY));
                g.DrawString("%", Fonty.Normal, Brushes.Black, new PointF(xPVat, pozY));
                g.DrawString("Vat", Fonty.Normal, Brushes.Black, new PointF(xVat + 20, pozY));
                g.DrawString("Brutto", Fonty.Normal, Brushes.Black, new PointF(xBrutto, pozY));

                pozY += 20;

                //LISTA FAKTUR CZYTANA Z BAZY
                _iloscPozycji = Convert.ToInt32(Baza.Wczytaj(
                            "SELECT COUNT(FAKTURY.ID) FROM FAKTURY JOIN OSOBY ON FAKTURY.ID_N = OSOBY.ID WHERE DATA >= '" +
                            _dataOd.ToString("dd.MM.yyyy") + "' AND DATA <= '"
                            + _dataDo.ToString("dd.MM.yyyy") + "' AND NUMER <> 0"));

                string numerFv = Baza.Wczytaj("SELECT NUMER_FV FROM DANE_SPRZEDAWCY");
                if (!string.IsNullOrWhiteSpace(numerFv)) numerFv = "/" + numerFv;

                var reader2D = Baza.Wczytaj2D("SELECT SKIP " + pozycjiNaStronie * _licznikStron +
                    " FAKTURY.ID, NUMER, DATA, NAZWISKO, IMIE, MIASTO, ULICA, DOM, MIESZKANIE, NIP FROM FAKTURY" +
                    " JOIN OSOBY ON FAKTURY.ID_N = OSOBY.ID WHERE DATA >= '" + _dataOd.ToString("dd.MM.yyyy") +
                    "' AND DATA <= '" + _dataDo.ToString("dd.MM.yyyy") + "' AND NUMER <> 0 ORDER BY NUMER");

                foreach (var reader1D in reader2D)
                {
                    if (_licznikPozycji >= (_licznikStron + 1) * pozycjiNaStronie)
                    {
                        _licznikStron++;
                        e.HasMorePages = true;
                        break;
                    }
                    pozY += 10;
                    _licznikPozycji++;
                    stawkiVat.Clear();

                    g.DrawLine(Fonty.DashPen, new PointF(50, pozY - 5), new PointF(775, pozY - 5));
                    string idF = reader1D[0];
                    DateTime dataFv = DateTime.Parse(reader1D[2]);
                    g.DrawString((_licznikPozycji).ToString(), Fonty.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                    string numer = reader1D[1] + numerFv + dataFv.ToString("/MM/yyyy", CultureInfo.InvariantCulture);
                    g.DrawString(numer, Fonty.Normal, Brushes.Black, new PointF(xNrD, pozY));
                    g.DrawString(dataFv.ToString("dd.MM.yyyy"), Fonty.Normal, Brushes.Black, new PointF(xNrD, pozY + 20));
                    g.DrawString(reader1D[3], Fonty.Normal, Brushes.Black, new PointF(xDaneN, pozY));
                    g.DrawString(reader1D[4], Fonty.Normal, Brushes.Black, new PointF(xDaneN, pozY + 20));
                    g.DrawString(reader1D[5] + " " + reader1D[6] + " " + reader1D[7] + " "
                                 + reader1D[8], Fonty.Normal, Brushes.Black, new PointF(xDaneN, pozY + 40));
                    if (!string.IsNullOrWhiteSpace(reader1D[9]))
                    {
                        g.DrawString("NIP: " + reader1D[9], Fonty.Normal, Brushes.Black, new PointF(xDaneN, pozY + 60));
                    }

                    Cena sumaFv = new Cena()
                    {
                        Netto = 0,
                        Vat = 0,
                    };

                    var reader1D2 = Baza.Wczytaj1Dp("SELECT DISTINCT(VAT) FROM SKLADNIKI WHERE ID IN (SELECT ID_S FROM FAKTURY_SKLADNIKI WHERE ID_F = '" + idF + "') ORDER BY VAT  DESC NULLS FIRST");
                    foreach (string vat in reader1D2)
                    {
                        stawkiVat.Add(int.TryParse(vat, out _) ? int.Parse(vat) : (int?)null);
                    }

                    int podkreślenie = 0;

                    foreach (int? vat in stawkiVat)
                    {
                        Cena cenaFv = new Cena()
                        {
                            Netto = 0,
                            Vat = 0,
                        };

                        int vatInt = vat ?? 0;

                        reader1D2 = Baza.Wczytaj1Dp("SELECT Faktury_Skladniki.CENA FROM Faktury_Skladniki JOIN skladniki on Faktury_Skladniki.ID_S = skladniki.ID "
                                                    + "WHERE Faktury_Skladniki.ID_F = '" + idF + "' AND " + GetWhereVat(vat));

                        foreach (string bazaCena in reader1D2)
                        {
                            double cena = Convert.ToDouble(bazaCena.Replace(".", ","));

                            cenaFv.Netto += Math.Round(cena, 2);
                            cenaFv.Vat += Math.Round(cena * vatInt / 100, 2);
                        }
                        sumaFv.Append(cenaFv);

                        if (cenaFv.Netto != 0)
                        {
                            g.DrawString(string.Format("{0:N2}", cenaFv.Netto), Fonty.Small, Brushes.Black, new PointF(xNetto + 50, pozY), Fonty.Left);
                            g.DrawString(vat.HasValue ? vat.ToString() : "zw", Fonty.Small, Brushes.Black, new PointF(xPVat, pozY));
                            g.DrawString(string.Format("{0:N2}", cenaFv.Vat), Fonty.Small, Brushes.Black, new PointF(xVat + 50, pozY), Fonty.Left);
                            g.DrawString(string.Format("{0:N2}", cenaFv.Netto + cenaFv.Vat), Fonty.Small, Brushes.Black, new PointF(xBrutto + 50, pozY), Fonty.Left);
                            pozY += 15;
                            podkreślenie++;
                        }
                    }

                    //Podsumowanie danej faktury
                    g.DrawLine(Fonty.NormalPen, new PointF(480, pozY), new PointF(775, pozY));
                    pozY += 5;

                    g.DrawString(string.Format("{0:N2}", sumaFv.Netto), Fonty.Normal, Brushes.Black, new PointF(xNetto + 50, pozY), Fonty.Left);
                    g.DrawString(string.Format("{0:N2}", sumaFv.Vat), Fonty.Normal, Brushes.Black, new PointF(xVat + 50, pozY), Fonty.Left);
                    g.DrawString(string.Format("{0:N2}", sumaFv.Netto + sumaFv.Vat), Fonty.Normal, Brushes.Black, new PointF(xBrutto + 50, pozY), Fonty.Left);

                    pozY += 35;

                    if (string.IsNullOrWhiteSpace(reader1D[9]))
                    {
                        pozY -= 20;
                    }

                    if (podkreślenie < 2) pozY += 20 * (2 - podkreślenie);
                }

                if (_licznikPozycji >= _iloscPozycji)
                {
                    if (pozY < 920)
                    {
                        PodsumowanieFv(ref g, pozY, ref e);
                    }
                    else
                    {
                        e.HasMorePages = true;
                        _fPodsumowanie = true;
                    }
                }
            }
            else
            {
                _licznikStron++;
                pozY += 40;
                PodsumowanieFv(ref g, pozY, ref e);
            }
        }

        public void WplatyZestawieniePrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            int pozY = 50;
            const int pozycjiNaStronie = 30;

            const int xDaneN = LewyMargX + 30; //80
            const int xDokument = xDaneN + 300; //380
            const int xData = xDokument + 140; //550
            const int xKwotaOds = xData + 50; //560
            const int xKwota = xKwotaOds + 70; //630
            const int xOdsetki = xKwota + 50; //680

            int idOsDruk = 0;

            e.HasMorePages = false;

            g.DrawString("Zestawienie wpłat", Fonty.Header, Brushes.Black, new PointF(315, pozY));
            OkresSprzedawcaZestawienie(ref g);

            if (!_fPodsumowanie)
            {
                //NAGLOWEK
                pozY += 80;
                g.DrawString("Lp.", Fonty.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                g.DrawString("Dane", Fonty.Normal, Brushes.Black, new PointF(xDaneN + 100, pozY));
                g.DrawString("Dokument Nr.", Fonty.Normal, Brushes.Black, new PointF(xDokument, pozY));
                g.DrawString("Data", Fonty.Normal, Brushes.Black, new PointF(xData + 20, pozY));
                g.DrawString("Wplata", Fonty.Normal, Brushes.Black, new PointF(xKwotaOds + 40, pozY));
                g.DrawString("Kwota", Fonty.Normal, Brushes.Black, new PointF(xKwota + 40, pozY));
                g.DrawString("Odsetki", Fonty.Normal, Brushes.Black, new PointF(xOdsetki + 60, pozY));

                pozY += 20;

                //USTAWIENIE SPODZIEWANEJ LICZBY WIERSZY NA WYDRUKU
                var reader1D = Baza.Wczytaj1D("SELECT COUNT(*) FROM WPLATY WHERE DATA >= '" + _dataOd.ToString("dd.MM.yyyy") + "' AND DATA <= '" + _dataDo.ToString("dd.MM.yyyy") + "'");
                if (reader1D[0].Length != 0) _iloscPozycji = Convert.ToInt32(reader1D[0]);
                else _iloscPozycji = 0;

                //LISTA WPLAT CZYTANA Z BAZY
                var reader2D = Baza.Wczytaj2D("SELECT SKIP " + pozycjiNaStronie * _licznikStron + " Osoby.ID, Nazwisko, Imie, Dokument, Numer_Dok, Wplaty.Data, Kwota, Odsetki, (Kwota + Odsetki) as"
                    + " Kwota_ods FROM WPLATY JOIN FAKTURY ON WPLATY.ID_F = FAKTURY.ID JOIN OSOBY ON FAKTURY.ID_N = OSOBY.ID WHERE WPLATY.DATA >= '" + _dataOd.ToString("dd.MM.yyyy")
                    + "' AND WPLATY.DATA <= '" + _dataDo.ToString("dd.MM.yyyy") + "' ORDER BY Grupa, Nazwisko, Imie");

                foreach (List<string> list in reader2D)
                {
                    if (_licznikPozycji >= (_licznikStron + 1) * pozycjiNaStronie)
                    {
                        _licznikStron++;
                        e.HasMorePages = true;
                        break;
                    }
                    pozY += 10;
                    _licznikPozycji++;

                    g.DrawLine(Fonty.DashPen, new PointF(50, pozY - 5), new PointF(785, pozY - 5));
                    g.DrawString((_licznikPozycji).ToString(), Fonty.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                    int idOsAkt = int.Parse(list[0]);
                    if (idOsDruk != idOsAkt)
                    {
                        //TODO: 40 czy 50? Różnie to widzi. Liczy bez spacji. Po dodaniu spacji potrafi wyjść.
                        const int dlugosc = 43;
                        string nazwa = list[1] + " " + list[2];
                        if (nazwa.Length > dlugosc) nazwa = nazwa.Substring(0, dlugosc);
                        g.DrawString(nazwa, Fonty.Normal, Brushes.Black, new PointF(xDaneN, pozY));
                        idOsDruk = idOsAkt;
                    }
                    else
                    {
                        g.DrawString("j. w.", Fonty.Normal, Brushes.Black, new PointF(xDaneN + 50, pozY));
                    }

                    //Numer
                    string numer = list[3] + list[4];
                    DateTime data = DateTime.Parse(list[5]);
                    if (list[3] == "FV")
                    {
                        numer = Baza.Wczytaj("SELECT NUMER_FV FROM DANE_SPRZEDAWCY");
                        if (!string.IsNullOrWhiteSpace(numer)) numer = "/" + numer;
                        numer = list[3] + " " + list[4] + numer + data.ToString("/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    if (list[3] == "KP")
                    {
                        numer = Baza.Wczytaj("SELECT NUMER_FV FROM DANE_SPRZEDAWCY");
                        if (!string.IsNullOrWhiteSpace(numer)) numer = "/" + numer + "/";
                        numer = list[3] + " " + list[4] + numer + data.ToString("yyyy");
                    }

                    g.DrawString(numer, Fonty.Normal, Brushes.Black, new PointF(xDokument, pozY));
                    g.DrawString(data.ToString("dd.MM.yyyy"), Fonty.Normal, Brushes.Black, new PointF(xData, pozY));
                    g.DrawString(string.Format("{0:N2}", Convert.ToDouble(list[8])), Fonty.Normal, Brushes.Black, new PointF(xKwotaOds + 100, pozY), Fonty.Left);
                    g.DrawString(string.Format("{0:N2}", Convert.ToDouble(list[6])), Fonty.Normal, Brushes.Black, new PointF(xKwota + 100, pozY), Fonty.Left);
                    g.DrawString(string.Format("{0:N2}", Convert.ToDouble(list[7])), Fonty.Normal, Brushes.Black, new PointF(xOdsetki + 100, pozY), Fonty.Left);

                    pozY += 20;
                }

                if (_licznikPozycji >= _iloscPozycji)
                {
                    if (pozY < 900)
                    {
                        PodsumowanieWplaty(ref g, pozY, ref e);
                    }
                    else
                    {
                        e.HasMorePages = true;
                        _fPodsumowanie = true;
                    }
                }
            }
            else
            {
                _licznikStron++;
                pozY += 40;
                PodsumowanieWplaty(ref g, pozY, ref e);
            }
        }

        public void WydatkiZestawieniePrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            int pozY = 50;
            const int pozycjiNaStronie = 19;

            const int xNrDat = LewyMargX + 30; //80
            const int xDaneOpis = xNrDat + 120; //200
            const int xKwota = xDaneOpis + 460; //660
            const int xZaplata = xKwota + 10; //670
            const int xSposob = xZaplata + 90; //760

            e.HasMorePages = false;

            g.DrawString("Zestawienie wydatków", Fonty.Header, Brushes.Black, new PointF(315, pozY));
            OkresSprzedawcaZestawienie(ref g);

            if (!_fPodsumowanie)
            {
                //NAGLOWEK
                pozY += 80;
                g.DrawString("Lp.", Fonty.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                g.DrawString("Nr. FV", Fonty.Normal, Brushes.Black, new PointF(xNrDat + 20, pozY));
                g.DrawString("Data", Fonty.Normal, Brushes.Black, new PointF(xNrDat + 25, pozY + 20));
                g.DrawString("Dane dostawcy", Fonty.Normal, Brushes.Black, new PointF(xDaneOpis, pozY));
                g.DrawString("Opis", Fonty.Normal, Brushes.Black, new PointF(xDaneOpis, pozY + 20));
                g.DrawString("Kwota", Fonty.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonty.Left);
                g.DrawString("Zapłacono", Fonty.Normal, Brushes.Black, new PointF(xZaplata, pozY));
                g.DrawString("Sposób", Fonty.Normal, Brushes.Black, new PointF(xSposob - 20, pozY));

                pozY += 40;


                //USTAWIENIE SPODZIEWANEJ LICZBY WIERSZY NA WYDRUKU
                List<string> reader1D = Baza.Wczytaj1D("SELECT COUNT(*) FROM ZAKUPY WHERE Data_wplaty >= '" + _dataOd.ToString("dd.MM.yyyy") +
                    "' AND Data_wplaty <= '" + _dataDo.ToString("dd.MM.yyyy") + "' AND Wplacona_Kwota <> '0'");
                if (reader1D[0].Length != 0) _iloscPozycji = Convert.ToInt32(reader1D[0]);
                else _iloscPozycji = 0;

                //LISTA WPLAT CZYTANA Z BAZY
                var reader2D = Baza.Wczytaj2D("SELECT SKIP " + pozycjiNaStronie * _licznikStron + " Numer, Data_wystawienia, Nazwa, Nazwa_cd, Opis, WPLACONA_KWOTA, Data_wplaty,"
                    + " Sposob_zaplaty FROM ZAKUPY JOIN DOSTAWCY ON ZAKUPY.ID_D = DOSTAWCY.ID WHERE Data_wplaty >= '" + _dataOd.ToString("dd.MM.yyyy") + "' AND Data_wplaty <= '"
                    + _dataDo.ToString("dd.MM.yyyy") + "' AND Wplacona_Kwota <> '0' ORDER BY Data_wplaty");

                foreach (List<string> list in reader2D)
                {
                    if (_licznikPozycji >= (_licznikStron + 1) * pozycjiNaStronie)
                    {
                        _licznikStron++;
                        e.HasMorePages = true;
                        break;
                    }
                    pozY += 10;
                    _licznikPozycji++;

                    g.DrawLine(Fonty.DashPen, new PointF(50, pozY - 5), new PointF(785, pozY - 5));
                    g.DrawString(_licznikPozycji.ToString(), Fonty.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                    g.DrawString(list[0], Fonty.Normal, Brushes.Black, new PointF(xNrDat, pozY));
                    g.DrawString(OperacjeTekstowe.TrymujDate(list[1]), Fonty.Normal, Brushes.Black, new PointF(xNrDat, pozY + 20));
                    g.DrawString(list[2] + " " + list[3], Fonty.Normal, Brushes.Black, new PointF(xDaneOpis, pozY));
                    g.DrawString(list[4], Fonty.Normal, Brushes.Black, new PointF(xDaneOpis, pozY + 20));
                    g.DrawString(string.Format("{0:N2}", Convert.ToDouble(list[5])), Fonty.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonty.Left);
                    g.DrawString(OperacjeTekstowe.TrymujDate(list[6]), Fonty.Normal, Brushes.Black, new PointF(xZaplata, pozY));
                    g.DrawString(list[7], Fonty.Normal, Brushes.Black, new PointF(xSposob, pozY));

                    pozY += 40;
                }

                if (_licznikPozycji >= _iloscPozycji)
                {
                    if (pozY < 950)
                    {
                        PodsumowanieWydatki(ref g, pozY, ref e);
                    }
                    else
                    {
                        e.HasMorePages = true;
                        _fPodsumowanie = true;
                    }
                }
            }
            else
            {
                _licznikStron++;
                pozY += 40;
                PodsumowanieWydatki(ref g, pozY, ref e);
            }
        }

        public void OdsetkiZestawieniePrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            int pozY = 50;
            const int pozycjiNaStronie = 20;

            const int xDaneN = LewyMargX + 50; //80
            const int xOdsetki = xDaneN + 550; //660

            e.HasMorePages = false;

            g.DrawString("Zestawienie odsetek", Fonty.Header, Brushes.Black, new PointF(315, pozY));
            OkresSprzedawcaZestawienie(ref g);

            if (!_fPodsumowanie)
            {
                //NAGLOWEK
                pozY += 80;
                g.DrawString("Lp.", Fonty.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                g.DrawString("Dane najemcy", Fonty.Normal, Brushes.Black, new PointF(xDaneN + 200, pozY));
                g.DrawString("Odsetki", Fonty.Normal, Brushes.Black, new PointF(xOdsetki + 60, pozY));

                pozY += 20;

                //USTAWIENIE SPODZIEWANEJ LICZBY WIERSZY NA WYDRUKU
                List<string> reader1D = Baza.Wczytaj1D("SELECT COUNT(*) FROM WPLATY WHERE DATA >= '" + _dataOd.ToString("dd.MM.yyyy") + "' AND DATA <= '" + _dataDo.ToString("dd.MM.yyyy") + "' AND ODSETKI <> 0");
                string tmp = reader1D[0];
                if (tmp.Length == 0) tmp = "0";
                _iloscPozycji = Convert.ToInt32(tmp);

                //LISTA ODSETEK CZYTANA Z BAZY
                var reader2D = Baza.Wczytaj2D("SELECT SKIP " + pozycjiNaStronie * _licznikStron + " Odsetki, Nazwisko, Imie FROM WPLATY JOIN FAKTURY ON WPLATY.ID_F = FAKTURY.ID JOIN OSOBY ON "
                    + "FAKTURY.ID_N = OSOBY.ID WHERE WPLATY.DATA >= '" + _dataOd.ToString("dd.MM.yyyy") + "' AND WPLATY.DATA <= '" + _dataDo.ToString("dd.MM.yyyy")
                    + "' AND ODSETKI <> 0 ORDER BY Grupa, Nazwisko, Imie");

                //while (myreader.Read() && !fStop)
                foreach (List<string> list in reader2D)
                {
                    if (_licznikPozycji >= (_licznikStron + 1) * pozycjiNaStronie)
                    {
                        //fStop = true;
                        _licznikStron++;
                        e.HasMorePages = true;
                        break;
                    }
                    pozY += 10;
                    _licznikPozycji++;

                    g.DrawLine(Fonty.DashPen, new PointF(50, pozY - 5), new PointF(775, pozY - 5));
                    g.DrawString((_licznikPozycji).ToString(), Fonty.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                    g.DrawString(list[1] + " " + list[2], Fonty.Normal, Brushes.Black, new PointF(xDaneN, pozY));
                    if (!string.IsNullOrWhiteSpace(list[0]) && list[0] != "0")
                    {
                        double odsetki = Convert.ToDouble(list[0]);
                        g.DrawString(string.Format("{0:N2}", odsetki), Fonty.Normal, Brushes.Black, new PointF(xOdsetki + 100, pozY), Fonty.Left);
                    }

                    pozY += 20;
                }

                if (_licznikPozycji >= _iloscPozycji)
                {
                    if (pozY < 820)
                    {
                        PodsumowanieOdsetki(ref g, pozY, ref e);
                    }
                    else
                    {
                        e.HasMorePages = true;
                        _fPodsumowanie = true;
                    }
                }
            }
            else
            {
                _licznikStron++;
                pozY += 40;
                PodsumowanieOdsetki(ref g, pozY, ref e);
            }

        }

        private void PodsumowanieFv(ref Graphics g, int pozY, ref PrintPageEventArgs e)
        {
            const int xNetto = LewyMargX + 375; //425
            const int xPVat = xNetto + 80; //505
            const int xVat = xPVat + 50; //555
            const int xBrutto = xVat + 90; //645

            pozY += 50;
            g.DrawString("Podsumowanie", Fonty.Dane, Brushes.Black, new PointF(520, pozY));
            pozY += 20;
            g.DrawString("Netto", Fonty.Normal, Brushes.Black, new PointF(xNetto + 10, pozY));
            g.DrawString("%", Fonty.Normal, Brushes.Black, new PointF(xPVat, pozY));
            g.DrawString("Vat", Fonty.Normal, Brushes.Black, new PointF(xVat + 20, pozY));
            g.DrawString("Brutto", Fonty.Normal, Brushes.Black, new PointF(xBrutto, pozY));
            pozY += 15;
            g.DrawLine(Fonty.NormalPen, xNetto - 20, pozY, xBrutto + 50, pozY);
            pozY += 5;

            var stawkiVat = new List<int?>();
            var reader1D = Baza.Wczytaj1Dp("SELECT DISTINCT(VAT) FROM SKLADNIKI WHERE ID IN (SELECT ID_S FROM FAKTURY_SKLADNIKI) ORDER BY VAT DESC NULLS FIRST");
            foreach (string vat in reader1D)
            {
                stawkiVat.Add(int.TryParse(vat, out _) ? int.Parse(vat) : (int?)null);
            }

            try
            {
                double sumaVatFv = 0;
                double sumaNettoFv = 0;

                foreach (int? vat in stawkiVat)
                {
                    double sumaCenNetto = 0;
                    double sumaCenVat = 0;
                    int vatInt = vat ?? 0;

                    string whereVat = GetWhereVat(vat);

                    var cenyNettoS = Baza.Wczytaj1Dp("SELECT CENA FROM Faktury_Skladniki JOIN SKLADNIKI ON FAKTURY_SKLADNIKI.ID_S = SKLADNIKI.ID WHERE " + whereVat + " AND ID_F IN " +
                                                 "( SELECT ID FROM FAKTURY WHERE DATA >= '" + _dataOd.ToString("dd.MM.yyyy") + "' AND DATA <= '" + _dataDo.ToString("dd.MM.yyyy") + "')");

                    foreach (var cenaNettoS in cenyNettoS)
                    {
                        double cenaNetto = Convert.ToDouble(cenaNettoS.Replace(".", ","));
                        sumaCenNetto += Math.Round(cenaNetto, 2);
                        sumaCenVat += Math.Round(cenaNetto * vatInt / 100, 2);
                    }

                    if (sumaCenNetto != 0)
                    {
                        g.DrawString(string.Format("{0:N2}", sumaCenNetto), Fonty.Normal, Brushes.Black, new PointF(xNetto + 50, pozY), Fonty.Left);
                        g.DrawString(!vat.HasValue ? "zw" : vat.ToString(), Fonty.Normal, Brushes.Black, new PointF(xPVat, pozY));
                        g.DrawString(string.Format("{0:N2}", sumaCenVat), Fonty.Normal, Brushes.Black, new PointF(xVat + 50, pozY), Fonty.Left);
                        g.DrawString(string.Format("{0:N2}", sumaCenNetto + sumaCenVat), Fonty.Normal, Brushes.Black, new PointF(xBrutto + 50, pozY), Fonty.Left);
                        pozY += 20;

                        sumaNettoFv += sumaCenNetto;
                        sumaVatFv += sumaCenVat;
                    }
                }

                g.DrawLine(Fonty.NormalPen, xNetto - 20, pozY, xBrutto + 50, pozY);
                pozY += 5;
                g.DrawString(string.Format("{0:N2}", sumaNettoFv), Fonty.Normal, Brushes.Black, new PointF(xNetto + 50, pozY), Fonty.Left);
                g.DrawString(string.Format("{0:N2}", sumaVatFv), Fonty.Normal, Brushes.Black, new PointF(xVat + 50, pozY), Fonty.Left);
                g.DrawString(string.Format("{0:N2}", sumaNettoFv + sumaVatFv), Fonty.Normal, Brushes.Black, new PointF(xBrutto + 50, pozY), Fonty.Left);
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }

            e.HasMorePages = false;
            _fPodsumowanie = false;
            _licznikPozycji = 0;
            _licznikStron = 0;
        }

        private void PodsumowanieWplaty(ref Graphics g, int pozY, ref PrintPageEventArgs e)
        {
            const int xLabel = LewyMargX + 600; //660
            const int xKwota = xLabel + 120; //660

            pozY += 50;
            g.DrawString("Podsumowanie", Fonty.Dane, Brushes.Black, new PointF(xLabel - 30, pozY));
            pozY += 20;
            g.DrawLine(Fonty.DashPen, xLabel - 35, pozY, xKwota + 5, pozY);
            pozY += 5;

            string kwerenda = "SELECT SUM(Kwota + Odsetki) FROM WPLATY WHERE DATA >= '" + _dataOd.ToString("dd.MM.yyyy") + "' AND DATA <= '"
                + _dataDo.ToString("dd.MM.yyyy") + "'";

            var reader = Baza.Wczytaj(kwerenda + " AND Sposob = 'G';");
            g.DrawString("Gotówka: ", Fonty.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString(string.Format("{0:N2}", Convert.ToDouble(reader)), Fonty.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonty.Left);

            pozY += 20;

            reader = Baza.Wczytaj(kwerenda + " AND Sposob = 'P';");
            g.DrawString("Przelew: ", Fonty.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString(string.Format("{0:N2}", Convert.ToDouble(reader)), Fonty.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonty.Left);

            pozY += 20;

            reader = Baza.Wczytaj(kwerenda + ";");
            g.DrawLine(Fonty.DashPen, xLabel - 35, pozY - 3, xKwota + 5, pozY - 3);
            g.DrawString("Suma:  ", Fonty.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString(string.Format("{0:N2}", Convert.ToDouble(reader)), Fonty.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonty.Left);

            e.HasMorePages = false;
            _fPodsumowanie = false;
            _licznikPozycji = 0;
            _licznikStron = 0;
        }

        private void PodsumowanieWydatki(ref Graphics g, int pozY, ref PrintPageEventArgs e)
        {
            const int xLabel = LewyMargX + 600; //660
            const int xKwota = xLabel + 120; //660

            pozY += 50;
            g.DrawString("Podsumowanie", Fonty.Dane, Brushes.Black, new PointF(xLabel - 30, pozY));
            pozY += 20;
            g.DrawLine(Fonty.DashPen, xLabel - 35, pozY, xKwota + 5, pozY);
            pozY += 5;


            // Gotówka, przelw itp.
            string kwerenda = "SELECT SUM(Suma_Netto + Suma_Vat) FROM ZAKUPY WHERE Data_wplaty >= '" + _dataOd.ToString("dd.MM.yyyy") + "' AND Data_wplaty <= '"
                + _dataDo.ToString("dd.MM.yyyy") + "' AND Wplacona_Kwota <> '0'";

            var reader = Baza.Wczytaj(kwerenda + " AND Sposob_zaplaty = 'G';");
            g.DrawString("Gotówka: ", Fonty.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString(string.Format("{0:N2}", Convert.ToDouble(reader)), Fonty.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonty.Left);

            pozY += 20;

            reader = Baza.Wczytaj(kwerenda + " AND Sposob_zaplaty = 'P';");
            g.DrawString("Przelew: ", Fonty.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString(string.Format("{0:N2}", Convert.ToDouble(reader)), Fonty.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonty.Left);

            pozY += 20;

            // Netto,Vat, brutto
            string query = "SELECT SUM({0}) FROM ZAKUPY WHERE Data_wplaty >= '" + _dataOd.ToString("dd.MM.yyyy") + "' AND Data_wplaty <= '"
                + _dataDo.ToString("dd.MM.yyyy") + "' AND Wplacona_Kwota <> '0';";

            reader = Baza.Wczytaj(string.Format(query, "Suma_Netto"));
            g.DrawLine(Fonty.DashPen, xLabel - 35, pozY - 3, xKwota + 5, pozY - 3);
            g.DrawString("Netto: ", Fonty.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString(string.Format("{0:N2}", Convert.ToDouble(reader)), Fonty.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonty.Left);

            pozY += 20;

            reader = Baza.Wczytaj(string.Format(query, "Suma_Vat"));
            g.DrawString("Vat:", Fonty.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString(string.Format("{0:N2}", Convert.ToDouble(reader)), Fonty.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonty.Left);

            pozY += 20;

            reader = Baza.Wczytaj(string.Format(query, "Suma_Netto + Suma_Vat"));
            g.DrawLine(Fonty.DashPen, xLabel - 35, pozY - 3, xKwota + 5, pozY - 3);
            g.DrawString("Suma: ", Fonty.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString(string.Format("{0:N2}", Convert.ToDouble(reader)), Fonty.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonty.Left);

            e.HasMorePages = false;
            _fPodsumowanie = false;
            _licznikPozycji = 0;
            _licznikStron = 0;
        }

        private void PodsumowanieOdsetki(ref Graphics g, int pozY, ref PrintPageEventArgs e)
        {
            const int xOdsetki = LewyMargX + 610; //660

            pozY += 50;
            g.DrawString("Podsumowanie", Fonty.Dane, Brushes.Black, new PointF(xOdsetki - 30, pozY));
            pozY += 20;
            g.DrawLine(Fonty.DashPen, xOdsetki - 35, pozY, xOdsetki + 110, pozY);
            pozY += 5;

            var reader = Baza.Wczytaj("SELECT SUM(ODSETKI) FROM WPLATY WHERE DATA >= '" + _dataOd.ToString("dd.MM.yyyy") + "' AND DATA <= '" + _dataDo.ToString("dd.MM.yyyy")
                    + "' AND ODSETKI <> 0");
            g.DrawString("Suma odsetek: " + string.Format("{0:N2}", Convert.ToDouble(reader)), Fonty.Normal, Brushes.Black, new PointF(xOdsetki - 30, pozY));

            e.HasMorePages = false;
            _fPodsumowanie = false;
            _licznikPozycji = 0;
            _licznikStron = 0;
        }

        private string GetWhereVat(int? vat)
        {
            return vat.HasValue ? "VAT = '" + vat + "'" : "VAT IS NULL";
        }
    }
}