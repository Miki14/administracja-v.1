﻿namespace Administracja.WydrukiOkna
{
    partial class Wydruki
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Inne_Obr = new System.Windows.Forms.Button();
            this.zestawienia = new System.Windows.Forms.Button();
            this.saldo = new System.Windows.Forms.Button();
            this.dluznicy = new System.Windows.Forms.Button();
            this.zestawieniaSkladnikow = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Inne_Obr
            // 
            this.Inne_Obr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Inne_Obr.Location = new System.Drawing.Point(65, 47);
            this.Inne_Obr.Name = "Inne_Obr";
            this.Inne_Obr.Size = new System.Drawing.Size(244, 135);
            this.Inne_Obr.TabIndex = 17;
            this.Inne_Obr.Text = "Inne obroty";
            this.Inne_Obr.UseVisualStyleBackColor = true;
            this.Inne_Obr.Click += new System.EventHandler(this.Inne_Obr_Click);
            // 
            // zestawienia
            // 
            this.zestawienia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zestawienia.Location = new System.Drawing.Point(65, 188);
            this.zestawienia.Name = "zestawienia";
            this.zestawienia.Size = new System.Drawing.Size(244, 135);
            this.zestawienia.TabIndex = 16;
            this.zestawienia.Text = "Zestawienia";
            this.zestawienia.UseVisualStyleBackColor = true;
            this.zestawienia.Click += new System.EventHandler(this.zestawienia_Click);
            // 
            // saldo
            // 
            this.saldo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.saldo.Location = new System.Drawing.Point(315, 47);
            this.saldo.Name = "saldo";
            this.saldo.Size = new System.Drawing.Size(244, 135);
            this.saldo.TabIndex = 15;
            this.saldo.Text = "Saldo";
            this.saldo.UseVisualStyleBackColor = true;
            this.saldo.Click += new System.EventHandler(this.saldo_Click);
            // 
            // dluznicy
            // 
            this.dluznicy.Enabled = false;
            this.dluznicy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dluznicy.Location = new System.Drawing.Point(565, 47);
            this.dluznicy.Name = "dluznicy";
            this.dluznicy.Size = new System.Drawing.Size(244, 135);
            this.dluznicy.TabIndex = 14;
            this.dluznicy.Text = "Dłużnicy";
            this.dluznicy.UseVisualStyleBackColor = true;
            this.dluznicy.Click += new System.EventHandler(this.Dluznicy_Click);
            // 
            // zestawieniaSkladnikow
            // 
            this.zestawieniaSkladnikow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zestawieniaSkladnikow.Location = new System.Drawing.Point(315, 188);
            this.zestawieniaSkladnikow.Name = "zestawieniaSkladnikow";
            this.zestawieniaSkladnikow.Size = new System.Drawing.Size(244, 135);
            this.zestawieniaSkladnikow.TabIndex = 18;
            this.zestawieniaSkladnikow.Text = "Zestawienia składników";
            this.zestawieniaSkladnikow.UseVisualStyleBackColor = true;
            this.zestawieniaSkladnikow.Click += new System.EventHandler(this.zestawieniaSkladnikow_Click);
            // 
            // Wydruki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.zestawieniaSkladnikow);
            this.Controls.Add(this.Inne_Obr);
            this.Controls.Add(this.zestawienia);
            this.Controls.Add(this.saldo);
            this.Controls.Add(this.dluznicy);
            this.Name = "Wydruki";
            this.Size = new System.Drawing.Size(1066, 555);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Inne_Obr;
        private System.Windows.Forms.Button zestawienia;
        private System.Windows.Forms.Button saldo;
        private System.Windows.Forms.Button dluznicy;
        private System.Windows.Forms.Button zestawieniaSkladnikow;


    }
}
