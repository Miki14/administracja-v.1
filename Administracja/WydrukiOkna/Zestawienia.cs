﻿using System;
using System.Drawing.Printing;
using System.Windows.Forms;
using Administracja.KlasyStatic;

namespace Administracja.WydrukiOkna
{
    public partial class Zestawienia : Form
    {
        private PrintDocument _printDok;

        public Zestawienia()
        {
            InitializeComponent();
            dateFrom.Value = DateTimeHelper.GetDateFirstDayOfMonth(DateTime.Today.AddMonths(-1));
            dateTo.Value = DateTimeHelper.GetDateLastDayOfMonth(DateTime.Today.AddMonths(-1));
            FullMonthCh_CheckedChanged(this, EventArgs.Empty);
        }

        private void UstawTyp()
        {
            _printDok = new PrintDocument();
            WydrukZestawienia druk = new WydrukZestawienia(dateFrom.Value, dateTo.Value);

            if (faktury_rad.Checked)
            {
                _printDok.PrintPage += druk.FakturyZestawieniePrintPage;
            }
            else
            {
                if (wplaty_rad.Checked)
                {
                    _printDok.PrintPage += druk.WplatyZestawieniePrintPage;
                }
                else
                {
                    if (wydatki_rad.Checked)
                    {
                        _printDok.PrintPage += druk.WydatkiZestawieniePrintPage;
                    }
                    else
                    {
                        _printDok.PrintPage += druk.OdsetkiZestawieniePrintPage;
                    }
                }
            }
        }

        private void ZamknijClick(object sender, EventArgs e)
        {
            Close();
        }

        private void DrukujClick(object sender, EventArgs e)
        {
            UstawTyp();

            _printDialog.Document = _printDok;
            if (_printDialog.ShowDialog() != DialogResult.OK) return;
            try
            {
                _printDok.Print();
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
        }

        private void PodgladClick(object sender, EventArgs e)
        {
            UstawTyp();

            podglad_dia.Document = _printDok;
            ((Form)podglad_dia).WindowState = FormWindowState.Maximized;
            podglad_dia.PrintPreviewControl.Zoom = 1.5;
            podglad_dia.ShowDialog();
        }

        private void FullMonthCh_CheckedChanged(object sender, EventArgs e)
        {
            dateTo.Enabled = !fullMonthCh.Checked;
            dateFrom.CustomFormat = fullMonthCh.Checked ? "MMMM yyyy" : "dd MMMM yyyy";
            dateFromGb.Text = fullMonthCh.Checked ? "Miesiąc zestawienia" : "Data od";
        }

        private void DateFrom_ValueChanged(object sender, EventArgs e)
        {
            if (fullMonthCh.Checked)
            {
                dateFrom.Value = DateTimeHelper.GetDateFirstDayOfMonth(dateFrom.Value);
                dateTo.Value = DateTimeHelper.GetDateLastDayOfMonth(dateFrom.Value);
            }
        }

        private void ZestawieniaKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.P && e.Control)
            {
                DrukujClick(this, EventArgs.Empty);
                e.Handled = true;
            }
        }
    }
}
