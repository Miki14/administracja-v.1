﻿namespace Administracja.WydrukiOkna
{
    partial class InneObrotyEdycja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InneObrotyEdycja));
            this.zapisz = new System.Windows.Forms.Button();
            this.typ = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.data = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.kwota = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.opis = new System.Windows.Forms.TextBox();
            this.anuluj = new System.Windows.Forms.Button();
            this.id_l = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // zapisz
            // 
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(256, 167);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(200, 70);
            this.zapisz.TabIndex = 5;
            this.zapisz.Text = "Zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.zapisz_Click);
            // 
            // typ
            // 
            this.typ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typ.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.typ.FormattingEnabled = true;
            this.typ.Items.AddRange(new object[] {
            "konto -> kasa",
            "konto -> inne",
            "kasa -> konto",
            "kasa -> inne",
            "inne -> konto",
            "inne -> kasa"});
            this.typ.Location = new System.Drawing.Point(6, 25);
            this.typ.Name = "typ";
            this.typ.Size = new System.Drawing.Size(200, 28);
            this.typ.TabIndex = 13;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.typ);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(12, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(213, 66);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Typ";
            // 
            // data
            // 
            this.data.CustomFormat = "dd MMMM yyyy";
            this.data.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.data.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.data.Location = new System.Drawing.Point(6, 19);
            this.data.Name = "data";
            this.data.Size = new System.Drawing.Size(200, 26);
            this.data.TabIndex = 15;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.data);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(12, 104);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(213, 57);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data";
            // 
            // kwota
            // 
            this.kwota.Location = new System.Drawing.Point(6, 25);
            this.kwota.Name = "kwota";
            this.kwota.Size = new System.Drawing.Size(213, 26);
            this.kwota.TabIndex = 17;
            this.kwota.Click += new System.EventHandler(this.kwota_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.kwota);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(231, 32);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(225, 66);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Kwota";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.opis);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox4.Location = new System.Drawing.Point(231, 104);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(225, 57);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Opis";
            // 
            // opis
            // 
            this.opis.Location = new System.Drawing.Point(6, 19);
            this.opis.MaxLength = 40;
            this.opis.Name = "opis";
            this.opis.Size = new System.Drawing.Size(212, 26);
            this.opis.TabIndex = 17;
            this.opis.Click += new System.EventHandler(this.opis_Click);
            // 
            // anuluj
            // 
            this.anuluj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anuluj.Location = new System.Drawing.Point(50, 167);
            this.anuluj.Name = "anuluj";
            this.anuluj.Size = new System.Drawing.Size(200, 70);
            this.anuluj.TabIndex = 6;
            this.anuluj.Text = "Anuluj";
            this.anuluj.UseVisualStyleBackColor = true;
            this.anuluj.Click += new System.EventHandler(this.anuluj_Click);
            // 
            // id_l
            // 
            this.id_l.AutoSize = true;
            this.id_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.id_l.Location = new System.Drawing.Point(12, 9);
            this.id_l.Name = "id_l";
            this.id_l.Size = new System.Drawing.Size(30, 20);
            this.id_l.TabIndex = 21;
            this.id_l.Text = "ID:";
            // 
            // InneObrotyEdycja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 245);
            this.Controls.Add(this.id_l);
            this.Controls.Add(this.zapisz);
            this.Controls.Add(this.anuluj);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InneObrotyEdycja";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edycja innego obrotu";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.ComboBox typ;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker data;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox kwota;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox opis;
        private System.Windows.Forms.Button anuluj;
        private System.Windows.Forms.Label id_l;
    }
}