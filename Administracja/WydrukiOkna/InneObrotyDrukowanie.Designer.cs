﻿namespace Administracja.WydrukiOkna
{
    partial class InneObrotyDrukowanie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InneObrotyDrukowanie));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.data_od = new System.Windows.Forms.MonthCalendar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.data_do = new System.Windows.Forms.MonthCalendar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.typ = new System.Windows.Forms.ComboBox();
            this.zamknij = new System.Windows.Forms.Button();
            this.drukuj = new System.Windows.Forms.Button();
            this.podglad = new System.Windows.Forms.Button();
            this.podglad_dia = new System.Windows.Forms.PrintPreviewDialog();
            this.print_dia = new System.Windows.Forms.PrintDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.data_od);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(285, 200);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Od daty";
            // 
            // data_od
            // 
            this.data_od.Location = new System.Drawing.Point(12, 31);
            this.data_od.MaxSelectionCount = 1;
            this.data_od.Name = "data_od";
            this.data_od.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.data_do);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(305, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(285, 200);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Do daty";
            // 
            // data_do
            // 
            this.data_do.Location = new System.Drawing.Point(12, 31);
            this.data_do.MaxSelectionCount = 1;
            this.data_do.Name = "data_do";
            this.data_do.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.typ);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(596, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(205, 70);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Typ";
            // 
            // typ
            // 
            this.typ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typ.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.typ.FormattingEnabled = true;
            this.typ.Items.AddRange(new object[] {
            "wszystkie",
            "konto -> kasa",
            "konto -> inne",
            "kasa -> konto",
            "kasa -> inne",
            "inne -> konto",
            "inne -> kasa"});
            this.typ.Location = new System.Drawing.Point(6, 25);
            this.typ.Name = "typ";
            this.typ.Size = new System.Drawing.Size(193, 28);
            this.typ.TabIndex = 12;
            // 
            // zamknij
            // 
            this.zamknij.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zamknij.Location = new System.Drawing.Point(673, 218);
            this.zamknij.Name = "zamknij";
            this.zamknij.Size = new System.Drawing.Size(122, 43);
            this.zamknij.TabIndex = 15;
            this.zamknij.Text = "Zamknij";
            this.zamknij.UseVisualStyleBackColor = true;
            this.zamknij.Click += new System.EventHandler(this.ZamknijClick);
            // 
            // drukuj
            // 
            this.drukuj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.drukuj.Location = new System.Drawing.Point(545, 218);
            this.drukuj.Name = "drukuj";
            this.drukuj.Size = new System.Drawing.Size(122, 43);
            this.drukuj.TabIndex = 16;
            this.drukuj.Text = "Drukuj";
            this.drukuj.UseVisualStyleBackColor = true;
            this.drukuj.Click += new System.EventHandler(this.DrukujClick);
            // 
            // podglad
            // 
            this.podglad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.podglad.Location = new System.Drawing.Point(417, 218);
            this.podglad.Name = "podglad";
            this.podglad.Size = new System.Drawing.Size(122, 43);
            this.podglad.TabIndex = 17;
            this.podglad.Text = "Podglad";
            this.podglad.UseVisualStyleBackColor = true;
            this.podglad.Click += new System.EventHandler(this.PodgladClick);
            // 
            // podglad_dia
            // 
            this.podglad_dia.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.podglad_dia.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.podglad_dia.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.podglad_dia.ClientSize = new System.Drawing.Size(400, 300);
            this.podglad_dia.Enabled = true;
            this.podglad_dia.Icon = ((System.Drawing.Icon)(resources.GetObject("podglad_dia.Icon")));
            this.podglad_dia.Name = "printPreviewDialog1";
            this.podglad_dia.Visible = false;
            // 
            // print_dia
            // 
            this.print_dia.UseEXDialog = true;
            // 
            // InneObrotyDrukowanie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 270);
            this.Controls.Add(this.podglad);
            this.Controls.Add(this.drukuj);
            this.Controls.Add(this.zamknij);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InneObrotyDrukowanie";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Drukowanie innych obrotów";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MonthCalendar data_od;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MonthCalendar data_do;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox typ;
        private System.Windows.Forms.Button zamknij;
        private System.Windows.Forms.Button drukuj;
        private System.Windows.Forms.Button podglad;
        private System.Windows.Forms.PrintPreviewDialog podglad_dia;
        private System.Windows.Forms.PrintDialog print_dia;
    }
}