﻿using System;
using System.Windows.Forms;

namespace Administracja.WydrukiOkna
{
    public partial class InneObrotyEdycja : Form
    {
        private readonly bool _edycja;
        private readonly int _id;

        public InneObrotyEdycja(bool edycja, int id)
        {
            InitializeComponent();
            _edycja = edycja;
            _id = id;
            typ.SelectedIndex = 0;
            Wyswietl();
        }

        private void Wyswietl()
        {
            var reader1D = Baza.Wczytaj1D("SELECT * FROM INNE_OBROTY WHERE ID = '" + _id + "'");

            id_l.Text = "ID: " + reader1D[0];
            data.Value = DateTime.Parse(reader1D[1]);
            typ.Text = reader1D[2];
            kwota.Text = Convert.ToDouble(reader1D[3].Replace('.', ',')).ToString("0.00");
            opis.Text = reader1D[4];
        }

        private void zapisz_Click(object sender, EventArgs e)
        {
            Baza.Zapisz("UPDATE INNE_OBROTY SET "
                + "Data = '" + data.Value.ToShortDateString()
                + "', Typ = '" + typ.Text
                + "', Kwota = '" + kwota.Text.Replace(",", ".")
                + "', Opis = '" + opis.Text
                + "' WHERE ID='" + _id + "';");

            Close();
        }

        private void anuluj_Click(object sender, EventArgs e)
        {
            if (!_edycja)
            {
                Baza.Zapisz("DELETE FROM INNE_OBROTY WHERE ID='" + _id + "';");
            }

            Close();
        }

        private void opis_Click(object sender, EventArgs e)
        {
            opis.SelectAll();
        }

        private void kwota_Click(object sender, EventArgs e)
        {
            kwota.SelectAll();
        }
    }
}
