﻿namespace Administracja.WydrukiOkna
{
    partial class Zestawienia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Zestawienia));
            this.zamknij = new System.Windows.Forms.Button();
            this.dateToGb = new System.Windows.Forms.GroupBox();
            this.dateTo = new System.Windows.Forms.DateTimePicker();
            this.dateFromGb = new System.Windows.Forms.GroupBox();
            this.dateFrom = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.odsetki_rad = new System.Windows.Forms.RadioButton();
            this.wydatki_rad = new System.Windows.Forms.RadioButton();
            this.wplaty_rad = new System.Windows.Forms.RadioButton();
            this.faktury_rad = new System.Windows.Forms.RadioButton();
            this.drukuj = new System.Windows.Forms.Button();
            this.podglad = new System.Windows.Forms.Button();
            this.podglad_dia = new System.Windows.Forms.PrintPreviewDialog();
            this._printDialog = new System.Windows.Forms.PrintDialog();
            this.fullMonthCh = new System.Windows.Forms.CheckBox();
            this.dateToGb.SuspendLayout();
            this.dateFromGb.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // zamknij
            // 
            this.zamknij.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zamknij.Location = new System.Drawing.Point(424, 172);
            this.zamknij.Name = "zamknij";
            this.zamknij.Size = new System.Drawing.Size(200, 70);
            this.zamknij.TabIndex = 43;
            this.zamknij.Text = "Zamknij";
            this.zamknij.UseVisualStyleBackColor = true;
            this.zamknij.Click += new System.EventHandler(this.ZamknijClick);
            this.zamknij.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ZestawieniaKeyDown);
            // 
            // dateToGb
            // 
            this.dateToGb.Controls.Add(this.dateTo);
            this.dateToGb.Location = new System.Drawing.Point(232, 67);
            this.dateToGb.Name = "dateToGb";
            this.dateToGb.Size = new System.Drawing.Size(214, 68);
            this.dateToGb.TabIndex = 45;
            this.dateToGb.TabStop = false;
            this.dateToGb.Text = "Data do";
            // 
            // dateTo
            // 
            this.dateTo.CustomFormat = "dd MMMM yyyy";
            this.dateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTo.Location = new System.Drawing.Point(6, 25);
            this.dateTo.Name = "dateTo";
            this.dateTo.Size = new System.Drawing.Size(200, 26);
            this.dateTo.TabIndex = 35;
            this.dateTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ZestawieniaKeyDown);
            // 
            // dateFromGb
            // 
            this.dateFromGb.Controls.Add(this.dateFrom);
            this.dateFromGb.Location = new System.Drawing.Point(12, 67);
            this.dateFromGb.Name = "dateFromGb";
            this.dateFromGb.Size = new System.Drawing.Size(214, 68);
            this.dateFromGb.TabIndex = 44;
            this.dateFromGb.TabStop = false;
            this.dateFromGb.Text = "Data od";
            // 
            // dateFrom
            // 
            this.dateFrom.CustomFormat = "dd MMMM yyyy";
            this.dateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateFrom.Location = new System.Drawing.Point(6, 25);
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Size = new System.Drawing.Size(200, 26);
            this.dateFrom.TabIndex = 35;
            this.dateFrom.ValueChanged += new System.EventHandler(this.DateFrom_ValueChanged);
            this.dateFrom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ZestawieniaKeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.odsetki_rad);
            this.groupBox3.Controls.Add(this.wydatki_rad);
            this.groupBox3.Controls.Add(this.wplaty_rad);
            this.groupBox3.Controls.Add(this.faktury_rad);
            this.groupBox3.Location = new System.Drawing.Point(511, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(108, 150);
            this.groupBox3.TabIndex = 46;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Typ";
            // 
            // odsetki_rad
            // 
            this.odsetki_rad.AutoSize = true;
            this.odsetki_rad.Location = new System.Drawing.Point(6, 115);
            this.odsetki_rad.Name = "odsetki_rad";
            this.odsetki_rad.Size = new System.Drawing.Size(81, 24);
            this.odsetki_rad.TabIndex = 3;
            this.odsetki_rad.Text = "Odsetki";
            this.odsetki_rad.UseVisualStyleBackColor = true;
            this.odsetki_rad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ZestawieniaKeyDown);
            // 
            // wydatki_rad
            // 
            this.wydatki_rad.AutoSize = true;
            this.wydatki_rad.Location = new System.Drawing.Point(6, 85);
            this.wydatki_rad.Name = "wydatki_rad";
            this.wydatki_rad.Size = new System.Drawing.Size(83, 24);
            this.wydatki_rad.TabIndex = 2;
            this.wydatki_rad.Text = "Wydatki";
            this.wydatki_rad.UseVisualStyleBackColor = true;
            this.wydatki_rad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ZestawieniaKeyDown);
            // 
            // wplaty_rad
            // 
            this.wplaty_rad.AutoSize = true;
            this.wplaty_rad.Location = new System.Drawing.Point(6, 55);
            this.wplaty_rad.Name = "wplaty_rad";
            this.wplaty_rad.Size = new System.Drawing.Size(75, 24);
            this.wplaty_rad.TabIndex = 1;
            this.wplaty_rad.Text = "Wplaty";
            this.wplaty_rad.UseVisualStyleBackColor = true;
            this.wplaty_rad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ZestawieniaKeyDown);
            // 
            // faktury_rad
            // 
            this.faktury_rad.AutoSize = true;
            this.faktury_rad.Checked = true;
            this.faktury_rad.Location = new System.Drawing.Point(6, 25);
            this.faktury_rad.Name = "faktury_rad";
            this.faktury_rad.Size = new System.Drawing.Size(80, 24);
            this.faktury_rad.TabIndex = 0;
            this.faktury_rad.TabStop = true;
            this.faktury_rad.Text = "Faktury";
            this.faktury_rad.UseVisualStyleBackColor = true;
            this.faktury_rad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ZestawieniaKeyDown);
            // 
            // drukuj
            // 
            this.drukuj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.drukuj.Location = new System.Drawing.Point(218, 172);
            this.drukuj.Name = "drukuj";
            this.drukuj.Size = new System.Drawing.Size(200, 70);
            this.drukuj.TabIndex = 47;
            this.drukuj.Text = "Drukuj";
            this.drukuj.UseVisualStyleBackColor = true;
            this.drukuj.Click += new System.EventHandler(this.DrukujClick);
            this.drukuj.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ZestawieniaKeyDown);
            // 
            // podglad
            // 
            this.podglad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.podglad.Location = new System.Drawing.Point(12, 172);
            this.podglad.Name = "podglad";
            this.podglad.Size = new System.Drawing.Size(200, 70);
            this.podglad.TabIndex = 48;
            this.podglad.Text = "Podgląd";
            this.podglad.UseVisualStyleBackColor = true;
            this.podglad.Click += new System.EventHandler(this.PodgladClick);
            this.podglad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ZestawieniaKeyDown);
            // 
            // podglad_dia
            // 
            this.podglad_dia.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.podglad_dia.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.podglad_dia.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.podglad_dia.ClientSize = new System.Drawing.Size(400, 300);
            this.podglad_dia.Enabled = true;
            this.podglad_dia.Icon = ((System.Drawing.Icon)(resources.GetObject("podglad_dia.Icon")));
            this.podglad_dia.Name = "printPreviewDialog1";
            this.podglad_dia.Visible = false;
            // 
            // _printDialog
            // 
            this._printDialog.UseEXDialog = true;
            // 
            // fullMonthCh
            // 
            this.fullMonthCh.AutoSize = true;
            this.fullMonthCh.Checked = true;
            this.fullMonthCh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.fullMonthCh.Location = new System.Drawing.Point(18, 23);
            this.fullMonthCh.Name = "fullMonthCh";
            this.fullMonthCh.Size = new System.Drawing.Size(129, 24);
            this.fullMonthCh.TabIndex = 49;
            this.fullMonthCh.Text = "Jeden miesiąc";
            this.fullMonthCh.UseVisualStyleBackColor = true;
            this.fullMonthCh.CheckedChanged += new System.EventHandler(this.FullMonthCh_CheckedChanged);
            this.fullMonthCh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ZestawieniaKeyDown);
            // 
            // Zestawienia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 256);
            this.Controls.Add(this.fullMonthCh);
            this.Controls.Add(this.podglad);
            this.Controls.Add(this.drukuj);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.dateToGb);
            this.Controls.Add(this.dateFromGb);
            this.Controls.Add(this.zamknij);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Zestawienia";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zestawienia";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ZestawieniaKeyDown);
            this.dateToGb.ResumeLayout(false);
            this.dateFromGb.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button zamknij;
        private System.Windows.Forms.GroupBox dateToGb;
        private System.Windows.Forms.DateTimePicker dateTo;
        private System.Windows.Forms.GroupBox dateFromGb;
        private System.Windows.Forms.DateTimePicker dateFrom;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton wydatki_rad;
        private System.Windows.Forms.RadioButton wplaty_rad;
        private System.Windows.Forms.RadioButton faktury_rad;
        private System.Windows.Forms.Button drukuj;
        private System.Windows.Forms.Button podglad;
        private System.Windows.Forms.PrintPreviewDialog podglad_dia;
        private System.Windows.Forms.PrintDialog _printDialog;
        private System.Windows.Forms.RadioButton odsetki_rad;
        private System.Windows.Forms.CheckBox fullMonthCh;
    }
}