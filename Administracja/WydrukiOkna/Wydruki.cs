﻿using System;
using System.Windows.Forms;
using Administracja.InneOkna;

namespace Administracja.WydrukiOkna
{
    public partial class Wydruki : UserControl
    {
        public Wydruki()
        {
            InitializeComponent();
        }

        private void Inne_Obr_Click(object sender, EventArgs e)
        {
            var z = new InneObroty { MdiParent = ParentWindow.ParentStatic };
            z.Show();
        }

        private void zestawienia_Click(object sender, EventArgs e)
        {
            new Zestawienia().ShowDialog();
        }

        private void saldo_Click(object sender, EventArgs e)
        {
            var z = new Salda { MdiParent = ParentWindow.ParentStatic };
            z.Show();
        }

        private void Dluznicy_Click(object sender, EventArgs e)
        {

        }

        private void zestawieniaSkladnikow_Click(object sender, EventArgs e)
        {
            var z = new ZestawieniaSkladnikow {MdiParent = ParentWindow.ParentStatic};
            z.Show();
        }
    }
}
