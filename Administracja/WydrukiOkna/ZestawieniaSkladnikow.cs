﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Administracja.WydrukiOkna
{
    public partial class ZestawieniaSkladnikow : Form
    {
        private double _suma;
        private readonly List<int> _idSList = new List<int>();
        private string _tabela;

        public ZestawieniaSkladnikow()
        {
            InitializeComponent();

            Wyswietl();
        }

        private void Wyswietl()
        {
            int selectedIndex;

            try
            {
                selectedIndex = lista.SelectedItems[0].Index;
            }
            catch
            {
                selectedIndex = 0;
            }

            lista.Items.Clear();
            _idSList.Clear();

            _tabela = wydatkiRad.Checked ? "Skladniki_Z" : "Skladniki";

            var reader2D = Baza.Wczytaj2D("SELECT * FROM " + _tabela + " ORDER BY Nazwa");
            foreach (var reader1D in reader2D)
            {
                _idSList.Add(int.Parse(reader1D[0]));
                ListViewItem element = lista.Items.Add(reader1D[1]);
                if (wydatkiRad.Checked) element.SubItems.Add(reader1D[2]);
                element.SubItems.Add(reader1D[3]);
                element.SubItems.Add(reader1D[4]);
            }

            try
            {
                lista.Items[selectedIndex].Selected = true;
            }
            catch (ArgumentOutOfRangeException)
            {
                if (lista.Items.Count != 0) lista.Items[0].Selected = true;
            }
        }

        private void LiczKolejneClick(object sender, EventArgs e)
        {
            string query;

            string grupa = "";
            if (!string.IsNullOrWhiteSpace(grupaNajemcow.Text)) grupa = " AND ID_N IN (SELECT ID FROM OSOBY WHERE GRUPA = '" + grupaNajemcow.Text + "')";

            if (przychodyRad.Checked)
            {
                query = "SELECT SUM(CENA) FROM FAKTURY_SKLADNIKI JOIN FAKTURY ON FAKTURY.ID = FAKTURY_SKLADNIKI.ID_F WHERE ID_S = '" +
                            _idSList[lista.SelectedItems[0].Index] + "' AND DATA >= '"
                            + dataOd.SelectionStart.ToString("dd.MM.yyyy") + "' AND DATA <= '" +
                            dataDo.SelectionEnd.ToString("dd.MM.yyyy") + "'" + grupa;
            }
            else
            {
                query = "SELECT SUM(NETTO * ILOSC) FROM ZAKUPY_SKLADNIKI JOIN ZAKUPY ON ZAKUPY.ID = ZAKUPY_SKLADNIKI.ID_Z WHERE ID_S = '" +
                            _idSList[lista.SelectedItems[0].Index] + "' AND DATA_WPLATY >= '"
                            + dataOd.SelectionStart.ToString("dd.MM.yyyy") + "' AND DATA_WPLATY <= '" +
                            dataDo.SelectionEnd.ToString("dd.MM.yyyy") + "'";
            }

            double sumaPosrenia = Convert.ToDouble(Baza.Wczytaj(query));

            _suma += sumaPosrenia;
            zaSkladnik.Text = "Za składnik: " + string.Format("{0:N2}", sumaPosrenia);
            wSumie.Text = "W sumie: " + string.Format("{0:N2}", _suma);
        }

        private void ZerujClick(object sender, EventArgs e)
        {
            _suma = 0;
            zaSkladnik.Text = "Za składnik: 0,00";
            wSumie.Text = "W sumie: 0,00";
        }

        private void ZamknijClick(object sender, EventArgs e)
        {
            Close();
        }

        private void RodzajSelectedIndexChanged(object sender, EventArgs e)
        {
            Wyswietl();

            grupaNajemcow.Enabled = przychodyRad.Checked;
        }
    }
}
