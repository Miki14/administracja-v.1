﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using Administracja.KlasyStatic;

namespace Administracja.WydrukiOkna
{
    public partial class InneObrotyDrukowanie : Form
    {
        private int licznikStron;
        private int licznikPozycji;
        private readonly int iloscPozycji;
        private bool fPodsumowanie;
        private string typS;

        public InneObrotyDrukowanie(int ilosc_pozycji)
        {
            InitializeComponent();
            typ.SelectedIndex = 0;
            licznikStron = 0;
            licznikPozycji = 0;
            fPodsumowanie = false;
            iloscPozycji = ilosc_pozycji;
            data_od.SelectionStart = DateTimeHelper.GetDateFirstDayOfMonth(DateTime.Today);
            data_do.SelectionStart = DateTimeHelper.GetDateLastDayOfMonth(DateTime.Today);
        }

        private void PodgladClick(object sender, EventArgs e)
        {
            licznikStron = 0;
            licznikPozycji = 0;

            var pd = new PrintDocument();
            pd.PrintPage += PdPrintPage;
            podglad_dia.Document = pd;
            ((Form)podglad_dia).WindowState = FormWindowState.Maximized;
            podglad_dia.PrintPreviewControl.Zoom = 1.5;
            podglad_dia.ShowDialog();
        }

        private void DrukujClick(object sender, EventArgs e)
        {
            licznikStron = 0;
            licznikPozycji = 0;

            var pd = new PrintDocument();
            pd.PrintPage += PdPrintPage;
            print_dia.Document = pd;
            if (print_dia.ShowDialog() != DialogResult.OK) return;
            try
            {
                pd.Print();
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
        }

        private void ZamknijClick(object sender, EventArgs e)
        {
            Close();
        }

        private void PdPrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            const int lewyMargX = 75;
            const int pozNaglowka = 50;
            const int pozycjiNaStronie = 45;
            int pozY = 70;
            bool fStop = false;

            g.DrawString("Zestawienie innych obrotów", Fonty.Header, Brushes.Black, new PointF(275, pozNaglowka));
            g.DrawString("Za okres od  " + data_od.SelectionStart.ToString("dd.MM.yyyy") + " do  " + data_do.SelectionStart.ToString("dd.MM.yyyy"), Fonty.Normal, Brushes.Black, new PointF(lewyMargX, pozNaglowka + 30));
            g.DrawString("Typ: " + typ.Text, Fonty.Normal, Brushes.Black, new PointF(450, pozNaglowka + 30));

            var reder1D = Baza.Wczytaj1D("SELECT Nazwa_1, Nazwa_2, Adres_1, Adres_2 FROM DANE_SPRZEDAWCY");
            g.DrawString(reder1D[0] + " " + reder1D[1] + " " + reder1D[2] + " " + reder1D[3], Fonty.Normal, Brushes.Black, new PointF(lewyMargX, pozNaglowka + 60));

            if (!fPodsumowanie)
            {
                pozY += 70;

                //NAGLOWKI
                g.DrawString("Lp.", Fonty.Goods, Brushes.Black, new PointF(lewyMargX, pozY));
                g.DrawString("Data", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 30, pozY));
                g.DrawString("Typ", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 130, pozY));
                g.DrawString("Kwota", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 280, pozY));
                g.DrawString("Opis", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 350, pozY));
                g.DrawLine(Fonty.DashPen, lewyMargX, pozY + 25, 750, pozY + 25);
                pozY += 10;

                if (typ.Text == "wszystkie") typS = "%";
                else typS = typ.Text;

                try
                {
                    var reader2D = Baza.Wczytaj2D("SELECT SKIP " + pozycjiNaStronie * licznikStron + " * FROM INNE_OBROTY WHERE TYP LIKE '" + typS + "' AND DATA >= '"
                        + data_od.SelectionStart.ToString("dd.MM.yyyy") + "' AND DATA <= '" + data_do.SelectionStart.ToString("dd.MM.yyyy") + "'ORDER BY DATA");

                    for (int i = 0; i < reader2D.Count && !fStop; i++)
                    {
                        if (licznikPozycji >= (licznikStron + 1) * pozycjiNaStronie)
                        {
                            fStop = true;
                            licznikStron++;
                            e.HasMorePages = true;
                        }
                        else
                        {
                            pozY += 20;
                            licznikPozycji++;

                            g.DrawString((licznikPozycji).ToString(), Fonty.Goods, Brushes.Black, new PointF(lewyMargX, pozY));
                            g.DrawString(OperacjeTekstowe.TrymujDate(reader2D[i][1]), Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 30, pozY));
                            g.DrawString(reader2D[i][2], Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 130, pozY));
                            g.DrawString(string.Format("{0:N2}", Convert.ToDouble(reader2D[i][3])), Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 340, pozY), Fonty.Left);
                            g.DrawString(reader2D[i][4], Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 350, pozY));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Wyjatki.Error(ex);
                }

                if (pozY < 820)
                {
                    Podsumowanie(ref g, pozY, ref e);
                }
                else
                {
                    if (licznikPozycji >= iloscPozycji)
                    {
                        e.HasMorePages = true;
                        fPodsumowanie = true;
                    }
                }
            }
            else
            {
                licznikStron++;

                Podsumowanie(ref g, pozY, ref e);
            }
        }

        private void Podsumowanie(ref Graphics g, int poz_y, ref PrintPageEventArgs e)
        {
            string dataOdS = data_od.SelectionStart.ToString("dd.MM.yyyy");
            string dataDoS = data_do.SelectionStart.ToString("dd.MM.yyyy");
            const string kwerendaP = "SELECT SUM(Kwota) FROM INNE_OBROTY WHERE TYP LIKE '";
            string kwerendaK = "' AND DATA >= '" + dataOdS + "' AND DATA <= '" + dataDoS + "'";
            const int pozOpisu = 250;
            const int pozSum = 500;

            poz_y += 100;
            g.DrawString("Podsumowanie", Fonty.Dane, Brushes.Black, new PointF(325, poz_y));

            if (typS == "%")
            {
                string[] typy = { "konto -> kasa", "konto -> inne", "kasa -> konto", "kasa -> inne", "inne -> konto", "inne -> kasa" };

                foreach (string x in typy)
                {
                    poz_y += 30;
                    g.DrawString(x, Fonty.Normal, Brushes.Black, new PointF(pozOpisu, poz_y));
                    g.DrawString(Convert.ToDouble(Baza.Wczytaj(kwerendaP + x + kwerendaK).Replace(".", ",")).ToString("0.00"), Fonty.Normal, Brushes.Black, new PointF(pozSum, poz_y), Fonty.Left);
                }
            }
            else
            {
                poz_y += 30;
                g.DrawString(typS, Fonty.Normal, Brushes.Black, new PointF(pozOpisu, poz_y));
                g.DrawString(Convert.ToDouble(Baza.Wczytaj(kwerendaP + typS + kwerendaK)).ToString("0.00"), Fonty.Normal, Brushes.Black, new PointF(pozSum, poz_y), Fonty.Left);
            }

            e.HasMorePages = false;
            fPodsumowanie = false;
            licznikPozycji = 0;
            licznikStron = 0;
        }
    }
}
