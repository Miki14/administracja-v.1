﻿namespace Administracja.WydrukiOkna
{
    public class Cena
    {
        public double Netto;
        public double Vat;

        public void Append(Cena cena)
        {
            Netto += cena.Netto;
            Vat += cena.Vat;
        }
    }
}
