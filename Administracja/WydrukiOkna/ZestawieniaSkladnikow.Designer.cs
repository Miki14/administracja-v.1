﻿namespace Administracja.WydrukiOkna
{
    partial class ZestawieniaSkladnikow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZestawieniaSkladnikow));
            this.zamknij = new System.Windows.Forms.Button();
            this.dataOd = new System.Windows.Forms.MonthCalendar();
            this.dataDo = new System.Windows.Forms.MonthCalendar();
            this.lista = new System.Windows.Forms.ListView();
            this.columnHeader0 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.liczKolejne = new System.Windows.Forms.Button();
            this.zeruj = new System.Windows.Forms.Button();
            this.zaSkladnik = new System.Windows.Forms.Label();
            this.wSumie = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.wydatkiRad = new System.Windows.Forms.RadioButton();
            this.przychodyRad = new System.Windows.Forms.RadioButton();
            this.grupaNajemcow = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // zamknij
            // 
            this.zamknij.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zamknij.Location = new System.Drawing.Point(1128, 578);
            this.zamknij.Name = "zamknij";
            this.zamknij.Size = new System.Drawing.Size(200, 70);
            this.zamknij.TabIndex = 109;
            this.zamknij.Text = "Zamknij";
            this.zamknij.UseVisualStyleBackColor = true;
            this.zamknij.Click += new System.EventHandler(this.ZamknijClick);
            // 
            // dataOd
            // 
            this.dataOd.Location = new System.Drawing.Point(12, 31);
            this.dataOd.Name = "dataOd";
            this.dataOd.TabIndex = 110;
            // 
            // dataDo
            // 
            this.dataDo.Location = new System.Drawing.Point(12, 31);
            this.dataDo.Name = "dataDo";
            this.dataDo.TabIndex = 111;
            // 
            // lista
            // 
            this.lista.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader0,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lista.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lista.FullRowSelect = true;
            this.lista.GridLines = true;
            this.lista.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lista.HideSelection = false;
            this.lista.Location = new System.Drawing.Point(12, 12);
            this.lista.MultiSelect = false;
            this.lista.Name = "lista";
            this.lista.Size = new System.Drawing.Size(734, 636);
            this.lista.TabIndex = 112;
            this.lista.UseCompatibleStateImageBehavior = false;
            this.lista.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader0
            // 
            this.columnHeader0.Text = "Nazwa";
            this.columnHeader0.Width = 346;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Netto";
            this.columnHeader1.Width = 127;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "% Vat";
            this.columnHeader2.Width = 110;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Grupa";
            this.columnHeader3.Width = 108;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataOd);
            this.groupBox1.Location = new System.Drawing.Point(752, 122);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(285, 200);
            this.groupBox1.TabIndex = 113;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data od";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataDo);
            this.groupBox2.Location = new System.Drawing.Point(1043, 122);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(285, 200);
            this.groupBox2.TabIndex = 114;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data do";
            // 
            // liczKolejne
            // 
            this.liczKolejne.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.liczKolejne.Location = new System.Drawing.Point(958, 328);
            this.liczKolejne.Name = "liczKolejne";
            this.liczKolejne.Size = new System.Drawing.Size(182, 70);
            this.liczKolejne.TabIndex = 115;
            this.liczKolejne.Text = "Licz kolejne";
            this.liczKolejne.UseVisualStyleBackColor = true;
            this.liczKolejne.Click += new System.EventHandler(this.LiczKolejneClick);
            // 
            // zeruj
            // 
            this.zeruj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zeruj.Location = new System.Drawing.Point(1146, 328);
            this.zeruj.Name = "zeruj";
            this.zeruj.Size = new System.Drawing.Size(182, 70);
            this.zeruj.TabIndex = 116;
            this.zeruj.Text = "Zeruj";
            this.zeruj.UseVisualStyleBackColor = true;
            this.zeruj.Click += new System.EventHandler(this.ZerujClick);
            // 
            // zaSkladnik
            // 
            this.zaSkladnik.AutoSize = true;
            this.zaSkladnik.Location = new System.Drawing.Point(1019, 456);
            this.zaSkladnik.Name = "zaSkladnik";
            this.zaSkladnik.Size = new System.Drawing.Size(129, 20);
            this.zaSkladnik.TabIndex = 117;
            this.zaSkladnik.Text = "Za składnik: 0,00";
            // 
            // wSumie
            // 
            this.wSumie.AutoSize = true;
            this.wSumie.Location = new System.Drawing.Point(1019, 514);
            this.wSumie.Name = "wSumie";
            this.wSumie.Size = new System.Drawing.Size(109, 20);
            this.wSumie.TabIndex = 118;
            this.wSumie.Text = "W sumie: 0,00";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.wydatkiRad);
            this.groupBox3.Controls.Add(this.przychodyRad);
            this.groupBox3.Location = new System.Drawing.Point(752, 14);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(117, 88);
            this.groupBox3.TabIndex = 120;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Rodzaj";
            // 
            // wydatkiRad
            // 
            this.wydatkiRad.AutoSize = true;
            this.wydatkiRad.Location = new System.Drawing.Point(12, 55);
            this.wydatkiRad.Name = "wydatkiRad";
            this.wydatkiRad.Size = new System.Drawing.Size(83, 24);
            this.wydatkiRad.TabIndex = 1;
            this.wydatkiRad.Text = "Wydatki";
            this.wydatkiRad.UseVisualStyleBackColor = true;
            this.wydatkiRad.CheckedChanged += new System.EventHandler(this.RodzajSelectedIndexChanged);
            // 
            // przychodyRad
            // 
            this.przychodyRad.AutoSize = true;
            this.przychodyRad.Checked = true;
            this.przychodyRad.Location = new System.Drawing.Point(12, 25);
            this.przychodyRad.Name = "przychodyRad";
            this.przychodyRad.Size = new System.Drawing.Size(99, 24);
            this.przychodyRad.TabIndex = 0;
            this.przychodyRad.TabStop = true;
            this.przychodyRad.Text = "Przychody";
            this.przychodyRad.UseVisualStyleBackColor = true;
            this.przychodyRad.CheckedChanged += new System.EventHandler(this.RodzajSelectedIndexChanged);
            // 
            // grupaNajemcow
            // 
            this.grupaNajemcow.Location = new System.Drawing.Point(18, 35);
            this.grupaNajemcow.Name = "grupaNajemcow";
            this.grupaNajemcow.Size = new System.Drawing.Size(124, 26);
            this.grupaNajemcow.TabIndex = 121;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.grupaNajemcow);
            this.groupBox4.Location = new System.Drawing.Point(875, 14);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(154, 88);
            this.groupBox4.TabIndex = 122;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Grupa najemców";
            // 
            // ZestawieniaSkladnikow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1340, 660);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.wSumie);
            this.Controls.Add(this.zaSkladnik);
            this.Controls.Add(this.zeruj);
            this.Controls.Add(this.liczKolejne);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lista);
            this.Controls.Add(this.zamknij);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ZestawieniaSkladnikow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ZestawieniaSkladnikow";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button zamknij;
        private System.Windows.Forms.MonthCalendar dataOd;
        private System.Windows.Forms.MonthCalendar dataDo;
        private System.Windows.Forms.ListView lista;
        private System.Windows.Forms.ColumnHeader columnHeader0;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button liczKolejne;
        private System.Windows.Forms.Button zeruj;
        private System.Windows.Forms.Label zaSkladnik;
        private System.Windows.Forms.Label wSumie;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.RadioButton wydatkiRad;
        private System.Windows.Forms.RadioButton przychodyRad;
        private System.Windows.Forms.TextBox grupaNajemcow;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}