﻿using System;
using System.Windows.Forms;
using Administracja.InneOkna;
using Administracja.OsobyOkna;
using Administracja.WydrukiOkna;

namespace Administracja
{
    public partial class ParentWindow : Form
    {
        public static ParentWindow ParentStatic;

        public ParentWindow()
        {
            InitializeComponent();

            ParentStatic = this;
            Text = "Administracja v. " + Application.ProductVersion;

            var z = new Glowne(0) { MdiParent = this };
            z.Show();
        }

        private void ParentFormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Program.Zamykam)
            {
                if (Wyjatki.Komunikat("Czy chcesz zakończyć pracę z programem?", "Zakończyć?", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                {
                    Baza.CloseConnection();
                    Program.Zamykam = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                Baza.CloseConnection();
            }
        }

        private void OsobyMenuClick(object sender, EventArgs e)
        {
            new Glowne(1) { MdiParent = this }.Show();
        }

        private void ZakupyMenuClick(object sender, EventArgs e)
        {
            new Glowne(2) { MdiParent = this }.Show();
        }

        private void ZestawieniaMenuClick(object sender, EventArgs e)
        {
            new Zestawienia().ShowDialog();
        }

        private void InneObrotyMenuClick(object sender, EventArgs e)
        {
            new InneObroty { MdiParent = this }.Show();
        }

        private void SaldoMenuClick(object sender, EventArgs e)
        {
            new Salda { MdiParent = this }.Show();
        }

        private void DaneSprzedawcyMenuClick(object sender, EventArgs e)
        {
            new Sprzedawca().ShowDialog();
        }

        private void SkładnikiMenuClick(object sender, EventArgs e)
        {
            new OsobySkladniki().ShowDialog();
        }

        private void ZarzadzajLatamiMenuClick(object sender, EventArgs e)
        {
            new Zarzadzanie { MdiParent = this }.Show();
        }

        private void PodgladZawBazyMenuClick(object sender, EventArgs e)
        {
            new PodgladBazy { MdiParent = this }.Show();
        }

        private void SprawdźAktualizacjeMenuClick(object sender, EventArgs e)
        {
            new Aktualizacja().ShowDialog();
        }

        private void OProgramieMenuClick(object sender, EventArgs e)
        {
            new OProgramie().ShowDialog();
        }

        private void OdsetkiMenuClick(object sender, EventArgs e)
        {
            new Odsetki().Show();
        }
    }
}
