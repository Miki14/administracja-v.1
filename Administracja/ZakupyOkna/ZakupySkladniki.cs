﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Administracja.ZakupyOkna
{
    public partial class ZakupySkladniki : Form
    {
        private readonly int _idZ;
        private int _idS;
        private int _indexDost;
        private int _indexWyb;

        public ZakupySkladniki(int idZ)
        {
            InitializeComponent();
            _idZ = idZ;
            grupy.Text = "3 - Zakupy";
            ListaDostSelectedIndexChanged(this, EventArgs.Empty);
            ListaWyb1SelectedIndexChanged(this, EventArgs.Empty);
            Wyswietl();
            WyswietlSkladniki();
        }

        private void Wyswietl()
        {
            lista_dost.Items.Clear();

            var reader2D = Baza.Wczytaj2D("SELECT ID, Nazwa, Vat FROM SKLADNIKI_Z WHERE GRUPA = '" + grupy.Text.Substring(0, 1) + "' order by Nazwa");

            foreach (var reader in reader2D)
            {
                ListViewItem element = lista_dost.Items.Add(reader[0]);
                element.SubItems.Add(reader[1]);
                element.SubItems.Add(reader[2] + "%");
            }

            if (lista_dost.Items.Count - 1 > _indexDost)
            {
                lista_dost.Items[_indexDost].Selected = true;
            }
        }

        private void WyswietlSkladniki()
        {
            lista_wyb_1.Items.Clear();
            lista_wyb_2.Items.Clear();
            lista_wyb_3.Items.Clear();

            var reader2D = Baza.Wczytaj2D("SELECT SKLADNIKI_Z.ID, Nazwa, Ilosc, ZAKUPY_SKLADNIKI.Netto, Vat FROM SKLADNIKI_Z JOIN ZAKUPY_SKLADNIKI ON SKLADNIKI_Z.ID=ZAKUPY_SKLADNIKI.ID_S "
                + "WHERE ZAKUPY_SKLADNIKI.ID_Z = '" + _idZ + "'");

            foreach (var reader1D in reader2D)
            {
                ListViewItem element = lista_wyb_1.Items.Add(reader1D[0]);
                element.SubItems.Add(reader1D[1]);
                int ilosc = Convert.ToInt32(reader1D[2]);
                lista_wyb_2.Items.Add(ilosc.ToString());
                double netto = Convert.ToDouble(reader1D[3].Replace(".", ","));
                element = lista_wyb_3.Items.Add(Math.Round(netto, 2).ToString("0.00"));
                int vat = int.Parse(reader1D[4]);
                element.SubItems.Add(vat.ToString());
                element.SubItems.Add(Math.Round(netto + netto * vat / 100, 2).ToString("0.00"));
                element.SubItems.Add(Math.Round((netto + netto * vat / 100) * ilosc, 2).ToString("0.00"));
            }

            if (lista_wyb_1.Items.Count != 0)
            {
                lista_wyb_1.Items[_indexWyb].Selected = true;
                lista_wyb_2.Items[_indexWyb].Selected = true;
                lista_wyb_3.Items[_indexWyb].Selected = true;
            }
        }

        private void OdswiezCeny()
        {
            for (int i = 0; i < lista_wyb_2.Items.Count; i++)
            {
                try
                {
                    double netto = Convert.ToDouble(lista_wyb_3.Items[i].Text.Replace(".", ","));
                    int vat = Convert.ToInt32(lista_wyb_3.Items[i].SubItems[1].Text);
                    int ilosc = Convert.ToInt32(lista_wyb_2.Items[i].Text);

                    lista_wyb_3.Items[i].Text = Math.Round(netto, 2).ToString("0.00");
                    lista_wyb_3.Items[i].SubItems[2].Text = Math.Round(netto * vat / 100 + netto, 2).ToString("0.00");
                    lista_wyb_3.Items[i].SubItems[3].Text = Math.Round((netto * vat / 100 + netto) * ilosc, 2).ToString("0.00");
                }
                catch
                {
                    Wyjatki.Komunikat("Błąd przy przeliczaniu wartości brutto.", "Błąd OdswiezCeny()", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void UsunClick(object sender, EventArgs e)
        {
            if (Wyjatki.Komunikat("Czy na pewno chcesz usunąć składnik \"" + lista_dost.Items[_indexDost].SubItems[1].Text + "\" ?", "Usunąć?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Baza.Zapisz("DELETE FROM SKLADNIKI_Z WHERE ID ='" + lista_dost.Items[_indexDost].Text + "'");
            }
            Wyswietl();
        }

        private void DodajClick(object sender, EventArgs e)
        {
            _idS = Baza.Ustaw("SKLADNIKI_Z");
            Baza.Zapisz("INSERT INTO SKLADNIKI_Z VALUES (GEN_ID(NUMERACJA_SKLADNIKI_Z,1),'',0,0,'3')");

            new ZakupySkladnikiEdycja(true, _idS).ShowDialog();
            Wyswietl();
        }

        private void EdytujClick(object sender, EventArgs e)
        {
            new ZakupySkladnikiEdycja(false, _idS).ShowDialog();
            Wyswietl();
        }

        private void AnulujClick(object sender, EventArgs e)
        {
            Close();
        }

        private void ZapiszClick(object sender, EventArgs e)
        {
            OdswiezCeny();

            double sumaNetto = 0;
            double sumaVat = 0;
            var sqlCommandText = new List<string>();

            Baza.Zapisz("DELETE FROM ZAKUPY_SKLADNIKI WHERE ID_Z ='" + _idZ + "'");
            Baza.Ustaw("ZAKUPY_SKLADNIKI");

            for (int i = 0; i < lista_wyb_1.Items.Count; i++)
            {
                sqlCommandText.Add("INSERT INTO ZAKUPY_SKLADNIKI VALUES(GEN_ID(NUMERACJA_ZAKUPY_SKLADNIKI,1)" +
                    ",'" + _idZ +
                    "','" + lista_wyb_1.Items[i].Text +
                    "','" + lista_wyb_2.Items[i].Text.Replace(",", ".") +
                    "','" + lista_wyb_3.Items[i].Text.Replace(",", ".") +
                    "');");

                double netto = Convert.ToDouble(lista_wyb_3.Items[i].Text.Replace(".", ","));
                sumaNetto += netto * Convert.ToInt32(lista_wyb_2.Items[i].Text);
                sumaVat += Convert.ToDouble(lista_wyb_3.Items[i].SubItems[1].Text.Replace(".", ",")) * netto / 100 * Convert.ToInt32(lista_wyb_2.Items[i].Text);
            }

            sqlCommandText.Add("UPDATE ZAKUPY SET SUMA_NETTO = '" + sumaNetto.ToString().Replace(",", ".") + "', SUMA_VAT='" + sumaVat.ToString().Replace(",", ".") + "' WHERE ID='" + _idZ + "'");

            Baza.Zapisz(sqlCommandText.ToArray());

            Close();
        }

        private void AddClick(object sender, EventArgs e)
        {
            if (lista_dost.Items.Count > 0)
            {
                var reader1D = Baza.Wczytaj1D("SELECT * FROM SKLADNIKI_Z WHERE ID = '" + lista_dost.Items[_indexDost].Text + "'");
                ListViewItem element = lista_wyb_1.Items.Add(reader1D[0]);
                element.SubItems.Add(reader1D[1]);
                lista_wyb_2.Items.Add("1");
                double netto = Convert.ToDouble(reader1D[2].Replace(".", ","));
                element = lista_wyb_3.Items.Add(Math.Round(netto, 2).ToString("0.00"));
                int vat = int.Parse(reader1D[3]);
                element.SubItems.Add(vat.ToString());
                element.SubItems.Add(Math.Round(netto + netto * vat / 100, 2).ToString("0.00").Replace(".", ","));
                element.SubItems.Add(Math.Round(netto + netto * vat / 100, 2).ToString("0.00").Replace(".", ","));

                try
                {
                    lista_wyb_1.Items[lista_wyb_1.Items.Count - 1].Selected = true;
                    lista_wyb_2.Items[lista_wyb_2.Items.Count - 1].Selected = true;
                    lista_wyb_3.Items[lista_wyb_3.Items.Count - 1].Selected = true;

                    lista_wyb_1.TopItem = lista_wyb_1.Items[lista_wyb_1.Items.Count - 1];
                    lista_wyb_2.TopItem = lista_wyb_2.Items[lista_wyb_2.Items.Count - 1];
                    lista_wyb_3.TopItem = lista_wyb_3.Items[lista_wyb_3.Items.Count - 1];
                }
                catch { }
            }
        }

        private void RemClick(object sender, EventArgs e)
        {
            int indexTmp = _indexWyb;

            if (lista_wyb_1.Items.Count > 0)
            {
                lista_wyb_1.Items[indexTmp].Remove();
                lista_wyb_2.Items[indexTmp].Remove();
                lista_wyb_3.Items[indexTmp].Remove();
            }
        }

        private void ListaDostSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _indexDost = lista_dost.SelectedItems[0].Index;
            }
            catch
            {
                _indexDost = 0;
            }

            if (lista_dost.Items.Count > 0)
            {
                _idS = Convert.ToInt32(lista_dost.Items[_indexDost].Text);
            }

            OdswiezCeny();
        }

        private void ListaWyb1SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _indexWyb = lista_wyb_1.SelectedItems[0].Index;
            }
            catch
            {
                _indexWyb = 0;
            }

            OdswiezCeny();
        }

        private void OdswiezCenyHandler(object sender, EventArgs e)
        {
            OdswiezCeny();
        }

        private void GrupySelectedIndexChanged(object sender, EventArgs e)
        {
            Wyswietl();
        }

        private void ListaWybDoubleClick(object sender, EventArgs e)
        {
            ((ListView)sender).SelectedItems[0].BeginEdit();
        }

        private void ListaWybAfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            ((ListView)sender).BeginInvoke(new MethodInvoker(OdswiezCeny));
        }
    }
}
