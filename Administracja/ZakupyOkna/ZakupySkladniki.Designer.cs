﻿namespace Administracja.ZakupyOkna
{
    partial class ZakupySkladniki
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZakupySkladniki));
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lista_wyb_2 = new System.Windows.Forms.ListView();
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lista_wyb_1 = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lista_wyb_3 = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grupy = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lista_dost = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.usun = new System.Windows.Forms.Button();
            this.dodaj = new System.Windows.Forms.Button();
            this.edytuj = new System.Windows.Forms.Button();
            this.rem = new System.Windows.Forms.Button();
            this.add = new System.Windows.Forms.Button();
            this.zapisz = new System.Windows.Forms.Button();
            this.anuluj = new System.Windows.Forms.Button();
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox7.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lista_wyb_2);
            this.groupBox7.Controls.Add(this.lista_wyb_1);
            this.groupBox7.Controls.Add(this.lista_wyb_3);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox7.Location = new System.Drawing.Point(554, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(774, 560);
            this.groupBox7.TabIndex = 22;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Lista Towarów Wybranych";
            // 
            // lista_wyb_2
            // 
            this.lista_wyb_2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader8});
            this.lista_wyb_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lista_wyb_2.FullRowSelect = true;
            this.lista_wyb_2.GridLines = true;
            this.lista_wyb_2.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lista_wyb_2.HideSelection = false;
            this.lista_wyb_2.LabelEdit = true;
            this.lista_wyb_2.Location = new System.Drawing.Point(358, 59);
            this.lista_wyb_2.MultiSelect = false;
            this.lista_wyb_2.Name = "lista_wyb_2";
            this.lista_wyb_2.Size = new System.Drawing.Size(54, 495);
            this.lista_wyb_2.TabIndex = 107;
            this.lista_wyb_2.UseCompatibleStateImageBehavior = false;
            this.lista_wyb_2.View = System.Windows.Forms.View.Details;
            this.lista_wyb_2.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.ListaWybAfterLabelEdit);
            this.lista_wyb_2.SelectedIndexChanged += new System.EventHandler(this.OdswiezCenyHandler);
            this.lista_wyb_2.DoubleClick += new System.EventHandler(this.ListaWybDoubleClick);
            this.lista_wyb_2.Leave += new System.EventHandler(this.OdswiezCenyHandler);
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Ilosć";
            this.columnHeader8.Width = 47;
            // 
            // lista_wyb_1
            // 
            this.lista_wyb_1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7});
            this.lista_wyb_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lista_wyb_1.FullRowSelect = true;
            this.lista_wyb_1.GridLines = true;
            this.lista_wyb_1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lista_wyb_1.HideSelection = false;
            this.lista_wyb_1.Location = new System.Drawing.Point(6, 59);
            this.lista_wyb_1.MultiSelect = false;
            this.lista_wyb_1.Name = "lista_wyb_1";
            this.lista_wyb_1.Size = new System.Drawing.Size(346, 495);
            this.lista_wyb_1.TabIndex = 103;
            this.lista_wyb_1.UseCompatibleStateImageBehavior = false;
            this.lista_wyb_1.View = System.Windows.Forms.View.Details;
            this.lista_wyb_1.SelectedIndexChanged += new System.EventHandler(this.ListaWyb1SelectedIndexChanged);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "ID";
            this.columnHeader6.Width = 37;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Nazwa";
            this.columnHeader7.Width = 302;
            // 
            // lista_wyb_3
            // 
            this.lista_wyb_3.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader9});
            this.lista_wyb_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lista_wyb_3.FullRowSelect = true;
            this.lista_wyb_3.GridLines = true;
            this.lista_wyb_3.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lista_wyb_3.HideSelection = false;
            this.lista_wyb_3.LabelEdit = true;
            this.lista_wyb_3.Location = new System.Drawing.Point(418, 59);
            this.lista_wyb_3.MultiSelect = false;
            this.lista_wyb_3.Name = "lista_wyb_3";
            this.lista_wyb_3.Size = new System.Drawing.Size(350, 495);
            this.lista_wyb_3.TabIndex = 106;
            this.lista_wyb_3.UseCompatibleStateImageBehavior = false;
            this.lista_wyb_3.View = System.Windows.Forms.View.Details;
            this.lista_wyb_3.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.ListaWybAfterLabelEdit);
            this.lista_wyb_3.SelectedIndexChanged += new System.EventHandler(this.OdswiezCenyHandler);
            this.lista_wyb_3.DoubleClick += new System.EventHandler(this.ListaWybDoubleClick);
            this.lista_wyb_3.Leave += new System.EventHandler(this.OdswiezCenyHandler);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Netto";
            this.columnHeader3.Width = 99;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "% VAT";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader4.Width = 56;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Brutto";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 95;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Suma Brutto";
            this.columnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader9.Width = 96;
            // 
            // grupy
            // 
            this.grupy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.grupy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grupy.FormattingEnabled = true;
            this.grupy.Items.AddRange(new object[] {
            "1 - Wynagrodzenia",
            "2 - Remonty",
            "3 - Zakupy",
            "4 - ZUS",
            "5 - Podatki",
            "6 - Opłaty"});
            this.grupy.Location = new System.Drawing.Point(8, 25);
            this.grupy.Name = "grupy";
            this.grupy.Size = new System.Drawing.Size(121, 28);
            this.grupy.TabIndex = 23;
            this.grupy.SelectedIndexChanged += new System.EventHandler(this.GrupySelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lista_dost);
            this.groupBox2.Controls.Add(this.grupy);
            this.groupBox2.Controls.Add(this.usun);
            this.groupBox2.Controls.Add(this.dodaj);
            this.groupBox2.Controls.Add(this.edytuj);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(470, 636);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lista Towarów Dostępnych";
            // 
            // lista_dost
            // 
            this.lista_dost.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader10});
            this.lista_dost.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lista_dost.FullRowSelect = true;
            this.lista_dost.GridLines = true;
            this.lista_dost.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lista_dost.HideSelection = false;
            this.lista_dost.Location = new System.Drawing.Point(8, 59);
            this.lista_dost.MultiSelect = false;
            this.lista_dost.Name = "lista_dost";
            this.lista_dost.Size = new System.Drawing.Size(456, 511);
            this.lista_dost.TabIndex = 102;
            this.lista_dost.UseCompatibleStateImageBehavior = false;
            this.lista_dost.View = System.Windows.Forms.View.Details;
            this.lista_dost.SelectedIndexChanged += new System.EventHandler(this.ListaDostSelectedIndexChanged);
            this.lista_dost.DoubleClick += new System.EventHandler(this.EdytujClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 37;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Nazwa";
            this.columnHeader2.Width = 345;
            // 
            // usun
            // 
            this.usun.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usun.Location = new System.Drawing.Point(8, 576);
            this.usun.Name = "usun";
            this.usun.Size = new System.Drawing.Size(148, 54);
            this.usun.TabIndex = 29;
            this.usun.Text = "Usuń";
            this.usun.UseVisualStyleBackColor = true;
            this.usun.Click += new System.EventHandler(this.UsunClick);
            // 
            // dodaj
            // 
            this.dodaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dodaj.Location = new System.Drawing.Point(162, 576);
            this.dodaj.Name = "dodaj";
            this.dodaj.Size = new System.Drawing.Size(148, 54);
            this.dodaj.TabIndex = 27;
            this.dodaj.Text = "Dodaj";
            this.dodaj.UseVisualStyleBackColor = true;
            this.dodaj.Click += new System.EventHandler(this.DodajClick);
            // 
            // edytuj
            // 
            this.edytuj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.edytuj.Location = new System.Drawing.Point(316, 576);
            this.edytuj.Name = "edytuj";
            this.edytuj.Size = new System.Drawing.Size(148, 54);
            this.edytuj.TabIndex = 28;
            this.edytuj.Text = "Edytuj";
            this.edytuj.UseVisualStyleBackColor = true;
            this.edytuj.Click += new System.EventHandler(this.EdytujClick);
            // 
            // rem
            // 
            this.rem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rem.Location = new System.Drawing.Point(488, 342);
            this.rem.Name = "rem";
            this.rem.Size = new System.Drawing.Size(60, 36);
            this.rem.TabIndex = 104;
            this.rem.Text = "<<";
            this.rem.UseVisualStyleBackColor = true;
            this.rem.Click += new System.EventHandler(this.RemClick);
            // 
            // add
            // 
            this.add.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.add.Location = new System.Drawing.Point(488, 296);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(60, 36);
            this.add.TabIndex = 103;
            this.add.Text = ">>";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.AddClick);
            // 
            // zapisz
            // 
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(1128, 578);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(200, 70);
            this.zapisz.TabIndex = 105;
            this.zapisz.Text = "Zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.ZapiszClick);
            // 
            // anuluj
            // 
            this.anuluj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anuluj.Location = new System.Drawing.Point(922, 578);
            this.anuluj.Name = "anuluj";
            this.anuluj.Size = new System.Drawing.Size(200, 70);
            this.anuluj.TabIndex = 106;
            this.anuluj.Text = "Anuluj";
            this.anuluj.UseVisualStyleBackColor = true;
            this.anuluj.Click += new System.EventHandler(this.AnulujClick);
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "VAT";
            this.columnHeader10.Width = 46;
            // 
            // ZakupySkladniki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1340, 660);
            this.ControlBox = false;
            this.Controls.Add(this.anuluj);
            this.Controls.Add(this.zapisz);
            this.Controls.Add(this.rem);
            this.Controls.Add(this.add);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ZakupySkladniki";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ZakupySkladniki";
            this.groupBox7.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox grupy;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button dodaj;
        private System.Windows.Forms.Button edytuj;
        private System.Windows.Forms.Button usun;
        private System.Windows.Forms.ListView lista_wyb_2;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ListView lista_wyb_1;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ListView lista_wyb_3;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ListView lista_dost;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button rem;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.Button anuluj;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
    }
}