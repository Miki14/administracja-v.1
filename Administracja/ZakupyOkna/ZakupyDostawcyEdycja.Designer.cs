﻿namespace Administracja.ZakupyOkna
{
    partial class ZakupyDostawcyEdycja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZakupyDostawcyEdycja));
            this.anuluj = new System.Windows.Forms.Button();
            this.zapisz = new System.Windows.Forms.Button();
            this.id_l = new System.Windows.Forms.Label();
            this.nazwa_l = new System.Windows.Forms.Label();
            this.nazwa_cd_l = new System.Windows.Forms.Label();
            this.nazwa_box = new System.Windows.Forms.TextBox();
            this.nazwa_cd_box = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.telefon_box = new System.Windows.Forms.TextBox();
            this.nip_box = new System.Windows.Forms.TextBox();
            this.adres_box = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // anuluj
            // 
            this.anuluj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anuluj.Location = new System.Drawing.Point(112, 203);
            this.anuluj.Name = "anuluj";
            this.anuluj.Size = new System.Drawing.Size(200, 70);
            this.anuluj.TabIndex = 7;
            this.anuluj.Text = "Anuluj";
            this.anuluj.UseVisualStyleBackColor = true;
            this.anuluj.Click += new System.EventHandler(this.anuluj_Click);
            // 
            // zapisz
            // 
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(318, 203);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(200, 70);
            this.zapisz.TabIndex = 6;
            this.zapisz.Text = "Zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.zapisz_Click);
            // 
            // id_l
            // 
            this.id_l.AutoSize = true;
            this.id_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.id_l.Location = new System.Drawing.Point(12, 9);
            this.id_l.Name = "id_l";
            this.id_l.Size = new System.Drawing.Size(33, 20);
            this.id_l.TabIndex = 0;
            this.id_l.Text = "ID:";
            // 
            // nazwa_l
            // 
            this.nazwa_l.AutoSize = true;
            this.nazwa_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwa_l.Location = new System.Drawing.Point(12, 45);
            this.nazwa_l.Name = "nazwa_l";
            this.nazwa_l.Size = new System.Drawing.Size(62, 20);
            this.nazwa_l.TabIndex = 0;
            this.nazwa_l.Text = "Nazwa";
            // 
            // nazwa_cd_l
            // 
            this.nazwa_cd_l.AutoSize = true;
            this.nazwa_cd_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwa_cd_l.Location = new System.Drawing.Point(12, 77);
            this.nazwa_cd_l.Name = "nazwa_cd_l";
            this.nazwa_cd_l.Size = new System.Drawing.Size(91, 20);
            this.nazwa_cd_l.TabIndex = 0;
            this.nazwa_cd_l.Text = "Nazwa cd.";
            // 
            // nazwa_box
            // 
            this.nazwa_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwa_box.Location = new System.Drawing.Point(109, 42);
            this.nazwa_box.MaxLength = 40;
            this.nazwa_box.Name = "nazwa_box";
            this.nazwa_box.Size = new System.Drawing.Size(409, 26);
            this.nazwa_box.TabIndex = 1;
            this.nazwa_box.TextChanged += new System.EventHandler(this.nazwa_box_TextChanged);
            // 
            // nazwa_cd_box
            // 
            this.nazwa_cd_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwa_cd_box.Location = new System.Drawing.Point(109, 74);
            this.nazwa_cd_box.MaxLength = 40;
            this.nazwa_cd_box.Name = "nazwa_cd_box";
            this.nazwa_cd_box.Size = new System.Drawing.Size(409, 26);
            this.nazwa_cd_box.TabIndex = 2;
            this.nazwa_cd_box.TextChanged += new System.EventHandler(this.nazwa_cd_box_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Adres";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(12, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "NIP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(12, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Telefon";
            // 
            // telefon_box
            // 
            this.telefon_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.telefon_box.Location = new System.Drawing.Point(109, 171);
            this.telefon_box.MaxLength = 15;
            this.telefon_box.Name = "telefon_box";
            this.telefon_box.Size = new System.Drawing.Size(409, 26);
            this.telefon_box.TabIndex = 5;
            // 
            // nip_box
            // 
            this.nip_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nip_box.Location = new System.Drawing.Point(109, 139);
            this.nip_box.MaxLength = 14;
            this.nip_box.Name = "nip_box";
            this.nip_box.Size = new System.Drawing.Size(409, 26);
            this.nip_box.TabIndex = 4;
            // 
            // adres_box
            // 
            this.adres_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.adres_box.Location = new System.Drawing.Point(109, 106);
            this.adres_box.MaxLength = 50;
            this.adres_box.Name = "adres_box";
            this.adres_box.Size = new System.Drawing.Size(409, 26);
            this.adres_box.TabIndex = 3;
            this.adres_box.TextChanged += new System.EventHandler(this.adres_box_TextChanged);
            // 
            // ZakupyDostawcyEdycja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 278);
            this.ControlBox = false;
            this.Controls.Add(this.adres_box);
            this.Controls.Add(this.nip_box);
            this.Controls.Add(this.telefon_box);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nazwa_cd_box);
            this.Controls.Add(this.nazwa_box);
            this.Controls.Add(this.nazwa_cd_l);
            this.Controls.Add(this.nazwa_l);
            this.Controls.Add(this.id_l);
            this.Controls.Add(this.zapisz);
            this.Controls.Add(this.anuluj);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ZakupyDostawcyEdycja";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edycja Dostawców";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button anuluj;
        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.Label id_l;
        private System.Windows.Forms.Label nazwa_l;
        private System.Windows.Forms.Label nazwa_cd_l;
        private System.Windows.Forms.TextBox nazwa_box;
        private System.Windows.Forms.TextBox nazwa_cd_box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox telefon_box;
        private System.Windows.Forms.TextBox nip_box;
        private System.Windows.Forms.TextBox adres_box;
    }
}