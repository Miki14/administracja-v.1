﻿namespace Administracja.ZakupyOkna
{
    partial class ZakupyDostawca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZakupyDostawca));
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lista = new System.Windows.Forms.ListView();
            this.nazwa_k = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.nip_k = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dodaj_dost = new System.Windows.Forms.Button();
            this.usun_dost = new System.Windows.Forms.Button();
            this.edytuj_dost = new System.Windows.Forms.Button();
            this.nip_box = new System.Windows.Forms.TextBox();
            this.nazwa_box = new System.Windows.Forms.TextBox();
            this.zapisz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(425, 18);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 20);
            this.label14.TabIndex = 35;
            this.label14.Text = "NIP";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(117, 18);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 20);
            this.label13.TabIndex = 31;
            this.label13.Text = "Nazwa";
            // 
            // lista
            // 
            this.lista.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nazwa_k,
            this.nip_k});
            this.lista.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lista.FullRowSelect = true;
            this.lista.GridLines = true;
            this.lista.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lista.HideSelection = false;
            this.lista.Location = new System.Drawing.Point(17, 50);
            this.lista.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lista.MultiSelect = false;
            this.lista.Name = "lista";
            this.lista.Size = new System.Drawing.Size(765, 501);
            this.lista.TabIndex = 34;
            this.lista.UseCompatibleStateImageBehavior = false;
            this.lista.View = System.Windows.Forms.View.Details;
            this.lista.SelectedIndexChanged += new System.EventHandler(this.ListaSelectedIndexChanged);
            this.lista.DoubleClick += new System.EventHandler(this.ZapiszClick);
            // 
            // nazwa_k
            // 
            this.nazwa_k.Text = "Nazwa";
            this.nazwa_k.Width = 540;
            // 
            // nip_k
            // 
            this.nip_k.Text = "NIP";
            this.nip_k.Width = 221;
            // 
            // dodaj_dost
            // 
            this.dodaj_dost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dodaj_dost.Location = new System.Drawing.Point(207, 561);
            this.dodaj_dost.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dodaj_dost.Name = "dodaj_dost";
            this.dodaj_dost.Size = new System.Drawing.Size(186, 57);
            this.dodaj_dost.TabIndex = 33;
            this.dodaj_dost.Text = "Dodaj";
            this.dodaj_dost.UseVisualStyleBackColor = true;
            this.dodaj_dost.Click += new System.EventHandler(this.DodajDostClick);
            // 
            // usun_dost
            // 
            this.usun_dost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usun_dost.Location = new System.Drawing.Point(13, 561);
            this.usun_dost.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.usun_dost.Name = "usun_dost";
            this.usun_dost.Size = new System.Drawing.Size(184, 57);
            this.usun_dost.TabIndex = 32;
            this.usun_dost.Text = "Usuń";
            this.usun_dost.UseVisualStyleBackColor = true;
            this.usun_dost.Click += new System.EventHandler(this.UsunDostClick);
            // 
            // edytuj_dost
            // 
            this.edytuj_dost.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.edytuj_dost.Location = new System.Drawing.Point(402, 561);
            this.edytuj_dost.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.edytuj_dost.Name = "edytuj_dost";
            this.edytuj_dost.Size = new System.Drawing.Size(186, 57);
            this.edytuj_dost.TabIndex = 31;
            this.edytuj_dost.Text = "Edytuj";
            this.edytuj_dost.UseVisualStyleBackColor = true;
            this.edytuj_dost.Click += new System.EventHandler(this.EdytujDostClick);
            // 
            // nip_box
            // 
            this.nip_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nip_box.Location = new System.Drawing.Point(468, 14);
            this.nip_box.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nip_box.Name = "nip_box";
            this.nip_box.Size = new System.Drawing.Size(184, 26);
            this.nip_box.TabIndex = 27;
            this.nip_box.TextChanged += new System.EventHandler(this.NipBoxTextChanged);
            // 
            // nazwa_box
            // 
            this.nazwa_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwa_box.Location = new System.Drawing.Point(182, 14);
            this.nazwa_box.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nazwa_box.Name = "nazwa_box";
            this.nazwa_box.Size = new System.Drawing.Size(142, 26);
            this.nazwa_box.TabIndex = 22;
            this.nazwa_box.TextChanged += new System.EventHandler(this.NazwaBoxTextChanged);
            // 
            // zapisz
            // 
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(596, 561);
            this.zapisz.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(186, 57);
            this.zapisz.TabIndex = 36;
            this.zapisz.Text = "Wybierz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.ZapiszClick);
            // 
            // ZakupyDostawca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 632);
            this.Controls.Add(this.zapisz);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lista);
            this.Controls.Add(this.nazwa_box);
            this.Controls.Add(this.dodaj_dost);
            this.Controls.Add(this.nip_box);
            this.Controls.Add(this.usun_dost);
            this.Controls.Add(this.edytuj_dost);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ZakupyDostawca";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dostawcy";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ListView lista;
        private System.Windows.Forms.ColumnHeader nazwa_k;
        private System.Windows.Forms.ColumnHeader nip_k;
        private System.Windows.Forms.Button dodaj_dost;
        private System.Windows.Forms.Button usun_dost;
        private System.Windows.Forms.Button edytuj_dost;
        private System.Windows.Forms.TextBox nip_box;
        private System.Windows.Forms.TextBox nazwa_box;
        private System.Windows.Forms.Button zapisz;
    }
}