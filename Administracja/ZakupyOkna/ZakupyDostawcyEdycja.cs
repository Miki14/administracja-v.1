﻿using System;
using System.Windows.Forms;

namespace Administracja.ZakupyOkna
{
    public partial class ZakupyDostawcyEdycja : Form
    {
        private readonly bool _dodawanie;
        private readonly int _idD;

        public ZakupyDostawcyEdycja(bool dodawanie, int idD)
        {
            InitializeComponent();
            _dodawanie = dodawanie;
            _idD = idD;
            Wyswietl();
        }

        private void Wyswietl()
        {
            id_l.Text = "ID: " + _idD;

            var reader1D = Baza.Wczytaj1D("SELECT Nazwa, NAZWA_CD, ADRES, NIP, TELEFON FROM DOSTAWCY WHERE ID='" + _idD + "'");

            nazwa_box.Text = reader1D[0];
            nazwa_cd_box.Text = reader1D[1];
            adres_box.Text = reader1D[2];
            nip_box.Text = reader1D[3];
            telefon_box.Text = reader1D[4];
        }

        private void anuluj_Click(object sender, EventArgs e)
        {
            if (_dodawanie)
            {
                Baza.Zapisz("DELETE FROM DOSTAWCY WHERE ID='" + _idD + "'");
            }
            Close();
        }

        private void zapisz_Click(object sender, EventArgs e)
        {
            Baza.Zapisz("UPDATE DOSTAWCY SET "
                    + "Nazwa = '" + nazwa_box.Text.Trim()
                    + "', Nazwa_cd = '" + nazwa_cd_box.Text.Trim()
                    + "', Adres = '" + adres_box.Text
                    + "', NIP = '" + nip_box.Text
                    + "', Telefon = '" + telefon_box.Text
                    + "' where ID = " + _idD + ";");
            Close();
        }

        private void nazwa_box_TextChanged(object sender, EventArgs e)
        {
            OperacjeTekstowe.SlowaToUpper(nazwa_box);
        }

        private void nazwa_cd_box_TextChanged(object sender, EventArgs e)
        {
            OperacjeTekstowe.SlowaToUpper(nazwa_cd_box);
        }

        private void adres_box_TextChanged(object sender, EventArgs e)
        {
            OperacjeTekstowe.PierwszaToUpper(adres_box);
        }
    }
}
