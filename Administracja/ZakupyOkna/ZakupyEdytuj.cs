﻿using System;
using System.Windows.Forms;

namespace Administracja.ZakupyOkna
{
    public partial class ZakupyEdytuj : Form
    {
        private readonly int _idZ;
        private double _sumaBrutto;
        private readonly bool _dodawanie;

        public ZakupyEdytuj(int idZ, bool dodawanie)
        {
            InitializeComponent();
            _idZ = idZ;
            _dodawanie = dodawanie;

            //Jeśli dodaje to zaznacz takie same daty, jak edycja to odznacz
            if (dodawanie)
            {
                jednakoweDatyCh.Checked = true;
                JednakoweDatyChCheckedChanged(this, EventArgs.Empty);
            }
            else
            {
                jednakoweDatyCh.Checked = false;
            }

            Wyswietl();
        }

        private void Wyswietl()
        {
            var reader1D = Baza.Wczytaj1D("SELECT * FROM ZAKUPY JOIN DOSTAWCY on ZAKUPY.ID_D = Dostawcy.ID where ZAKUPY.ID = '" + _idZ + "'");

            id_l.Text = "ID: " + reader1D[0];
            nr_fv.Text = reader1D[2];
            nazwa_l.Text = "Nazwa : " + reader1D[13];
            nazwa_cd_l.Text = "Nazwa cd. : " + reader1D[14];
            adres_l.Text = "Adres : " + reader1D[15];
            nip_l.Text = "NIP : " + reader1D[16];
            telefon_l.Text = "Telefon : " + reader1D[17];

            data_otrzymania.Value = DateTime.Parse(reader1D[5]);
            data_wystawienia.Value = DateTime.Parse(reader1D[6]);
            termin_zaplaty.Value = DateTime.Parse(reader1D[7]);
            data_wplaty.Value = DateTime.Parse(reader1D[9]);

            if (reader1D[11] != "0")
            {
                zaplat_ch.Checked = true;
                wplata_box.Text = Convert.ToDouble(reader1D[11].Replace(".", ",")).ToString("0.00");
                if (reader1D[10] == "P") radio_prze.Checked = true;
                else radio_got.Checked = true;
            }
            else
            {
                wplata_box.Text = Math.Round(Convert.ToDouble(reader1D[3].Replace(".", ",")) + Convert.ToDouble(reader1D[4].Replace(".", ",")), 2).ToString("0.00");
                radio_got.Checked = true;
            }
            opis_box.Text = reader1D[8];

            OdswiezSumy();
        }

        private void OdswiezSumy()
        {
            try
            {
                var reader1D = Baza.Wczytaj1D("SELECT Suma_netto, Suma_vat FROM ZAKUPY WHERE ZAKUPY.ID = '" + _idZ + "'");

                double netto = Convert.ToDouble(reader1D[0].Replace(".", ","));
                double vat = Convert.ToDouble(reader1D[1].Replace(".", ","));
                _sumaBrutto = netto + vat;

                netto_l.Text = "Netto : " + string.Format("{0:N2}", netto) + " zł";
                vat_l.Text = "Vat : " + string.Format("{0:N2}", vat) + " zł";
                brutto_l.Text = "Brutto : " + string.Format("{0:N2}", _sumaBrutto) + " zł";
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
        }

        private int ZapiszZakup()
        {
            OdswiezSumy();

            int idD = 0;

            try
            {
                idD = int.Parse(Baza.Wczytaj("SELECT ID_D FROM ZAKUPY where ID = '" + _idZ + "'"));
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }

            string sposobZaplaty = "G";
            if (radio_prze.Checked) sposobZaplaty = "P";

            string kwota = "0";
            if (zaplat_ch.Checked) kwota = wplata_box.Text.Replace(",", ".");

            Baza.Zapisz("UPDATE ZAKUPY SET "
                + "Numer = '" + nr_fv.Text
                + "', Data_otrzymania = '" + data_otrzymania.Value.ToString().Substring(0, 10)
                + "', Data_wystawienia = '" + data_wystawienia.Value.ToString().Substring(0, 10)
                + "', Termin_zaplaty = '" + termin_zaplaty.Value.ToString().Substring(0, 10)
                + "', Opis = '" + opis_box.Text
                + "', Data_wplaty = '" + data_wplaty.Value.ToString().Substring(0, 10)
                + "', Sposob_zaplaty = '" + sposobZaplaty
                + "', Wplacona_kwota = '" + kwota
                + "' where ID = " + _idZ + ";");

            return idD;
        }

        private void SkladnikiClick(object sender, EventArgs e)
        {
            ZapiszZakup();

            var z = new ZakupySkladniki(_idZ) { MdiParent = ParentWindow.ParentStatic };
            z.Show();
            z.FormClosed += Zamkniete;
        }

        private void ZapiszClick(object sender, EventArgs e)
        {
            bool fBlad = false;

            if (opis_box.Text.Length == 0)
            {
                Wyjatki.Komunikat("Proszę dodać opis zakupu.", "Brak opisu.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                fBlad = true;
            }
            if (ZapiszZakup() == 0 && !fBlad)
            {
                Wyjatki.Komunikat("Proszę wybrać dostawcę.", "Brak dostawcy.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                fBlad = true;
            }
            if (Math.Round(Convert.ToDouble(wplata_box.Text), 2) > Math.Round(_sumaBrutto, 2) && !fBlad)
            {
                Wyjatki.Komunikat("Podana kwota wpłaty jest większa niż wynika to ze składników!", "Błędna kwota wpłaty", MessageBoxButtons.OK, MessageBoxIcon.Error);
                fBlad = true;
            }
            if (!fBlad)
            {
                Close();
            }
        }

        private void AnulujClick(object sender, EventArgs e)
        {
            if (_dodawanie)
            {
                Baza.Zapisz("DELETE FROM ZAKUPY_SKLADNIKI WHERE ID_Z = '" + _idZ + "'");
                Baza.Zapisz("DELETE FROM ZAKUPY WHERE ID = '" + _idZ + "'");
                Baza.Ustaw("ZAKUPY");
            }
            Close();
        }

        private void ZaplatChCheckedChanged(object sender, EventArgs e)
        {
            data_wplaty.Enabled = !data_wplaty.Enabled;
            radio_got.Enabled = !radio_got.Enabled;
            radio_prze.Enabled = !radio_prze.Enabled;
            wplata_box.Enabled = !wplata_box.Enabled;
            wplata_box.Text = _sumaBrutto.ToString("0.00");

            if (!zaplat_ch.Checked) data_wplaty.Value = termin_zaplaty.Value;
        }

        private void NrFvClick(object sender, EventArgs e)
        {
            nr_fv.SelectAll();
        }

        private void ZmienDostawceClick(object sender, EventArgs e)
        {
            ZapiszZakup();

            new ZakupyDostawca(_idZ).ShowDialog();
            Wyswietl();
        }

        private void Zamkniete(object sender, FormClosedEventArgs e)
        {
            OdswiezSumy();
            Wyswietl();
        }

        private void TerminZaplatyValueChanged(object sender, EventArgs e)
        {
            if (!zaplat_ch.Checked) data_wplaty.Value = termin_zaplaty.Value;
        }

        private void KontolkiKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                ZapiszClick(this, EventArgs.Empty);
            }
        }

        private void JednakoweDatyChCheckedChanged(object sender, EventArgs e)
        {
            if (jednakoweDatyCh.Checked)
            {
                data_wystawienia.Enabled = false;
                termin_zaplaty.Enabled = false;

                data_wystawienia.Value = data_otrzymania.Value;
                termin_zaplaty.Value = data_otrzymania.Value;
            }
            else
            {
                data_wystawienia.Enabled = true;
                termin_zaplaty.Enabled = true;
            }
        }

        private void DataOtrzymaniaValueChanged(object sender, EventArgs e)
        {
            if (jednakoweDatyCh.Checked)
            {
                data_wystawienia.Value = data_otrzymania.Value;
                termin_zaplaty.Value = data_otrzymania.Value;
            }
        }
    }
}
