﻿namespace Administracja.ZakupyOkna
{
    partial class ZakupySkladnikiEdycja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.anuluj = new System.Windows.Forms.Button();
            this.zapisz = new System.Windows.Forms.Button();
            this.id_l = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grupy = new System.Windows.Forms.ComboBox();
            this.nazwa_l = new System.Windows.Forms.Label();
            this.nazwa_box = new System.Windows.Forms.TextBox();
            this.netto_l = new System.Windows.Forms.Label();
            this.vat_l = new System.Windows.Forms.Label();
            this.vat_box = new System.Windows.Forms.TextBox();
            this.netto_box = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // anuluj
            // 
            this.anuluj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anuluj.Location = new System.Drawing.Point(338, 67);
            this.anuluj.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.anuluj.Name = "anuluj";
            this.anuluj.Size = new System.Drawing.Size(100, 58);
            this.anuluj.TabIndex = 6;
            this.anuluj.Text = "Anuluj";
            this.anuluj.UseVisualStyleBackColor = true;
            this.anuluj.Click += new System.EventHandler(this.anuluj_Click);
            // 
            // zapisz
            // 
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(446, 67);
            this.zapisz.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(100, 58);
            this.zapisz.TabIndex = 1;
            this.zapisz.Text = "Zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.zapisz_Click);
            // 
            // id_l
            // 
            this.id_l.AutoSize = true;
            this.id_l.Location = new System.Drawing.Point(12, 9);
            this.id_l.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.id_l.Name = "id_l";
            this.id_l.Size = new System.Drawing.Size(30, 20);
            this.id_l.TabIndex = 0;
            this.id_l.Text = "ID:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.grupy);
            this.groupBox1.Location = new System.Drawing.Point(181, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(139, 60);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Grupa";
            // 
            // grupy
            // 
            this.grupy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.grupy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grupy.FormattingEnabled = true;
            this.grupy.Items.AddRange(new object[] {
            "1 - Wynagrodzenia",
            "2 - Remonty",
            "3 - Zakupy",
            "4 - ZUS",
            "5 - Podatki",
            "6 - Opłaty"});
            this.grupy.Location = new System.Drawing.Point(6, 25);
            this.grupy.Name = "grupy";
            this.grupy.Size = new System.Drawing.Size(121, 28);
            this.grupy.TabIndex = 5;
            // 
            // nazwa_l
            // 
            this.nazwa_l.AutoSize = true;
            this.nazwa_l.Location = new System.Drawing.Point(12, 36);
            this.nazwa_l.Name = "nazwa_l";
            this.nazwa_l.Size = new System.Drawing.Size(57, 20);
            this.nazwa_l.TabIndex = 0;
            this.nazwa_l.Text = "Nazwa";
            // 
            // nazwa_box
            // 
            this.nazwa_box.Location = new System.Drawing.Point(75, 33);
            this.nazwa_box.MaxLength = 60;
            this.nazwa_box.Name = "nazwa_box";
            this.nazwa_box.Size = new System.Drawing.Size(471, 26);
            this.nazwa_box.TabIndex = 2;
            this.nazwa_box.Click += new System.EventHandler(this.nazwa_box_Click);
            this.nazwa_box.TextChanged += new System.EventHandler(this.nazwa_box_TextChanged);
            // 
            // netto_l
            // 
            this.netto_l.AutoSize = true;
            this.netto_l.Location = new System.Drawing.Point(12, 70);
            this.netto_l.Name = "netto_l";
            this.netto_l.Size = new System.Drawing.Size(48, 20);
            this.netto_l.TabIndex = 0;
            this.netto_l.Text = "Netto";
            // 
            // vat_l
            // 
            this.vat_l.AutoSize = true;
            this.vat_l.Location = new System.Drawing.Point(12, 102);
            this.vat_l.Name = "vat_l";
            this.vat_l.Size = new System.Drawing.Size(40, 20);
            this.vat_l.TabIndex = 0;
            this.vat_l.Text = "VAT";
            // 
            // vat_box
            // 
            this.vat_box.Location = new System.Drawing.Point(75, 99);
            this.vat_box.Name = "vat_box";
            this.vat_box.Size = new System.Drawing.Size(71, 26);
            this.vat_box.TabIndex = 4;
            this.vat_box.Click += new System.EventHandler(this.vat_box_Click);
            // 
            // netto_box
            // 
            this.netto_box.Location = new System.Drawing.Point(75, 67);
            this.netto_box.Name = "netto_box";
            this.netto_box.Size = new System.Drawing.Size(100, 26);
            this.netto_box.TabIndex = 3;
            this.netto_box.Click += new System.EventHandler(this.netto_box_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(152, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "%";
            // 
            // ZakupySkladnikiEdycja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 133);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.netto_box);
            this.Controls.Add(this.vat_box);
            this.Controls.Add(this.vat_l);
            this.Controls.Add(this.netto_l);
            this.Controls.Add(this.nazwa_box);
            this.Controls.Add(this.nazwa_l);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.id_l);
            this.Controls.Add(this.anuluj);
            this.Controls.Add(this.zapisz);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ZakupySkladnikiEdycja";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edycja Składników";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button anuluj;
        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.Label id_l;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox grupy;
        private System.Windows.Forms.Label nazwa_l;
        private System.Windows.Forms.TextBox nazwa_box;
        private System.Windows.Forms.Label netto_l;
        private System.Windows.Forms.Label vat_l;
        private System.Windows.Forms.TextBox vat_box;
        private System.Windows.Forms.TextBox netto_box;
        private System.Windows.Forms.Label label1;
    }
}