﻿namespace Administracja.ZakupyOkna
{
    partial class ZakupyEdytuj
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZakupyEdytuj));
            this.nazwa_l = new System.Windows.Forms.Label();
            this.zapisz = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.data_otrzymania = new System.Windows.Forms.DateTimePicker();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.data_wystawienia = new System.Windows.Forms.DateTimePicker();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.wplata_box = new System.Windows.Forms.TextBox();
            this.radio_prze = new System.Windows.Forms.RadioButton();
            this.radio_got = new System.Windows.Forms.RadioButton();
            this.data_wplaty = new System.Windows.Forms.DateTimePicker();
            this.zaplat_ch = new System.Windows.Forms.CheckBox();
            this.nr_fv = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.termin_zaplaty = new System.Windows.Forms.DateTimePicker();
            this.netto_l = new System.Windows.Forms.Label();
            this.vat_l = new System.Windows.Forms.Label();
            this.brutto_l = new System.Windows.Forms.Label();
            this.anuluj = new System.Windows.Forms.Button();
            this.skladniki = new System.Windows.Forms.Button();
            this.nip_l = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.nazwa_cd_l = new System.Windows.Forms.Label();
            this.adres_l = new System.Windows.Forms.Label();
            this.telefon_l = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.opis_box = new System.Windows.Forms.TextBox();
            this.zmien_dostawce = new System.Windows.Forms.Button();
            this.id_l = new System.Windows.Forms.Label();
            this.jednakoweDatyCh = new System.Windows.Forms.CheckBox();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // nazwa_l
            // 
            this.nazwa_l.AutoSize = true;
            this.nazwa_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwa_l.Location = new System.Drawing.Point(6, 32);
            this.nazwa_l.Name = "nazwa_l";
            this.nazwa_l.Size = new System.Drawing.Size(69, 20);
            this.nazwa_l.TabIndex = 6;
            this.nazwa_l.Text = "Nazwa : ";
            // 
            // zapisz
            // 
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(1128, 578);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(200, 70);
            this.zapisz.TabIndex = 7;
            this.zapisz.Text = "Zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.ZapiszClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.data_otrzymania);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(12, 43);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(218, 61);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data otrzymania";
            // 
            // data_otrzymania
            // 
            this.data_otrzymania.CustomFormat = "dd MMMM yyyy";
            this.data_otrzymania.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.data_otrzymania.Location = new System.Drawing.Point(6, 25);
            this.data_otrzymania.Name = "data_otrzymania";
            this.data_otrzymania.Size = new System.Drawing.Size(200, 26);
            this.data_otrzymania.TabIndex = 26;
            this.data_otrzymania.ValueChanged += new System.EventHandler(this.DataOtrzymaniaValueChanged);
            this.data_otrzymania.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KontolkiKeyPress);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.data_wystawienia);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox4.Location = new System.Drawing.Point(12, 110);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(215, 63);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Data wystawienia";
            // 
            // data_wystawienia
            // 
            this.data_wystawienia.CustomFormat = "dd MMMM yyyy";
            this.data_wystawienia.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.data_wystawienia.Location = new System.Drawing.Point(6, 25);
            this.data_wystawienia.Name = "data_wystawienia";
            this.data_wystawienia.Size = new System.Drawing.Size(200, 26);
            this.data_wystawienia.TabIndex = 23;
            this.data_wystawienia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KontolkiKeyPress);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.wplata_box);
            this.groupBox5.Controls.Add(this.radio_prze);
            this.groupBox5.Controls.Add(this.radio_got);
            this.groupBox5.Controls.Add(this.data_wplaty);
            this.groupBox5.Controls.Add(this.zaplat_ch);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox5.Location = new System.Drawing.Point(695, 196);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(343, 120);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Sposób zapłaty";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(6, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 20);
            this.label7.TabIndex = 21;
            this.label7.Text = "Zapłacono";
            // 
            // wplata_box
            // 
            this.wplata_box.Enabled = false;
            this.wplata_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wplata_box.Location = new System.Drawing.Point(97, 82);
            this.wplata_box.Name = "wplata_box";
            this.wplata_box.Size = new System.Drawing.Size(236, 26);
            this.wplata_box.TabIndex = 4;
            this.wplata_box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KontolkiKeyPress);
            // 
            // radio_prze
            // 
            this.radio_prze.AutoSize = true;
            this.radio_prze.Checked = true;
            this.radio_prze.Enabled = false;
            this.radio_prze.Location = new System.Drawing.Point(103, 53);
            this.radio_prze.Name = "radio_prze";
            this.radio_prze.Size = new System.Drawing.Size(82, 24);
            this.radio_prze.TabIndex = 3;
            this.radio_prze.TabStop = true;
            this.radio_prze.Text = "Przelew";
            this.radio_prze.UseVisualStyleBackColor = true;
            this.radio_prze.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KontolkiKeyPress);
            // 
            // radio_got
            // 
            this.radio_got.AutoSize = true;
            this.radio_got.Enabled = false;
            this.radio_got.Location = new System.Drawing.Point(6, 53);
            this.radio_got.Name = "radio_got";
            this.radio_got.Size = new System.Drawing.Size(91, 24);
            this.radio_got.TabIndex = 2;
            this.radio_got.Text = "Gotówka";
            this.radio_got.UseVisualStyleBackColor = true;
            this.radio_got.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KontolkiKeyPress);
            // 
            // data_wplaty
            // 
            this.data_wplaty.CustomFormat = "dd MMMM yyyy";
            this.data_wplaty.Enabled = false;
            this.data_wplaty.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.data_wplaty.Location = new System.Drawing.Point(51, 21);
            this.data_wplaty.Name = "data_wplaty";
            this.data_wplaty.Size = new System.Drawing.Size(200, 26);
            this.data_wplaty.TabIndex = 1;
            this.data_wplaty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KontolkiKeyPress);
            // 
            // zaplat_ch
            // 
            this.zaplat_ch.AutoSize = true;
            this.zaplat_ch.Location = new System.Drawing.Point(16, 25);
            this.zaplat_ch.Name = "zaplat_ch";
            this.zaplat_ch.Size = new System.Drawing.Size(15, 14);
            this.zaplat_ch.TabIndex = 0;
            this.zaplat_ch.TabStop = false;
            this.zaplat_ch.UseVisualStyleBackColor = true;
            this.zaplat_ch.CheckedChanged += new System.EventHandler(this.ZaplatChCheckedChanged);
            this.zaplat_ch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KontolkiKeyPress);
            // 
            // nr_fv
            // 
            this.nr_fv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nr_fv.Location = new System.Drawing.Point(72, 249);
            this.nr_fv.MaxLength = 40;
            this.nr_fv.Name = "nr_fv";
            this.nr_fv.Size = new System.Drawing.Size(461, 26);
            this.nr_fv.TabIndex = 1;
            this.nr_fv.Click += new System.EventHandler(this.NrFvClick);
            this.nr_fv.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KontolkiKeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(12, 252);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Nr. FV";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.termin_zaplaty);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox6.Location = new System.Drawing.Point(12, 179);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(212, 63);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Termin zapłaty";
            // 
            // termin_zaplaty
            // 
            this.termin_zaplaty.CustomFormat = "dd MMMM yyyy";
            this.termin_zaplaty.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.termin_zaplaty.Location = new System.Drawing.Point(6, 25);
            this.termin_zaplaty.Name = "termin_zaplaty";
            this.termin_zaplaty.Size = new System.Drawing.Size(200, 26);
            this.termin_zaplaty.TabIndex = 24;
            this.termin_zaplaty.ValueChanged += new System.EventHandler(this.TerminZaplatyValueChanged);
            this.termin_zaplaty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KontolkiKeyPress);
            // 
            // netto_l
            // 
            this.netto_l.AutoSize = true;
            this.netto_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.netto_l.Location = new System.Drawing.Point(6, 22);
            this.netto_l.Name = "netto_l";
            this.netto_l.Size = new System.Drawing.Size(60, 20);
            this.netto_l.TabIndex = 13;
            this.netto_l.Text = "Netto : ";
            // 
            // vat_l
            // 
            this.vat_l.AutoSize = true;
            this.vat_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.vat_l.Location = new System.Drawing.Point(6, 47);
            this.vat_l.Name = "vat_l";
            this.vat_l.Size = new System.Drawing.Size(52, 20);
            this.vat_l.TabIndex = 15;
            this.vat_l.Text = "VAT : ";
            // 
            // brutto_l
            // 
            this.brutto_l.AutoSize = true;
            this.brutto_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.brutto_l.Location = new System.Drawing.Point(6, 72);
            this.brutto_l.Name = "brutto_l";
            this.brutto_l.Size = new System.Drawing.Size(65, 20);
            this.brutto_l.TabIndex = 17;
            this.brutto_l.Text = "Brutto : ";
            // 
            // anuluj
            // 
            this.anuluj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anuluj.Location = new System.Drawing.Point(922, 578);
            this.anuluj.Name = "anuluj";
            this.anuluj.Size = new System.Drawing.Size(200, 70);
            this.anuluj.TabIndex = 8;
            this.anuluj.Text = "Anuluj";
            this.anuluj.UseVisualStyleBackColor = true;
            this.anuluj.Click += new System.EventHandler(this.AnulujClick);
            // 
            // skladniki
            // 
            this.skladniki.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.skladniki.Location = new System.Drawing.Point(218, 578);
            this.skladniki.Name = "skladniki";
            this.skladniki.Size = new System.Drawing.Size(200, 70);
            this.skladniki.TabIndex = 9;
            this.skladniki.Text = "Skladniki";
            this.skladniki.UseVisualStyleBackColor = true;
            this.skladniki.Click += new System.EventHandler(this.SkladnikiClick);
            // 
            // nip_l
            // 
            this.nip_l.AutoSize = true;
            this.nip_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nip_l.Location = new System.Drawing.Point(6, 124);
            this.nip_l.Name = "nip_l";
            this.nip_l.Size = new System.Drawing.Size(47, 20);
            this.nip_l.TabIndex = 23;
            this.nip_l.Text = "NIP : ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.nazwa_cd_l);
            this.groupBox2.Controls.Add(this.adres_l);
            this.groupBox2.Controls.Add(this.telefon_l);
            this.groupBox2.Controls.Add(this.nazwa_l);
            this.groupBox2.Controls.Add(this.nip_l);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(12, 281);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(521, 190);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dostawca";
            // 
            // nazwa_cd_l
            // 
            this.nazwa_cd_l.AutoSize = true;
            this.nazwa_cd_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwa_cd_l.Location = new System.Drawing.Point(6, 62);
            this.nazwa_cd_l.Name = "nazwa_cd_l";
            this.nazwa_cd_l.Size = new System.Drawing.Size(94, 20);
            this.nazwa_cd_l.TabIndex = 28;
            this.nazwa_cd_l.Text = "Nazwa cd. : ";
            // 
            // adres_l
            // 
            this.adres_l.AutoSize = true;
            this.adres_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.adres_l.Location = new System.Drawing.Point(6, 92);
            this.adres_l.Name = "adres_l";
            this.adres_l.Size = new System.Drawing.Size(63, 20);
            this.adres_l.TabIndex = 27;
            this.adres_l.Text = "Adres : ";
            // 
            // telefon_l
            // 
            this.telefon_l.AutoSize = true;
            this.telefon_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.telefon_l.Location = new System.Drawing.Point(6, 156);
            this.telefon_l.Name = "telefon_l";
            this.telefon_l.Size = new System.Drawing.Size(74, 20);
            this.telefon_l.TabIndex = 25;
            this.telefon_l.Text = "Telefon : ";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.netto_l);
            this.groupBox7.Controls.Add(this.vat_l);
            this.groupBox7.Controls.Add(this.brutto_l);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox7.Location = new System.Drawing.Point(695, 37);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(223, 102);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Wartość FV";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(691, 406);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 20);
            this.label10.TabIndex = 29;
            this.label10.Text = "Opis";
            // 
            // opis_box
            // 
            this.opis_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.opis_box.Location = new System.Drawing.Point(738, 403);
            this.opis_box.MaxLength = 80;
            this.opis_box.Name = "opis_box";
            this.opis_box.Size = new System.Drawing.Size(397, 26);
            this.opis_box.TabIndex = 6;
            this.opis_box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KontolkiKeyPress);
            // 
            // zmien_dostawce
            // 
            this.zmien_dostawce.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zmien_dostawce.Location = new System.Drawing.Point(12, 578);
            this.zmien_dostawce.Name = "zmien_dostawce";
            this.zmien_dostawce.Size = new System.Drawing.Size(200, 70);
            this.zmien_dostawce.TabIndex = 10;
            this.zmien_dostawce.Text = "Zmień dostawcę";
            this.zmien_dostawce.UseVisualStyleBackColor = true;
            this.zmien_dostawce.Click += new System.EventHandler(this.ZmienDostawceClick);
            // 
            // id_l
            // 
            this.id_l.AutoSize = true;
            this.id_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.id_l.Location = new System.Drawing.Point(283, 73);
            this.id_l.Name = "id_l";
            this.id_l.Size = new System.Drawing.Size(30, 20);
            this.id_l.TabIndex = 30;
            this.id_l.Text = "ID:";
            // 
            // jednakoweDatyCh
            // 
            this.jednakoweDatyCh.AutoSize = true;
            this.jednakoweDatyCh.Checked = true;
            this.jednakoweDatyCh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.jednakoweDatyCh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.jednakoweDatyCh.Location = new System.Drawing.Point(12, 12);
            this.jednakoweDatyCh.Name = "jednakoweDatyCh";
            this.jednakoweDatyCh.Size = new System.Drawing.Size(213, 24);
            this.jednakoweDatyCh.TabIndex = 31;
            this.jednakoweDatyCh.Text = "Wszystkie daty jednakowe";
            this.jednakoweDatyCh.UseVisualStyleBackColor = true;
            this.jednakoweDatyCh.CheckedChanged += new System.EventHandler(this.JednakoweDatyChCheckedChanged);
            // 
            // ZakupyEdytuj
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1340, 660);
            this.ControlBox = false;
            this.Controls.Add(this.jednakoweDatyCh);
            this.Controls.Add(this.id_l);
            this.Controls.Add(this.zmien_dostawce);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.opis_box);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.skladniki);
            this.Controls.Add(this.anuluj);
            this.Controls.Add(this.nr_fv);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.zapisz);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ZakupyEdytuj";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edycja zakupu";
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nazwa_l;
        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox nr_fv;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label netto_l;
        private System.Windows.Forms.Label vat_l;
        private System.Windows.Forms.Label brutto_l;
        private System.Windows.Forms.Button anuluj;
        private System.Windows.Forms.Button skladniki;
        private System.Windows.Forms.DateTimePicker data_wplaty;
        private System.Windows.Forms.CheckBox zaplat_ch;
        private System.Windows.Forms.RadioButton radio_prze;
        private System.Windows.Forms.RadioButton radio_got;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox wplata_box;
        private System.Windows.Forms.Label nip_l;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label telefon_l;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label adres_l;
        private System.Windows.Forms.DateTimePicker data_otrzymania;
        private System.Windows.Forms.DateTimePicker data_wystawienia;
        private System.Windows.Forms.DateTimePicker termin_zaplaty;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox opis_box;
        private System.Windows.Forms.Label nazwa_cd_l;
        private System.Windows.Forms.Button zmien_dostawce;
        private System.Windows.Forms.Label id_l;
        private System.Windows.Forms.CheckBox jednakoweDatyCh;
    }
}