﻿namespace Administracja.ZakupyOkna
{
    partial class Zakupy
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lista = new System.Windows.Forms.ListView();
            this.dostawca_l = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.suma = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.data = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.oplacona = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dataZakupow = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataZakupowCh = new System.Windows.Forms.CheckBox();
            this.dodaj = new System.Windows.Forms.Button();
            this.edytuj = new System.Windows.Forms.Button();
            this.usun = new System.Windows.Forms.Button();
            this.kopiujButt = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lista);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox1.Location = new System.Drawing.Point(3, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1060, 406);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Lista zakupów";
            // 
            // lista
            // 
            this.lista.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dostawca_l,
            this.suma,
            this.data,
            this.oplacona});
            this.lista.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lista.FullRowSelect = true;
            this.lista.GridLines = true;
            this.lista.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lista.HideSelection = false;
            this.lista.Location = new System.Drawing.Point(6, 25);
            this.lista.MultiSelect = false;
            this.lista.Name = "lista";
            this.lista.Size = new System.Drawing.Size(1038, 364);
            this.lista.TabIndex = 1;
            this.lista.UseCompatibleStateImageBehavior = false;
            this.lista.View = System.Windows.Forms.View.Details;
            this.lista.SelectedIndexChanged += new System.EventHandler(this.ListaSelectedIndexChanged);
            this.lista.DoubleClick += new System.EventHandler(this.EdytujClick);
            // 
            // dostawca_l
            // 
            this.dostawca_l.Text = "Dostawca";
            this.dostawca_l.Width = 648;
            // 
            // suma
            // 
            this.suma.Text = "Suma brutto";
            this.suma.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.suma.Width = 120;
            // 
            // data
            // 
            this.data.Text = "Data zapłaty";
            this.data.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.data.Width = 136;
            // 
            // oplacona
            // 
            this.oplacona.Text = "Opłacono";
            this.oplacona.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.oplacona.Width = 114;
            // 
            // dataZakupow
            // 
            this.dataZakupow.CustomFormat = "MMMM yyyy";
            this.dataZakupow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataZakupow.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataZakupow.Location = new System.Drawing.Point(29, 25);
            this.dataZakupow.Name = "dataZakupow";
            this.dataZakupow.Size = new System.Drawing.Size(171, 26);
            this.dataZakupow.TabIndex = 6;
            this.dataZakupow.ValueChanged += new System.EventHandler(this.DataZakupówValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataZakupowCh);
            this.groupBox2.Controls.Add(this.dataZakupow);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(215, 61);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data zakupów";
            // 
            // dataZakupowCh
            // 
            this.dataZakupowCh.AutoSize = true;
            this.dataZakupowCh.Checked = true;
            this.dataZakupowCh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dataZakupowCh.Location = new System.Drawing.Point(8, 34);
            this.dataZakupowCh.Name = "dataZakupowCh";
            this.dataZakupowCh.Size = new System.Drawing.Size(15, 14);
            this.dataZakupowCh.TabIndex = 7;
            this.dataZakupowCh.UseVisualStyleBackColor = true;
            this.dataZakupowCh.CheckedChanged += new System.EventHandler(this.DataZakupowChCheckedChanged);
            // 
            // dodaj
            // 
            this.dodaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dodaj.Location = new System.Drawing.Point(866, 482);
            this.dodaj.Name = "dodaj";
            this.dodaj.Size = new System.Drawing.Size(200, 70);
            this.dodaj.TabIndex = 9;
            this.dodaj.Text = "Dodaj";
            this.dodaj.UseVisualStyleBackColor = true;
            this.dodaj.Click += new System.EventHandler(this.DodajClick);
            // 
            // edytuj
            // 
            this.edytuj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.edytuj.Location = new System.Drawing.Point(660, 482);
            this.edytuj.Name = "edytuj";
            this.edytuj.Size = new System.Drawing.Size(200, 70);
            this.edytuj.TabIndex = 10;
            this.edytuj.Text = "Edytuj";
            this.edytuj.UseVisualStyleBackColor = true;
            this.edytuj.Click += new System.EventHandler(this.EdytujClick);
            // 
            // usun
            // 
            this.usun.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usun.Location = new System.Drawing.Point(3, 482);
            this.usun.Name = "usun";
            this.usun.Size = new System.Drawing.Size(200, 70);
            this.usun.TabIndex = 11;
            this.usun.Text = "Usuń";
            this.usun.UseVisualStyleBackColor = true;
            this.usun.Click += new System.EventHandler(this.UsunClick);
            // 
            // kopiujButt
            // 
            this.kopiujButt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.kopiujButt.Location = new System.Drawing.Point(454, 482);
            this.kopiujButt.Name = "kopiujButt";
            this.kopiujButt.Size = new System.Drawing.Size(200, 70);
            this.kopiujButt.TabIndex = 12;
            this.kopiujButt.Text = "Kopiuj";
            this.kopiujButt.UseVisualStyleBackColor = true;
            this.kopiujButt.Click += new System.EventHandler(this.KopiujButtClick);
            // 
            // Zakupy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.kopiujButt);
            this.Controls.Add(this.usun);
            this.Controls.Add(this.edytuj);
            this.Controls.Add(this.dodaj);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Zakupy";
            this.Size = new System.Drawing.Size(1066, 555);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dataZakupow;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button dodaj;
        private System.Windows.Forms.Button edytuj;
        private System.Windows.Forms.Button usun;
        private System.Windows.Forms.ListView lista;
        private System.Windows.Forms.ColumnHeader dostawca_l;
        private System.Windows.Forms.ColumnHeader data;
        private System.Windows.Forms.ColumnHeader suma;
        private System.Windows.Forms.ColumnHeader oplacona;
        private System.Windows.Forms.CheckBox dataZakupowCh;
        private System.Windows.Forms.Button kopiujButt;
    }
}
