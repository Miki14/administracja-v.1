﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Administracja.ZakupyOkna
{
    public partial class Zakupy : UserControl
    {
        private int _idZ;
        private readonly List<int> _idZl = new List<int>();
        private int _index;

        public Zakupy()
        {
            InitializeComponent();
            Wyswietl();
        }

        private void Wyswietl()
        {
            int topItemIndex = 0;

            if (lista.Items.Count > 0)
            {
                topItemIndex = lista.TopItem.Index;
            }

            lista.Items.Clear();
            _idZl.Clear();

            string dataS = dataZakupowCh.Checked ? " WHERE Data_wplaty LIKE '" + dataZakupow.Value.ToString("yyyy-MM") + "-%'" : "";

            try
            {
                var reader2D = Baza.Wczytaj2D("SELECT ZAKUPY.ID, Nazwa, Nazwa_cd, Suma_Netto, Suma_Vat, Data_wplaty, Wplacona_kwota FROM ZAKUPY JOIN DOSTAWCY ON ZAKUPY.ID_D = Dostawcy.ID"
                    + dataS + " ORDER BY Data_wplaty, Nazwa, Nazwa_cd");

                foreach (var reader in reader2D)
                {
                    _idZl.Add(int.Parse(reader[0]));
                    ListViewItem element = lista.Items.Add(reader[1] + " " + reader[2]);
                    double brutto = Math.Round(Convert.ToDouble(reader[3].Replace(".", ",")) + Convert.ToDouble(reader[4].Replace(".", ",")), 2);
                    element.SubItems.Add(string.Format("{0:N2}", brutto));
                    element.SubItems.Add(OperacjeTekstowe.TrymujDate(reader[5]));
                    element.SubItems.Add(string.Format("{0:N2}", Convert.ToDouble(reader[6])));
                    if (Convert.ToDouble(reader[6]) < brutto) element.BackColor = Color.Orange;
                }
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }

            try
            {
                lista.Items[_index].Selected = true;
                _idZ = _idZl[_index];
                lista.TopItem = lista.Items[topItemIndex];
            }
            catch
            {
                _index = 0;

                try
                {
                    lista.Items[_index].Selected = true;
                    _idZ = _idZl[_index];
                    lista.TopItem = lista.Items[topItemIndex];
                }
                catch { }
            }
        }

        private void EdytujClick(object sender, EventArgs e)
        {
            if (lista.Items.Count > 0)
            {
                var z = new ZakupyEdytuj(_idZ, false) { MdiParent = ParentWindow.ParentStatic };
                z.FormClosed += Zamkniete;
                z.Show();
            }
            else
            {
                Wyjatki.Komunikat("Nie wybrano żadnego składnika", "Brak składnika!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DodajClick(object sender, EventArgs e)
        {
            string dataAk = dataZakupow.Value.Year + "-" + dataZakupow.Value.Month + "-" + DateTime.Now.Day;

            _idZ = Baza.Ustaw("ZAKUPY");
            Baza.Zapisz("INSERT INTO ZAKUPY VALUES(GEN_ID(NUMERACJA_ZAKUPY,1),'0','','0','0','" + dataAk + "','" + dataAk + "','" + dataAk + "','','" + dataAk + "','p','0')");

            var z = new ZakupyEdytuj(_idZ, true) { MdiParent = ParentWindow.ParentStatic };
            z.FormClosed += Zamkniete;
            z.Show();
        }

        private void UsunClick(object sender, EventArgs e)
        {
            if (lista.Items.Count > 0)
            {
                if (Wyjatki.Komunikat("Czy na pewno chcesz usunąć ID = " + _idZ + " ?", "Usunąć?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Baza.Zapisz("DELETE FROM ZAKUPY_SKLADNIKI WHERE ID_Z = '" + _idZ + "'");
                    Baza.Zapisz("DELETE FROM ZAKUPY WHERE ID = '" + _idZ + "'");
                }
                Wyswietl();
            }
            else
            {
                Wyjatki.Komunikat("Nie wybrano żadnego składnika", "Brak składnika!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void KopiujButtClick(object sender, EventArgs e)
        {
            int idOldZ = _idZ;

            Baza.Zapisz("Insert Into ZAKUPY (ID, ID_D, NUMER, SUMA_NETTO, SUMA_VAT, DATA_OTRZYMANIA,DATA_WYSTAWIENIA, TERMIN_ZAPLATY,OPIS, DATA_WPLATY,SPOSOB_ZAPLATY, WPLACONA_KWOTA) " +
                "SELECT GEN_ID(NUMERACJA_ZAKUPY,1), r.ID_D, r.NUMER, r.SUMA_NETTO, r.SUMA_VAT, r.DATA_OTRZYMANIA, r.DATA_WYSTAWIENIA, r.TERMIN_ZAPLATY, r.OPIS, r.DATA_WPLATY, " +
                "r.SPOSOB_ZAPLATY, 0 FROM ZAKUPY r Where ID = " + _idZ);

            int idNewZ = Baza.Ustaw("ZAKUPY") - 1;

            Baza.Zapisz("Insert Into ZAKUPY_SKLADNIKI (ID, ID_Z, ID_S, ILOSC, NETTO) " +
                "SELECT GEN_ID(NUMERACJA_ZAKUPY_SKLADNIKI,1), " + idNewZ + ", r.ID_S, r.ILOSC, r.NETTO FROM ZAKUPY_SKLADNIKI r Where ID_Z = " + idOldZ);

            _idZ = idNewZ;

            var z = new ZakupyEdytuj(_idZ, false) { MdiParent = ParentWindow.ParentStatic };
            z.FormClosed += Zamkniete;
            z.Show();
        }

        private void ListaSelectedIndexChanged(object sender, EventArgs e)
        {
            if (lista.Items.Count > 0)
            {
                try
                {
                    _index = lista.SelectedItems[0].Index;
                }
                catch
                {
                    _index = 0;
                }

                _idZ = _idZl[_index];
            }
        }

        private void DataZakupówValueChanged(object sender, EventArgs e)
        {
            Wyswietl();
        }

        private void Zamkniete(object sender, FormClosedEventArgs e)
        {
            Wyswietl();
        }

        private void DataZakupowChCheckedChanged(object sender, EventArgs e)
        {
            dataZakupow.Enabled = dataZakupowCh.Checked;

            Wyswietl();
        }
    }
}
