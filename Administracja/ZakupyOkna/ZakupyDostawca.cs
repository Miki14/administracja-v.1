﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Administracja.ZakupyOkna
{
    public partial class ZakupyDostawca : Form
    {
        private readonly int _idZ;
        private int _idD;
        private int _index;
        private readonly List<int> _idDl = new List<int>();

        public ZakupyDostawca(int idZ)
        {
            InitializeComponent();
            _idZ = idZ;
            Wyswietl();

            var reader = Baza.Wczytaj("SELECT DOSTAWCY.ID FROM DOSTAWCY JOIN ZAKUPY ON ZAKUPY.ID_D = DOSTAWCY.ID WHERE ZAKUPY.ID = '" + idZ + "'");

            try
            {
                _idD = int.Parse(reader);
                lista.Items[_idDl.IndexOf(_idD)].Selected = true;
            }
            catch
            {
                lista.Items[0].Selected = true;
            }
        }

        private void Wyswietl()
        {
            try
            {
                lista.Items.Clear();
                _idDl.Clear();

                var reader2D = Baza.Wczytaj2D("SELECT ID, Nazwa, Nazwa_cd, NIP FROM DOSTAWCY where (lower(nazwa) LIKE '%" + nazwa_box.Text.ToLower()
                    + "%' OR lower(Nazwa_cd) LIKE '%" + nazwa_box.Text.ToLower()
                    + "%') AND nip LIKE '%" + nip_box.Text
                    + "%'  AND DOSTAWCY.ID != 0 ORDER BY Nazwa, Nazwa_cd, NIP");

                foreach (List<string> reader1D in reader2D)
                {
                    _idDl.Add(int.Parse(reader1D[0]));
                    ListViewItem element = lista.Items.Add(reader1D[1] + " " + reader1D[2]);
                    element.SubItems.Add(reader1D[3]);
                }
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }

            if (lista.Items.Count > 0)
            {
                try
                {
                    lista.Items[_index].Selected = true;
                }
                catch
                {
                    _index = 0;
                    lista.Items[_index].Selected = true;
                }
            }
        }

        private void UsunDostClick(object sender, EventArgs e)
        {
            if (Wyjatki.Komunikat("Czy na pewno chcesz usunąć dostawcę " + '\"' + lista.Items[_index].Text + '\"' + " ?", "Usunąć?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Baza.Zapisz("DELETE FROM DOSTAWCY WHERE ID='" + _idD + "'");
                Baza.Ustaw("DOSTAWCY");
            }
            Wyswietl();
        }

        private void DodajDostClick(object sender, EventArgs e)
        {
            try
            {
                _idD = Baza.Ustaw("DOSTAWCY");
                Baza.Zapisz("INSERT INTO DOSTAWCY VALUES (GEN_ID(NUMERACJA_DOSTAWCY,1),'','','','','')");

                new ZakupyDostawcyEdycja(true, _idD).ShowDialog();

                Wyswietl();
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
        }

        private void EdytujDostClick(object sender, EventArgs e)
        {
            new ZakupyDostawcyEdycja(false, _idD).ShowDialog();
            Wyswietl();
        }

        private void ListaSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _index = lista.SelectedItems[0].Index;
            }
            catch
            {
                _index = 0;
            }

            _idD = _idDl[_index];
        }

        private void NazwaBoxTextChanged(object sender, EventArgs e)
        {
            Wyswietl();
        }

        private void NipBoxTextChanged(object sender, EventArgs e)
        {
            Wyswietl();
        }

        private void ZapiszClick(object sender, EventArgs e)
        {
            Baza.Zapisz("UPDATE ZAKUPY SET ID_D = '" + _idD + "' where ID = " + _idZ + ";");
            Close();
        }
    }
}
