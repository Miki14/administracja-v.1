﻿using System;
using System.Windows.Forms;

namespace Administracja.ZakupyOkna
{
    public partial class ZakupySkladnikiEdycja : Form
    {
        private readonly bool _dodawanie;
        private readonly int _idS;

        public ZakupySkladnikiEdycja(bool dodawanie, int idS)
        {
            InitializeComponent();
            _dodawanie = dodawanie;
            _idS = idS;
            grupy.SelectedIndex = 2;
            Wyswietl();
        }

        private void Wyswietl()
        {
            id_l.Text = "ID: " + _idS;

            var reader1D = Baza.Wczytaj1D("SELECT * FROM SKLADNIKI_Z WHERE ID = '" + _idS + "'");

            nazwa_box.Text = reader1D[1];
            netto_box.Text = reader1D[2];
            vat_box.Text = reader1D[3];
            grupy.SelectedIndex = int.Parse(reader1D[4]) - 1;
        }

        private void anuluj_Click(object sender, EventArgs e)
        {
            if (_dodawanie)
            {
                Baza.Zapisz("DELETE FROM SKLADNIKI_Z WHERE ID ='" + _idS + "'");
            }
            Close();
        }

        private void zapisz_Click(object sender, EventArgs e)
        {
            Baza.Zapisz("UPDATE SKLADNIKI_Z SET "
                    + "Nazwa = '" + nazwa_box.Text
                    + "', Netto = '" + netto_box.Text.Replace(",", ".")
                    + "', Vat = '" + vat_box.Text
                    + "', Grupa = '" + grupy.Text.Substring(0, 1)
                    + "' WHERE ID = " + _idS + ";");
            Close();
        }

        private void nazwa_box_Click(object sender, EventArgs e)
        {
            nazwa_box.SelectAll();
        }

        private void vat_box_Click(object sender, EventArgs e)
        {
            vat_box.SelectAll();
        }

        private void netto_box_Click(object sender, EventArgs e)
        {
            netto_box.SelectAll();
        }

        private void nazwa_box_TextChanged(object sender, EventArgs e)
        {
            OperacjeTekstowe.PierwszaToUpper(nazwa_box);
        }
    }
}
