﻿namespace Administracja
{
    partial class Glowne
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Glowne));
            this.Karty = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Panel();
            this.zamknij = new System.Windows.Forms.Button();
            this.Kartoteki = new System.Windows.Forms.Button();
            this.budynek = new System.Windows.Forms.ComboBox();
            this.Zakupy = new System.Windows.Forms.Button();
            this.Inne = new System.Windows.Forms.Button();
            this.wroc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Karty
            // 
            this.Karty.BackColor = System.Drawing.Color.Transparent;
            this.Karty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Karty.Location = new System.Drawing.Point(12, 46);
            this.Karty.Name = "Karty";
            this.Karty.Size = new System.Drawing.Size(244, 135);
            this.Karty.TabIndex = 2;
            this.Karty.Text = "Karty";
            this.Karty.UseVisualStyleBackColor = false;
            this.Karty.Click += new System.EventHandler(this.KartyClick);
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.Transparent;
            this.panel.Location = new System.Drawing.Point(262, 12);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(1066, 555);
            this.panel.TabIndex = 6;
            // 
            // zamknij
            // 
            this.zamknij.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zamknij.Location = new System.Drawing.Point(1128, 578);
            this.zamknij.Name = "zamknij";
            this.zamknij.Size = new System.Drawing.Size(200, 70);
            this.zamknij.TabIndex = 7;
            this.zamknij.Text = "Zamknij";
            this.zamknij.UseVisualStyleBackColor = true;
            this.zamknij.Click += new System.EventHandler(this.ZamknijClick);
            // 
            // Kartoteki
            // 
            this.Kartoteki.BackColor = System.Drawing.Color.Transparent;
            this.Kartoteki.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Kartoteki.Location = new System.Drawing.Point(12, 328);
            this.Kartoteki.Name = "Kartoteki";
            this.Kartoteki.Size = new System.Drawing.Size(244, 135);
            this.Kartoteki.TabIndex = 4;
            this.Kartoteki.Text = "Wydruki";
            this.Kartoteki.UseVisualStyleBackColor = false;
            this.Kartoteki.Click += new System.EventHandler(this.KartotekiClick);
            // 
            // budynek
            // 
            this.budynek.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.budynek.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.budynek.FormattingEnabled = true;
            this.budynek.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.budynek.Location = new System.Drawing.Point(12, 12);
            this.budynek.Name = "budynek";
            this.budynek.Size = new System.Drawing.Size(244, 28);
            this.budynek.TabIndex = 1;
            this.budynek.SelectedIndexChanged += new System.EventHandler(this.BuildingSelectedIndexChanged);
            this.budynek.MouseClick += new System.Windows.Forms.MouseEventHandler(this.BuildingMouseClick);
            // 
            // Zakupy
            // 
            this.Zakupy.BackColor = System.Drawing.Color.Transparent;
            this.Zakupy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Zakupy.Location = new System.Drawing.Point(12, 187);
            this.Zakupy.Name = "Zakupy";
            this.Zakupy.Size = new System.Drawing.Size(244, 135);
            this.Zakupy.TabIndex = 3;
            this.Zakupy.Text = "Zakupy";
            this.Zakupy.UseVisualStyleBackColor = false;
            this.Zakupy.Click += new System.EventHandler(this.ZakupyClick);
            // 
            // Inne
            // 
            this.Inne.BackColor = System.Drawing.Color.Transparent;
            this.Inne.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Inne.Location = new System.Drawing.Point(12, 469);
            this.Inne.Name = "Inne";
            this.Inne.Size = new System.Drawing.Size(244, 135);
            this.Inne.TabIndex = 5;
            this.Inne.Text = "Inne";
            this.Inne.UseVisualStyleBackColor = false;
            this.Inne.Click += new System.EventHandler(this.InneClick);
            // 
            // wroc
            // 
            this.wroc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wroc.Location = new System.Drawing.Point(922, 578);
            this.wroc.Name = "wroc";
            this.wroc.Size = new System.Drawing.Size(200, 70);
            this.wroc.TabIndex = 8;
            this.wroc.Text = "Wróć";
            this.wroc.UseVisualStyleBackColor = true;
            this.wroc.Click += new System.EventHandler(this.BackClick);
            // 
            // Glowne
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.DarkGreen;
            this.ClientSize = new System.Drawing.Size(1340, 660);
            this.Controls.Add(this.wroc);
            this.Controls.Add(this.Zakupy);
            this.Controls.Add(this.Inne);
            this.Controls.Add(this.Kartoteki);
            this.Controls.Add(this.budynek);
            this.Controls.Add(this.zamknij);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.Karty);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Glowne";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administracja";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Karty;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button zamknij;
        private System.Windows.Forms.Button Kartoteki;
        private System.Windows.Forms.ComboBox budynek;
        private System.Windows.Forms.Button Zakupy;
        private System.Windows.Forms.Button Inne;
        private System.Windows.Forms.Button wroc;

    }
}

