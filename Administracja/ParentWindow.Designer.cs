﻿namespace Administracja
{
    partial class ParentWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ParentWindow));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.główneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.osobyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zakupyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wydrukiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zestawieniaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inneObrotyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saldoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zaawansowaneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.daneSprzedawcyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.składnikiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ąDanychToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.podgladZawBazyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.odsetkiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pomocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sprawdźAktualizacjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oProgramieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.główneToolStripMenuItem,
            this.wydrukiToolStripMenuItem,
            this.zaawansowaneToolStripMenuItem,
            this.pomocToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1344, 29);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // główneToolStripMenuItem
            // 
            this.główneToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.osobyToolStripMenuItem,
            this.zakupyToolStripMenuItem});
            this.główneToolStripMenuItem.Name = "główneToolStripMenuItem";
            this.główneToolStripMenuItem.Size = new System.Drawing.Size(75, 25);
            this.główneToolStripMenuItem.Text = "Główne";
            // 
            // osobyToolStripMenuItem
            // 
            this.osobyToolStripMenuItem.Name = "osobyToolStripMenuItem";
            this.osobyToolStripMenuItem.Size = new System.Drawing.Size(131, 26);
            this.osobyToolStripMenuItem.Text = "Osoby";
            this.osobyToolStripMenuItem.Click += new System.EventHandler(this.OsobyMenuClick);
            // 
            // zakupyToolStripMenuItem
            // 
            this.zakupyToolStripMenuItem.Name = "zakupyToolStripMenuItem";
            this.zakupyToolStripMenuItem.Size = new System.Drawing.Size(131, 26);
            this.zakupyToolStripMenuItem.Text = "Zakupy";
            this.zakupyToolStripMenuItem.Click += new System.EventHandler(this.ZakupyMenuClick);
            // 
            // wydrukiToolStripMenuItem
            // 
            this.wydrukiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zestawieniaToolStripMenuItem,
            this.inneObrotyToolStripMenuItem,
            this.saldoToolStripMenuItem});
            this.wydrukiToolStripMenuItem.Name = "wydrukiToolStripMenuItem";
            this.wydrukiToolStripMenuItem.Size = new System.Drawing.Size(81, 25);
            this.wydrukiToolStripMenuItem.Text = "Wydruki";
            // 
            // zestawieniaToolStripMenuItem
            // 
            this.zestawieniaToolStripMenuItem.Name = "zestawieniaToolStripMenuItem";
            this.zestawieniaToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.zestawieniaToolStripMenuItem.Text = "Zestawienia";
            this.zestawieniaToolStripMenuItem.Click += new System.EventHandler(this.ZestawieniaMenuClick);
            // 
            // inneObrotyToolStripMenuItem
            // 
            this.inneObrotyToolStripMenuItem.Name = "inneObrotyToolStripMenuItem";
            this.inneObrotyToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.inneObrotyToolStripMenuItem.Text = "Inne obroty";
            this.inneObrotyToolStripMenuItem.Click += new System.EventHandler(this.InneObrotyMenuClick);
            // 
            // saldoToolStripMenuItem
            // 
            this.saldoToolStripMenuItem.Name = "saldoToolStripMenuItem";
            this.saldoToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.saldoToolStripMenuItem.Text = "Saldo";
            this.saldoToolStripMenuItem.Click += new System.EventHandler(this.SaldoMenuClick);
            // 
            // zaawansowaneToolStripMenuItem
            // 
            this.zaawansowaneToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.daneSprzedawcyToolStripMenuItem,
            this.składnikiToolStripMenuItem,
            this.ąDanychToolStripMenuItem,
            this.podgladZawBazyToolStripMenuItem,
            this.odsetkiToolStripMenuItem});
            this.zaawansowaneToolStripMenuItem.Name = "zaawansowaneToolStripMenuItem";
            this.zaawansowaneToolStripMenuItem.Size = new System.Drawing.Size(129, 25);
            this.zaawansowaneToolStripMenuItem.Text = "Zaawansowane";
            // 
            // daneSprzedawcyToolStripMenuItem
            // 
            this.daneSprzedawcyToolStripMenuItem.Name = "daneSprzedawcyToolStripMenuItem";
            this.daneSprzedawcyToolStripMenuItem.Size = new System.Drawing.Size(206, 26);
            this.daneSprzedawcyToolStripMenuItem.Text = "Dane sprzedawcy";
            this.daneSprzedawcyToolStripMenuItem.Click += new System.EventHandler(this.DaneSprzedawcyMenuClick);
            // 
            // składnikiToolStripMenuItem
            // 
            this.składnikiToolStripMenuItem.Name = "składnikiToolStripMenuItem";
            this.składnikiToolStripMenuItem.Size = new System.Drawing.Size(206, 26);
            this.składnikiToolStripMenuItem.Text = "Składniki faktur";
            this.składnikiToolStripMenuItem.Click += new System.EventHandler(this.SkładnikiMenuClick);
            // 
            // ąDanychToolStripMenuItem
            // 
            this.ąDanychToolStripMenuItem.Name = "ąDanychToolStripMenuItem";
            this.ąDanychToolStripMenuItem.Size = new System.Drawing.Size(206, 26);
            this.ąDanychToolStripMenuItem.Text = "Zarzadzaj latami";
            this.ąDanychToolStripMenuItem.Click += new System.EventHandler(this.ZarzadzajLatamiMenuClick);
            // 
            // podgladZawBazyToolStripMenuItem
            // 
            this.podgladZawBazyToolStripMenuItem.Name = "podgladZawBazyToolStripMenuItem";
            this.podgladZawBazyToolStripMenuItem.Size = new System.Drawing.Size(206, 26);
            this.podgladZawBazyToolStripMenuItem.Text = "Podglad zaw. bazy";
            this.podgladZawBazyToolStripMenuItem.Click += new System.EventHandler(this.PodgladZawBazyMenuClick);
            // 
            // odsetkiToolStripMenuItem
            // 
            this.odsetkiToolStripMenuItem.Name = "odsetkiToolStripMenuItem";
            this.odsetkiToolStripMenuItem.Size = new System.Drawing.Size(206, 26);
            this.odsetkiToolStripMenuItem.Text = "Odsetki";
            this.odsetkiToolStripMenuItem.Click += new System.EventHandler(this.OdsetkiMenuClick);
            // 
            // pomocToolStripMenuItem
            // 
            this.pomocToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sprawdźAktualizacjeToolStripMenuItem,
            this.oProgramieToolStripMenuItem});
            this.pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
            this.pomocToolStripMenuItem.Size = new System.Drawing.Size(69, 25);
            this.pomocToolStripMenuItem.Text = "Pomoc";
            // 
            // sprawdźAktualizacjeToolStripMenuItem
            // 
            this.sprawdźAktualizacjeToolStripMenuItem.Name = "sprawdźAktualizacjeToolStripMenuItem";
            this.sprawdźAktualizacjeToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.sprawdźAktualizacjeToolStripMenuItem.Text = "Sprawdź Aktualizacje";
            this.sprawdźAktualizacjeToolStripMenuItem.Click += new System.EventHandler(this.SprawdźAktualizacjeMenuClick);
            // 
            // oProgramieToolStripMenuItem
            // 
            this.oProgramieToolStripMenuItem.Name = "oProgramieToolStripMenuItem";
            this.oProgramieToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.oProgramieToolStripMenuItem.Text = "O programie";
            this.oProgramieToolStripMenuItem.Click += new System.EventHandler(this.OProgramieMenuClick);
            // 
            // ParentWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 693);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "ParentWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administracja v. ?.?";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ParentFormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem główneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem osobyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zakupyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wydrukiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zestawieniaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inneObrotyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saldoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zaawansowaneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem daneSprzedawcyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ąDanychToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem podgladZawBazyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pomocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sprawdźAktualizacjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem składnikiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem odsetkiToolStripMenuItem;
    }
}