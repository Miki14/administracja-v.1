﻿namespace Administracja.OsobyOkna
{
    partial class OsobySkladnikiEdycja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OsobySkladnikiEdycja));
            this.zapisz = new System.Windows.Forms.Button();
            this.anuluj = new System.Windows.Forms.Button();
            this.cena_brutto_l = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ID_lab = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.symbolBox = new System.Windows.Forms.TextBox();
            this.vatBox = new System.Windows.Forms.TextBox();
            this.cenaNettoBox = new System.Windows.Forms.TextBox();
            this.nazwaBox = new System.Windows.Forms.TextBox();
            this.VatZwCh = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // zapisz
            // 
            this.zapisz.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(639, 41);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(169, 70);
            this.zapisz.TabIndex = 5;
            this.zapisz.Text = "Zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.ZapiszClick);
            // 
            // anuluj
            // 
            this.anuluj.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.anuluj.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anuluj.Location = new System.Drawing.Point(465, 41);
            this.anuluj.Name = "anuluj";
            this.anuluj.Size = new System.Drawing.Size(168, 70);
            this.anuluj.TabIndex = 6;
            this.anuluj.Text = "Anuluj";
            this.anuluj.UseVisualStyleBackColor = true;
            this.anuluj.Click += new System.EventHandler(this.AnulujClick);
            // 
            // cena_brutto_l
            // 
            this.cena_brutto_l.AutoSize = true;
            this.cena_brutto_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cena_brutto_l.Location = new System.Drawing.Point(12, 82);
            this.cena_brutto_l.Name = "cena_brutto_l";
            this.cena_brutto_l.Size = new System.Drawing.Size(106, 20);
            this.cena_brutto_l.TabIndex = 43;
            this.cena_brutto_l.Text = "Cena Brutto";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(420, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 20);
            this.label5.TabIndex = 42;
            this.label5.Text = "%";
            // 
            // ID_lab
            // 
            this.ID_lab.AutoSize = true;
            this.ID_lab.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ID_lab.Location = new System.Drawing.Point(12, 9);
            this.ID_lab.Name = "ID_lab";
            this.ID_lab.Size = new System.Drawing.Size(38, 20);
            this.ID_lab.TabIndex = 41;
            this.ID_lab.Text = "ID :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(608, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 20);
            this.label4.TabIndex = 40;
            this.label4.Text = "PKWiU";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(284, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 20);
            this.label3.TabIndex = 39;
            this.label3.Text = "VAT";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.TabIndex = 38;
            this.label2.Text = "Cena Netto";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(106, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 20);
            this.label1.TabIndex = 37;
            this.label1.Text = "Nazwa";
            // 
            // symbolBox
            // 
            this.symbolBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.symbolBox.Location = new System.Drawing.Point(678, 6);
            this.symbolBox.MaxLength = 10;
            this.symbolBox.Name = "symbolBox";
            this.symbolBox.Size = new System.Drawing.Size(130, 26);
            this.symbolBox.TabIndex = 2;
            // 
            // vatBox
            // 
            this.vatBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.vatBox.Location = new System.Drawing.Point(333, 38);
            this.vatBox.MaxLength = 3;
            this.vatBox.Name = "vatBox";
            this.vatBox.Size = new System.Drawing.Size(81, 26);
            this.vatBox.TabIndex = 4;
            this.vatBox.Text = "0";
            this.vatBox.Click += new System.EventHandler(this.VatBoxClick);
            this.vatBox.TextChanged += new System.EventHandler(this.LiczBrutto);
            // 
            // cenaNettoBox
            // 
            this.cenaNettoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cenaNettoBox.Location = new System.Drawing.Point(118, 38);
            this.cenaNettoBox.MaxLength = 10;
            this.cenaNettoBox.Name = "cenaNettoBox";
            this.cenaNettoBox.Size = new System.Drawing.Size(143, 26);
            this.cenaNettoBox.TabIndex = 3;
            this.cenaNettoBox.Text = "0";
            this.cenaNettoBox.Click += new System.EventHandler(this.CenaNettoBoxClick);
            this.cenaNettoBox.TextChanged += new System.EventHandler(this.LiczBrutto);
            // 
            // nazwaBox
            // 
            this.nazwaBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwaBox.Location = new System.Drawing.Point(174, 6);
            this.nazwaBox.MaxLength = 40;
            this.nazwaBox.Name = "nazwaBox";
            this.nazwaBox.Size = new System.Drawing.Size(428, 26);
            this.nazwaBox.TabIndex = 1;
            this.nazwaBox.TextChanged += new System.EventHandler(this.NazwaBoxTextChanged);
            // 
            // VatZwCh
            // 
            this.VatZwCh.AutoSize = true;
            this.VatZwCh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.VatZwCh.Location = new System.Drawing.Point(333, 78);
            this.VatZwCh.Name = "VatZwCh";
            this.VatZwCh.Size = new System.Drawing.Size(88, 24);
            this.VatZwCh.TabIndex = 44;
            this.VatZwCh.Text = "VAT zw";
            this.VatZwCh.UseVisualStyleBackColor = true;
            this.VatZwCh.CheckedChanged += new System.EventHandler(this.VatZwChCheckedChanged);
            // 
            // OsobySkladnikiEdycja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(820, 122);
            this.ControlBox = false;
            this.Controls.Add(this.VatZwCh);
            this.Controls.Add(this.cena_brutto_l);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ID_lab);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.symbolBox);
            this.Controls.Add(this.vatBox);
            this.Controls.Add(this.cenaNettoBox);
            this.Controls.Add(this.nazwaBox);
            this.Controls.Add(this.anuluj);
            this.Controls.Add(this.zapisz);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OsobySkladnikiEdycja";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edycja";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.Button anuluj;
        private System.Windows.Forms.Label cena_brutto_l;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label ID_lab;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox symbolBox;
        private System.Windows.Forms.TextBox vatBox;
        private System.Windows.Forms.TextBox cenaNettoBox;
        private System.Windows.Forms.TextBox nazwaBox;
        private System.Windows.Forms.CheckBox VatZwCh;
    }
}