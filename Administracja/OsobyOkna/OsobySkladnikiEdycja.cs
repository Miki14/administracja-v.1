﻿using System;
using System.Windows.Forms;

namespace Administracja.OsobyOkna
{
    public partial class OsobySkladnikiEdycja : Form
    {
        private readonly int _id;
        private readonly bool _dodawanie;

        public OsobySkladnikiEdycja(int id, bool dodawanie)
        {
            InitializeComponent();
            _id = id;
            _dodawanie = dodawanie;

            Wyswietl();
        }

        private void Wyswietl()
        {
            var reader1D = Baza.Wczytaj1D("SELECT * FROM SKLADNIKI WHERE ID ='" + _id + "'");

            ID_lab.Text = "ID : " + reader1D[0];
            nazwaBox.Text = reader1D[1];
            symbolBox.Text = reader1D[2];

            cenaNettoBox.Text = Convert.ToDouble(reader1D[3].Replace(".", ",")).ToString("0.00");

            VatZwCh.Checked = string.IsNullOrWhiteSpace(reader1D[4]);
            vatBox.Text = VatZwCh.Checked ? "0" : reader1D[4];

            LiczBrutto(this, EventArgs.Empty);
        }

        private void LiczBrutto(object sender, EventArgs e)
        {
            cenaNettoBox.Text = cenaNettoBox.Text.Replace('.', ',');

            bool okNetto = double.TryParse(cenaNettoBox.Text, out double netto);

            bool okVat = int.TryParse(vatBox.Text, out int vat);

            double brutto = 0;

            if (okNetto && okVat)
            {
                brutto = netto + netto * vat / 100;
            }

            cena_brutto_l.Text = "Cena Brutto : " + string.Format("{0:N2}", brutto);
        }

        private void ZapiszClick(object sender, EventArgs e)
        {
            string vat = VatZwCh.Checked ? null : vatBox.Text.Replace(",", ".");
            Baza.Zapisz("UPDATE SKLADNIKI SET "
                    + "nazwa = '" + nazwaBox.Text
                    + "', symbol = '" + symbolBox.Text
                    + "', cena_netto = '" + cenaNettoBox.Text.Replace(",", ".")
                    + "', vat = '" + vat
                    + "' WHERE ID = " + _id + ";");
            Close();
        }

        private void AnulujClick(object sender, EventArgs e)
        {
            if (_dodawanie)
            {
                Baza.Zapisz("DELETE FROM SKLADNIKI WHERE ID = '" + _id + "'");
                Baza.Ustaw("SKLADNIKI");
            }
        }

        private void CenaNettoBoxClick(object sender, EventArgs e)
        {
            cenaNettoBox.SelectAll();

            try
            {
                vatBox.Text = Convert.ToInt32(vatBox.Text.Replace('.', ',')).ToString();
            }
            catch { }
        }

        private void VatBoxClick(object sender, EventArgs e)
        {
            vatBox.SelectAll();

            try
            {
                cenaNettoBox.Text = Convert.ToDouble(cenaNettoBox.Text.Replace('.', ',')).ToString("0.00");
            }
            catch { }
        }

        private void NazwaBoxTextChanged(object sender, EventArgs e)
        {
            int poz = nazwaBox.SelectionStart;
            try
            {
                nazwaBox.Text = nazwaBox.Text.Substring(0, 1).ToUpper() + nazwaBox.Text.Substring(1);
            }
            catch { }
            nazwaBox.SelectionStart = poz;
        }

        private void VatZwChCheckedChanged(object sender, EventArgs e)
        {
            vatBox.Enabled = !VatZwCh.Checked;
        }
    }
}
