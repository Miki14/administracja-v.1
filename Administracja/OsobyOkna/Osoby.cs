﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Administracja.OsobyOkna
{
    public partial class Osoby : UserControl
    {
        private int _id;
        private int _index;
        private readonly List<int> _idOsob = new List<int>();

        public Osoby()
        {
            InitializeComponent();
            Wyswietl();
        }

        private void Wyswietl()
        {
            int topItemIndex = 0;

            if (lista.Items.Count > 0)
            {
                topItemIndex = lista.TopItem.Index;
            }

            lista.Items.Clear();
            _idOsob.Clear();

            string wydrukowaneFv = "";
            if (niewydrukowaneFvCh.Checked)
            {
                wydrukowaneFv = " AND ID IN ( SELECT ID_N FROM FAKTURY WHERE WYDRUKOWANO != 'T')";
            }

            string ukryteOsoby = "";
            if (!ukryteOsobyCh.Checked)
            {
                ukryteOsoby = " AND GRUPA != '6' AND GRUPA != '100'";
            }

            var reader2D = Baza.Wczytaj2D("SELECT * FROM OSOBY WHERE NAZWISKO LIKE '" + szukaj.Text + "%'" + wydrukowaneFv + ukryteOsoby + " ORDER BY GRUPA, NAZWISKO, IMIE");

            foreach (var reader1D in reader2D)
            {
                int idO = int.Parse(reader1D[0]);
                _idOsob.Add(idO);
                //Nazwisko
                ListViewItem element = lista.Items.Add(reader1D[1]);
                //Imię
                element.SubItems.Add(reader1D[2]);
                //Misato
                element.SubItems.Add(reader1D[3]);
                //Ulica
                element.SubItems.Add(reader1D[4]);
                //Dom
                element.SubItems.Add(reader1D[5]);
                //Mieszkanie
                element.SubItems.Add(reader1D[6]);
                //Osób
                //element.SubItems.Add(reader1D[8]);
                //Grupa
                var grupa = Baza.GrupyOsob.FirstOrDefault(x => x.Key == Convert.ToInt32(reader1D[12])).Value;
                element.SubItems.Add(grupa);

                double bilans = Baza.LiczBilansOsob(idO);
                element.SubItems.Add(string.Format("{0:N2}", bilans));
                if (bilans < 0)
                {
                    element.BackColor = Color.Orange;
                }
                else if (bilans > 0)
                {
                    element.BackColor = Color.GreenYellow;
                }
            }

            for (int i = 0; i < _idOsob.Count; i++)
            {
                try
                {
                    DateTime data = DateTime.Parse(Baza.Wczytaj("SELECT MAX(DATA) FROM FAKTURY WHERE ID_N = '" + _idOsob[i] + "'"));
                    lista.Items[i].SubItems.Add(data.ToString("MM"));
                }
                catch { }
            }

            try
            {
                lista.Items[_index].Selected = true;
                lista.TopItem = lista.Items[topItemIndex];
                _id = _idOsob[_index];
            }
            catch { }
        }

        private void UsunClick(object sender, EventArgs e)
        {
            var reader1D = Baza.Wczytaj1D("SELECT Nazwisko, Imie FROM OSOBY WHERE ID = '" + _id + "'");

            string nazwa = reader1D[0] + " " + reader1D[1];

            if (Wyjatki.Komunikat("Czy na pewno chcesz usunąć " + nazwa + " ?", "Usunąć?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                int liczbaFaktur = int.Parse(Baza.Wczytaj("SELECT Count(*) FROM FAKTURY WHERE ID_N ='" + _id + "'"));

                if (liczbaFaktur > 1)
                {
                    Wyjatki.Komunikat(nazwa + " posiada faktury, najpierw musisz je usunąć!", "Usuń faktury!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    Baza.Zapisz("DELETE FROM FAKTURY_SKLADNIKI WHERE ID_F IN (SELECT ID FROM FAKTURY WHERE ID_N ='" + _id + "')",
                        "DELETE FROM WPLATY WHERE ID_F IN (SELECT ID FROM FAKTURY WHERE ID_N ='" + _id + "')",
                        "DELETE FROM FAKTURY WHERE ID_N = '" + _id + "'",
                        "DELETE FROM OSOBY WHERE ID = '" + _id + "'");
                }
            }
            Wyswietl();
        }

        private void DodajClick(object sender, EventArgs e)
        {
            DateTime data = DateTime.Today.AddMonths(-1);

            _id = Baza.Ustaw("Osoby");
            Baza.Ustaw("Faktury");

            Baza.Zapisz("INSERT INTO OSOBY VALUES (GEN_ID(NUMERACJA_OSOBY,1),'','','','','','','','','','','','1','','" +
                        DateTime.Now.ToString("yyyy.MM.dd") + "','')");
            Baza.Zapisz("INSERT INTO FAKTURY VALUES(GEN_ID(NUMERACJA_FAKTURY,1),'0','" + data.ToString("dd.MM.yyyy") + "',GEN_ID(NUMERACJA_OSOBY,0),'0','0','0','0','','T')");

            new OsobyEdycja(_id, true).ShowDialog();
            Wyswietl();
        }

        private void EdycjaClick(object sender, EventArgs e)
        {
            new OsobyEdycja(_id, false).ShowDialog();
            Wyswietl();
        }

        private void WplatyClick(object sender, EventArgs e)
        {
            var z = new OsobyWplaty(_id) { MdiParent = ParentWindow.ParentStatic };
            z.FormClosed += FormClosed;
            z.Show();
        }

        private void SzukajTextChanged(object sender, EventArgs e)
        {
            int poz = szukaj.SelectionStart;
            try
            {
                szukaj.Text = szukaj.Text.Substring(0, 1).ToUpper() + szukaj.Text.Substring(1);
            }
            catch { }
            szukaj.SelectionStart = poz;
            Wyswietl();
        }

        private void ListaSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _index = lista.SelectedItems[0].Index;
            }
            catch
            {
                _index = 0;
            }

            _id = _idOsob[_index];
        }

        private void FormClosed(object sender, FormClosedEventArgs e)
        {
            Wyswietl();
        }

        private void CheckboxCheckedChanged(object sender, EventArgs e)
        {
            Wyswietl();
        }
    }
}
