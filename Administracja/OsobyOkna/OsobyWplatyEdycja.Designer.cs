﻿namespace Administracja.OsobyOkna
{
    partial class OsobyWplatyEdycja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OsobyWplatyEdycja));
            this.zapisz = new System.Windows.Forms.Button();
            this.anuluj = new System.Windows.Forms.Button();
            this.id_l = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.numerDokBox = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.wyciagRad = new System.Windows.Forms.RadioButton();
            this.fvRad = new System.Windows.Forms.RadioButton();
            this.kpRad = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.wplataBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataWplaty = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.odsetkiBox = new System.Windows.Forms.TextBox();
            this.nr_fak_l = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.opisBox = new System.Windows.Forms.TextBox();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // zapisz
            // 
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(217, 342);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(200, 70);
            this.zapisz.TabIndex = 0;
            this.zapisz.Text = "Zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.ZapiszClick);
            // 
            // anuluj
            // 
            this.anuluj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anuluj.Location = new System.Drawing.Point(11, 342);
            this.anuluj.Name = "anuluj";
            this.anuluj.Size = new System.Drawing.Size(200, 70);
            this.anuluj.TabIndex = 7;
            this.anuluj.Text = "Anuluj";
            this.anuluj.UseVisualStyleBackColor = true;
            this.anuluj.Click += new System.EventHandler(this.AnulujClick);
            // 
            // id_l
            // 
            this.id_l.AutoSize = true;
            this.id_l.Location = new System.Drawing.Point(12, 19);
            this.id_l.Name = "id_l";
            this.id_l.Size = new System.Drawing.Size(30, 20);
            this.id_l.TabIndex = 33;
            this.id_l.Text = "ID:";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.numerDokBox);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox9.Location = new System.Drawing.Point(255, 207);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(149, 67);
            this.groupBox9.TabIndex = 5;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Numer dok.";
            // 
            // numerDokBox
            // 
            this.numerDokBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numerDokBox.Location = new System.Drawing.Point(6, 25);
            this.numerDokBox.MaxLength = 10;
            this.numerDokBox.Name = "numerDokBox";
            this.numerDokBox.Size = new System.Drawing.Size(135, 26);
            this.numerDokBox.TabIndex = 0;
            this.numerDokBox.Click += new System.EventHandler(this.NumerDokBoxClick);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.wyciagRad);
            this.groupBox8.Controls.Add(this.fvRad);
            this.groupBox8.Controls.Add(this.kpRad);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox8.Location = new System.Drawing.Point(255, 68);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(109, 117);
            this.groupBox8.TabIndex = 3;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Dokument";
            // 
            // wyciagRad
            // 
            this.wyciagRad.AutoSize = true;
            this.wyciagRad.Location = new System.Drawing.Point(6, 85);
            this.wyciagRad.Name = "wyciagRad";
            this.wyciagRad.Size = new System.Drawing.Size(78, 24);
            this.wyciagRad.TabIndex = 3;
            this.wyciagRad.Text = "Wyciąg";
            this.wyciagRad.UseVisualStyleBackColor = true;
            // 
            // fvRad
            // 
            this.fvRad.AutoSize = true;
            this.fvRad.Location = new System.Drawing.Point(6, 25);
            this.fvRad.Name = "fvRad";
            this.fvRad.Size = new System.Drawing.Size(48, 24);
            this.fvRad.TabIndex = 1;
            this.fvRad.Text = "FV";
            this.fvRad.UseVisualStyleBackColor = true;
            // 
            // kpRad
            // 
            this.kpRad.AutoSize = true;
            this.kpRad.Location = new System.Drawing.Point(6, 55);
            this.kpRad.Name = "kpRad";
            this.kpRad.Size = new System.Drawing.Size(47, 24);
            this.kpRad.TabIndex = 2;
            this.kpRad.Text = "KP";
            this.kpRad.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.wplataBox);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(12, 134);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(237, 67);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Kwota wpłaty";
            // 
            // wplataBox
            // 
            this.wplataBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wplataBox.Location = new System.Drawing.Point(6, 25);
            this.wplataBox.Name = "wplataBox";
            this.wplataBox.Size = new System.Drawing.Size(225, 26);
            this.wplataBox.TabIndex = 0;
            this.wplataBox.Text = "0";
            this.wplataBox.Click += new System.EventHandler(this.WplataBoxClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataWplaty);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(12, 61);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(237, 67);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data wpłaty";
            // 
            // dataWplaty
            // 
            this.dataWplaty.CustomFormat = "dd MMMM yyyy";
            this.dataWplaty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataWplaty.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataWplaty.Location = new System.Drawing.Point(6, 25);
            this.dataWplaty.Name = "dataWplaty";
            this.dataWplaty.Size = new System.Drawing.Size(200, 26);
            this.dataWplaty.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.odsetkiBox);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(12, 207);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(237, 67);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Kwota odsetek";
            // 
            // odsetkiBox
            // 
            this.odsetkiBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.odsetkiBox.Location = new System.Drawing.Point(6, 25);
            this.odsetkiBox.Name = "odsetkiBox";
            this.odsetkiBox.Size = new System.Drawing.Size(225, 26);
            this.odsetkiBox.TabIndex = 0;
            this.odsetkiBox.Text = "0";
            this.odsetkiBox.Click += new System.EventHandler(this.OdsetkiBoxClick);
            // 
            // nr_fak_l
            // 
            this.nr_fak_l.AutoSize = true;
            this.nr_fak_l.Location = new System.Drawing.Point(178, 19);
            this.nr_fak_l.Name = "nr_fak_l";
            this.nr_fak_l.Size = new System.Drawing.Size(85, 20);
            this.nr_fak_l.TabIndex = 39;
            this.nr_fak_l.Text = "Nr. faktury:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(8, 289);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 20);
            this.label1.TabIndex = 41;
            this.label1.Text = "Opis:";
            // 
            // opisBox
            // 
            this.opisBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.opisBox.Location = new System.Drawing.Point(64, 286);
            this.opisBox.MaxLength = 55;
            this.opisBox.Name = "opisBox";
            this.opisBox.Size = new System.Drawing.Size(340, 26);
            this.opisBox.TabIndex = 6;
            // 
            // OsobyWplatyEdycja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 424);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.opisBox);
            this.Controls.Add(this.nr_fak_l);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.id_l);
            this.Controls.Add(this.anuluj);
            this.Controls.Add(this.zapisz);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OsobyWplatyEdycja";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edycja wpłaty";
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.Button anuluj;
        private System.Windows.Forms.Label id_l;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox numerDokBox;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton wyciagRad;
        private System.Windows.Forms.RadioButton fvRad;
        private System.Windows.Forms.RadioButton kpRad;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox wplataBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dataWplaty;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox odsetkiBox;
        private System.Windows.Forms.Label nr_fak_l;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox opisBox;
    }
}