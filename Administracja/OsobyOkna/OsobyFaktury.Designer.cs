﻿namespace Administracja.OsobyOkna
{
    partial class OsobyFaktury
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OsobyFaktury));
            this.drukuj = new System.Windows.Forms.Button();
            this._podgladDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.print_dia = new System.Windows.Forms.PrintDialog();
            this.podglad = new System.Windows.Forms.Button();
            this.data_w = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.termin = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.metoda = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.list_all = new System.Windows.Forms.ListView();
            this.Nazwa_add = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PKWiU = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Cena_N = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.VAT = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.add = new System.Windows.Forms.Button();
            this.rem = new System.Windows.Forms.Button();
            this.list_sel = new System.Windows.Forms.ListView();
            this.nazwa2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.up = new System.Windows.Forms.Button();
            this.down = new System.Windows.Forms.Button();
            this.notatka = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.list_sel_c = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.zapisz = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.kopia_box = new System.Windows.Forms.CheckBox();
            this.oryginal_box = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.skladniki = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.duplikat_ch = new System.Windows.Forms.CheckBox();
            this.duplikat_c = new System.Windows.Forms.DateTimePicker();
            this.wydrukowanoCH = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // drukuj
            // 
            this.drukuj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.drukuj.Location = new System.Drawing.Point(672, 349);
            this.drukuj.Name = "drukuj";
            this.drukuj.Size = new System.Drawing.Size(160, 70);
            this.drukuj.TabIndex = 1;
            this.drukuj.Text = "Drukuj\r\n(Ctrl+P)";
            this.drukuj.UseVisualStyleBackColor = true;
            this.drukuj.Click += new System.EventHandler(this.DrukujClick);
            this.drukuj.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // _podgladDialog
            // 
            this._podgladDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this._podgladDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this._podgladDialog.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._podgladDialog.ClientSize = new System.Drawing.Size(400, 300);
            this._podgladDialog.Enabled = true;
            this._podgladDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("_podgladDialog.Icon")));
            this._podgladDialog.Name = "printPreviewDialog1";
            this._podgladDialog.Visible = false;
            // 
            // print_dia
            // 
            this.print_dia.UseEXDialog = true;
            // 
            // podglad
            // 
            this.podglad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.podglad.Location = new System.Drawing.Point(508, 349);
            this.podglad.Name = "podglad";
            this.podglad.Size = new System.Drawing.Size(160, 70);
            this.podglad.TabIndex = 0;
            this.podglad.Text = "Podgląd wydruku";
            this.podglad.UseVisualStyleBackColor = true;
            this.podglad.Click += new System.EventHandler(this.PodgladClick);
            this.podglad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // data_w
            // 
            this.data_w.CustomFormat = "dd MMMM yyyy";
            this.data_w.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.data_w.Location = new System.Drawing.Point(6, 19);
            this.data_w.Name = "data_w";
            this.data_w.Size = new System.Drawing.Size(187, 24);
            this.data_w.TabIndex = 4;
            this.data_w.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.data_w);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(398, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(199, 52);
            this.groupBox1.TabIndex = 100;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data faktury";
            // 
            // termin
            // 
            this.termin.FormattingEnabled = true;
            this.termin.Items.AddRange(new object[] {
            "...............",
            "7 dni",
            "14 dni",
            "30 dni"});
            this.termin.Location = new System.Drawing.Point(6, 19);
            this.termin.MaxLength = 15;
            this.termin.Name = "termin";
            this.termin.Size = new System.Drawing.Size(169, 26);
            this.termin.TabIndex = 2;
            this.termin.Text = "(data)";
            this.termin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.termin);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(183, 52);
            this.groupBox3.TabIndex = 100;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Termin płatności";
            // 
            // metoda
            // 
            this.metoda.FormattingEnabled = true;
            this.metoda.Items.AddRange(new object[] {
            "zapłacono gotówką",
            "gotówka/przelew",
            "przelew",
            "..........."});
            this.metoda.Location = new System.Drawing.Point(6, 19);
            this.metoda.MaxLength = 25;
            this.metoda.Name = "metoda";
            this.metoda.Size = new System.Drawing.Size(170, 26);
            this.metoda.TabIndex = 3;
            this.metoda.Text = "zapłacono gotówką";
            this.metoda.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.metoda);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox4.Location = new System.Drawing.Point(203, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(189, 52);
            this.groupBox4.TabIndex = 100;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Sposób zapłaty";
            // 
            // list_all
            // 
            this.list_all.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Nazwa_add,
            this.PKWiU,
            this.Cena_N,
            this.VAT});
            this.list_all.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.list_all.FullRowSelect = true;
            this.list_all.GridLines = true;
            this.list_all.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.list_all.HideSelection = false;
            this.list_all.Location = new System.Drawing.Point(6, 25);
            this.list_all.MultiSelect = false;
            this.list_all.Name = "list_all";
            this.list_all.Size = new System.Drawing.Size(426, 226);
            this.list_all.TabIndex = 6;
            this.list_all.UseCompatibleStateImageBehavior = false;
            this.list_all.View = System.Windows.Forms.View.Details;
            this.list_all.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // Nazwa_add
            // 
            this.Nazwa_add.Text = "Nazwa";
            this.Nazwa_add.Width = 206;
            // 
            // PKWiU
            // 
            this.PKWiU.Text = "Symbol";
            this.PKWiU.Width = 69;
            // 
            // Cena_N
            // 
            this.Cena_N.Text = "Cena Netto";
            this.Cena_N.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Cena_N.Width = 93;
            // 
            // VAT
            // 
            this.VAT.Text = "VAT";
            this.VAT.Width = 43;
            // 
            // add
            // 
            this.add.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.add.Location = new System.Drawing.Point(456, 149);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(40, 40);
            this.add.TabIndex = 7;
            this.add.Text = ">>";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.AddClick);
            this.add.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // rem
            // 
            this.rem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rem.Location = new System.Drawing.Point(456, 195);
            this.rem.Name = "rem";
            this.rem.Size = new System.Drawing.Size(40, 40);
            this.rem.TabIndex = 8;
            this.rem.Text = "<<";
            this.rem.UseVisualStyleBackColor = true;
            this.rem.Click += new System.EventHandler(this.RemClick);
            this.rem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // list_sel
            // 
            this.list_sel.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nazwa2});
            this.list_sel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.list_sel.FullRowSelect = true;
            this.list_sel.GridLines = true;
            this.list_sel.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.list_sel.HideSelection = false;
            this.list_sel.Location = new System.Drawing.Point(6, 19);
            this.list_sel.MultiSelect = false;
            this.list_sel.Name = "list_sel";
            this.list_sel.Size = new System.Drawing.Size(179, 248);
            this.list_sel.TabIndex = 9;
            this.list_sel.UseCompatibleStateImageBehavior = false;
            this.list_sel.View = System.Windows.Forms.View.Details;
            this.list_sel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // nazwa2
            // 
            this.nazwa2.Text = "Nazwa";
            this.nazwa2.Width = 174;
            // 
            // up
            // 
            this.up.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.up.Location = new System.Drawing.Point(191, 79);
            this.up.Name = "up";
            this.up.Size = new System.Drawing.Size(40, 40);
            this.up.TabIndex = 100;
            this.up.Text = "/\\";
            this.up.UseVisualStyleBackColor = true;
            this.up.Click += new System.EventHandler(this.UpClick);
            this.up.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // down
            // 
            this.down.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.down.Location = new System.Drawing.Point(191, 125);
            this.down.Name = "down";
            this.down.Size = new System.Drawing.Size(40, 40);
            this.down.TabIndex = 100;
            this.down.Text = "\\/";
            this.down.UseVisualStyleBackColor = true;
            this.down.Click += new System.EventHandler(this.DownClick);
            this.down.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // notatka
            // 
            this.notatka.Location = new System.Drawing.Point(6, 23);
            this.notatka.MaxLength = 60;
            this.notatka.Name = "notatka";
            this.notatka.Size = new System.Drawing.Size(306, 24);
            this.notatka.TabIndex = 16;
            this.notatka.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.notatka);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox5.Location = new System.Drawing.Point(18, 333);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(318, 55);
            this.groupBox5.TabIndex = 100;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Notka";
            // 
            // list_sel_c
            // 
            this.list_sel_c.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.list_sel_c.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.list_sel_c.FullRowSelect = true;
            this.list_sel_c.GridLines = true;
            this.list_sel_c.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.list_sel_c.LabelEdit = true;
            this.list_sel_c.Location = new System.Drawing.Point(237, 19);
            this.list_sel_c.MultiSelect = false;
            this.list_sel_c.Name = "list_sel_c";
            this.list_sel_c.Size = new System.Drawing.Size(250, 248);
            this.list_sel_c.TabIndex = 101;
            this.list_sel_c.UseCompatibleStateImageBehavior = false;
            this.list_sel_c.View = System.Windows.Forms.View.Details;
            this.list_sel_c.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.ListSelCAfterLabelEdit);
            this.list_sel_c.DoubleClick += new System.EventHandler(this.ListSelCDoubleClick);
            this.list_sel_c.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Cena Netto";
            this.columnHeader3.Width = 95;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "% VAT";
            this.columnHeader4.Width = 56;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Cena Brutto";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 95;
            // 
            // zapisz
            // 
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(838, 349);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(160, 70);
            this.zapisz.TabIndex = 102;
            this.zapisz.Text = "Zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.ZapiszClick);
            this.zapisz.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.kopia_box);
            this.groupBox2.Controls.Add(this.oryginal_box);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(603, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(167, 52);
            this.groupBox2.TabIndex = 103;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Orginał / Kopia";
            // 
            // kopia_box
            // 
            this.kopia_box.AutoSize = true;
            this.kopia_box.Checked = true;
            this.kopia_box.CheckState = System.Windows.Forms.CheckState.Checked;
            this.kopia_box.Location = new System.Drawing.Point(93, 23);
            this.kopia_box.Name = "kopia_box";
            this.kopia_box.Size = new System.Drawing.Size(65, 22);
            this.kopia_box.TabIndex = 1;
            this.kopia_box.Text = "Kopia";
            this.kopia_box.UseVisualStyleBackColor = true;
            this.kopia_box.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // oryginal_box
            // 
            this.oryginal_box.AutoSize = true;
            this.oryginal_box.Checked = true;
            this.oryginal_box.CheckState = System.Windows.Forms.CheckState.Checked;
            this.oryginal_box.Location = new System.Drawing.Point(6, 21);
            this.oryginal_box.Name = "oryginal_box";
            this.oryginal_box.Size = new System.Drawing.Size(81, 22);
            this.oryginal_box.TabIndex = 0;
            this.oryginal_box.Text = "Oryginał";
            this.oryginal_box.UseVisualStyleBackColor = true;
            this.oryginal_box.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.list_all);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox6.Location = new System.Drawing.Point(12, 70);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(438, 257);
            this.groupBox6.TabIndex = 104;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Baza składników";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.list_sel);
            this.groupBox7.Controls.Add(this.up);
            this.groupBox7.Controls.Add(this.down);
            this.groupBox7.Controls.Add(this.list_sel_c);
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox7.Location = new System.Drawing.Point(502, 70);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(496, 273);
            this.groupBox7.TabIndex = 105;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Wybarne składniki";
            // 
            // skladniki
            // 
            this.skladniki.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.skladniki.Location = new System.Drawing.Point(342, 349);
            this.skladniki.Name = "skladniki";
            this.skladniki.Size = new System.Drawing.Size(160, 70);
            this.skladniki.TabIndex = 106;
            this.skladniki.Text = "Składniki";
            this.skladniki.UseVisualStyleBackColor = true;
            this.skladniki.Click += new System.EventHandler(this.SkladnikiClick);
            this.skladniki.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.duplikat_ch);
            this.groupBox8.Controls.Add(this.duplikat_c);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox8.Location = new System.Drawing.Point(776, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(222, 52);
            this.groupBox8.TabIndex = 101;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Duplikat";
            // 
            // duplikat_ch
            // 
            this.duplikat_ch.AutoSize = true;
            this.duplikat_ch.Location = new System.Drawing.Point(8, 27);
            this.duplikat_ch.Name = "duplikat_ch";
            this.duplikat_ch.Size = new System.Drawing.Size(15, 14);
            this.duplikat_ch.TabIndex = 5;
            this.duplikat_ch.UseVisualStyleBackColor = true;
            this.duplikat_ch.CheckedChanged += new System.EventHandler(this.DuplikatChCheckedChanged);
            this.duplikat_ch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // duplikat_c
            // 
            this.duplikat_c.CustomFormat = "dd MMMM yyyy";
            this.duplikat_c.Enabled = false;
            this.duplikat_c.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.duplikat_c.Location = new System.Drawing.Point(27, 23);
            this.duplikat_c.Name = "duplikat_c";
            this.duplikat_c.Size = new System.Drawing.Size(186, 24);
            this.duplikat_c.TabIndex = 4;
            this.duplikat_c.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // wydrukowanoCH
            // 
            this.wydrukowanoCH.AutoSize = true;
            this.wydrukowanoCH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wydrukowanoCH.Location = new System.Drawing.Point(202, 394);
            this.wydrukowanoCH.Name = "wydrukowanoCH";
            this.wydrukowanoCH.Size = new System.Drawing.Size(128, 24);
            this.wydrukowanoCH.TabIndex = 108;
            this.wydrukowanoCH.Text = "Wydrukowano";
            this.wydrukowanoCH.UseVisualStyleBackColor = true;
            this.wydrukowanoCH.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            // 
            // OsobyFaktury
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 431);
            this.Controls.Add(this.wydrukowanoCH);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.rem);
            this.Controls.Add(this.skladniki);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.zapisz);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.add);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.podglad);
            this.Controls.Add(this.drukuj);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OsobyFaktury";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Faktury";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyFakturyKeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button drukuj;
        private System.Windows.Forms.PrintDialog print_dia;
        private System.Windows.Forms.Button podglad;
        private System.Windows.Forms.DateTimePicker data_w;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox termin;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox metoda;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListView list_all;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Button rem;
        private System.Windows.Forms.ColumnHeader Nazwa_add;
        private System.Windows.Forms.ColumnHeader Cena_N;
        private System.Windows.Forms.ColumnHeader VAT;
        private System.Windows.Forms.ColumnHeader PKWiU;
        private System.Windows.Forms.ListView list_sel;
        private System.Windows.Forms.ColumnHeader nazwa2;
        private System.Windows.Forms.Button up;
        private System.Windows.Forms.Button down;
        private System.Windows.Forms.TextBox notatka;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ListView list_sel_c;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox kopia_box;
        private System.Windows.Forms.CheckBox oryginal_box;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button skladniki;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.CheckBox duplikat_ch;
        private System.Windows.Forms.DateTimePicker duplikat_c;
        private System.Windows.Forms.PrintPreviewDialog _podgladDialog;
        private System.Windows.Forms.CheckBox wydrukowanoCH;
    }
}