﻿namespace Administracja.OsobyOkna
{
    partial class OsobyEdycja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OsobyEdycja));
            this.zapisz = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.nazwisko = new System.Windows.Forms.TextBox();
            this.imie = new System.Windows.Forms.TextBox();
            this.nip = new System.Windows.Forms.TextBox();
            this.kod = new System.Windows.Forms.TextBox();
            this.mieszkanie = new System.Windows.Forms.TextBox();
            this.dom = new System.Windows.Forms.TextBox();
            this.metrow = new System.Windows.Forms.TextBox();
            this.osob = new System.Windows.Forms.TextBox();
            this.ulica = new System.Windows.Forms.TextBox();
            this.miasto = new System.Windows.Forms.TextBox();
            this.dowod = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label_ID = new System.Windows.Forms.Label();
            this.anuluj = new System.Windows.Forms.Button();
            this.parking_ch = new System.Windows.Forms.CheckBox();
            this.notatka = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.umowa = new System.Windows.Forms.TextBox();
            this.grupaCombo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // zapisz
            // 
            this.zapisz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zapisz.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(719, 241);
            this.zapisz.MaximumSize = new System.Drawing.Size(200, 200);
            this.zapisz.MinimumSize = new System.Drawing.Size(0, 58);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(168, 58);
            this.zapisz.TabIndex = 15;
            this.zapisz.Text = "Zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.ZapiszClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nazwisko";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(12, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Dom";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(520, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ulica";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(12, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Miasto";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(12, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "Imię";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(226, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 20);
            this.label6.TabIndex = 7;
            this.label6.Text = "Mieszkanie";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(321, 146);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 20);
            this.label7.TabIndex = 8;
            this.label7.Text = "NIP";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(311, 175);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 20);
            this.label8.TabIndex = 10;
            this.label8.Text = "Dowód";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(12, 175);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 20);
            this.label9.TabIndex = 11;
            this.label9.Text = "Grupa";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(211, 146);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 20);
            this.label10.TabIndex = 12;
            this.label10.Text = "Parking";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(10, 146);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 20);
            this.label11.TabIndex = 13;
            this.label11.Text = "Metrów";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label123.Location = new System.Drawing.Point(627, 111);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(47, 20);
            this.label123.TabIndex = 14;
            this.label123.Text = "Osób";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(457, 111);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 20);
            this.label13.TabIndex = 15;
            this.label13.Text = "Kod";
            // 
            // nazwisko
            // 
            this.nazwisko.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nazwisko.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwisko.Location = new System.Drawing.Point(94, 12);
            this.nazwisko.MaxLength = 35;
            this.nazwisko.Name = "nazwisko";
            this.nazwisko.Size = new System.Drawing.Size(793, 26);
            this.nazwisko.TabIndex = 0;
            this.nazwisko.Click += new System.EventHandler(this.SelectAll);
            this.nazwisko.TextChanged += new System.EventHandler(this.FirstToUpper);
            this.nazwisko.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // imie
            // 
            this.imie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.imie.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.imie.Location = new System.Drawing.Point(94, 44);
            this.imie.MaxLength = 35;
            this.imie.Name = "imie";
            this.imie.Size = new System.Drawing.Size(793, 26);
            this.imie.TabIndex = 1;
            this.imie.Click += new System.EventHandler(this.SelectAll);
            this.imie.TextChanged += new System.EventHandler(this.FirstToUpper);
            this.imie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // nip
            // 
            this.nip.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nip.Location = new System.Drawing.Point(355, 143);
            this.nip.MaxLength = 15;
            this.nip.Name = "nip";
            this.nip.Size = new System.Drawing.Size(180, 26);
            this.nip.TabIndex = 10;
            this.nip.Click += new System.EventHandler(this.SelectAll);
            this.nip.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // kod
            // 
            this.kod.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.kod.Location = new System.Drawing.Point(500, 108);
            this.kod.MaxLength = 10;
            this.kod.Name = "kod";
            this.kod.Size = new System.Drawing.Size(121, 26);
            this.kod.TabIndex = 6;
            this.kod.Click += new System.EventHandler(this.SelectAll);
            this.kod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // mieszkanie
            // 
            this.mieszkanie.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.mieszkanie.Location = new System.Drawing.Point(320, 108);
            this.mieszkanie.MaxLength = 8;
            this.mieszkanie.Name = "mieszkanie";
            this.mieszkanie.Size = new System.Drawing.Size(131, 26);
            this.mieszkanie.TabIndex = 5;
            this.mieszkanie.Click += new System.EventHandler(this.SelectAll);
            this.mieszkanie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // dom
            // 
            this.dom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dom.Location = new System.Drawing.Point(94, 108);
            this.dom.MaxLength = 10;
            this.dom.Name = "dom";
            this.dom.Size = new System.Drawing.Size(126, 26);
            this.dom.TabIndex = 4;
            this.dom.Click += new System.EventHandler(this.SelectAll);
            this.dom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // metrow
            // 
            this.metrow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.metrow.Location = new System.Drawing.Point(94, 140);
            this.metrow.MaxLength = 10;
            this.metrow.Name = "metrow";
            this.metrow.Size = new System.Drawing.Size(72, 26);
            this.metrow.TabIndex = 8;
            this.metrow.Click += new System.EventHandler(this.SelectAll);
            this.metrow.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // osob
            // 
            this.osob.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.osob.Location = new System.Drawing.Point(680, 108);
            this.osob.MaxLength = 5;
            this.osob.Name = "osob";
            this.osob.Size = new System.Drawing.Size(126, 26);
            this.osob.TabIndex = 7;
            this.osob.Click += new System.EventHandler(this.SelectAll);
            this.osob.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // ulica
            // 
            this.ulica.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ulica.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ulica.Location = new System.Drawing.Point(570, 76);
            this.ulica.MaxLength = 25;
            this.ulica.Name = "ulica";
            this.ulica.Size = new System.Drawing.Size(317, 26);
            this.ulica.TabIndex = 3;
            this.ulica.Click += new System.EventHandler(this.SelectAll);
            this.ulica.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // miasto
            // 
            this.miasto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.miasto.Location = new System.Drawing.Point(94, 76);
            this.miasto.MaxLength = 20;
            this.miasto.Name = "miasto";
            this.miasto.Size = new System.Drawing.Size(420, 26);
            this.miasto.TabIndex = 2;
            this.miasto.Click += new System.EventHandler(this.SelectAll);
            this.miasto.TextChanged += new System.EventHandler(this.FirstToUpper);
            this.miasto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // dowod
            // 
            this.dowod.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dowod.Location = new System.Drawing.Point(376, 172);
            this.dowod.MaxLength = 20;
            this.dowod.Name = "dowod";
            this.dowod.Size = new System.Drawing.Size(159, 26);
            this.dowod.TabIndex = 13;
            this.dowod.Click += new System.EventHandler(this.SelectAll);
            this.dowod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(172, 146);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 20);
            this.label12.TabIndex = 29;
            this.label12.Text = "m2";
            // 
            // label_ID
            // 
            this.label_ID.AutoSize = true;
            this.label_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_ID.Location = new System.Drawing.Point(812, 111);
            this.label_ID.Name = "label_ID";
            this.label_ID.Size = new System.Drawing.Size(34, 20);
            this.label_ID.TabIndex = 30;
            this.label_ID.Text = "ID :";
            // 
            // anuluj
            // 
            this.anuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.anuluj.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.anuluj.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anuluj.Location = new System.Drawing.Point(545, 241);
            this.anuluj.MaximumSize = new System.Drawing.Size(200, 200);
            this.anuluj.MinimumSize = new System.Drawing.Size(0, 58);
            this.anuluj.Name = "anuluj";
            this.anuluj.Size = new System.Drawing.Size(168, 58);
            this.anuluj.TabIndex = 16;
            this.anuluj.Text = "Anuluj";
            this.anuluj.UseVisualStyleBackColor = true;
            this.anuluj.Click += new System.EventHandler(this.AnulujClick);
            // 
            // parking_ch
            // 
            this.parking_ch.AutoSize = true;
            this.parking_ch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.parking_ch.Location = new System.Drawing.Point(279, 150);
            this.parking_ch.Name = "parking_ch";
            this.parking_ch.Size = new System.Drawing.Size(15, 14);
            this.parking_ch.TabIndex = 10;
            this.parking_ch.UseVisualStyleBackColor = true;
            this.parking_ch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // notatka
            // 
            this.notatka.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.notatka.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.notatka.Location = new System.Drawing.Point(94, 206);
            this.notatka.MaxLength = 30;
            this.notatka.Name = "notatka";
            this.notatka.Size = new System.Drawing.Size(793, 26);
            this.notatka.TabIndex = 14;
            this.notatka.Click += new System.EventHandler(this.SelectAll);
            this.notatka.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(12, 209);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 20);
            this.label14.TabIndex = 35;
            this.label14.Text = "Notatka";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.Location = new System.Drawing.Point(556, 146);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 20);
            this.label15.TabIndex = 34;
            this.label15.Text = "Umowa do";
            // 
            // umowa
            // 
            this.umowa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.umowa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.umowa.Location = new System.Drawing.Point(647, 143);
            this.umowa.MaxLength = 15;
            this.umowa.Name = "umowa";
            this.umowa.Size = new System.Drawing.Size(240, 26);
            this.umowa.TabIndex = 11;
            this.umowa.Click += new System.EventHandler(this.SelectAll);
            // 
            // grupaCombo
            // 
            this.grupaCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.grupaCombo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.grupaCombo.FormattingEnabled = true;
            this.grupaCombo.Location = new System.Drawing.Point(94, 172);
            this.grupaCombo.Name = "grupaCombo";
            this.grupaCombo.Size = new System.Drawing.Size(211, 28);
            this.grupaCombo.TabIndex = 12;
            // 
            // OsobyEdycja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 311);
            this.Controls.Add(this.grupaCombo);
            this.Controls.Add(this.umowa);
            this.Controls.Add(this.notatka);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.parking_ch);
            this.Controls.Add(this.anuluj);
            this.Controls.Add(this.label_ID);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.dowod);
            this.Controls.Add(this.miasto);
            this.Controls.Add(this.ulica);
            this.Controls.Add(this.osob);
            this.Controls.Add(this.metrow);
            this.Controls.Add(this.dom);
            this.Controls.Add(this.mieszkanie);
            this.Controls.Add(this.kod);
            this.Controls.Add(this.nip);
            this.Controls.Add(this.imie);
            this.Controls.Add(this.nazwisko);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label123);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.zapisz);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(2000, 350);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(915, 350);
            this.Name = "OsobyEdycja";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edycja";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox nazwisko;
        private System.Windows.Forms.TextBox imie;
        private System.Windows.Forms.TextBox nip;
        private System.Windows.Forms.TextBox kod;
        private System.Windows.Forms.TextBox mieszkanie;
        private System.Windows.Forms.TextBox dom;
        private System.Windows.Forms.TextBox metrow;
        private System.Windows.Forms.TextBox osob;
        private System.Windows.Forms.TextBox ulica;
        private System.Windows.Forms.TextBox miasto;
        private System.Windows.Forms.TextBox dowod;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label_ID;
        private System.Windows.Forms.Button anuluj;
        private System.Windows.Forms.CheckBox parking_ch;
        private System.Windows.Forms.TextBox notatka;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox umowa;
        private System.Windows.Forms.ComboBox grupaCombo;
    }
}