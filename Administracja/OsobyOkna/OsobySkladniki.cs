﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Administracja.OsobyOkna
{
    public partial class OsobySkladniki : Form
    {
        private readonly List<int> _idS = new List<int>();
        private int _index;

        public OsobySkladniki()
        {
            InitializeComponent();
            Wyswietl();
            try
            {
                lista.Items[0].Selected = true;
            }
            catch { }
        }

        private void Wyswietl()
        {
            lista.Items.Clear();
            _idS.Clear();

            var reader2D = Baza.Wczytaj2D("SELECT * FROM SKLADNIKI");
            foreach (var list in reader2D)
            {
                _idS.Add(int.Parse(list[0]));
                ListViewItem element = lista.Items.Add(list[1]);
                element.SubItems.Add(list[2]);
                double netto = Convert.ToDouble(list[3].Replace('.', ','));
                element.SubItems.Add(netto.ToString("0.00"));
                int? vat = int.TryParse(list[4], out _) ? int.Parse(list[4]) : (int?)null;
                if (vat != null)
                {
                    element.SubItems.Add(vat.ToString());
                    element.SubItems.Add((netto * vat.Value / 100 + netto).ToString("0.00"));
                }
                else
                {
                    element.SubItems.Add("zw");
                    element.SubItems.Add(netto.ToString("0.00"));
                }
            }

            Baza.Ustaw("SKLADNIKI");

            try
            {
                lista.Items[_index].Selected = true;
            }
            catch { }
        }

        private void ListaSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _index = lista.SelectedItems[0].Index;
            }
            catch
            {
                _index = 0;
            }
        }

        private void DodajClick(object sender, EventArgs e)
        {
            Baza.Zapisz("INSERT INTO SKLADNIKI VALUES (GEN_ID(NUMERACJA_SKLADNIKI,1),'','','0','0');");

            int idD = Baza.Ustaw("SKLADNIKI") - 1;

            new OsobySkladnikiEdycja(idD, true).ShowDialog();
            Wyswietl();
        }

        private void UsunClick(object sender, EventArgs e)
        {
            if (Wyjatki.Komunikat("Czy na pewno chcesz usunąć składnik \"" + lista.SelectedItems[0].Text + "\" ?", "Usunąć?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Baza.Zapisz("DELETE FROM SKLADNIKI WHERE ID = '" + _idS[_index] + "'");
                Baza.Ustaw("SKLADNIKI");
                Wyswietl();
                ListaSelectedIndexChanged(this, EventArgs.Empty);
            }
        }

        private void EdytujClick(object sender, EventArgs e)
        {
            new OsobySkladnikiEdycja(_idS[_index], false).ShowDialog();
            Wyswietl();
        }

        private void ZamknijClick(object sender, EventArgs e)
        {
            Close();
        }
    }
}
