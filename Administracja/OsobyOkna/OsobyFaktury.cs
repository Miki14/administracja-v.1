﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Administracja.KlasyStatic;

namespace Administracja.OsobyOkna
{
    public partial class OsobyFaktury : Form
    {
        private readonly int _id;
        private readonly int _idF;
        private int _nrF;
        private int _orgKop;
        //private bool f_lista = false;
        private DateTime _dataOrg;

        public OsobyFaktury(int id, int idF)
        {
            InitializeComponent();
            _id = id;
            _idF = idF;
            Wyswietl();
        }

        private void Wyswietl()
        {
            int indexAll;
            int indexSel;

            try
            {
                indexAll = list_all.SelectedItems[0].Index;
                indexSel = list_sel.SelectedItems[0].Index;
            }
            catch
            {
                indexAll = 0;
                indexSel = 0;
            }

            list_all.Items.Clear();
            list_sel.Items.Clear();
            list_sel_c.Items.Clear();

            var reader2D = Baza.Wczytaj2D("SELECT skladniki.ID, Nazwa, Symbol, VAT, Cena FROM Skladniki JOIN faktury_skladniki ON skladniki.ID = faktury_skladniki.ID_S WHERE faktury_skladniki.ID_F = '"
                + _idF + "' ORDER BY FAKTURY_SKLADNIKI.ID");

            ListViewItem element;

            double cenaS;
            foreach (var reader1Dl in reader2D)
            {
                element = list_sel.Items.Add(reader1Dl[1]);
                element.SubItems.Add(reader1Dl[2]);
                cenaS = Math.Round(Convert.ToDouble(reader1Dl[4].Replace('.', ',')), 2);
                element = list_sel_c.Items.Add(cenaS.ToString("0.00"));
                element.SubItems.Add(!string.IsNullOrWhiteSpace(reader1Dl[3]) ? reader1Dl[3].Replace('.', ',') : "zw");
                element.SubItems.Add("0.00");
                element.SubItems.Add(reader1Dl[0]);
            }

            reader2D = Baza.Wczytaj2D("SELECT * FROM SKLADNIKI order by ID");
            foreach (var reader1Dl in reader2D)
            {
                element = list_all.Items.Add(reader1Dl[1]);
                element.SubItems.Add(reader1Dl[2]);
                cenaS = Math.Round(Convert.ToDouble(reader1Dl[3].Replace('.', ',')), 2);
                element.SubItems.Add(cenaS.ToString("0.00"));
                element.SubItems.Add(!string.IsNullOrWhiteSpace(reader1Dl[4]) ? reader1Dl[4].Replace('.', ',') : "zw");
                element.SubItems.Add(reader1Dl[0]);
            }

            var reader1D = Baza.Wczytaj1D("SELECT Metoda_platnosci, Termin_platnosci, Data, Numer, Notatka, WYDRUKOWANO FROM Faktury WHERE ID = '" + _idF + "';");
            metoda.Text = reader1D[0];
            termin.Text = reader1D[1];
            data_w.Value = DateTime.Parse(reader1D[2]);
            _dataOrg = data_w.Value;
            _nrF = int.Parse(reader1D[3]);
            notatka.Text = reader1D[4];
            wydrukowanoCH.Checked = reader1D[5] == "T";

            LiczBrutto();

            try
            {
                list_all.Items[indexAll].Selected = true;
                list_sel.Items[indexSel].Selected = true;
                list_sel_c.Items[indexSel].Selected = true;
            }
            catch { }

            //dodanie do listy dostępnch terminów płatności dzisiejszej daty
            string staraWartosc = termin.Text;
            termin.Items.Insert(0, DateTime.Now.ToString("dd.MM.yyyy"));
            termin.Text = staraWartosc;
        }

        private bool ZapiszFakture()
        {
            //Jeśli dane na FV wyglądają ok to je zapisujemy
            if (CanSaveInvoice())
            {
                double sumaB = 0;

                for (int i = 0; i < list_sel.Items.Count; i++)
                {
                    sumaB += Math.Round(Convert.ToDouble(list_sel_c.Items[i].SubItems[2].Text), 2);
                }

                string updateTmp = "UPDATE FAKTURY SET" +
                                    " Data = '" + data_w.Value.ToShortDateString() +
                                    "', Metoda_platnosci = '" + metoda.Text +
                                    "', Termin_platnosci = '" + termin.Text +
                                    "', Numer = '" + _nrF +
                                    "', suma_b = '" + sumaB.ToString(CultureInfo.InvariantCulture) +
                                    "', notatka = '" + notatka.Text +
                                    "', WYDRUKOWANO = '" + (wydrukowanoCH.Checked ? "T" : "N") +
                                    "' WHERE ID = '" + _idF + "';";


                Baza.Zapisz(updateTmp, "DELETE FROM FAKTURY_SKLADNIKI WHERE ID_F = '" + _idF + "'");
                Baza.Ustaw("FAKTURY_SKLADNIKI");

                var sqlSkladniki = new List<string>();

                for (int i = 0; i < list_sel.Items.Count; i++)
                {
                    sqlSkladniki.Add("INSERT INTO FAKTURY_SKLADNIKI VALUES (GEN_ID(NUMERACJA_FAKTURY_SKLADNIKI,1)" +
                                     ",'" + _idF +
                                     "','" + list_sel_c.Items[i].SubItems[3].Text +
                                     "','" + list_sel_c.Items[i].Text.Replace(",", ".") +
                                     "')");
                }

                Baza.Zapisz(sqlSkladniki.ToArray());
                return true;
            }

            //Jak !zapisujemy to zwracamy false, żeby nie drukować FV
            return false;
        }

        private bool CanSaveInvoice()
        {
            //Sprawdzenie, czy była zmieniana data FV
            if (data_w.Value.Year == _dataOrg.Year && data_w.Value.Month == _dataOrg.Month && data_w.Value.Day == _dataOrg.Day)
            {
                return true;
            }

            //Czy był zmieniany miesiac i to jest ostatnia FV w tamtym miesiacu?
            if (data_w.Value.Month != _dataOrg.Month && _nrF != Baza.GetNextFvNumber(_dataOrg, false) - 1)
            {
                MessageBox.Show("Zmieniono miesiąc faktury. Nie była to ostatnia faktura w miesiącu, więc nie jest możliwe jej przeniesienie.",
                    "Zmiana numeru faktury", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }


            if (data_w.Value.Year == _dataOrg.Year && data_w.Value.Month == _dataOrg.Month)
            {
                //Jeśli nie zmienił się miesiąc ani rok i wiemy, że była to ostatnia FV w miesiącu to
                //nowy dzień nie może być mniejszy niż dzień poprzedniej FV i większy niż dzień kolejnej, wiec można zapisywać
                if (_nrF == 1)
                {
                    return MessageBox.Show(
                        "Zmieniono datę faktury. Była to jedyna faktura w miesiącu i zmieniono jej tylko dzień miesiąca. Czy na pewno chcesz zapisać tą zmianę?",
                        "Zmiana dnia faktury", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
                }

                int dataPopFv = Convert.ToInt32(Baza.Wczytaj("SELECT DATA FROM FAKTURY WHERE NUMER = " + (_nrF - 1) + " AND DATA >= '"
                            + DateTimeHelper.GetDateFirstDayOfMonthString(_dataOrg) + "' AND DATA <= '" + DateTimeHelper.GetDateLastDayOfMonthString(_dataOrg) + "'").Substring(0, 2));

                var dataKolejnejFvDat = Baza.Wczytaj("SELECT DATA FROM FAKTURY WHERE NUMER = " + (_nrF + 1) + " AND DATA >= '"
                    + DateTimeHelper.GetDateFirstDayOfMonthString(_dataOrg) + "' AND DATA <= '" + DateTimeHelper.GetDateLastDayOfMonthString(_dataOrg) + "'");
                var dataKolejnejFv = dataKolejnejFvDat == "0" ? data_w.Value.Day : Convert.ToInt32(dataKolejnejFvDat.Substring(0, 2));

                if (data_w.Value.Day >= dataPopFv && data_w.Value.Day <= dataKolejnejFv)
                {
                    return MessageBox.Show("Zmieniono datę faktury. Nie zmieniono roku i miesiąca, a jedynie dzień. Nowa data jest większa od daty poprzedniej FV "
                                           + "i mniejsza niż data kolejnej, więc zmiana jest douszczalna. Czy na pewno chcesz zapisać tą zmianę?",
                        "Zmiana dnia faktury", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
                }

                MessageBox.Show("Zmieniono dzień faktury i jest on mniejszy niż dzień poprzedniej faktury lub większy niż dzień kolejnej. Nie można zapisać takiej zmiany.",
                    "Zmiana dnia faktury", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;

            }

            var result =
                MessageBox.Show("Zmieniono datę faktury. Była to ostatnia faktura w miesiącu, więc możliwe jest jej przeniesienie do nowego miesiąca i "
                                + "nadanie jej nowego numeru. Czy na pewno chcesz to zrobić?", "Zmiana numeru faktury", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result != DialogResult.Yes) return false;
            try
            {
                //Jeśli zgoda na przeniesienie ustal max numer dla nowego miesiąca, zmień i zapisz FV
                _nrF = Baza.GetNextFvNumber(data_w.Value, true);
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show("Nowy dzień faktury nr." + _nrF + " jest mniejszy niż dzień poprzedniej faktury w tym miesiącu. Faktura o wyższym numerze nie może mieć mniejszego dnia miesiąca.",
                    "Zmiana numeru faktury", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void LiczBrutto()
        {
            try
            {
                for (int i = 0; i < list_sel_c.Items.Count; i++)
                {
                    double netto = Math.Round(Convert.ToDouble(list_sel_c.Items[i].Text.Replace(".", ",")), 2);
                    int vat = list_sel_c.Items[i].SubItems[1].Text != "zw"
                        ? Convert.ToInt32(list_sel_c.Items[i].SubItems[1].Text.Replace(".", ","))
                        : 0;
                    list_sel_c.Items[i].Text = netto.ToString("0.00");
                    list_sel_c.Items[i].SubItems[2].Text = string.Format("{0:N2}", Math.Round(netto * vat / 100 + netto, 2));
                }
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
        }

        private bool Drukuj()
        {
            try
            {
                wydrukowanoCH.Checked = true;

                if (ZapiszFakture())
                {
                    var pd = new PrintDocument();
                    pd.PrintPage += PdPrintPage;
                    print_dia.Document = pd;
                    if (print_dia.ShowDialog() != DialogResult.OK) { return false; };

                    if (oryginal_box.Checked)
                    {
                        _orgKop = 0;
                        pd.Print();
                    }

                    if (kopia_box.Checked)
                    {
                        _orgKop = 1;
                        pd.Print();
                    }
                }
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
                return false;
            }

            return true;
        }

        #region handlery
        private void ZapiszClick(object sender, EventArgs e)
        {
            if (ZapiszFakture())
            {
                Close();
            }
        }

        private void PodgladClick(object sender, EventArgs e)
        {
            ZapiszFakture();

            var podgladDruku = new PrintDocument();
            podgladDruku.PrintPage += PdPrintPage;
            _podgladDialog.Document = podgladDruku;
            ((Form)_podgladDialog).WindowState = FormWindowState.Maximized;
            _podgladDialog.PrintPreviewControl.Zoom = 1.5;
            _podgladDialog.ShowDialog();
        }

        private void DrukujClick(object sender, EventArgs e)
        {
            if (Drukuj()) Close();
        }

        private void PdPrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            const int pozNabX = 480;
            const int pozNabY = 200;

            int lewyMargX = 75;
            const int pozSprzedY = 200;

            //przygotowane do przenoszenia składników na kolejne strony
            //bool fStop = false;

            //DANE NABYWCY
            var reader1D = Baza.Wczytaj1D("SELECT * FROM OSOBY WHERE ID = '" + _id + "'");

            g.DrawString("Nabywca:", Fonty.Normal, Brushes.Black, new PointF(pozNabX - 10, pozNabY - 5));
            g.DrawString(reader1D[1], Fonty.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 20));
            g.DrawString(reader1D[2], Fonty.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 40));

            string adres = reader1D[4] + " " + reader1D[5];
            if (!string.IsNullOrWhiteSpace(reader1D[6]))
            {
                g.DrawString(adres += " m." + reader1D[6], Fonty.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 60));
            }
            g.DrawString(adres, Fonty.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 60));

            g.DrawString(reader1D[7] + " " + reader1D[3], Fonty.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 80));
            if (!string.IsNullOrWhiteSpace(reader1D[11]))
            {
                g.DrawString("NIP: " + reader1D[11], Fonty.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 100));
            }

            //DANE SPRZEDAWCY
            reader1D = Baza.Wczytaj1D("SELECT * FROM DANE_SPRZEDAWCY");

            g.DrawString("Sprzedawca:", Fonty.Normal, Brushes.Black, new PointF(lewyMargX - 10, pozSprzedY - 5));
            g.DrawString(reader1D[1], Fonty.Dane, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 20));
            g.DrawString(reader1D[2], Fonty.Dane, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 40));
            g.DrawString("ul. " + reader1D[3], Fonty.Dane, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 60));
            g.DrawString(reader1D[4], Fonty.Dane, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 80));
            g.DrawString("NIP: " + reader1D[5], Fonty.Dane, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 100));
            g.DrawString("Regon: " + reader1D[6], Fonty.Dane, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 120));
            g.DrawString("Telefon: " + reader1D[7], Fonty.Normal, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 150));
            g.DrawString("Nr. konta: " + reader1D[8], Fonty.Normal, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 170));

            if (!string.IsNullOrWhiteSpace(reader1D[9]))
            {
                g.DrawImage(Image.FromFile(Raport.Folder + @"\Loga\" + reader1D[9]), 50, 140, 150, 50);
            }

            //NR. FAKTURY
            string numerFv = reader1D[10];
            if (!string.IsNullOrWhiteSpace(numerFv)) numerFv += " / ";
            else numerFv = "";

            g.DrawString("Faktura VAT nr: " + _nrF + " / " + numerFv + data_w.Value.Month.ToString("00") + " / " + data_w.Value.Year, Fonty.Header, Brushes.Black, new PointF(50, 100));

            //SKLADANIE DO KOPERTY
            g.DrawLine(Fonty.NormalPen, new PointF(lewyMargX - 20, 390), new PointF(lewyMargX - 15, 390));
            g.DrawLine(Fonty.NormalPen, new PointF(795, 390), new PointF(800, 390));
            g.DrawLine(Fonty.NormalPen, new PointF(lewyMargX - 20, 780), new PointF(lewyMargX - 15, 780));
            g.DrawLine(Fonty.NormalPen, new PointF(795, 780), new PointF(800, 780));

            //DUPLIKAT
            if (duplikat_ch.Checked) g.DrawString("Duplikat z dnia: " + duplikat_c.Value.ToString("dd.MM.yyyy"), new Font("Arial", 15, FontStyle.Bold), Brushes.Black, new PointF(lewyMargX, 50));

            //ORYGINAŁ / KOPIA
            g.DrawString(_orgKop == 0 ? "Oryginał" : "Kopia", Fonty.Header, Brushes.Black, new PointF(530, 50));

            //DATY
            string dataTmp = data_w.Value.ToString("dd / MM / yyyy", CultureInfo.InvariantCulture);
            g.DrawString("Data wystawienia:", Fonty.Normal, Brushes.Black, new PointF(500, 80));
            g.DrawString(dataTmp, Fonty.Normal, Brushes.Black, new PointF(650, 80));
            g.DrawString("Data wykonania usługi:", Fonty.Normal, Brushes.Black, new PointF(500, 100));
            g.DrawString(dataTmp, Fonty.Normal, Brushes.Black, new PointF(650, 100));

            //Sposób i termin
            g.DrawString("Sposób zapłaty: " + metoda.Text, Fonty.Normal, Brushes.Black, new PointF(pozNabX, 350));
            g.DrawString("Termin płatności: " + termin.Text, Fonty.Normal, Brushes.Black, new PointF(pozNabX, 370));

            int pozTowarY = 415;

            //TABELA TOWAROW
            g.DrawString("Lp.", Fonty.Goods, Brushes.Black, new PointF(lewyMargX - 10, pozTowarY));
            g.DrawString("Nazwa", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 20, pozTowarY));
            g.DrawString("PKWiU", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 260, pozTowarY));
            g.DrawString("Cena Netto", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 350, pozTowarY));
            g.DrawString("% VAT", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 450, pozTowarY));
            g.DrawString("Kwota VAT", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 510, pozTowarY));
            g.DrawString("Cena Brutto", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 600, pozTowarY));
            g.DrawLine(Fonty.NormalPen, lewyMargX, 435, 750, 435);

            pozTowarY += 5;

            double sumaNetto = 0;
            double sumaVat = 0;

            if (list_sel.Items.Count > 18)
            {
                Wyjatki.Komunikat("Za dużo składników. Nie mieszczą się na fakturze. Skontaktuj się z autorem oprogramowania.", "Za dużo składników.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                //LISTA TOWAROW
                for (int i = 0; i < list_sel.Items.Count; i++)
                {
                    //if (!fStop)
                    //{
                    pozTowarY += 20;
                    g.DrawString((i + 1).ToString(), Fonty.Goods, Brushes.Black, new PointF(lewyMargX - 10, pozTowarY));
                    //NAZWA
                    g.DrawString(list_sel.Items[i].SubItems[0].Text, Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 20, pozTowarY));
                    //PKWiU
                    g.DrawString(list_sel.Items[i].SubItems[1].Text, Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 260, pozTowarY));

                    double netto = Convert.ToDouble(list_sel_c.Items[i].SubItems[0].Text);
                    sumaNetto += netto;
                    g.DrawString(string.Format("{0:N2}", netto), Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 420, pozTowarY), Fonty.Left);

                    bool vatZw = list_sel_c.Items[i].SubItems[1].Text.Equals("zw");
                    double vat;
                    if (vatZw)
                    {
                        g.DrawString("zw", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 470, pozTowarY));
                        vat = 0;
                    }
                    else
                    {
                        g.DrawString(list_sel_c.Items[i].SubItems[1].Text, Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 470, pozTowarY));
                        vat = Math.Round(netto * (Convert.ToDouble(list_sel_c.Items[i].SubItems[1].Text) / 100), 2);
                    }

                    sumaVat += vat;
                    g.DrawString(string.Format("{0:N2}", vat), Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 580, pozTowarY), Fonty.Left);

                    g.DrawString(string.Format("{0:N2}", netto + vat), Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 670, pozTowarY), Fonty.Left);
                    //}
                    //else
                    //{
                    //if (i < 10) e.HasMorePages = true;
                    //}
                }
            }

            //PODSUMOWANIE TOWAROW
            g.DrawLine(Fonty.NormalPen, lewyMargX + 100, pozTowarY + 30, lewyMargX + 700, pozTowarY + 30);
            pozTowarY += 40;
            g.DrawString("Razem:", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 250, pozTowarY));
            g.DrawString(string.Format("{0:N2}", sumaNetto), Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 420, pozTowarY), Fonty.Left);
            g.DrawString(string.Format("{0:N2}", sumaVat), Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 580, pozTowarY), Fonty.Left);
            g.DrawString(string.Format("{0:N2}", sumaVat + sumaNetto), Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 670, pozTowarY), Fonty.Left);

            //WYPISYWANIE POSZCZEGOLNYCH STAWEK VAT
            pozTowarY += 30;
            g.DrawString("W tym:", Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 254, pozTowarY));
            var stawkiVat = new List<int?>();

            var reader1Dp = Baza.Wczytaj1Dp("SELECT DISTINCT(VAT) FROM SKLADNIKI WHERE ID IN (SELECT ID_S FROM FAKTURY_SKLADNIKI WHERE ID_F = '" + _idF + "') ORDER BY VAT DESC NULLS FIRST");
            foreach (string x in reader1Dp)
            {
                stawkiVat.Add(!string.IsNullOrWhiteSpace(x) ? int.Parse(x) : (int?)null);
            }

            foreach (int? vat in stawkiVat)
            {
                double sumaCen = 0;
                double sumaCenVat = 0;
                string where;
                if (vat != null)
                {
                    where = "= '" + vat + "'";
                }
                else
                {
                    where = "is null";
                }

                reader1Dp = Baza.Wczytaj1Dp("SELECT Faktury_Skladniki.CENA FROM Faktury_Skladniki JOIN skladniki on Faktury_Skladniki.ID_S = skladniki.ID WHERE Faktury_Skladniki.ID_F = '"
                                  + _idF + "' AND skladniki.VAT " + where);

                foreach (string sumaS in reader1Dp)
                {
                    double cena = Convert.ToDouble(sumaS.Replace(".", ","));
                    sumaCen += Math.Round(cena, 2);
                    sumaCenVat += Math.Round(cena * (vat ?? 0) / 100, 2);
                }

                if (sumaCen != 0)
                {
                    g.DrawString(string.Format("{0:N2}", sumaCen), Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 420, pozTowarY), Fonty.Left);
                    g.DrawString(vat == null ? "zw" : vat.ToString(), Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 470, pozTowarY));
                    g.DrawString(string.Format("{0:N2}", sumaCenVat), Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 580, pozTowarY), Fonty.Left);
                    g.DrawString(string.Format("{0:N2}", sumaCen + sumaCenVat), Fonty.Goods, Brushes.Black, new PointF(lewyMargX + 670, pozTowarY), Fonty.Left);
                    pozTowarY += 20;
                }
            }

            //SUMA DO ZAPLATY I KWOTA SLOWNIE
            g.DrawString("Do zapłaty: " + string.Format("{0:N2}", sumaNetto + sumaVat) + " zł", Fonty.Header, Brushes.Black, new PointF(lewyMargX, pozTowarY));
            g.DrawString("Słownie: " + OperacjeTekstowe.SlownieLiczba(sumaNetto + sumaVat), new Font("Arial", 10, FontStyle.Bold), Brushes.Black, new PointF(lewyMargX, pozTowarY + 30));
            g.DrawString(notatka.Text, Fonty.Normal, Brushes.Black, new PointF(lewyMargX, pozTowarY + 60));

            //MIEJSCA NA PODPISY
            pozTowarY += 150;
            lewyMargX += 10;

            g.DrawString("Michał Gołacki", Fonty.Normal, Brushes.Black, new PointF(lewyMargX + 50, pozTowarY - 10));
            g.DrawString(".................................................", Fonty.Normal, Brushes.Black, new PointF(lewyMargX, pozTowarY));
            g.DrawString("Osoba upoważniona do wystawienia faktury", Fonty.Small, Brushes.Black, new PointF(lewyMargX - 10, pozTowarY + 20));
            //g.DrawString("", Fonty.Mala, Brushes.Black, new PointF(lewyMargX + 25, pozTowarY + 40));

            g.DrawString("..........................................", Fonty.Normal, Brushes.Black, new PointF(580, pozTowarY));
            g.DrawString("Podpis osoby upoważnionej", Fonty.Small, Brushes.Black, new PointF(580 + 15, pozTowarY + 20));
            g.DrawString("do otrzymania faktury", Fonty.Small, Brushes.Black, new PointF(580 + 35, pozTowarY + 40));
        }

        private void AddClick(object sender, EventArgs e)
        {
            try
            {
                if (list_all.Items.Count > 0)
                {
                    list_sel.Items.Add((ListViewItem)list_all.SelectedItems[0].Clone());
                    ListViewItem element = list_sel_c.Items.Add(list_all.SelectedItems[0].SubItems[2].Text);
                    element.SubItems.Add(list_all.SelectedItems[0].SubItems[3].Text);
                    element.SubItems.Add("0,00");
                    element.SubItems.Add(list_all.SelectedItems[0].SubItems[4].Text);
                    LiczBrutto();
                }
            }
            catch
            {
                list_all.Items[0].Selected = true;
                list_sel.Items[0].Selected = true;
                list_sel_c.Items[0].Selected = true;

                AddClick(this, EventArgs.Empty);
            }
        }

        private void RemClick(object sender, EventArgs e)
        {
            try
            {
                if (list_sel.Items.Count > 0)
                {
                    int indexTmp = list_sel.SelectedItems[0].Index;

                    list_sel_c.Items.Remove(list_sel_c.Items[list_sel.SelectedItems[0].Index]);
                    list_sel.Items.Remove(list_sel.SelectedItems[0]);

                    if (indexTmp < list_sel.Items.Count)
                    {
                        list_sel.Items[indexTmp].Selected = true;
                        list_sel_c.Items[indexTmp].Selected = true;
                    }
                    else
                    {
                        list_sel.Items[list_sel.Items.Count - 1].Selected = true;
                        list_sel_c.Items[list_sel.Items.Count - 1].Selected = true;
                    }
                }
            }
            catch
            {
                list_all.Items[0].Selected = true;
                list_sel.Items[0].Selected = true;
                list_sel_c.Items[0].Selected = true;

                RemClick(this, EventArgs.Empty);
            }
        }

        private void UpClick(object sender, EventArgs e)
        {
            if (list_sel.SelectedItems.Count > 0)
            {
                int indexTmp = list_sel.SelectedItems[0].Index;

                try
                {
                    if (indexTmp > 0)
                    {
                        ListViewItem el = list_sel.Items[indexTmp];
                        list_sel.Items.Remove(el);
                        list_sel.Items.Insert(indexTmp - 1, el);

                        el = list_sel_c.Items[indexTmp];
                        list_sel_c.Items.Remove(el);
                        list_sel_c.Items.Insert(indexTmp - 1, el);
                    }
                }
                catch (Exception ex)
                {
                    Wyjatki.Error(ex);
                }
            }
        }

        private void DownClick(object sender, EventArgs e)
        {
            if (list_sel.SelectedItems.Count > 0)
            {
                int indexTmp = list_sel.SelectedItems[0].Index;

                try
                {
                    if (indexTmp < list_sel.Items.Count - 1)
                    {
                        ListViewItem el = list_sel.Items[indexTmp];
                        list_sel.Items.Remove(el);
                        list_sel.Items.Insert(indexTmp + 1, el);

                        el = list_sel_c.Items[indexTmp];
                        list_sel_c.Items.Remove(el);
                        list_sel_c.Items.Insert(indexTmp + 1, el);
                    }
                }
                catch (Exception ex)
                {
                    Wyjatki.Error(ex);
                }
            }
        }

        private void SkladnikiClick(object sender, EventArgs e)
        {
            ZapiszFakture();
            new OsobySkladniki().ShowDialog();
            Wyswietl();
        }

        private void DuplikatChCheckedChanged(object sender, EventArgs e)
        {
            duplikat_c.Enabled = duplikat_ch.Checked;
        }

        private void ListSelCAfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            list_sel_c.BeginInvoke(new MethodInvoker(LiczBrutto));
        }

        private void ListSelCDoubleClick(object sender, EventArgs e)
        {
            list_sel_c.SelectedItems[0].BeginEdit();
        }

        private void OsobyFakturyKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.P && e.Control)
            {
                Drukuj();
                e.Handled = true;
            }
        }
        #endregion handlery
    }
}
