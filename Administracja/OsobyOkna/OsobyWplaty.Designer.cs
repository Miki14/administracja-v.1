﻿namespace Administracja.OsobyOkna
{
    partial class OsobyWplaty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OsobyWplaty));
            this.id_lab = new System.Windows.Forms.Label();
            this.bilans = new System.Windows.Forms.Label();
            this.edytuj_fv = new System.Windows.Forms.Button();
            this.dodaj_fv = new System.Windows.Forms.Button();
            this.lista_faktur = new System.Windows.Forms.ListView();
            this.l_nr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.l_data = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.l_suma = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.l_winien = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dodaj_wp = new System.Windows.Forms.Button();
            this.wplata_box = new System.Windows.Forms.TextBox();
            this.nazwaLab = new System.Windows.Forms.Label();
            this.lista_wplat = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dataWplaty = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.nadplataCh = new System.Windows.Forms.CheckBox();
            this.odsetkiCh = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.przepisz = new System.Windows.Forms.Button();
            this.pozostalo_l = new System.Windows.Forms.Label();
            this.za_fv = new System.Windows.Forms.Label();
            this.odsetki_l = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.odsetkiProcNum = new System.Windows.Forms.TextBox();
            this.liczba_dni = new System.Windows.Forms.Label();
            this.usun_fv = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.wplataDodaj = new System.Windows.Forms.Button();
            this.usun_wp = new System.Windows.Forms.Button();
            this.edytuj_wp = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.wyciag_rad = new System.Windows.Forms.RadioButton();
            this.fvRad = new System.Windows.Forms.RadioButton();
            this.kpRad = new System.Windows.Forms.RadioButton();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.numerDokBox = new System.Windows.Forms.TextBox();
            this.zapisz = new System.Windows.Forms.Button();
            this.razem_l = new System.Windows.Forms.Label();
            this.zaproponuj = new System.Windows.Forms.Button();
            this.notatka = new System.Windows.Forms.Label();
            this.drukuj_konto = new System.Windows.Forms.Button();
            this.podglad_konta = new System.Windows.Forms.Button();
            this.kpDrukButt = new System.Windows.Forms.Button();
            this.opisBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.kpPodgladButt = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // id_lab
            // 
            this.id_lab.AutoSize = true;
            this.id_lab.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.id_lab.Location = new System.Drawing.Point(8, 12);
            this.id_lab.Name = "id_lab";
            this.id_lab.Size = new System.Drawing.Size(38, 20);
            this.id_lab.TabIndex = 1;
            this.id_lab.Text = "ID: ";
            // 
            // bilans
            // 
            this.bilans.AutoSize = true;
            this.bilans.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bilans.Location = new System.Drawing.Point(670, 12);
            this.bilans.Name = "bilans";
            this.bilans.Size = new System.Drawing.Size(68, 20);
            this.bilans.TabIndex = 2;
            this.bilans.Text = "Bilans :";
            // 
            // edytuj_fv
            // 
            this.edytuj_fv.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.edytuj_fv.Location = new System.Drawing.Point(262, 546);
            this.edytuj_fv.Name = "edytuj_fv";
            this.edytuj_fv.Size = new System.Drawing.Size(122, 50);
            this.edytuj_fv.TabIndex = 3;
            this.edytuj_fv.Text = "Edytuj Fakturę";
            this.edytuj_fv.UseVisualStyleBackColor = true;
            this.edytuj_fv.Click += new System.EventHandler(this.EdytujFvClick);
            this.edytuj_fv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // dodaj_fv
            // 
            this.dodaj_fv.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dodaj_fv.Location = new System.Drawing.Point(134, 546);
            this.dodaj_fv.Name = "dodaj_fv";
            this.dodaj_fv.Size = new System.Drawing.Size(122, 50);
            this.dodaj_fv.TabIndex = 4;
            this.dodaj_fv.Text = "Dodaj Fakturę";
            this.dodaj_fv.UseVisualStyleBackColor = true;
            this.dodaj_fv.Click += new System.EventHandler(this.DodajFvClick);
            this.dodaj_fv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // lista_faktur
            // 
            this.lista_faktur.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.l_nr,
            this.l_data,
            this.l_suma,
            this.l_winien});
            this.lista_faktur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lista_faktur.FullRowSelect = true;
            this.lista_faktur.GridLines = true;
            this.lista_faktur.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lista_faktur.HideSelection = false;
            this.lista_faktur.Location = new System.Drawing.Point(6, 23);
            this.lista_faktur.MultiSelect = false;
            this.lista_faktur.Name = "lista_faktur";
            this.lista_faktur.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lista_faktur.Size = new System.Drawing.Size(378, 517);
            this.lista_faktur.TabIndex = 5;
            this.lista_faktur.UseCompatibleStateImageBehavior = false;
            this.lista_faktur.View = System.Windows.Forms.View.Details;
            this.lista_faktur.SelectedIndexChanged += new System.EventHandler(this.ListaFakturSelectedIndexChanged);
            this.lista_faktur.DoubleClick += new System.EventHandler(this.EdytujFvClick);
            this.lista_faktur.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // l_nr
            // 
            this.l_nr.Text = "Nr";
            this.l_nr.Width = 39;
            // 
            // l_data
            // 
            this.l_data.Text = "Data";
            this.l_data.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l_data.Width = 92;
            // 
            // l_suma
            // 
            this.l_suma.Text = "Suma Brutto";
            this.l_suma.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l_suma.Width = 115;
            // 
            // l_winien
            // 
            this.l_winien.Text = "Winien";
            this.l_winien.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.l_winien.Width = 102;
            // 
            // dodaj_wp
            // 
            this.dodaj_wp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dodaj_wp.Location = new System.Drawing.Point(923, 348);
            this.dodaj_wp.Name = "dodaj_wp";
            this.dodaj_wp.Size = new System.Drawing.Size(98, 57);
            this.dodaj_wp.TabIndex = 10;
            this.dodaj_wp.Text = "Dodaj";
            this.dodaj_wp.UseVisualStyleBackColor = true;
            this.dodaj_wp.Click += new System.EventHandler(this.DodajWpClick);
            this.dodaj_wp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // wplata_box
            // 
            this.wplata_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wplata_box.Location = new System.Drawing.Point(58, 25);
            this.wplata_box.Name = "wplata_box";
            this.wplata_box.Size = new System.Drawing.Size(205, 26);
            this.wplata_box.TabIndex = 11;
            this.wplata_box.Text = "0";
            this.wplata_box.Click += new System.EventHandler(this.WplataBoxClick);
            this.wplata_box.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            this.wplata_box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // nazwaLab
            // 
            this.nazwaLab.AutoSize = true;
            this.nazwaLab.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwaLab.Location = new System.Drawing.Point(83, 12);
            this.nazwaLab.Name = "nazwaLab";
            this.nazwaLab.Size = new System.Drawing.Size(60, 20);
            this.nazwaLab.TabIndex = 13;
            this.nazwaLab.Text = "nazwa";
            // 
            // lista_wplat
            // 
            this.lista_wplat.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.lista_wplat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lista_wplat.FullRowSelect = true;
            this.lista_wplat.GridLines = true;
            this.lista_wplat.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lista_wplat.HideSelection = false;
            this.lista_wplat.Location = new System.Drawing.Point(6, 23);
            this.lista_wplat.MultiSelect = false;
            this.lista_wplat.Name = "lista_wplat";
            this.lista_wplat.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lista_wplat.Size = new System.Drawing.Size(496, 517);
            this.lista_wplat.TabIndex = 14;
            this.lista_wplat.UseCompatibleStateImageBehavior = false;
            this.lista_wplat.View = System.Windows.Forms.View.Details;
            this.lista_wplat.SelectedIndexChanged += new System.EventHandler(this.ListaWplatSelectedIndexChanged);
            this.lista_wplat.DoubleClick += new System.EventHandler(this.EdytujWpClick);
            this.lista_wplat.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Data";
            this.columnHeader1.Width = 62;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Kwota";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader3.Width = 88;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Nr. FV";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader4.Width = 57;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Odsetki";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 67;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Sposób";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader6.Width = 56;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Dok";
            this.columnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader7.Width = 54;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Numer";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader8.Width = 103;
            // 
            // dataWplaty
            // 
            this.dataWplaty.CustomFormat = "dd MMMM yyyy";
            this.dataWplaty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataWplaty.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataWplaty.Location = new System.Drawing.Point(6, 22);
            this.dataWplaty.Name = "dataWplaty";
            this.dataWplaty.Size = new System.Drawing.Size(239, 26);
            this.dataWplaty.TabIndex = 15;
            this.dataWplaty.ValueChanged += new System.EventHandler(this.DataWplatyValueChanged);
            this.dataWplaty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataWplaty);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(927, 46);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(251, 67);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data wpłaty";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.nadplataCh);
            this.groupBox2.Controls.Add(this.odsetkiCh);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(927, 125);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(104, 95);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rozliczanie";
            // 
            // nadplataCh
            // 
            this.nadplataCh.AutoSize = true;
            this.nadplataCh.Location = new System.Drawing.Point(9, 59);
            this.nadplataCh.Name = "nadplataCh";
            this.nadplataCh.Size = new System.Drawing.Size(93, 24);
            this.nadplataCh.TabIndex = 19;
            this.nadplataCh.Text = "Nadpłata";
            this.nadplataCh.UseVisualStyleBackColor = true;
            this.nadplataCh.CheckedChanged += new System.EventHandler(this.NadplataChCheckedChanged);
            this.nadplataCh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // odsetkiCh
            // 
            this.odsetkiCh.AutoSize = true;
            this.odsetkiCh.Location = new System.Drawing.Point(9, 29);
            this.odsetkiCh.Name = "odsetkiCh";
            this.odsetkiCh.Size = new System.Drawing.Size(82, 24);
            this.odsetkiCh.TabIndex = 18;
            this.odsetkiCh.Text = "Odsetki";
            this.odsetkiCh.UseVisualStyleBackColor = true;
            this.odsetkiCh.CheckedChanged += new System.EventHandler(this.OdsetkiChCheckedChanged);
            this.odsetkiCh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.przepisz);
            this.groupBox3.Controls.Add(this.wplata_box);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(933, 240);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(269, 67);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Kwota wpłaty";
            // 
            // przepisz
            // 
            this.przepisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.przepisz.Location = new System.Drawing.Point(10, 25);
            this.przepisz.Name = "przepisz";
            this.przepisz.Size = new System.Drawing.Size(42, 26);
            this.przepisz.TabIndex = 37;
            this.przepisz.Text = "-->";
            this.przepisz.UseVisualStyleBackColor = true;
            this.przepisz.Click += new System.EventHandler(this.PrzepiszClick);
            this.przepisz.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // pozostalo_l
            // 
            this.pozostalo_l.AutoSize = true;
            this.pozostalo_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pozostalo_l.Location = new System.Drawing.Point(923, 514);
            this.pozostalo_l.Name = "pozostalo_l";
            this.pozostalo_l.Size = new System.Drawing.Size(94, 20);
            this.pozostalo_l.TabIndex = 19;
            this.pozostalo_l.Text = "Pozostało:";
            // 
            // za_fv
            // 
            this.za_fv.AutoSize = true;
            this.za_fv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.za_fv.Location = new System.Drawing.Point(923, 410);
            this.za_fv.Name = "za_fv";
            this.za_fv.Size = new System.Drawing.Size(63, 20);
            this.za_fv.TabIndex = 21;
            this.za_fv.Text = "Za FV:";
            // 
            // odsetki_l
            // 
            this.odsetki_l.AutoSize = true;
            this.odsetki_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.odsetki_l.Location = new System.Drawing.Point(923, 444);
            this.odsetki_l.Name = "odsetki_l";
            this.odsetki_l.Size = new System.Drawing.Size(75, 20);
            this.odsetki_l.TabIndex = 22;
            this.odsetki_l.Text = "Odsetki:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.odsetkiProcNum);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox4.Location = new System.Drawing.Point(1262, 169);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(71, 60);
            this.groupBox4.TabIndex = 23;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "%";
            // 
            // odsetkiProcNum
            // 
            this.odsetkiProcNum.Location = new System.Drawing.Point(12, 27);
            this.odsetkiProcNum.Name = "odsetkiProcNum";
            this.odsetkiProcNum.Size = new System.Drawing.Size(50, 25);
            this.odsetkiProcNum.TabIndex = 41;
            this.odsetkiProcNum.Text = "8";
            // 
            // liczba_dni
            // 
            this.liczba_dni.AutoSize = true;
            this.liczba_dni.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.liczba_dni.Location = new System.Drawing.Point(922, 548);
            this.liczba_dni.Name = "liczba_dni";
            this.liczba_dni.Size = new System.Drawing.Size(95, 20);
            this.liczba_dni.TabIndex = 24;
            this.liczba_dni.Text = "Liczba dni:";
            // 
            // usun_fv
            // 
            this.usun_fv.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usun_fv.Location = new System.Drawing.Point(6, 546);
            this.usun_fv.Name = "usun_fv";
            this.usun_fv.Size = new System.Drawing.Size(122, 50);
            this.usun_fv.TabIndex = 25;
            this.usun_fv.Text = "Usuń Fakturę";
            this.usun_fv.UseVisualStyleBackColor = true;
            this.usun_fv.Click += new System.EventHandler(this.UsunFvClick);
            this.usun_fv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.wplataDodaj);
            this.groupBox5.Controls.Add(this.lista_wplat);
            this.groupBox5.Controls.Add(this.usun_wp);
            this.groupBox5.Controls.Add(this.edytuj_wp);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox5.Location = new System.Drawing.Point(408, 46);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(509, 602);
            this.groupBox5.TabIndex = 26;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Wpłaty tej faktury";
            // 
            // wplataDodaj
            // 
            this.wplataDodaj.Enabled = false;
            this.wplataDodaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wplataDodaj.Location = new System.Drawing.Point(173, 546);
            this.wplataDodaj.Name = "wplataDodaj";
            this.wplataDodaj.Size = new System.Drawing.Size(161, 50);
            this.wplataDodaj.TabIndex = 34;
            this.wplataDodaj.Text = "Dodaj Wpłatę";
            this.wplataDodaj.UseVisualStyleBackColor = true;
            // 
            // usun_wp
            // 
            this.usun_wp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usun_wp.Location = new System.Drawing.Point(6, 546);
            this.usun_wp.Name = "usun_wp";
            this.usun_wp.Size = new System.Drawing.Size(161, 50);
            this.usun_wp.TabIndex = 28;
            this.usun_wp.Text = "Usuń Wpłatę";
            this.usun_wp.UseVisualStyleBackColor = true;
            this.usun_wp.Click += new System.EventHandler(this.UsunWpClick);
            this.usun_wp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // edytuj_wp
            // 
            this.edytuj_wp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.edytuj_wp.Location = new System.Drawing.Point(340, 546);
            this.edytuj_wp.Name = "edytuj_wp";
            this.edytuj_wp.Size = new System.Drawing.Size(162, 50);
            this.edytuj_wp.TabIndex = 33;
            this.edytuj_wp.Text = "Edytuj Wpłatę";
            this.edytuj_wp.UseVisualStyleBackColor = true;
            this.edytuj_wp.Click += new System.EventHandler(this.EdytujWpClick);
            this.edytuj_wp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lista_faktur);
            this.groupBox6.Controls.Add(this.usun_fv);
            this.groupBox6.Controls.Add(this.dodaj_fv);
            this.groupBox6.Controls.Add(this.edytuj_fv);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox6.Location = new System.Drawing.Point(12, 46);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(390, 602);
            this.groupBox6.TabIndex = 27;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Lista Faktur";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.wyciag_rad);
            this.groupBox8.Controls.Add(this.fvRad);
            this.groupBox8.Controls.Add(this.kpRad);
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox8.Location = new System.Drawing.Point(1184, 46);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(149, 117);
            this.groupBox8.TabIndex = 20;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Dokument";
            // 
            // wyciag_rad
            // 
            this.wyciag_rad.AutoSize = true;
            this.wyciag_rad.Location = new System.Drawing.Point(6, 85);
            this.wyciag_rad.Name = "wyciag_rad";
            this.wyciag_rad.Size = new System.Drawing.Size(78, 24);
            this.wyciag_rad.TabIndex = 2;
            this.wyciag_rad.Text = "Wyciąg";
            this.wyciag_rad.UseVisualStyleBackColor = true;
            this.wyciag_rad.CheckedChanged += new System.EventHandler(this.FvKpWycRadCheckedChanged);
            this.wyciag_rad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // fvRad
            // 
            this.fvRad.AutoSize = true;
            this.fvRad.Checked = true;
            this.fvRad.Location = new System.Drawing.Point(6, 25);
            this.fvRad.Name = "fvRad";
            this.fvRad.Size = new System.Drawing.Size(48, 24);
            this.fvRad.TabIndex = 1;
            this.fvRad.TabStop = true;
            this.fvRad.Text = "FV";
            this.fvRad.UseVisualStyleBackColor = true;
            this.fvRad.CheckedChanged += new System.EventHandler(this.FvKpWycRadCheckedChanged);
            this.fvRad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // kpRad
            // 
            this.kpRad.AutoSize = true;
            this.kpRad.Location = new System.Drawing.Point(6, 55);
            this.kpRad.Name = "kpRad";
            this.kpRad.Size = new System.Drawing.Size(47, 24);
            this.kpRad.TabIndex = 0;
            this.kpRad.Text = "KP";
            this.kpRad.UseVisualStyleBackColor = true;
            this.kpRad.CheckedChanged += new System.EventHandler(this.FvKpWycRadCheckedChanged);
            this.kpRad.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.numerDokBox);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox9.Location = new System.Drawing.Point(1208, 240);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(125, 67);
            this.groupBox9.TabIndex = 19;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Numer dok.";
            // 
            // numerDokBox
            // 
            this.numerDokBox.Enabled = false;
            this.numerDokBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numerDokBox.Location = new System.Drawing.Point(6, 25);
            this.numerDokBox.MaxLength = 10;
            this.numerDokBox.Name = "numerDokBox";
            this.numerDokBox.Size = new System.Drawing.Size(112, 26);
            this.numerDokBox.TabIndex = 11;
            this.numerDokBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // zapisz
            // 
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(1128, 578);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(200, 70);
            this.zapisz.TabIndex = 30;
            this.zapisz.Text = "Zamknij";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.ZapiszClick);
            this.zapisz.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // razem_l
            // 
            this.razem_l.AutoSize = true;
            this.razem_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.razem_l.Location = new System.Drawing.Point(923, 481);
            this.razem_l.Name = "razem_l";
            this.razem_l.Size = new System.Drawing.Size(70, 20);
            this.razem_l.TabIndex = 31;
            this.razem_l.Text = "Razem:";
            // 
            // zaproponuj
            // 
            this.zaproponuj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zaproponuj.Location = new System.Drawing.Point(1027, 348);
            this.zaproponuj.Name = "zaproponuj";
            this.zaproponuj.Size = new System.Drawing.Size(98, 57);
            this.zaproponuj.TabIndex = 32;
            this.zaproponuj.Text = "Proponuj";
            this.zaproponuj.UseVisualStyleBackColor = true;
            this.zaproponuj.Click += new System.EventHandler(this.ZaproponujClick);
            this.zaproponuj.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // notatka
            // 
            this.notatka.AutoSize = true;
            this.notatka.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.notatka.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.notatka.Location = new System.Drawing.Point(853, 12);
            this.notatka.Name = "notatka";
            this.notatka.Size = new System.Drawing.Size(70, 20);
            this.notatka.TabIndex = 34;
            this.notatka.Text = "notatka";
            // 
            // drukuj_konto
            // 
            this.drukuj_konto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.drukuj_konto.Location = new System.Drawing.Point(1131, 348);
            this.drukuj_konto.Name = "drukuj_konto";
            this.drukuj_konto.Size = new System.Drawing.Size(98, 57);
            this.drukuj_konto.TabIndex = 35;
            this.drukuj_konto.Text = "Drukuj konto";
            this.drukuj_konto.UseVisualStyleBackColor = true;
            this.drukuj_konto.Click += new System.EventHandler(this.DrukujKontoClick);
            this.drukuj_konto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // podglad_konta
            // 
            this.podglad_konta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.podglad_konta.Location = new System.Drawing.Point(1235, 348);
            this.podglad_konta.Name = "podglad_konta";
            this.podglad_konta.Size = new System.Drawing.Size(98, 57);
            this.podglad_konta.TabIndex = 36;
            this.podglad_konta.Text = "Podgląd konta";
            this.podglad_konta.UseVisualStyleBackColor = true;
            this.podglad_konta.Click += new System.EventHandler(this.PodgladKontaClick);
            this.podglad_konta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // kpDrukButt
            // 
            this.kpDrukButt.Enabled = false;
            this.kpDrukButt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.kpDrukButt.Location = new System.Drawing.Point(1235, 411);
            this.kpDrukButt.Name = "kpDrukButt";
            this.kpDrukButt.Size = new System.Drawing.Size(98, 68);
            this.kpDrukButt.TabIndex = 37;
            this.kpDrukButt.Text = "KP\r\nWydruk\r\n(Ctrl+P)";
            this.kpDrukButt.UseVisualStyleBackColor = true;
            this.kpDrukButt.Click += new System.EventHandler(this.KpDrukButtClick);
            this.kpDrukButt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // opisBox
            // 
            this.opisBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.opisBox.Location = new System.Drawing.Point(1005, 313);
            this.opisBox.MaxLength = 55;
            this.opisBox.Name = "opisBox";
            this.opisBox.Size = new System.Drawing.Size(328, 26);
            this.opisBox.TabIndex = 20;
            this.opisBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(937, 316);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 20);
            this.label1.TabIndex = 39;
            this.label1.Text = "Opis:";
            // 
            // kpPodgladButt
            // 
            this.kpPodgladButt.Enabled = false;
            this.kpPodgladButt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.kpPodgladButt.Location = new System.Drawing.Point(1235, 482);
            this.kpPodgladButt.Name = "kpPodgladButt";
            this.kpPodgladButt.Size = new System.Drawing.Size(98, 49);
            this.kpPodgladButt.TabIndex = 40;
            this.kpPodgladButt.Text = "KP Podgląd";
            this.kpPodgladButt.UseVisualStyleBackColor = true;
            this.kpPodgladButt.Click += new System.EventHandler(this.KpPodgladButtClick);
            this.kpPodgladButt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            // 
            // OsobyWplaty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1340, 660);
            this.Controls.Add(this.kpPodgladButt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.opisBox);
            this.Controls.Add(this.kpDrukButt);
            this.Controls.Add(this.podglad_konta);
            this.Controls.Add(this.drukuj_konto);
            this.Controls.Add(this.notatka);
            this.Controls.Add(this.zaproponuj);
            this.Controls.Add(this.razem_l);
            this.Controls.Add(this.zapisz);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.liczba_dni);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.odsetki_l);
            this.Controls.Add(this.za_fv);
            this.Controls.Add(this.pozostalo_l);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.nazwaLab);
            this.Controls.Add(this.dodaj_wp);
            this.Controls.Add(this.bilans);
            this.Controls.Add(this.id_lab);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OsobyWplaty";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Wplaty";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OsobyWplatyKeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label id_lab;
        private System.Windows.Forms.Label bilans;
        private System.Windows.Forms.Button edytuj_fv;
        private System.Windows.Forms.Button dodaj_fv;
        private System.Windows.Forms.ListView lista_faktur;
        private System.Windows.Forms.ColumnHeader l_data;
        private System.Windows.Forms.ColumnHeader l_suma;
        private System.Windows.Forms.ColumnHeader l_nr;
        private System.Windows.Forms.ColumnHeader l_winien;
        private System.Windows.Forms.Button dodaj_wp;
        private System.Windows.Forms.TextBox wplata_box;
        private System.Windows.Forms.Label nazwaLab;
        private System.Windows.Forms.ListView lista_wplat;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.DateTimePicker dataWplaty;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox odsetkiCh;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox nadplataCh;
        private System.Windows.Forms.Label pozostalo_l;
        private System.Windows.Forms.Label za_fv;
        private System.Windows.Forms.Label odsetki_l;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label liczba_dni;
        private System.Windows.Forms.Button usun_fv;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton wyciag_rad;
        private System.Windows.Forms.RadioButton fvRad;
        private System.Windows.Forms.RadioButton kpRad;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox numerDokBox;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Button usun_wp;
        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.Label razem_l;
        private System.Windows.Forms.Button zaproponuj;
        private System.Windows.Forms.Button edytuj_wp;
        private System.Windows.Forms.Label notatka;
        private System.Windows.Forms.Button drukuj_konto;
        private System.Windows.Forms.Button podglad_konta;
        private System.Windows.Forms.TextBox odsetkiProcNum;
        private System.Windows.Forms.Button przepisz;
        private System.Windows.Forms.Button kpDrukButt;
        private System.Windows.Forms.TextBox opisBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button kpPodgladButt;
        private System.Windows.Forms.Button wplataDodaj;
    }
}