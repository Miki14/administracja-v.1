﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using Administracja.KlasyStatic;

namespace Administracja.OsobyOkna
{
    public partial class OsobyWplaty : Form
    {
        private readonly int _idO;

        private int _idF = 1;
        private readonly List<int> _idFl = new List<int>();
        private int _indexF = -1;

        private int _idW = 1;
        private readonly List<int> _idWl = new List<int>();
        private int _indexW;

        public OsobyWplaty(int idO)
        {
            InitializeComponent();
            _idO = idO;
            Wyswietl();
        }

        private void Wyswietl()
        {
            lista_faktur.Items.Clear();
            _idFl.Clear();

            OdswiezBilans();

            id_lab.Text = "ID: " + _idO;

            var reader1D2 = Baza.Wczytaj1D("SELECT Nazwisko, Imie, Notatka FROM OSOBY WHERE ID = '" + _idO + "' ORDER BY ID");
            nazwaLab.Text = reader1D2[0] + " " + reader1D2[1];
            notatka.Text = reader1D2[2];

            var reader2D = Baza.Wczytaj2D("SELECT ID, Numer, Data, Suma_B, Oplata, WYDRUKOWANO FROM FAKTURY WHERE ID_N = '" + _idO + "' ORDER BY DATA DESC, ID DESC");
            foreach (List<string> reader1D in reader2D)
            {
                _idFl.Add(int.Parse(reader1D[0]));

                ListViewItem element = lista_faktur.Items.Add(reader1D[1]);
                DateTime data = DateTime.Parse(reader1D[2]);
                element.SubItems.Add(data.ToString("dd.MM.yyyy"));

                double sumaB = Convert.ToDouble(reader1D[3].Replace(".", ","));
                element.SubItems.Add(string.Format("{0:N2}", sumaB));

                double oplata = Convert.ToDouble(reader1D[4].Replace(".", ","));
                element.SubItems.Add(string.Format("{0:N2}", sumaB - oplata));

                if (reader1D[5] != "T")
                {
                    element.ForeColor = Color.Red;
                }
            }

            double bilansD = Baza.LiczBilansOsob(_idO);
            if (bilansD < 0) bilans.ForeColor = Color.Red;
            else bilans.ForeColor = Color.Green;
            bilans.Text = "Bilans : " + string.Format("{0:N2}", bilansD) + " zł";

            try //po wyświetleniu przywraca starą pozycję zaznaczenia
            {
                if (lista_faktur.Items.Count != 0)
                {
                    lista_faktur.Items[_indexF].Selected = true;
                }
            }
            catch
            {
                lista_faktur.Items[0].Selected = true;
            }

            //Wywalenie czasu z kontrolki dodawania daty wpłaty
            dataWplaty.Value = DateTime.Today;

            //Wczytanie oprocentowania z bazy
            odsetkiProcNum.Text = Baza.ModelData.Interest.ToString();
        }

        private void WyswietlWplaty()
        {
            lista_wplat.Items.Clear();
            _idWl.Clear();

            var reader2D = Baza.Wczytaj2D("SELECT WPLATY.ID, WPLATY.DATA, KWOTA, NUMER, ODSETKI, SPOSOB, DOKUMENT, NUMER_DOK "
                + "FROM WPLATY JOIN FAKTURY ON WPLATY.ID_F = FAKTURY.ID WHERE ID_F = '" + _idF + "' ORDER BY WPLATY.DATA");
            foreach (var reader1D in reader2D)
            {
                _idWl.Add(int.Parse(reader1D[0]));
                ListViewItem element = lista_wplat.Items.Add(DateTime.Parse(reader1D[1]).ToString("dd.MM"));

                //reader1D[1].Substring(8, 2) + "." + reader1D[1].Substring(5, 2));
                element.SubItems.Add(string.Format("{0:N2}", Convert.ToDouble(reader1D[2].Replace(".", ","))));
                element.SubItems.Add(reader1D[3]);
                element.SubItems.Add(string.Format("{0:N2}", Convert.ToDouble(reader1D[4].Replace(".", ","))));
                element.SubItems.Add(reader1D[5]);
                element.SubItems.Add(reader1D[6]);
                element.SubItems.Add(reader1D[7]);
            }

            try
            {
                if (lista_wplat.Items.Count != 0)
                {
                    lista_wplat.Items[_indexW].Selected = true;
                }
            }
            catch
            {
                lista_wplat.Items[lista_wplat.Items.Count - 1].Selected = true;
            }

            WstawNumerDok();
        }

        private void ObliczOdsetki(bool propozycja)
        {
            Baza.Ustaw("WPLATY");

            double wplata = Convert.ToDouble(wplata_box.Text.Replace(".", ","));   //kwota wpłaty
            var dataFaktury = DateTime.Parse(Baza.Wczytaj("SELECT Data FROM Faktury WHERE ID = '" + _idF + "'"));

            var reader1D = Baza.Wczytaj1D("SELECT Suma_B, Oplata FROM FAKTURY WHERE ID = '" + _idF + "'");
            var sumaB = Convert.ToDouble(reader1D[0].Replace(".", ","));
            var oplacono = Convert.ToDouble(reader1D[1].Replace(".", ","));

            OdsetkiKlasa rozliczenie =
                new OdsetkiKlasa(dataFaktury, dataWplaty.Value, Convert.ToDouble(odsetkiProcNum.Text.Replace(".", ",")),
                wplata, nadplataCh.Checked, odsetkiCh.Checked, sumaB, oplacono);

            za_fv.Text = "Za FV: " + string.Format("{0:N2}", Math.Round(rozliczenie.ZaFv, 2));
            odsetki_l.Text = "Odsetki: " + string.Format("{0:N2}", Math.Round(rozliczenie.OdsetkiOp, 2));
            razem_l.Text = "Razem: " + string.Format("{0:N2}", Math.Round(rozliczenie.ZaFv + rozliczenie.OdsetkiOp, 2));
            pozostalo_l.Text = "Pozostało: " + string.Format("{0:N2}", Math.Round(rozliczenie.Pozostalo, 2));
            liczba_dni.Text = "Liczba dni: " + rozliczenie.LiczbaDni;

            if (!propozycja)
            {
                wplata_box.Text = Math.Round(rozliczenie.Pozostalo, 2).ToString("0.00");
            }

            if (!propozycja)
            {
                string sposob = "G";
                string dokument = "FV";

                if (wyciag_rad.Checked)
                {
                    sposob = "P";
                    dokument = "W";
                }
                if (kpRad.Checked) dokument = "KP";

                Baza.Zapisz("INSERT INTO WPLATY VALUES (GEN_ID(NUMERACJA_WPLATY,1),'" +
                    _idF + "','" +
                    dataWplaty.Value.ToString("dd.MM.yyyy") + "','" +
                    rozliczenie.ZaFv.ToString("0.00", CultureInfo.InvariantCulture) + "','" +
                    rozliczenie.OdsetkiOp.ToString("0.00", CultureInfo.InvariantCulture) + "','" +
                    sposob + "','" +
                    dokument + "','" +
                    numerDokBox.Text + "','" +
                    opisBox.Text + "');");

                Baza.Ustaw("WPLATY");
                Wyswietl();
                WyswietlWplaty();
            }
        }

        private void OdswiezBilans()
        {
            //TODO: Odświeżanie SumyB jest chyba niepotrzebne
            //Bilans odświeżamy tylko dla numeru != 0;
            if (int.Parse(Baza.Wczytaj("SELECT NUMER FROM FAKTURY WHERE ID='" + _idF + "'")) != 0)
            {
                //Aktualizacja Suma_B faktury
                var ceny = new List<double>();
                var vat = new List<int?>();
                double sumaB = 0;

                var reader2D = Baza.Wczytaj2D("SELECT CENA, VAT FROM FAKTURY_SKLADNIKI JOIN SKLADNIKI ON FAKTURY_SKLADNIKI.ID_S"
                    + " = SKLADNIKI.ID WHERE ID_F = '" + _idF + "'");
                foreach (var a in reader2D)
                {
                    ceny.Add(Convert.ToDouble(a[0]));
                    vat.Add(!string.IsNullOrWhiteSpace(a[1]) ? int.Parse(a[1]) : (int?)null);
                }

                for (int i = 0; i < ceny.Count; i++)
                {
                    sumaB += Math.Round(ceny[i] + ceny[i] * (vat[i] ?? 0) / 100, 2);
                }

                Baza.Zapisz("UPDATE FAKTURY SET Suma_B = '" + sumaB.ToString("0.00", CultureInfo.InvariantCulture) + "' WHERE ID = '" + _idF + "';");
            }

            //Aktualizacja oplaty danej fv
            var kwota = Convert.ToDouble(Baza.Wczytaj("SELECT SUM(Kwota) FROM WPLATY WHERE ID_F = '" + _idF + "'").Replace(".", ","));

            Baza.Zapisz("UPDATE FAKTURY SET Oplata = '" + kwota.ToString("0.00", CultureInfo.InvariantCulture) + "' WHERE ID = '" + _idF + "';");
        }

        private void DodajWplate()
        {
            //Sprawdzenie czy wszystkie pola wypełnione
            if (wplata_box.Text.CompareTo("0") != 0 && wplata_box.Text.Length != 0 && numerDokBox.Text.Length != 0)
            {
                //sprawdzenie czy wpłata gotówką na FV, jak tak to miesiąc Fv i miesiąc wpłaty muszą być te same
                DateTime dataFv = DateTime.Parse(Baza.Wczytaj("SELECT DATA FROM FAKTURY WHERE id='" + _idF + "'"));
                if (fvRad.Checked && !(dataWplaty.Value.Month == dataFv.Month && dataWplaty.Value.Year == dataFv.Year))
                {
                    Wyjatki.Komunikat("Wpłata do FV możliwa tylko w tym miesiącu co FV. Dodaj wpłatę na KP.", "Błąd dodawania wpłaty", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    //Sprawdzenie czy KP o tym numerze można dodać tej osobie
                    //(czy ten numer nie jest przypisany do kogoś innego)
                    if (kpRad.Checked)
                    {
                        var nextNumber = Baza.GetNextKpNumber(dataWplaty.Value);
                            //int.Parse(Baza.Wczytaj("SELECT COUNT(DISTINCT NUMER_DOK) FROM WPLATY WHERE DOKUMENT='KP'"));
                        int aktNumer = int.Parse(numerDokBox.Text);

                        if (nextNumber < aktNumer)
                        {
                            Wyjatki.Komunikat("Błąd numeracji KP. Prawdopodobnie pominięto jakiś numer KP.",
                                "Błąd numeru KP",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            var reader1D = Baza.Wczytaj1D("SELECT WPLATY.DATA, FAKTURY.ID_N FROM WPLATY JOIN FAKTURY "
                                            + "ON WPLATY.ID_F = FAKTURY.ID WHERE DOKUMENT = 'KP' AND NUMER_DOK = '"
                                            + numerDokBox.Text + "' AND WPLATY.DATA >= '01.01."
                                            + dataWplaty.Value.Year + "' AND WPLATY.DATA <= '31.12." + dataWplaty.Value.Year + "'");

                            if (reader1D.Count != 0)
                            {
                                if (DateTime.Parse(reader1D[0]) != dataWplaty.Value)
                                {
                                    Wyjatki.Komunikat(
                                        "Data wpłaty KP o danym numerze musi być taka sama dla wszystkich wpłat.",
                                        "Błąd daty wpłaty", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                else
                                {
                                    int idWplaty = int.Parse(reader1D[1]);
                                    if (idWplaty != _idO)
                                    {
                                        Wyjatki.Komunikat("Nie można dwóm różnym osobom nadać KP o tym samym numerze.",
                                            "Błąd numeru KP",
                                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    else
                                    {
                                        //Jeśli numer KP ok to dodaj wpłatę
                                        ObliczOdsetki(false);
                                    }
                                }
                            }
                            else
                            {
                                //Jeśli nie ma w bazie KP o tym numerze to dodaj wpłątę
                                ObliczOdsetki(false);
                            }
                        }
                    }
                    else
                    {
                        //Jak nie na KP to od razu dodajemy
                        ObliczOdsetki(false);
                    }
                }
            }
            else
            {
                Wyjatki.Komunikat("Wypełnij wszystkie pola przed dodaniem wpłaty.", "Wypełnij pola", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void WstawNumerDok()
        {
            //jeśli fv wybrana, disable okno numeru dok.
            numerDokBox.Enabled = !fvRad.Checked;

            //jeśli fv wybana numer dok to numer fv, jak nie to wyczyść
            if (fvRad.Checked)
            {
                string reader = Baza.Wczytaj("SELECT NUMER FROM FAKTURY WHERE ID='" + _idF + "'");
                numerDokBox.Text = reader;
            }
            else if (kpRad.Checked)
            {

                numerDokBox.Text = Baza.GetNextKpNumber(dataWplaty.Value).ToString();
            }
            else
            {
                numerDokBox.Text = dataWplaty.Value.Month.ToString();
            }
        }

        private bool ZaznaczonoKp()
        {
            return lista_wplat.SelectedItems.Count > 0
                && lista_wplat.SelectedItems[0].SubItems[5].Text == "KP";
        }

        #region handlery

        private void UsunFvClick(object sender, EventArgs e)
        {
            var fv = Baza.Wczytaj1D("SELECT Numer, Data, ID FROM FAKTURY WHERE ID IN (SELECT MAX(ID) FROM FAKTURY)");
            string numer = fv[0];
            DateTime data = Convert.ToDateTime(fv[1]);
            int id = Convert.ToInt32(fv[2]);

            if (numer == "0") Wyjatki.Komunikat("Nie ma faktur do usunięcia!", "Brak faktur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else if (id != _idF)
            {
                if (Wyjatki.Komunikat("Ostatnia faktura w tym miesiącu ma numer " + numer + data.ToString(" '/' MM '/' yyyy")
                        + " i\u00A0należy do innej osoby. Czy na pewno chcesz ją usunąć?", "Usunąć?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Baza.Zapisz("DELETE FROM FAKTURY_SKLADNIKI WHERE ID_F = '" + id + "'",
                        "DELETE FROM WPLATY WHERE ID_F = '" + id + "'");
                    //Najpierw commit usunięcie składników, dopieo później usuwaj FV
                    Baza.Zapisz("DELETE FROM FAKTURY WHERE ID = '" + id + "'");
                }
            }
            else
            {
                if (Wyjatki.Komunikat("Czy na pewno chcesz usunąć fakturę numer " + numer + data.ToString(" '/' MM '/' yyyy ?"), "Usunąć?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Baza.Zapisz("DELETE FROM FAKTURY_SKLADNIKI WHERE ID_F = '" + id + "'",
                        "DELETE FROM WPLATY WHERE ID_F = '" + id + "'");
                    //Najpierw commit usunięcie składników, dopieo później usuwaj FV
                    Baza.Zapisz("DELETE FROM FAKTURY WHERE ID = '" + id + "'");
                }
            }
            Wyswietl();
        }

        private void DodajFvClick(object sender, EventArgs e)
        {
            Baza.Ustaw("FAKTURY");

            var reader1D = Baza.Wczytaj1D("SELECT DATA, ID_N, METODA_PLATNOSCI, TERMIN_PLATNOSCI, SUMA_B, NOTATKA FROM FAKTURY WHERE ID = '"
                                          + _idF + "'");

            var dataPop = DateTime.Parse(reader1D[0]);
            string idN = reader1D[1];
            string metoda = reader1D[2];
            string termin = reader1D[3];
            string sumaB = reader1D[4];
            string notatkaBaza = reader1D[5];

            //dataNastepnejFV to miesiąc poprzedniej +1, domyślnie dzień 10
            DateTime dataNastepnejFv = DateTimeHelper.GetDateTenthDayOfMonth(dataPop).AddMonths(1);

            int numerNastepnejFv = Baza.GetNextFvNumber(dataNastepnejFv, false);
            if (DateTime.TryParse(termin, out _))
            {
                termin = DateTime.Now.ToString("dd.MM.yyyy");
            }

            Baza.Zapisz("INSERT INTO FAKTURY VALUES(" +
                        "GEN_ID(NUMERACJA_FAKTURY,1),'" +
                        numerNastepnejFv + "','" +
                        dataNastepnejFv.ToString("yyyy.MM.dd") + "','" +
                        idN + "','" +
                        metoda + "','" +
                        termin + "','" +
                        sumaB.Replace(",", ".") +
                        "','0','" +
                        notatkaBaza +
                        "','N');");

            var reader2D = Baza.Wczytaj2D("SELECT ID_S, Cena FROM Faktury_skladniki WHERE ID_F = '" + _idF + "'");
            foreach (var reader in reader2D)
            {
                Baza.Zapisz("INSERT INTO FAKTURY_SKLADNIKI VALUES(" +
                            "GEN_ID(NUMERACJA_FAKTURY_SKLADNIKI,1),GEN_ID(NUMERACJA_FAKTURY, 0),'" +
                            reader[0].Replace(",", ".") + "','" +
                            reader[1].Replace(",", ".") + "');");
            }

            _idF = int.Parse(Baza.Wczytaj("SELECT MAX(ID) FROM FAKTURY"));
            OdswiezBilans();

            ListaFakturSelectedIndexChanged(this, EventArgs.Empty);
            Wyswietl();
            WyswietlWplaty();
        }

        private void EdytujFvClick(object sender, EventArgs e)
        {
            if (lista_faktur.SelectedItems.Count > 0 && lista_faktur.SelectedItems[0].Text.Equals("0"))
            {
                Wyjatki.Komunikat("Nie można edytować bilansu.", "Błąd edycji bilansu.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                new OsobyFaktury(_idO, _idF).ShowDialog();

                Wyswietl();
            }
        }

        private void UsunWpClick(object sender, EventArgs e)
        {
            if (lista_wplat.Items.Count == 0)
            {
                Wyjatki.Komunikat("Brak wpłat do usunięcia.", "Brak wpłat", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (Wyjatki.Komunikat("Czy na pewno chcesz usunąć wpłatę na kwotę " + string.Format("{0:N2}", Convert.ToDouble(Baza.Wczytaj("SELECT Kwota FROM WPLATY WHERE ID = '" + _idW + "'")))
                    + " ?", "Usunąć?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Baza.Zapisz("DELETE FROM WPLATY WHERE ID='" + _idW + "'");
                    Baza.Ustaw("WPLATY");
                }
            }

            Wyswietl();
            WyswietlWplaty();
        }

        private void EdytujWpClick(object sender, EventArgs e)
        {
            if (lista_wplat.Items.Count == 0)
            {
                Wyjatki.Komunikat("Brak wpłat do edycji.", "Brak wpłat", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                new OsobyWplatyEdycja(_idW).ShowDialog();
            }

            Baza.Ustaw("WPLATY");
            Wyswietl();
            WyswietlWplaty();
        }

        private void DodajWpClick(object sender, EventArgs e)
        {
            DodajWplate();
        }

        private void ListaFakturSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _indexF = lista_faktur.SelectedItems[0].Index;
            }
            catch
            {
                _indexF = 0;
            }

            _idF = _idFl[_indexF];

            WyswietlWplaty();
        }

        private void ListaWplatSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _indexW = lista_wplat.SelectedItems[0].Index;
            }
            catch
            {
                _indexW = lista_wplat.Items.Count - 1;
            }

            _idW = _idWl[_indexW];

            //Wydruk KP tylko dla KP
            bool zaznaczono = ZaznaczonoKp();
            kpDrukButt.Enabled = zaznaczono;
            kpPodgladButt.Enabled = zaznaczono;
        }

        private void WplataBoxClick(object sender, EventArgs e)
        {
            wplata_box.SelectAll();
        }

        private void DrukujKontoClick(object sender, EventArgs e)
        {
            //TODO:
            Wyjatki.Komunikat("Jeszcze nie zrobione", "Brak funkcji", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void PodgladKontaClick(object sender, EventArgs e)
        {
            //TODO:
            Wyjatki.Komunikat("Jeszcze nie zrobione", "Brak funkcji", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ZapiszClick(object sender, EventArgs e)
        {
            Close();
        }

        private void ZaproponujClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(wplata_box.Text) || wplata_box.Text != ("0"))
            {
                ObliczOdsetki(true);
            }
            else
            {
                Wyjatki.Komunikat("Wypełnij pole wpłaty przed proponowaniem rozliczenia.", "Wypełnij pola", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void NadplataChCheckedChanged(object sender, EventArgs e)
        {
            if (nadplataCh.Checked) odsetkiCh.Checked = false;
        }

        private void OdsetkiChCheckedChanged(object sender, EventArgs e)
        {
            if (odsetkiCh.Checked) nadplataCh.Checked = false;
        }

        private void CheckKeys(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                DodajWplate();
                e.KeyChar = (char)0;
                e.Handled = true;
            }
        }

        private void PrzepiszClick(object sender, EventArgs e)
        {
            var reader1D = Baza.Wczytaj1D("SELECT Suma_B, Oplata, Data FROM FAKTURY WHERE ID = '" + _idF + "'");
            var sumaB = Convert.ToDouble(reader1D[0].Replace(".", ","));
            var oplacono = Convert.ToDouble(reader1D[1].Replace(".", ","));
            var dataFaktury = DateTime.Parse(reader1D[2]);

            OdsetkiKlasa odsetki =
                new OdsetkiKlasa(dataFaktury, dataWplaty.Value, Convert.ToDouble(odsetkiProcNum.Text.Replace(".", ",")),
                1000000, nadplataCh.Checked, odsetkiCh.Checked, sumaB, oplacono);

            wplata_box.Text = (odsetki.ZaFv + odsetki.OdsetkiOp).ToString("0.00");
        }

        private void FvKpWycRadCheckedChanged(object sender, EventArgs e)
        {
            WstawNumerDok();
        }

        private void KpDrukButtClick(object sender, EventArgs e)
        {
            Wydruki.Kp(false, _idW);
        }

        private void KpPodgladButtClick(object sender, EventArgs e)
        {
            Wydruki.Kp(true, _idW);
        }

        private void OsobyWplatyKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.P && e.Control && ZaznaczonoKp())
            {
                Wydruki.Kp(false, _idW);
                e.Handled = true;
            }
        }

        private void DataWplatyValueChanged(object sender, EventArgs e)
        {
            WstawNumerDok();
        }

        #endregion
    }
}
