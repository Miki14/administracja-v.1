﻿namespace Administracja.OsobyOkna
{
    partial class Osoby
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.szukaj = new System.Windows.Forms.TextBox();
            this.lista = new System.Windows.Forms.ListView();
            this.Nazwisko = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Imię = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Miasto = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Ulica = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Dom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Mieszkanie = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Grupa = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Bilans = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Miesiac = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.edycja = new System.Windows.Forms.Button();
            this.dodaj = new System.Windows.Forms.Button();
            this.usun = new System.Windows.Forms.Button();
            this.wplaty = new System.Windows.Forms.Button();
            this.niewydrukowaneFvCh = new System.Windows.Forms.CheckBox();
            this.ukryteOsobyCh = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // szukaj
            // 
            this.szukaj.Location = new System.Drawing.Point(24, 21);
            this.szukaj.Name = "szukaj";
            this.szukaj.Size = new System.Drawing.Size(257, 20);
            this.szukaj.TabIndex = 5;
            this.szukaj.TextChanged += new System.EventHandler(this.SzukajTextChanged);
            // 
            // lista
            // 
            this.lista.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lista.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Nazwisko,
            this.Imię,
            this.Miasto,
            this.Ulica,
            this.Dom,
            this.Mieszkanie,
            this.Grupa,
            this.Bilans,
            this.Miesiac});
            this.lista.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lista.FullRowSelect = true;
            this.lista.GridLines = true;
            this.lista.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lista.HideSelection = false;
            this.lista.Location = new System.Drawing.Point(24, 47);
            this.lista.MultiSelect = false;
            this.lista.Name = "lista";
            this.lista.Size = new System.Drawing.Size(1020, 433);
            this.lista.TabIndex = 0;
            this.lista.UseCompatibleStateImageBehavior = false;
            this.lista.View = System.Windows.Forms.View.Details;
            this.lista.SelectedIndexChanged += new System.EventHandler(this.ListaSelectedIndexChanged);
            this.lista.DoubleClick += new System.EventHandler(this.WplatyClick);
            // 
            // Nazwisko
            // 
            this.Nazwisko.Text = "Nazwisko";
            this.Nazwisko.Width = 277;
            // 
            // Imię
            // 
            this.Imię.Text = "Imię";
            this.Imię.Width = 168;
            // 
            // Miasto
            // 
            this.Miasto.Text = "Miasto";
            this.Miasto.Width = 73;
            // 
            // Ulica
            // 
            this.Ulica.Text = "Ulica";
            this.Ulica.Width = 119;
            // 
            // Dom
            // 
            this.Dom.Text = "Dom";
            this.Dom.Width = 52;
            // 
            // Mieszkanie
            // 
            this.Mieszkanie.Text = "Mieszkanie";
            this.Mieszkanie.Width = 55;
            // 
            // Grupa
            // 
            this.Grupa.Text = "Grupa";
            this.Grupa.Width = 97;
            // 
            // Bilans
            // 
            this.Bilans.Text = "Bilans";
            this.Bilans.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Bilans.Width = 99;
            // 
            // Miesiac
            // 
            this.Miesiac.Text = "Miesiąc";
            this.Miesiac.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Miesiac.Width = 59;
            // 
            // edycja
            // 
            this.edycja.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.edycja.Location = new System.Drawing.Point(538, 486);
            this.edycja.Name = "edycja";
            this.edycja.Size = new System.Drawing.Size(250, 58);
            this.edycja.TabIndex = 4;
            this.edycja.Text = "Edycja";
            this.edycja.UseVisualStyleBackColor = true;
            this.edycja.Click += new System.EventHandler(this.EdycjaClick);
            // 
            // dodaj
            // 
            this.dodaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dodaj.Location = new System.Drawing.Point(282, 486);
            this.dodaj.Name = "dodaj";
            this.dodaj.Size = new System.Drawing.Size(250, 58);
            this.dodaj.TabIndex = 3;
            this.dodaj.Text = "Dodaj";
            this.dodaj.UseVisualStyleBackColor = true;
            this.dodaj.Click += new System.EventHandler(this.DodajClick);
            // 
            // usun
            // 
            this.usun.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usun.Location = new System.Drawing.Point(24, 486);
            this.usun.Name = "usun";
            this.usun.Size = new System.Drawing.Size(251, 58);
            this.usun.TabIndex = 2;
            this.usun.Text = "Usuń";
            this.usun.UseVisualStyleBackColor = true;
            this.usun.Click += new System.EventHandler(this.UsunClick);
            // 
            // wplaty
            // 
            this.wplaty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wplaty.Location = new System.Drawing.Point(794, 486);
            this.wplaty.Name = "wplaty";
            this.wplaty.Size = new System.Drawing.Size(250, 58);
            this.wplaty.TabIndex = 1;
            this.wplaty.Text = "Karta Osoby";
            this.wplaty.UseVisualStyleBackColor = true;
            this.wplaty.Click += new System.EventHandler(this.WplatyClick);
            // 
            // niewydrukowaneFvCh
            // 
            this.niewydrukowaneFvCh.AutoSize = true;
            this.niewydrukowaneFvCh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.niewydrukowaneFvCh.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.niewydrukowaneFvCh.Location = new System.Drawing.Point(329, 17);
            this.niewydrukowaneFvCh.Name = "niewydrukowaneFvCh";
            this.niewydrukowaneFvCh.Size = new System.Drawing.Size(282, 24);
            this.niewydrukowaneFvCh.TabIndex = 6;
            this.niewydrukowaneFvCh.Text = "Tylko osoby z niewydrukowanymi FV";
            this.niewydrukowaneFvCh.UseVisualStyleBackColor = true;
            this.niewydrukowaneFvCh.CheckedChanged += new System.EventHandler(this.CheckboxCheckedChanged);
            // 
            // ukryteOsobyCh
            // 
            this.ukryteOsobyCh.AutoSize = true;
            this.ukryteOsobyCh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ukryteOsobyCh.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.ukryteOsobyCh.Location = new System.Drawing.Point(630, 15);
            this.ukryteOsobyCh.Name = "ukryteOsobyCh";
            this.ukryteOsobyCh.Size = new System.Drawing.Size(165, 24);
            this.ukryteOsobyCh.TabIndex = 7;
            this.ukryteOsobyCh.Text = "Pokaż ukryte osoby";
            this.ukryteOsobyCh.UseVisualStyleBackColor = true;
            this.ukryteOsobyCh.CheckedChanged += new System.EventHandler(this.CheckboxCheckedChanged);
            // 
            // Osoby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGreen;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.ukryteOsobyCh);
            this.Controls.Add(this.niewydrukowaneFvCh);
            this.Controls.Add(this.wplaty);
            this.Controls.Add(this.usun);
            this.Controls.Add(this.dodaj);
            this.Controls.Add(this.edycja);
            this.Controls.Add(this.szukaj);
            this.Controls.Add(this.lista);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "Osoby";
            this.Size = new System.Drawing.Size(1066, 555);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox szukaj;
        private System.Windows.Forms.ListView lista;
        private System.Windows.Forms.ColumnHeader Nazwisko;
        private System.Windows.Forms.ColumnHeader Imię;
        private System.Windows.Forms.ColumnHeader Miasto;
        private System.Windows.Forms.ColumnHeader Ulica;
        private System.Windows.Forms.ColumnHeader Dom;
        private System.Windows.Forms.ColumnHeader Mieszkanie;
        private System.Windows.Forms.ColumnHeader Miesiac;
        private System.Windows.Forms.ColumnHeader Grupa;
        private System.Windows.Forms.Button edycja;
        private System.Windows.Forms.Button dodaj;
        private System.Windows.Forms.Button usun;
        private System.Windows.Forms.Button wplaty;
        private System.Windows.Forms.ColumnHeader Bilans;
        private System.Windows.Forms.CheckBox niewydrukowaneFvCh;
        private System.Windows.Forms.CheckBox ukryteOsobyCh;
    }
}
