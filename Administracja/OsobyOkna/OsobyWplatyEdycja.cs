﻿using System;
using System.Windows.Forms;

namespace Administracja.OsobyOkna
{
    public partial class OsobyWplatyEdycja : Form
    {
        private readonly int _idWp;

        public OsobyWplatyEdycja(int idWp)
        {
            InitializeComponent();
            _idWp = idWp;
            Wyswietl();
        }

        private void Wyswietl()
        {
            id_l.Text = "ID: " + _idWp;

            var reader1D = Baza.Wczytaj1D("SELECT * FROM WPLATY WHERE ID = '" + _idWp + "'");

            nr_fak_l.Text = "Nr. faktury: " + Baza.Wczytaj("SELECT Numer FROM FAKTURY WHERE ID = '" + reader1D[1] + "'");
            dataWplaty.Value = DateTime.Parse(reader1D[2]);
            wplataBox.Text = Convert.ToDouble(reader1D[3]).ToString("0.00");
            odsetkiBox.Text = Convert.ToDouble(reader1D[4]).ToString("0.00");
            string dok = reader1D[6];
            numerDokBox.Text = reader1D[7];
            opisBox.Text = reader1D[8];

            if (dok == "FV") fvRad.Checked = true;
            else if (dok == "KP") kpRad.Checked = true;
            else if (dok == "W") wyciagRad.Checked = true;
        }

        private void ZapiszClick(object sender, EventArgs e)
        {
            string dokument;
            string sposob;

            if (fvRad.Checked)
            {
                dokument = "FV";
                sposob = "G";
            }
            else if (kpRad.Checked)
            {
                dokument = "KP";
                sposob = "G";
            }
            else
            {
                dokument = "W";
                sposob = "P";
            }

            Baza.Zapisz("UPDATE WPLATY SET "
                + "Data = '" + dataWplaty.Value.ToString("dd.MM.yyyy")
                + "', Kwota = '" + wplataBox.Text.Replace(",", ".")
                + "', Odsetki = '" + odsetkiBox.Text.Replace(",", ".")
                + "', Sposob = '" + sposob
                + "', Dokument = '" + dokument
                + "', Numer_Dok = '" + numerDokBox.Text
                + "', Opis = '" + opisBox.Text
                + "' WHERE ID = " + _idWp + ";");

            Close();
        }

        private void AnulujClick(object sender, EventArgs e)
        {
            Close();
        }

        private void WplataBoxClick(object sender, EventArgs e)
        {
            wplataBox.SelectAll();
        }

        private void OdsetkiBoxClick(object sender, EventArgs e)
        {
            odsetkiBox.SelectAll();
        }

        private void NumerDokBoxClick(object sender, EventArgs e)
        {
            numerDokBox.SelectAll();
        }
    }
}
