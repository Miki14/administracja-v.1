﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Administracja.OsobyOkna
{
    public partial class OsobyEdycja : Form
    {
        private readonly int _id;
        private readonly bool _dodawanie;

        public OsobyEdycja(int id, bool dodawanie)
        {
            InitializeComponent();
            _id = id;
            _dodawanie = dodawanie;

            Wyswietl();
        }

        private void Wyswietl()
        {
            foreach (string grupyOsobValue in Baza.GrupyOsob.Values)
            {
                grupaCombo.Items.Add(grupyOsobValue);
            }

            var reader1D = Baza.Wczytaj1D("SELECT * FROM OSOBY WHERE ID = '" + _id + "'");

            label_ID.Text += reader1D[0];
            nazwisko.Text = reader1D[1];
            imie.Text = reader1D[2];
            miasto.Text = reader1D[3];
            ulica.Text = reader1D[4];
            dom.Text = reader1D[5];
            mieszkanie.Text = reader1D[6];
            kod.Text = reader1D[7];
            osob.Text = reader1D[8];
            metrow.Text = reader1D[9];

            parking_ch.Checked = reader1D[10] == "t";

            nip.Text = reader1D[11];

            grupaCombo.Text = Baza.GrupyOsob.FirstOrDefault(x => x.Key == Convert.ToInt32(reader1D[12])).Value;

            dowod.Text = reader1D[13];

            umowa.Text = reader1D[14];
            notatka.Text = reader1D[15];
        }

        private void ZapiszClick(object sender, EventArgs e)
        {
            string parking = parking_ch.Checked ? "t" : "n";

            string grupaOsob = "";

            try
            {
            
                var aa = Baza.GrupyOsob.FirstOrDefault(x => x.Value == grupaCombo.Text);
                grupaOsob = aa.Key.ToString();

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }

            Baza.Zapisz("UPDATE OSOBY SET "
                + "nazwisko = '" + nazwisko.Text.Trim()
                + "', imie = '" + imie.Text.Trim()
                + "', miasto = '" + miasto.Text.Trim()
                + "', ulica = '" + ulica.Text.Trim()
                + "', dom = '" + dom.Text.Trim()
                + "', mieszkanie = '" + mieszkanie.Text.Trim()
                + "', kod = '" + kod.Text.Trim()
                + "', osob = '" + osob.Text.Trim()
                + "', metry = '" + metrow.Text.Trim()
                + "', parking = '" + parking
                + "', nip = '" + nip.Text.Trim()
                + "', grupa = '" + grupaOsob
                + "', dowod = '" + dowod.Text.Trim()
                + "', umowa = '" + umowa.Text.Trim()
                + "', notatka = '" + notatka.Text.Trim()
                + "' WHERE ID = '" + _id + "'");
            Close();
        }

        private void AnulujClick(object sender, EventArgs e)
        {
            if (_dodawanie)
            {
                Baza.Zapisz("DELETE FROM FAKTURY WHERE ID_N = '" + _id + "'",
                    "DELETE FROM OSOBY WHERE ID = '" + _id + "'");
            }
        }

        private void CheckKeys(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                ZapiszClick(sender, e);
                e.Handled = true;
                e.KeyChar = (char)0;
            }
        }

        private void FirstToUpper(object sender, EventArgs e)
        {
            var senderTb = (TextBox)sender;

            int poz = senderTb.SelectionStart;

            try
            {
                senderTb.Text = senderTb.Text.Substring(0, 1).ToUpper() + senderTb.Text.Substring(1);
            }
            catch { }

            senderTb.SelectionStart = poz;
        }

        private void SelectAll(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
        }
    }
}
