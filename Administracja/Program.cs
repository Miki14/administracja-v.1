﻿using System;
using System.Windows.Forms;

namespace Administracja
{
    static class Program
    {
        public static bool Zamykam;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                var log = new Logowanie();
                log.ShowDialog();
                if (!log.IsOk)
                {
                    Zamykam = true;
                    Application.Exit();
                }
                else
                {
                    Application.Run(new ParentWindow());
                }
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
        }
    }
}
