CREATE DATABASE "D:\Dropbox\Programowanie\VS\ADM\Administracja\Administracja\bin\Debug\Baza\TESTY.FDB" user 'SYSDBA'
password 'masterkey';
IN D:\Dropbox\Programowanie\VS\ADM\Administracja\Administracja\bin\Debug\Baza\tabele.sql;

INSERT INTO OSOBY VALUES(GEN_ID(NUMERACJA_OSOBY,1),'Czernik-Bere�niewicz','Krystyna','��d�','Piotrkowska','114','10','90-006',
'1','100','n','725-100-96-90','1','ABC123123','umowa','notatka');
INSERT INTO OSOBY VALUES(GEN_ID(NUMERACJA_OSOBY,1),'Ericpol','','��d�','Targowa','81','','90-11','2','71','n','725-100-84-15',
'2','','','');
INSERT INTO SKLADNIKI VALUES(GEN_ID(NUMERACJA_SKLADNIKI,1),'Najem Lokalu mieszkalnego','73.20.11','225','0');
INSERT INTO SKLADNIKI VALUES(GEN_ID(NUMERACJA_SKLADNIKI,1),'Za zu�yt� wode','40.00.20','61.92','8');
INSERT INTO SKLADNIKI VALUES(GEN_ID(NUMERACJA_SKLADNIKI,1),'Wjazd i parkowanie','','100','23');
INSERT INTO SKLADNIKI VALUES(GEN_ID(NUMERACJA_SKLADNIKI,1),'Kr�tkotrwa�e zakwaterowanie','50.20.Z','2314.82','8');
INSERT INTO FAKTURY VALUES(GEN_ID(NUMERACJA_FAKTURY,1),'0','2014.03.10','1','Bilans','7 dni','15','15');
INSERT INTO FAKTURY VALUES(GEN_ID(NUMERACJA_FAKTURY,1),GEN_ID(NUMER_FAKTURY,1),'2014.04.10','1','Przelew','7 dni','3603.96',
'3471.14');
INSERT INTO FAKTURY VALUES(GEN_ID(NUMERACJA_FAKTURY,1),'0','2014.03.10','2','0','0','0','0');
INSERT INTO FAKTURY VALUES(GEN_ID(NUMERACJA_FAKTURY,1),GEN_ID(NUMER_FAKTURY,1),'2014.04.10','2','przelew','14 dni','2297.38',
'400');
INSERT INTO FAKTURY_SKLADNIKI VALUES(GEN_ID(NUMERACJA_FAKTURY_SKLADNIKI,1),'2','1','890');
INSERT INTO FAKTURY_SKLADNIKI VALUES(GEN_ID(NUMERACJA_FAKTURY_SKLADNIKI,1),'1','1','15');
INSERT INTO FAKTURY_SKLADNIKI VALUES(GEN_ID(NUMERACJA_FAKTURY_SKLADNIKI,1),'2','2','84.22');
INSERT INTO FAKTURY_SKLADNIKI VALUES(GEN_ID(NUMERACJA_FAKTURY_SKLADNIKI,1),'2','4','2314.82');
INSERT INTO FAKTURY_SKLADNIKI VALUES(GEN_ID(NUMERACJA_FAKTURY_SKLADNIKI,1),'2','3','100');
INSERT INTO WPLATY VALUES(GEN_ID(NUMERACJA_WPLATY,1),'1','20.04.2014','10.00','1.10','G','FV','10');
INSERT INTO WPLATY VALUES(GEN_ID(NUMERACJA_WPLATY,1),'1','22.04.2014','5','0.37','G','KP','kp-29/2014');
INSERT INTO WPLATY VALUES(GEN_ID(NUMERACJA_WPLATY,1),'2','15.04.2014','2740.38','259.62','G','KP','kp-29/2014');
INSERT INTO WPLATY VALUES(GEN_ID(NUMERACJA_WPLATY,1),'2','21.04.2014','730.76','69.24','G','KP','30/2014');
INSERT INTO DOSTAWCY VALUES(GEN_ID(NUMERACJA_DOSTAWCY,1),'Castorama sp. zoo.','market budowlany','Wroblewskiego 31'
,'725-14-33-645','608241774');
INSERT INTO DOSTAWCY VALUES(GEN_ID(NUMERACJA_DOSTAWCY,1),'Polskie G�rnictwo Naftowe','I Gazownictwo S.A.','02-305 Warszawa'
,'525-000-80-28','');
INSERT INTO ZAKUPY VALUES(GEN_ID(NUMERACJA_ZAKUPY,1),'1','5','12.50','1','2014.12.07','2014.12.27','2014.12.27','opis tego'
,'2014.10.12','G','12.50');
INSERT INTO ZAKUPY VALUES(GEN_ID(NUMERACJA_ZAKUPY,1),'2','2014/15','568.12','130.67','2014.12.25','2014.12.20','2014.12.28'
,'','2014.12.27','P','698.79');
INSERT INTO SKLADNIKI_Z VALUES(GEN_ID(NUMERACJA_SKLADNIKI_Z,1),'List polecony','1.50','8','3');
INSERT INTO SKLADNIKI_Z VALUES(GEN_ID(NUMERACJA_SKLADNIKI_Z,1),'M�otek','27.45','23','3');
INSERT INTO SKLADNIKI_Z VALUES(GEN_ID(NUMERACJA_SKLADNIKI_Z,1),'Za gaz CO w apartamencie','568.12','23','6');
INSERT INTO ZAKUPY_SKLADNIKI VALUES(GEN_ID(NUMERACJA_ZAKUPY_SKLADNIKI,1),'1','1','5','2.50');
INSERT INTO ZAKUPY_SKLADNIKI VALUES(GEN_ID(NUMERACJA_ZAKUPY_SKLADNIKI,1),'2','3','1','568.12');
INSERT INTO INNE_OBROTY VALUES(GEN_ID(NUMERACJA_INNE_OBROTY,1),'2014.01.09','kasa -> inne','1000.50','Pensja dla P. Eli');
INSERT INTO SALDA VALUES(GEN_ID(NUMERACJA_SALDA,1),'2013.12.31','-253.31','1569.50');

UPDATE DANE_SPRZEDAWCY SET nazwa_1 = '"Wynajem nieruchomo�ci', nazwa_2 = 'Marian Go�acki" s.c.', adres_1 = 'Piotrkowska 114'
,adres_2 = '90-006 ��d�', nip = '725-14-33-674', regon = '471478484', telefon = '042 632 05 93'
,Konto = '61 1020 3352 0000 1802 0099 5852', Logo = 'PIO114.png';

COMMIT;