﻿namespace Administracja.InneOkna
{
    partial class ZarzadzanieNowa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZarzadzanieNowa));
            this.nazwa_box = new System.Windows.Forms.TextBox();
            this.nazwa_lista_l = new System.Windows.Forms.Label();
            this.zapisz = new System.Windows.Forms.Button();
            this.anuluj = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // nazwa_box
            // 
            this.nazwa_box.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nazwa_box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwa_box.Location = new System.Drawing.Point(157, 12);
            this.nazwa_box.MaxLength = 16;
            this.nazwa_box.Name = "nazwa_box";
            this.nazwa_box.Size = new System.Drawing.Size(414, 26);
            this.nazwa_box.TabIndex = 1;
            this.nazwa_box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NazwaBoxKeyPress);
            // 
            // nazwa_lista_l
            // 
            this.nazwa_lista_l.AutoSize = true;
            this.nazwa_lista_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwa_lista_l.Location = new System.Drawing.Point(12, 15);
            this.nazwa_lista_l.Name = "nazwa_lista_l";
            this.nazwa_lista_l.Size = new System.Drawing.Size(139, 20);
            this.nazwa_lista_l.TabIndex = 120;
            this.nazwa_lista_l.Text = "Nazwa budynku:";
            // 
            // zapisz
            // 
            this.zapisz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(371, 46);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(200, 70);
            this.zapisz.TabIndex = 2;
            this.zapisz.Text = "Zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.ZapiszClick);
            // 
            // anuluj
            // 
            this.anuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.anuluj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anuluj.Location = new System.Drawing.Point(165, 45);
            this.anuluj.Name = "anuluj";
            this.anuluj.Size = new System.Drawing.Size(200, 70);
            this.anuluj.TabIndex = 3;
            this.anuluj.Text = "Anuluj";
            this.anuluj.UseVisualStyleBackColor = true;
            this.anuluj.Click += new System.EventHandler(this.AnulujClick);
            // 
            // ZarzadzanieNowa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(583, 128);
            this.Controls.Add(this.nazwa_box);
            this.Controls.Add(this.nazwa_lista_l);
            this.Controls.Add(this.zapisz);
            this.Controls.Add(this.anuluj);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ZarzadzanieNowa";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tworzenie nowego roku";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nazwa_box;
        private System.Windows.Forms.Label nazwa_lista_l;
        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.Button anuluj;
    }
}