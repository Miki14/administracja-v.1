﻿namespace Administracja.InneOkna
{
    partial class ZarzadzanieDodajIstniejacy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZarzadzanieDodajIstniejacy));
            this.wyborButt = new System.Windows.Forms.Button();
            this.zapiszButt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nazwaBox = new System.Windows.Forms.TextBox();
            this.sciezka_l = new System.Windows.Forms.Label();
            this.anulujButt = new System.Windows.Forms.Button();
            this.serverLokRad = new System.Windows.Forms.RadioButton();
            this.serverZdalnyRad = new System.Windows.Forms.RadioButton();
            this.serverBox = new System.Windows.Forms.TextBox();
            this.serverLab = new System.Windows.Forms.Label();
            this.sciezkaBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // wyborButt
            // 
            this.wyborButt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.wyborButt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wyborButt.Location = new System.Drawing.Point(7, 127);
            this.wyborButt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.wyborButt.Name = "wyborButt";
            this.wyborButt.Size = new System.Drawing.Size(200, 70);
            this.wyborButt.TabIndex = 0;
            this.wyborButt.Text = "Wybierz plik";
            this.wyborButt.UseVisualStyleBackColor = true;
            this.wyborButt.Click += new System.EventHandler(this.WyborClick);
            // 
            // zapiszButt
            // 
            this.zapiszButt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.zapiszButt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapiszButt.Location = new System.Drawing.Point(423, 127);
            this.zapiszButt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.zapiszButt.Name = "zapiszButt";
            this.zapiszButt.Size = new System.Drawing.Size(200, 70);
            this.zapiszButt.TabIndex = 8;
            this.zapiszButt.Text = "Zapisz";
            this.zapiszButt.UseVisualStyleBackColor = true;
            this.zapiszButt.Click += new System.EventHandler(this.ZapiszClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(13, 53);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 20);
            this.label1.TabIndex = 115;
            this.label1.Text = "Nazwa";
            // 
            // nazwaBox
            // 
            this.nazwaBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nazwaBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwaBox.Location = new System.Drawing.Point(79, 50);
            this.nazwaBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.nazwaBox.Name = "nazwaBox";
            this.nazwaBox.Size = new System.Drawing.Size(544, 26);
            this.nazwaBox.TabIndex = 4;
            // 
            // sciezka_l
            // 
            this.sciezka_l.AutoSize = true;
            this.sciezka_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sciezka_l.Location = new System.Drawing.Point(13, 93);
            this.sciezka_l.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.sciezka_l.Name = "sciezka_l";
            this.sciezka_l.Size = new System.Drawing.Size(144, 20);
            this.sciezka_l.TabIndex = 117;
            this.sciezka_l.Text = "Scieżka do bazy:";
            // 
            // anulujButt
            // 
            this.anulujButt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.anulujButt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anulujButt.Location = new System.Drawing.Point(215, 127);
            this.anulujButt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.anulujButt.Name = "anulujButt";
            this.anulujButt.Size = new System.Drawing.Size(200, 70);
            this.anulujButt.TabIndex = 7;
            this.anulujButt.Text = "Anuluj";
            this.anulujButt.UseVisualStyleBackColor = true;
            this.anulujButt.Click += new System.EventHandler(this.AnulujButtClick);
            // 
            // serverLokRad
            // 
            this.serverLokRad.AutoSize = true;
            this.serverLokRad.Checked = true;
            this.serverLokRad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.serverLokRad.Location = new System.Drawing.Point(13, 14);
            this.serverLokRad.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.serverLokRad.Name = "serverLokRad";
            this.serverLokRad.Size = new System.Drawing.Size(134, 24);
            this.serverLokRad.TabIndex = 1;
            this.serverLokRad.TabStop = true;
            this.serverLokRad.Text = "Lokalna baza";
            this.serverLokRad.UseVisualStyleBackColor = true;
            this.serverLokRad.CheckedChanged += new System.EventHandler(this.ServerRadCheckedChanged);
            // 
            // serverZdalnyRad
            // 
            this.serverZdalnyRad.AutoSize = true;
            this.serverZdalnyRad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.serverZdalnyRad.Location = new System.Drawing.Point(155, 14);
            this.serverZdalnyRad.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.serverZdalnyRad.Name = "serverZdalnyRad";
            this.serverZdalnyRad.Size = new System.Drawing.Size(126, 24);
            this.serverZdalnyRad.TabIndex = 2;
            this.serverZdalnyRad.Text = "Zdalna baza";
            this.serverZdalnyRad.UseVisualStyleBackColor = true;
            this.serverZdalnyRad.CheckedChanged += new System.EventHandler(this.ServerRadCheckedChanged);
            // 
            // serverBox
            // 
            this.serverBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serverBox.Enabled = false;
            this.serverBox.Location = new System.Drawing.Point(453, 13);
            this.serverBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.serverBox.Name = "serverBox";
            this.serverBox.Size = new System.Drawing.Size(170, 26);
            this.serverBox.TabIndex = 3;
            this.serverBox.Text = "localhost";
            // 
            // serverLab
            // 
            this.serverLab.AutoSize = true;
            this.serverLab.Enabled = false;
            this.serverLab.Location = new System.Drawing.Point(383, 15);
            this.serverLab.Name = "serverLab";
            this.serverLab.Size = new System.Drawing.Size(70, 20);
            this.serverLab.TabIndex = 123;
            this.serverLab.Text = "Serwer:";
            // 
            // sciezkaBox
            // 
            this.sciezkaBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sciezkaBox.Location = new System.Drawing.Point(155, 90);
            this.sciezkaBox.Name = "sciezkaBox";
            this.sciezkaBox.Size = new System.Drawing.Size(469, 26);
            this.sciezkaBox.TabIndex = 6;
            // 
            // ZarzadzanieDodajIstniejacy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 211);
            this.Controls.Add(this.sciezkaBox);
            this.Controls.Add(this.serverLab);
            this.Controls.Add(this.serverBox);
            this.Controls.Add(this.serverZdalnyRad);
            this.Controls.Add(this.serverLokRad);
            this.Controls.Add(this.anulujButt);
            this.Controls.Add(this.sciezka_l);
            this.Controls.Add(this.nazwaBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.zapiszButt);
            this.Controls.Add(this.wyborButt);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(2000, 250);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(652, 250);
            this.Name = "ZarzadzanieDodajIstniejacy";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dodaj istniejącą bazę";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button wyborButt;
        private System.Windows.Forms.Button zapiszButt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nazwaBox;
        private System.Windows.Forms.Label sciezka_l;
        private System.Windows.Forms.Button anulujButt;
        private System.Windows.Forms.RadioButton serverLokRad;
        private System.Windows.Forms.RadioButton serverZdalnyRad;
        private System.Windows.Forms.TextBox serverBox;
        private System.Windows.Forms.Label serverLab;
        private System.Windows.Forms.TextBox sciezkaBox;
    }
}