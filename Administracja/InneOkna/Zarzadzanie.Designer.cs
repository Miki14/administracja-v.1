﻿namespace Administracja.InneOkna
{
    partial class Zarzadzanie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.zapisz = new System.Windows.Forms.Button();
            this.usun = new System.Windows.Forms.Button();
            this.lista = new System.Windows.Forms.ListView();
            this.id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.nazwaBudynku = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.nazwaPliku = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.serwer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.istniejaca = new System.Windows.Forms.Button();
            this.nowa = new System.Windows.Forms.Button();
            this.folder = new System.Windows.Forms.Button();
            this.kopia = new System.Windows.Forms.Button();
            this.edytuj = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // zapisz
            // 
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(1148, 578);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(180, 70);
            this.zapisz.TabIndex = 108;
            this.zapisz.Text = "Zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.ZapiszClick);
            // 
            // usun
            // 
            this.usun.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usun.Location = new System.Drawing.Point(962, 578);
            this.usun.Name = "usun";
            this.usun.Size = new System.Drawing.Size(180, 70);
            this.usun.TabIndex = 107;
            this.usun.Text = "Usuń budynek z listy";
            this.usun.UseVisualStyleBackColor = true;
            this.usun.Click += new System.EventHandler(this.UsunClick);
            // 
            // lista
            // 
            this.lista.CheckBoxes = true;
            this.lista.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.id,
            this.nazwaBudynku,
            this.nazwaPliku,
            this.serwer});
            this.lista.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lista.FullRowSelect = true;
            this.lista.GridLines = true;
            this.lista.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lista.HideSelection = false;
            this.lista.Location = new System.Drawing.Point(12, 12);
            this.lista.MultiSelect = false;
            this.lista.Name = "lista";
            this.lista.Size = new System.Drawing.Size(1316, 560);
            this.lista.TabIndex = 106;
            this.lista.UseCompatibleStateImageBehavior = false;
            this.lista.View = System.Windows.Forms.View.Details;
            this.lista.SelectedIndexChanged += new System.EventHandler(this.ListaSelectedIndexChanged);
            // 
            // id
            // 
            this.id.Text = "ID";
            // 
            // nazwaBudynku
            // 
            this.nazwaBudynku.Text = "Nazwa budynku";
            this.nazwaBudynku.Width = 196;
            // 
            // nazwaPliku
            // 
            this.nazwaPliku.Text = "Nazwa pliku";
            this.nazwaPliku.Width = 819;
            // 
            // serwer
            // 
            this.serwer.Text = "Serwer";
            this.serwer.Width = 169;
            // 
            // istniejaca
            // 
            this.istniejaca.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.istniejaca.Location = new System.Drawing.Point(776, 578);
            this.istniejaca.Name = "istniejaca";
            this.istniejaca.Size = new System.Drawing.Size(180, 70);
            this.istniejaca.TabIndex = 109;
            this.istniejaca.Text = "Podłącz istniejący budynek";
            this.istniejaca.UseVisualStyleBackColor = true;
            this.istniejaca.Click += new System.EventHandler(this.IstniejacaClick);
            // 
            // nowa
            // 
            this.nowa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nowa.Location = new System.Drawing.Point(590, 578);
            this.nowa.Name = "nowa";
            this.nowa.Size = new System.Drawing.Size(180, 70);
            this.nowa.TabIndex = 110;
            this.nowa.Text = "Nowy budynek";
            this.nowa.UseVisualStyleBackColor = true;
            this.nowa.Click += new System.EventHandler(this.NowaClick);
            // 
            // folder
            // 
            this.folder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.folder.Location = new System.Drawing.Point(12, 578);
            this.folder.Name = "folder";
            this.folder.Size = new System.Drawing.Size(180, 70);
            this.folder.TabIndex = 111;
            this.folder.Text = "Folder z plikami baz";
            this.folder.UseVisualStyleBackColor = true;
            this.folder.Click += new System.EventHandler(this.FolderClick);
            // 
            // kopia
            // 
            this.kopia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.kopia.Location = new System.Drawing.Point(218, 578);
            this.kopia.Name = "kopia";
            this.kopia.Size = new System.Drawing.Size(180, 70);
            this.kopia.TabIndex = 112;
            this.kopia.Text = "Kopia zapasowa";
            this.kopia.UseVisualStyleBackColor = true;
            this.kopia.Click += new System.EventHandler(this.KopiaClick);
            // 
            // edytuj
            // 
            this.edytuj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.edytuj.Location = new System.Drawing.Point(404, 578);
            this.edytuj.Name = "edytuj";
            this.edytuj.Size = new System.Drawing.Size(180, 70);
            this.edytuj.TabIndex = 113;
            this.edytuj.Text = "Edytuj budynek";
            this.edytuj.UseVisualStyleBackColor = true;
            this.edytuj.Click += new System.EventHandler(this.EdytujClick);
            // 
            // Zarzadzanie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1340, 660);
            this.ControlBox = false;
            this.Controls.Add(this.edytuj);
            this.Controls.Add(this.kopia);
            this.Controls.Add(this.folder);
            this.Controls.Add(this.nowa);
            this.Controls.Add(this.istniejaca);
            this.Controls.Add(this.zapisz);
            this.Controls.Add(this.usun);
            this.Controls.Add(this.lista);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Zarzadzanie";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zarzadzanie";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.Button usun;
        private System.Windows.Forms.ListView lista;
        private System.Windows.Forms.ColumnHeader nazwaBudynku;
        private System.Windows.Forms.ColumnHeader nazwaPliku;
        private System.Windows.Forms.Button istniejaca;
        private System.Windows.Forms.Button nowa;
        private System.Windows.Forms.ColumnHeader id;
        private System.Windows.Forms.Button folder;
        private System.Windows.Forms.Button kopia;
        private System.Windows.Forms.ColumnHeader serwer;
        private System.Windows.Forms.Button edytuj;
    }
}