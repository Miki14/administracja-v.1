﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Administracja.InneOkna
{
    public partial class Sprzedawca : Form
    {
        private string _sciezka;

        public Sprzedawca()
        {
            InitializeComponent();
            Wyswietl();
        }

        private void Wyswietl()
        {
            var reader1D = Baza.Wczytaj1D("SELECT Nazwa_1, Nazwa_2, Adres_1, Adres_2, Nip, Regon, Telefon, Konto, Logo, Numer_FV FROM DANE_SPRZEDAWCY");

            nazwa1Box.Text = reader1D[0];
            nazwa2Box.Text = reader1D[1];
            adresBox.Text = reader1D[2];
            adresCdBox.Text = reader1D[3];
            nipBox.Text = reader1D[4];
            regonBox.Text = reader1D[5];
            telefonBox.Text = reader1D[6];
            kontoBox.Text = reader1D[7];
            _sciezka = reader1D[8];
            numerFvBox.Text = reader1D[9];

            folder_l.Text = "Logo: " + _sciezka;

            oprocOdsetkiNum.Text = Baza.ModelData.Interest.ToString();
        }

        private void ZapiszClick(object sender, EventArgs e)
        {
            Baza.Zapisz("UPDATE DANE_SPRZEDAWCY SET "
                    + "nazwa_1 = '" + nazwa1Box.Text
                    + "', nazwa_2 = '" + nazwa2Box.Text
                    + "', adres_1 = '" + adresBox.Text
                    + "', adres_2 = '" + adresCdBox.Text
                    + "', nip = '" + nipBox.Text
                    + "', regon = '" + regonBox.Text
                    + "', telefon = '" + telefonBox.Text
                    + "', Konto = '" + kontoBox.Text
                    + "', Logo = '" + _sciezka
                    + "', Numer_FV = '" + numerFvBox.Text
                    + "';");

            Baza.ModelData.Interest = Convert.ToDouble(oprocOdsetkiNum.Text.Replace(".", ","));

            Close();
        }

        private void AnulujClick(object sender, EventArgs e)
        {
            Close();
        }

        private void WyborClick(object sender, EventArgs e)
        {
            var okienko = new OpenFileDialog
            {
                Filter = "Image Files(*.BMP;*.JPG;*.JPEG;*.PNG)|*.BMP;*.JPG;*.JPEG;*.PNG|All Files (*.*)|*.*",
                InitialDirectory = Application.StartupPath + "\\Loga"
            };

            if (okienko.ShowDialog() == DialogResult.OK)
            {
                _sciezka = new FileInfo(okienko.FileName).Name;
                folder_l.Text = "Logo: " + _sciezka;
            }
        }

        private void CheckKeys(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                ZapiszClick(this, EventArgs.Empty);
                e.KeyChar = (char)0;
                e.Handled = true;
            }
        }
    }
}
