﻿namespace Administracja.InneOkna
{
    partial class ZarzadzanieKopia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZarzadzanieKopia));
            this.utworz = new System.Windows.Forms.Button();
            this.wybor = new System.Windows.Forms.Button();
            this.folder_l = new System.Windows.Forms.Label();
            this.Kopiowanie = new System.ComponentModel.BackgroundWorker();
            this.trwa_zapis = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // utworz
            // 
            this.utworz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.utworz.Location = new System.Drawing.Point(391, 99);
            this.utworz.Name = "utworz";
            this.utworz.Size = new System.Drawing.Size(200, 70);
            this.utworz.TabIndex = 121;
            this.utworz.Text = "Utwórz kopię";
            this.utworz.UseVisualStyleBackColor = true;
            this.utworz.Click += new System.EventHandler(this.UtworzClick);
            // 
            // wybor
            // 
            this.wybor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wybor.Location = new System.Drawing.Point(185, 99);
            this.wybor.Name = "wybor";
            this.wybor.Size = new System.Drawing.Size(200, 70);
            this.wybor.TabIndex = 122;
            this.wybor.Text = "Wybierz miejsce zapisu";
            this.wybor.UseVisualStyleBackColor = true;
            this.wybor.Click += new System.EventHandler(this.WyborClick);
            // 
            // folder_l
            // 
            this.folder_l.AutoSize = true;
            this.folder_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.folder_l.Location = new System.Drawing.Point(12, 20);
            this.folder_l.Name = "folder_l";
            this.folder_l.Size = new System.Drawing.Size(195, 20);
            this.folder_l.TabIndex = 125;
            this.folder_l.Text = "Wybrany folder zapisu :";
            // 
            // Kopiowanie
            // 
            this.Kopiowanie.WorkerReportsProgress = true;
            this.Kopiowanie.DoWork += new System.ComponentModel.DoWorkEventHandler(this.KopiowanieDoWork);
            this.Kopiowanie.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.KopiowanieProgressChanged);
            // 
            // trwa_zapis
            // 
            this.trwa_zapis.AutoSize = true;
            this.trwa_zapis.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.trwa_zapis.ForeColor = System.Drawing.Color.Red;
            this.trwa_zapis.Location = new System.Drawing.Point(12, 58);
            this.trwa_zapis.Name = "trwa_zapis";
            this.trwa_zapis.Size = new System.Drawing.Size(20, 20);
            this.trwa_zapis.TabIndex = 126;
            this.trwa_zapis.Text = "Z";
            // 
            // ZarzadzanieKopia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 181);
            this.Controls.Add(this.trwa_zapis);
            this.Controls.Add(this.folder_l);
            this.Controls.Add(this.wybor);
            this.Controls.Add(this.utworz);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ZarzadzanieKopia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kopia zapasowa";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button utworz;
        private System.Windows.Forms.Button wybor;
        private System.Windows.Forms.Label folder_l;
        private System.ComponentModel.BackgroundWorker Kopiowanie;
        private System.Windows.Forms.Label trwa_zapis;
    }
}