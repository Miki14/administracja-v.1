﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Administracja.InneOkna
{
    public partial class ZarzadzanieKopia : Form
    {
        private bool _fWybrano;
        private bool _fPlikIstnieje;
        private string _sciezka;

        public ZarzadzanieKopia()
        {
            InitializeComponent();
            trwa_zapis.Text = "";
        }

        private void UtworzClick(object sender, EventArgs e)
        {
            _fPlikIstnieje = false;

            if (!_fWybrano)
            {
                Wyjatki.Komunikat("Nie wybrano folderu zapisu", "Wybierz folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Baza.CloseConnection();
                trwa_zapis.Text = "Trwa zapis, proszę czekać.";
                Kopiowanie.RunWorkerAsync();
                Baza.OpenConnection();
            }
        }

        private void WyborClick(object sender, EventArgs e)
        {
            var okienko = new FolderBrowserDialog
            {
                RootFolder = Environment.SpecialFolder.MyComputer,
                Description = "Wybierz folder gdzie chcesz utworzyć kopię zapsaową.\nNie może on zawierać plików o takiej samej nazwie jak te, które zamierzasz kopiować."
            };
            if (okienko.ShowDialog() == DialogResult.OK)
            {
                _sciezka = okienko.SelectedPath;
                folder_l.Text = "Wybrany folder zapisu : " + _sciezka;
                _fWybrano = true;
            }
        }

        private void KopiowanieDoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                string[] files = Directory.GetFiles("Baza");
                foreach (string file in files)
                {
                    File.Copy(file, Path.Combine(_sciezka, Path.GetFileName(file)));
                }
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
                _fPlikIstnieje = true;
            }

            if (!_fPlikIstnieje)
            {
                Kopiowanie.ReportProgress(100);
            }

            Kopiowanie.Dispose();
        }

        private void KopiowanieProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            trwa_zapis.Text = "";
            Wyjatki.Komunikat("Kopia wykonana pomyślnie", "Wykonano kopię", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Close();
        }
    }
}
