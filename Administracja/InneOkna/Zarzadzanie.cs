﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Administracja.InneOkna
{
    public partial class Zarzadzanie : Form
    {
        private int _idB;
        private string _nazwaPliku = "";

        public Zarzadzanie()
        {
            InitializeComponent();
            Wyswietl();
        }

        private void Wyswietl()
        {
            int zaznaczony = 0;

            if (lista.Items.Count > 0)
            {
                zaznaczony = lista.SelectedItems[0].Index;
            }

            lista.Items.Clear();

            var listaLat = from x in Baza.ModelData.BazaJson
                           orderby x.Name
                           select x;

            foreach (var rok in listaLat)
            {
                ListViewItem element = lista.Items.Add(rok.Id.ToString());
                element.SubItems.Add(rok.Name);
                element.SubItems.Add(rok.Path);
                element.Checked = rok.Show == "T";
                element.SubItems.Add(rok.Server);
            }

            if (lista.Items.Count == zaznaczony) zaznaczony--;

            if (lista.Items.Count > zaznaczony)
            {
                lista.Items[zaznaczony].Selected = true;
            }
            else if (lista.Items.Count > 0)
            {
                lista.Items[0].Selected = true;
            }
        }

        private void Zapisz()
        {
            foreach (ListViewItem item in lista.Items)
            {
                var rok = Baza.ModelData.BazaJson.FirstOrDefault(x => x.Id == Convert.ToInt32(item.Text));

                rok.Show = item.Checked ? "T" : "N";
            }
        }

        private void NowaClick(object sender, EventArgs e)
        {
            Zapisz();
            new ZarzadzanieNowa().ShowDialog();
            Wyswietl();
        }

        private void EdytujClick(object sender, EventArgs e)
        {
            Zapisz();
            new ZarzadzanieDodajIstniejacy(_idB).ShowDialog();
            Wyswietl();
        }

        private void IstniejacaClick(object sender, EventArgs e)
        {
            Zapisz();
            new ZarzadzanieDodajIstniejacy().ShowDialog();
            Wyswietl();
        }

        private void UsunClick(object sender, EventArgs e)
        {
            if (Wyjatki.Komunikat("Czy napewne chcesz usunąć \"" + lista.SelectedItems[0].SubItems[1].Text + "\" z listy? Pliki na dysku zostaną zachowane.", "Usunać?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Baza.ModelData.BazaJson.RemoveAll(x => x.Id == _idB);
            }

            Wyswietl();
        }

        private void ZapiszClick(object sender, EventArgs e)
        {
            Zapisz();
            Close();
        }

        private void FolderClick(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("Baza", "/max");
        }

        private void ListaSelectedIndexChanged(object sender, EventArgs e)
        {
            int index;

            try
            {
                index = lista.SelectedItems[0].Index;
            }
            catch
            {
                index = 0;
            }

            _idB = int.Parse(lista.Items[index].Text);

            try
            {
                _nazwaPliku = lista.SelectedItems[0].SubItems[2].Text;
            }
            catch
            {
                _nazwaPliku = lista.Items[0].SubItems[2].Text;
            }
        }

        private void KopiaClick(object sender, EventArgs e)
        {
            new ZarzadzanieKopia().ShowDialog();

            Wyswietl();
        }
    }
}