﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Administracja.InneOkna
{
    public partial class Aktualizacja : Form
    {
        private readonly AktualizacjaLogika _logika;

        public Aktualizacja()
        {
            InitializeComponent();

            _logika = new AktualizacjaLogika(this);


            aktVer.Text = "Aktualna wersja: " + _logika.Aktualna;
            posVer.Text = "Posiadana wersja: " + _logika.AppVersion;

            if (_logika.Aktualna.CompareTo(_logika.AppVersion) <= 0)
            {
                stan.ForeColor = Color.Green;
                stan.Text = "Posiadasz najnowszą\nwersję.";
            }
            else
            {
                stan.ForeColor = Color.Red;
                stan.Text = "Jest dostępna nowsza\nwersja";
            }
        }

        public void OdswiezPasek(int proc)
        {
            pasek.Invoke(new Action(delegate
            {
                pasek.Value = proc;
            }));
        }

        public void AktualizacjaZaczeta()
        {
            groupBoxPasek.Enabled = true;
            stan.Text = "Pobieram aktualizacje";
        }

        private void ZamknijClick(object sender, EventArgs e)
        {
            try
            {
                _logika.Watek.Abort();
            }
            catch
            {
                Raport.Instance.Raportuj("Przerwano aktualizację.");
            }

            Close();
        }

        private void AktualizujClick(object sender, EventArgs e)
        {
            _logika.Aktualizuj();
        }
    }
}
