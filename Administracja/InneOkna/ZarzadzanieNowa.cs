﻿using System;
using System.IO;
using System.Windows.Forms;
using Administracja.Model;

namespace Administracja.InneOkna
{
    public partial class ZarzadzanieNowa : Form
    {
        public ZarzadzanieNowa()
        {
            InitializeComponent();
        }

        private void AnulujClick(object sender, EventArgs e)
        {
            Close();
        }

        private void ZapiszClick(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(nazwa_box.Text))
            {
                Wyjatki.Komunikat("Musisz podać nazwę pliku.", "Błąd nazwy pliku", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (File.Exists("Baza\\" + nazwa_box.Text + ".FDB"))
            {
                Wyjatki.Komunikat("Rok o podanej nazwie już istnieje, wybierz inną.", "Błąd nazwy pliku", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (string.Compare(nazwa_box.Text, "PUSTA", true) == 0)
            {
                Wyjatki.Komunikat("Baza o tej nazwie jest bazą systemową i nie możesz zapisać nowego roku pod tą nazwą.", "Błąd nazwy pliku", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                File.Copy("Baza//PUSTA.FDB", "Baza//" + nazwa_box.Text + ".FDB");

                Baza.ModelData.BazaJson.Add(new BazaJson(nazwa_box.Text,
                    Application.StartupPath + @"\Baza\" + nazwa_box.Text + ".FDB", "T", "localhost"));

                Close();
            }
        }

        private void NazwaBoxKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                ZapiszClick(this, EventArgs.Empty);
                e.KeyChar = '0';
            }
        }
    }
}
