﻿namespace Administracja.InneOkna
{
    partial class Aktualizacja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.zamknij = new System.Windows.Forms.Button();
            this.aktualizuj = new System.Windows.Forms.Button();
            this.pasek = new System.Windows.Forms.ProgressBar();
            this.posVer = new System.Windows.Forms.Label();
            this.groupBoxPasek = new System.Windows.Forms.GroupBox();
            this.aktVer = new System.Windows.Forms.Label();
            this.stan = new System.Windows.Forms.Label();
            this.groupBoxPasek.SuspendLayout();
            this.SuspendLayout();
            // 
            // zamknij
            // 
            this.zamknij.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zamknij.Location = new System.Drawing.Point(12, 105);
            this.zamknij.Name = "zamknij";
            this.zamknij.Size = new System.Drawing.Size(200, 70);
            this.zamknij.TabIndex = 1;
            this.zamknij.Text = "Anuluj";
            this.zamknij.UseVisualStyleBackColor = true;
            this.zamknij.Click += new System.EventHandler(this.ZamknijClick);
            // 
            // aktualizuj
            // 
            this.aktualizuj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.aktualizuj.Location = new System.Drawing.Point(218, 105);
            this.aktualizuj.Name = "aktualizuj";
            this.aktualizuj.Size = new System.Drawing.Size(200, 70);
            this.aktualizuj.TabIndex = 0;
            this.aktualizuj.Text = "Aktualizuj";
            this.aktualizuj.UseVisualStyleBackColor = true;
            this.aktualizuj.Click += new System.EventHandler(this.AktualizujClick);
            // 
            // pasek
            // 
            this.pasek.Location = new System.Drawing.Point(6, 22);
            this.pasek.Name = "pasek";
            this.pasek.Size = new System.Drawing.Size(194, 23);
            this.pasek.TabIndex = 3;
            // 
            // posVer
            // 
            this.posVer.AutoSize = true;
            this.posVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.posVer.Location = new System.Drawing.Point(12, 9);
            this.posVer.Name = "posVer";
            this.posVer.Size = new System.Drawing.Size(163, 20);
            this.posVer.TabIndex = 4;
            this.posVer.Text = "Posiadana wersja: ?.?";
            // 
            // groupBoxPasek
            // 
            this.groupBoxPasek.Controls.Add(this.pasek);
            this.groupBoxPasek.Enabled = false;
            this.groupBoxPasek.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBoxPasek.Location = new System.Drawing.Point(12, 41);
            this.groupBoxPasek.Name = "groupBoxPasek";
            this.groupBoxPasek.Size = new System.Drawing.Size(206, 58);
            this.groupBoxPasek.TabIndex = 5;
            this.groupBoxPasek.TabStop = false;
            this.groupBoxPasek.Text = "Postęp aktualizacji";
            // 
            // aktVer
            // 
            this.aktVer.AutoSize = true;
            this.aktVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.aktVer.Location = new System.Drawing.Point(212, 9);
            this.aktVer.Name = "aktVer";
            this.aktVer.Size = new System.Drawing.Size(151, 20);
            this.aktVer.TabIndex = 6;
            this.aktVer.Text = "Aktualna wersja: ?.?";
            // 
            // stan
            // 
            this.stan.AutoSize = true;
            this.stan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.stan.Location = new System.Drawing.Point(224, 64);
            this.stan.Name = "stan";
            this.stan.Size = new System.Drawing.Size(63, 20);
            this.stan.TabIndex = 7;
            this.stan.Text = "Gotowy";
            // 
            // Aktualizacja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 187);
            this.ControlBox = false;
            this.Controls.Add(this.stan);
            this.Controls.Add(this.aktVer);
            this.Controls.Add(this.groupBoxPasek);
            this.Controls.Add(this.posVer);
            this.Controls.Add(this.aktualizuj);
            this.Controls.Add(this.zamknij);
            this.Name = "Aktualizacja";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aktualizacja";
            this.groupBoxPasek.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button zamknij;
        private System.Windows.Forms.Button aktualizuj;
        private System.Windows.Forms.Label posVer;
        private System.Windows.Forms.GroupBox groupBoxPasek;
        private System.Windows.Forms.Label aktVer;
        private System.Windows.Forms.Label stan;
        private System.Windows.Forms.ProgressBar pasek;
    }
}