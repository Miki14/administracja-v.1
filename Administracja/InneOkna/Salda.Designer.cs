﻿namespace Administracja.InneOkna
{
    partial class Salda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Salda));
            this.zapisz = new System.Windows.Forms.Button();
            this.anuluj = new System.Windows.Forms.Button();
            this.drukuj = new System.Windows.Forms.Button();
            this.podglad = new System.Windows.Forms.Button();
            this.data_od = new System.Windows.Forms.DateTimePicker();
            this.dataOdGroup = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.data_do = new System.Windows.Forms.DateTimePicker();
            this.poprzednie_saldo_ch = new System.Windows.Forms.CheckBox();
            this.stary_stan_konta = new System.Windows.Forms.Label();
            this.stary_stan_kasy = new System.Windows.Forms.Label();
            this.wplaty_konto = new System.Windows.Forms.Label();
            this.wyplaty_konto = new System.Windows.Forms.Label();
            this.saldo_konta = new System.Windows.Forms.Label();
            this.wplaty_kasa = new System.Windows.Forms.Label();
            this.wyplaty_kasa = new System.Windows.Forms.Label();
            this.saldo_kasy = new System.Windows.Forms.Label();
            this.saldo_konto_kasa = new System.Windows.Forms.Label();
            this.usun = new System.Windows.Forms.Button();
            this.podglad_dia = new System.Windows.Forms.PrintPreviewDialog();
            this.print_dia = new System.Windows.Forms.PrintDialog();
            this.konto_label = new System.Windows.Forms.Label();
            this.kasa_label = new System.Windows.Forms.Label();
            this.jedenMiesiacCh = new System.Windows.Forms.CheckBox();
            this.wyplaty_kasa_zakupy = new System.Windows.Forms.Label();
            this.wyplaty_konto_zakupy = new System.Windows.Forms.Label();
            this.wyplaty_kasa_inneObroty = new System.Windows.Forms.Label();
            this.wyplaty_konto_inneObroty = new System.Windows.Forms.Label();
            this.wplaty_kasa_inneObroty = new System.Windows.Forms.Label();
            this.wplaty_konto_inneObroty = new System.Windows.Forms.Label();
            this.wplaty_kasa_fv = new System.Windows.Forms.Label();
            this.wplaty_konto_fv = new System.Windows.Forms.Label();
            this.dataOdGroup.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // zapisz
            // 
            this.zapisz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapisz.Location = new System.Drawing.Point(1128, 578);
            this.zapisz.Name = "zapisz";
            this.zapisz.Size = new System.Drawing.Size(200, 70);
            this.zapisz.TabIndex = 31;
            this.zapisz.Text = "Zapisz";
            this.zapisz.UseVisualStyleBackColor = true;
            this.zapisz.Click += new System.EventHandler(this.ZapiszClick);
            // 
            // anuluj
            // 
            this.anuluj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anuluj.Location = new System.Drawing.Point(922, 578);
            this.anuluj.Name = "anuluj";
            this.anuluj.Size = new System.Drawing.Size(200, 70);
            this.anuluj.TabIndex = 32;
            this.anuluj.Text = "Anuluj";
            this.anuluj.UseVisualStyleBackColor = true;
            this.anuluj.Click += new System.EventHandler(this.AnulujClick);
            // 
            // drukuj
            // 
            this.drukuj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.drukuj.Location = new System.Drawing.Point(716, 578);
            this.drukuj.Name = "drukuj";
            this.drukuj.Size = new System.Drawing.Size(200, 70);
            this.drukuj.TabIndex = 33;
            this.drukuj.Text = "Drukuj";
            this.drukuj.UseVisualStyleBackColor = true;
            this.drukuj.Click += new System.EventHandler(this.DrukujClick);
            // 
            // podglad
            // 
            this.podglad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.podglad.Location = new System.Drawing.Point(510, 578);
            this.podglad.Name = "podglad";
            this.podglad.Size = new System.Drawing.Size(200, 70);
            this.podglad.TabIndex = 34;
            this.podglad.Text = "Podgląd";
            this.podglad.UseVisualStyleBackColor = true;
            this.podglad.Click += new System.EventHandler(this.PodgladClick);
            // 
            // data_od
            // 
            this.data_od.CustomFormat = "dd MMMM yyyy";
            this.data_od.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.data_od.Location = new System.Drawing.Point(6, 25);
            this.data_od.Name = "data_od";
            this.data_od.Size = new System.Drawing.Size(200, 30);
            this.data_od.TabIndex = 35;
            this.data_od.ValueChanged += new System.EventHandler(this.DataOdValueChanged);
            // 
            // dataOdGroup
            // 
            this.dataOdGroup.Controls.Add(this.data_od);
            this.dataOdGroup.Location = new System.Drawing.Point(12, 50);
            this.dataOdGroup.Name = "dataOdGroup";
            this.dataOdGroup.Size = new System.Drawing.Size(214, 68);
            this.dataOdGroup.TabIndex = 36;
            this.dataOdGroup.TabStop = false;
            this.dataOdGroup.Text = "Data od";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.data_do);
            this.groupBox2.Location = new System.Drawing.Point(12, 124);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(214, 68);
            this.groupBox2.TabIndex = 37;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data do";
            // 
            // data_do
            // 
            this.data_do.CustomFormat = "dd MMMM yyyy";
            this.data_do.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.data_do.Location = new System.Drawing.Point(6, 25);
            this.data_do.Name = "data_do";
            this.data_do.Size = new System.Drawing.Size(200, 30);
            this.data_do.TabIndex = 35;
            this.data_do.ValueChanged += new System.EventHandler(this.WyswietlHandler);
            // 
            // poprzednie_saldo_ch
            // 
            this.poprzednie_saldo_ch.AutoSize = true;
            this.poprzednie_saldo_ch.Checked = true;
            this.poprzednie_saldo_ch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.poprzednie_saldo_ch.Location = new System.Drawing.Point(12, 221);
            this.poprzednie_saldo_ch.Name = "poprzednie_saldo_ch";
            this.poprzednie_saldo_ch.Size = new System.Drawing.Size(253, 29);
            this.poprzednie_saldo_ch.TabIndex = 38;
            this.poprzednie_saldo_ch.Text = "Uwzględnij ostatnie saldo";
            this.poprzednie_saldo_ch.UseVisualStyleBackColor = true;
            this.poprzednie_saldo_ch.CheckedChanged += new System.EventHandler(this.WyswietlHandler);
            // 
            // stary_stan_konta
            // 
            this.stary_stan_konta.AutoSize = true;
            this.stary_stan_konta.Location = new System.Drawing.Point(505, 96);
            this.stary_stan_konta.Name = "stary_stan_konta";
            this.stary_stan_konta.Size = new System.Drawing.Size(161, 25);
            this.stary_stan_konta.TabIndex = 39;
            this.stary_stan_konta.Text = "stary_stan_konta";
            // 
            // stary_stan_kasy
            // 
            this.stary_stan_kasy.AutoSize = true;
            this.stary_stan_kasy.Location = new System.Drawing.Point(969, 96);
            this.stary_stan_kasy.Name = "stary_stan_kasy";
            this.stary_stan_kasy.Size = new System.Drawing.Size(154, 25);
            this.stary_stan_kasy.TabIndex = 40;
            this.stary_stan_kasy.Text = "stary_stan_kasy";
            // 
            // wplaty_konto
            // 
            this.wplaty_konto.AutoSize = true;
            this.wplaty_konto.Location = new System.Drawing.Point(505, 163);
            this.wplaty_konto.Name = "wplaty_konto";
            this.wplaty_konto.Size = new System.Drawing.Size(126, 25);
            this.wplaty_konto.TabIndex = 39;
            this.wplaty_konto.Text = "wplaty_konto";
            // 
            // wyplaty_konto
            // 
            this.wyplaty_konto.AutoSize = true;
            this.wyplaty_konto.Location = new System.Drawing.Point(505, 275);
            this.wyplaty_konto.Name = "wyplaty_konto";
            this.wyplaty_konto.Size = new System.Drawing.Size(136, 25);
            this.wyplaty_konto.TabIndex = 39;
            this.wyplaty_konto.Text = "wyplaty_konto";
            // 
            // saldo_konta
            // 
            this.saldo_konta.AutoSize = true;
            this.saldo_konta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.saldo_konta.Location = new System.Drawing.Point(505, 410);
            this.saldo_konta.Name = "saldo_konta";
            this.saldo_konta.Size = new System.Drawing.Size(129, 25);
            this.saldo_konta.TabIndex = 39;
            this.saldo_konta.Text = "saldo_konta";
            // 
            // wplaty_kasa
            // 
            this.wplaty_kasa.AutoSize = true;
            this.wplaty_kasa.Location = new System.Drawing.Point(969, 158);
            this.wplaty_kasa.Name = "wplaty_kasa";
            this.wplaty_kasa.Size = new System.Drawing.Size(120, 25);
            this.wplaty_kasa.TabIndex = 40;
            this.wplaty_kasa.Text = "wplaty_kasa";
            // 
            // wyplaty_kasa
            // 
            this.wyplaty_kasa.AutoSize = true;
            this.wyplaty_kasa.Location = new System.Drawing.Point(969, 275);
            this.wyplaty_kasa.Name = "wyplaty_kasa";
            this.wyplaty_kasa.Size = new System.Drawing.Size(130, 25);
            this.wyplaty_kasa.TabIndex = 40;
            this.wyplaty_kasa.Text = "wyplaty_kasa";
            // 
            // saldo_kasy
            // 
            this.saldo_kasy.AutoSize = true;
            this.saldo_kasy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.saldo_kasy.Location = new System.Drawing.Point(969, 410);
            this.saldo_kasy.Name = "saldo_kasy";
            this.saldo_kasy.Size = new System.Drawing.Size(121, 25);
            this.saldo_kasy.TabIndex = 40;
            this.saldo_kasy.Text = "saldo_kasy";
            // 
            // saldo_konto_kasa
            // 
            this.saldo_konto_kasa.AutoSize = true;
            this.saldo_konto_kasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.saldo_konto_kasa.Location = new System.Drawing.Point(731, 492);
            this.saldo_konto_kasa.Name = "saldo_konto_kasa";
            this.saldo_konto_kasa.Size = new System.Drawing.Size(187, 25);
            this.saldo_konto_kasa.TabIndex = 40;
            this.saldo_konto_kasa.Text = "saldo_konto_kasa";
            // 
            // usun
            // 
            this.usun.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usun.Location = new System.Drawing.Point(12, 578);
            this.usun.Name = "usun";
            this.usun.Size = new System.Drawing.Size(200, 70);
            this.usun.TabIndex = 41;
            this.usun.Text = "Usuń ostatnio zapisane saldo";
            this.usun.UseVisualStyleBackColor = true;
            this.usun.Click += new System.EventHandler(this.UsunClick);
            // 
            // podglad_dia
            // 
            this.podglad_dia.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.podglad_dia.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.podglad_dia.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.podglad_dia.ClientSize = new System.Drawing.Size(400, 300);
            this.podglad_dia.Enabled = true;
            this.podglad_dia.Icon = ((System.Drawing.Icon)(resources.GetObject("podglad_dia.Icon")));
            this.podglad_dia.Name = "printPreviewDialog1";
            this.podglad_dia.Visible = false;
            // 
            // print_dia
            // 
            this.print_dia.UseEXDialog = true;
            // 
            // konto_label
            // 
            this.konto_label.AutoSize = true;
            this.konto_label.Location = new System.Drawing.Point(576, 36);
            this.konto_label.Name = "konto_label";
            this.konto_label.Size = new System.Drawing.Size(64, 25);
            this.konto_label.TabIndex = 42;
            this.konto_label.Text = "Konto";
            // 
            // kasa_label
            // 
            this.kasa_label.AutoSize = true;
            this.kasa_label.Location = new System.Drawing.Point(1014, 36);
            this.kasa_label.Name = "kasa_label";
            this.kasa_label.Size = new System.Drawing.Size(58, 25);
            this.kasa_label.TabIndex = 43;
            this.kasa_label.Text = "Kasa";
            // 
            // jedenMiesiacCh
            // 
            this.jedenMiesiacCh.AutoSize = true;
            this.jedenMiesiacCh.Checked = true;
            this.jedenMiesiacCh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.jedenMiesiacCh.Location = new System.Drawing.Point(12, 15);
            this.jedenMiesiacCh.Name = "jedenMiesiacCh";
            this.jedenMiesiacCh.Size = new System.Drawing.Size(160, 29);
            this.jedenMiesiacCh.TabIndex = 44;
            this.jedenMiesiacCh.Text = "Jeden miesiąc";
            this.jedenMiesiacCh.UseVisualStyleBackColor = true;
            this.jedenMiesiacCh.CheckedChanged += new System.EventHandler(this.JedenMiesiacChCheckedChanged);
            // 
            // wyplaty_kasa_zakupy
            // 
            this.wyplaty_kasa_zakupy.AutoSize = true;
            this.wyplaty_kasa_zakupy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wyplaty_kasa_zakupy.Location = new System.Drawing.Point(970, 306);
            this.wyplaty_kasa_zakupy.Name = "wyplaty_kasa_zakupy";
            this.wyplaty_kasa_zakupy.Size = new System.Drawing.Size(151, 18);
            this.wyplaty_kasa_zakupy.TabIndex = 46;
            this.wyplaty_kasa_zakupy.Text = "wyplaty_kasa_zakupy";
            // 
            // wyplaty_konto_zakupy
            // 
            this.wyplaty_konto_zakupy.AutoSize = true;
            this.wyplaty_konto_zakupy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wyplaty_konto_zakupy.Location = new System.Drawing.Point(506, 306);
            this.wyplaty_konto_zakupy.Name = "wyplaty_konto_zakupy";
            this.wyplaty_konto_zakupy.Size = new System.Drawing.Size(157, 18);
            this.wyplaty_konto_zakupy.TabIndex = 45;
            this.wyplaty_konto_zakupy.Text = "wyplaty_konto_zakupy";
            // 
            // wyplaty_kasa_inneObroty
            // 
            this.wyplaty_kasa_inneObroty.AutoSize = true;
            this.wyplaty_kasa_inneObroty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wyplaty_kasa_inneObroty.Location = new System.Drawing.Point(970, 335);
            this.wyplaty_kasa_inneObroty.Name = "wyplaty_kasa_inneObroty";
            this.wyplaty_kasa_inneObroty.Size = new System.Drawing.Size(176, 18);
            this.wyplaty_kasa_inneObroty.TabIndex = 48;
            this.wyplaty_kasa_inneObroty.Text = "wyplaty_kasa_inneObroty";
            // 
            // wyplaty_konto_inneObroty
            // 
            this.wyplaty_konto_inneObroty.AutoSize = true;
            this.wyplaty_konto_inneObroty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wyplaty_konto_inneObroty.Location = new System.Drawing.Point(506, 335);
            this.wyplaty_konto_inneObroty.Name = "wyplaty_konto_inneObroty";
            this.wyplaty_konto_inneObroty.Size = new System.Drawing.Size(182, 18);
            this.wyplaty_konto_inneObroty.TabIndex = 47;
            this.wyplaty_konto_inneObroty.Text = "wyplaty_konto_inneObroty";
            // 
            // wplaty_kasa_inneObroty
            // 
            this.wplaty_kasa_inneObroty.AutoSize = true;
            this.wplaty_kasa_inneObroty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wplaty_kasa_inneObroty.Location = new System.Drawing.Point(970, 217);
            this.wplaty_kasa_inneObroty.Name = "wplaty_kasa_inneObroty";
            this.wplaty_kasa_inneObroty.Size = new System.Drawing.Size(169, 18);
            this.wplaty_kasa_inneObroty.TabIndex = 52;
            this.wplaty_kasa_inneObroty.Text = "wplaty_kasa_inneObroty";
            // 
            // wplaty_konto_inneObroty
            // 
            this.wplaty_konto_inneObroty.AutoSize = true;
            this.wplaty_konto_inneObroty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wplaty_konto_inneObroty.Location = new System.Drawing.Point(506, 217);
            this.wplaty_konto_inneObroty.Name = "wplaty_konto_inneObroty";
            this.wplaty_konto_inneObroty.Size = new System.Drawing.Size(175, 18);
            this.wplaty_konto_inneObroty.TabIndex = 51;
            this.wplaty_konto_inneObroty.Text = "wplaty_konto_inneObroty";
            // 
            // wplaty_kasa_fv
            // 
            this.wplaty_kasa_fv.AutoSize = true;
            this.wplaty_kasa_fv.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wplaty_kasa_fv.Location = new System.Drawing.Point(970, 190);
            this.wplaty_kasa_fv.Name = "wplaty_kasa_fv";
            this.wplaty_kasa_fv.Size = new System.Drawing.Size(108, 18);
            this.wplaty_kasa_fv.TabIndex = 50;
            this.wplaty_kasa_fv.Text = "wplaty_kasa_fv";
            // 
            // wplaty_konto_fv
            // 
            this.wplaty_konto_fv.AutoSize = true;
            this.wplaty_konto_fv.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wplaty_konto_fv.Location = new System.Drawing.Point(506, 190);
            this.wplaty_konto_fv.Name = "wplaty_konto_fv";
            this.wplaty_konto_fv.Size = new System.Drawing.Size(114, 18);
            this.wplaty_konto_fv.TabIndex = 49;
            this.wplaty_konto_fv.Text = "wplaty_konto_fv";
            // 
            // Salda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1340, 660);
            this.Controls.Add(this.wplaty_kasa_inneObroty);
            this.Controls.Add(this.wplaty_konto_inneObroty);
            this.Controls.Add(this.wplaty_kasa_fv);
            this.Controls.Add(this.wplaty_konto_fv);
            this.Controls.Add(this.wyplaty_kasa_inneObroty);
            this.Controls.Add(this.wyplaty_konto_inneObroty);
            this.Controls.Add(this.wyplaty_kasa_zakupy);
            this.Controls.Add(this.wyplaty_konto_zakupy);
            this.Controls.Add(this.jedenMiesiacCh);
            this.Controls.Add(this.kasa_label);
            this.Controls.Add(this.konto_label);
            this.Controls.Add(this.usun);
            this.Controls.Add(this.saldo_konto_kasa);
            this.Controls.Add(this.saldo_kasy);
            this.Controls.Add(this.wyplaty_kasa);
            this.Controls.Add(this.wplaty_kasa);
            this.Controls.Add(this.stary_stan_kasy);
            this.Controls.Add(this.saldo_konta);
            this.Controls.Add(this.wyplaty_konto);
            this.Controls.Add(this.wplaty_konto);
            this.Controls.Add(this.stary_stan_konta);
            this.Controls.Add(this.poprzednie_saldo_ch);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dataOdGroup);
            this.Controls.Add(this.podglad);
            this.Controls.Add(this.drukuj);
            this.Controls.Add(this.anuluj);
            this.Controls.Add(this.zapisz);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Salda";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Salda";
            this.dataOdGroup.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button zapisz;
        private System.Windows.Forms.Button anuluj;
        private System.Windows.Forms.Button drukuj;
        private System.Windows.Forms.Button podglad;
        private System.Windows.Forms.DateTimePicker data_od;
        private System.Windows.Forms.GroupBox dataOdGroup;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker data_do;
        private System.Windows.Forms.CheckBox poprzednie_saldo_ch;
        private System.Windows.Forms.Label stary_stan_konta;
        private System.Windows.Forms.Label stary_stan_kasy;
        private System.Windows.Forms.Label wplaty_konto;
        private System.Windows.Forms.Label wyplaty_konto;
        private System.Windows.Forms.Label saldo_konta;
        private System.Windows.Forms.Label wplaty_kasa;
        private System.Windows.Forms.Label wyplaty_kasa;
        private System.Windows.Forms.Label saldo_kasy;
        private System.Windows.Forms.Label saldo_konto_kasa;
        private System.Windows.Forms.Button usun;
        private System.Windows.Forms.PrintPreviewDialog podglad_dia;
        private System.Windows.Forms.PrintDialog print_dia;
        private System.Windows.Forms.Label konto_label;
        private System.Windows.Forms.Label kasa_label;
        private System.Windows.Forms.CheckBox jedenMiesiacCh;
        private System.Windows.Forms.Label wyplaty_kasa_zakupy;
        private System.Windows.Forms.Label wyplaty_konto_zakupy;
        private System.Windows.Forms.Label wyplaty_kasa_inneObroty;
        private System.Windows.Forms.Label wyplaty_konto_inneObroty;
        private System.Windows.Forms.Label wplaty_kasa_inneObroty;
        private System.Windows.Forms.Label wplaty_konto_inneObroty;
        private System.Windows.Forms.Label wplaty_kasa_fv;
        private System.Windows.Forms.Label wplaty_konto_fv;
    }
}