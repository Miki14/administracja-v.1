﻿namespace Administracja.InneOkna
{
    partial class Odsetki
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.obliczButt = new System.Windows.Forms.Button();
            this.wynikRazemLab = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataDo = new System.Windows.Forms.DateTimePicker();
            this.wplataBox = new System.Windows.Forms.TextBox();
            this.dataOd = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.zaFvBox = new System.Windows.Forms.TextBox();
            this.wynikDniLab = new System.Windows.Forms.Label();
            this.wynikOdsetkiLab = new System.Windows.Forms.Label();
            this.wynikCzynszLab = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.procentNum = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // obliczButt
            // 
            this.obliczButt.BackColor = System.Drawing.Color.Azure;
            this.obliczButt.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.obliczButt.Location = new System.Drawing.Point(389, 102);
            this.obliczButt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.obliczButt.Name = "obliczButt";
            this.obliczButt.Size = new System.Drawing.Size(163, 78);
            this.obliczButt.TabIndex = 5;
            this.obliczButt.Text = "Oblicz";
            this.obliczButt.UseVisualStyleBackColor = false;
            this.obliczButt.Click += new System.EventHandler(this.ObliczClick);
            // 
            // wynikRazemLab
            // 
            this.wynikRazemLab.AutoSize = true;
            this.wynikRazemLab.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wynikRazemLab.Location = new System.Drawing.Point(16, 257);
            this.wynikRazemLab.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wynikRazemLab.Name = "wynikRazemLab";
            this.wynikRazemLab.Size = new System.Drawing.Size(140, 39);
            this.wynikRazemLab.TabIndex = 5;
            this.wynikRazemLab.Text = "Razem:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.procentNum);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(445, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(107, 80);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "%";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataDo);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(16, 15);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(208, 80);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dzisiejsza data";
            // 
            // dataDo
            // 
            this.dataDo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataDo.CustomFormat = "dd MMMM yyyy";
            this.dataDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataDo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataDo.Location = new System.Drawing.Point(8, 31);
            this.dataDo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataDo.Name = "dataDo";
            this.dataDo.ShowUpDown = true;
            this.dataDo.Size = new System.Drawing.Size(191, 24);
            this.dataDo.TabIndex = 0;
            this.dataDo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // wplataBox
            // 
            this.wplataBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.wplataBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wplataBox.Location = new System.Drawing.Point(8, 33);
            this.wplataBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.wplataBox.Name = "wplataBox";
            this.wplataBox.Size = new System.Drawing.Size(135, 28);
            this.wplataBox.TabIndex = 3;
            this.wplataBox.Text = "0";
            this.wplataBox.Click += new System.EventHandler(this.SelectAll);
            this.wplataBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // dataOd
            // 
            this.dataOd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataOd.CustomFormat = "dd MMMM yyyy";
            this.dataOd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataOd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dataOd.Location = new System.Drawing.Point(8, 31);
            this.dataOd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataOd.Name = "dataOd";
            this.dataOd.ShowUpDown = true;
            this.dataOd.Size = new System.Drawing.Size(188, 24);
            this.dataOd.TabIndex = 1;
            this.dataOd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataOd);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(232, 15);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(205, 80);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Miesiąc opłacany";
            // 
            // zaFvBox
            // 
            this.zaFvBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.zaFvBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zaFvBox.HideSelection = false;
            this.zaFvBox.Location = new System.Drawing.Point(21, 32);
            this.zaFvBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.zaFvBox.Name = "zaFvBox";
            this.zaFvBox.Size = new System.Drawing.Size(175, 30);
            this.zaFvBox.TabIndex = 4;
            this.zaFvBox.Text = "0";
            this.zaFvBox.Click += new System.EventHandler(this.SelectAll);
            this.zaFvBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // wynikDniLab
            // 
            this.wynikDniLab.AutoSize = true;
            this.wynikDniLab.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wynikDniLab.Location = new System.Drawing.Point(16, 183);
            this.wynikDniLab.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wynikDniLab.Name = "wynikDniLab";
            this.wynikDniLab.Size = new System.Drawing.Size(106, 25);
            this.wynikDniLab.TabIndex = 20;
            this.wynikDniLab.Text = "Liczba dni:";
            // 
            // wynikOdsetkiLab
            // 
            this.wynikOdsetkiLab.AutoSize = true;
            this.wynikOdsetkiLab.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wynikOdsetkiLab.Location = new System.Drawing.Point(19, 233);
            this.wynikOdsetkiLab.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wynikOdsetkiLab.Name = "wynikOdsetkiLab";
            this.wynikOdsetkiLab.Size = new System.Drawing.Size(85, 25);
            this.wynikOdsetkiLab.TabIndex = 21;
            this.wynikOdsetkiLab.Text = "Odsetki:";
            // 
            // wynikCzynszLab
            // 
            this.wynikCzynszLab.AutoSize = true;
            this.wynikCzynszLab.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wynikCzynszLab.Location = new System.Drawing.Point(16, 208);
            this.wynikCzynszLab.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wynikCzynszLab.Name = "wynikCzynszLab";
            this.wynikCzynszLab.Size = new System.Drawing.Size(84, 25);
            this.wynikCzynszLab.TabIndex = 22;
            this.wynikCzynszLab.Text = "Czynsz:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.wplataBox);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox4.Location = new System.Drawing.Point(16, 102);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(152, 78);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Do podziału";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.zaFvBox);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox5.Location = new System.Drawing.Point(176, 102);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Size = new System.Drawing.Size(205, 78);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Opłata za miesiąc";
            // 
            // procentNum
            // 
            this.procentNum.Location = new System.Drawing.Point(7, 31);
            this.procentNum.Name = "procentNum";
            this.procentNum.Size = new System.Drawing.Size(93, 30);
            this.procentNum.TabIndex = 23;
            this.procentNum.Text = "8";
            // 
            // Odsetki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(565, 308);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.wynikCzynszLab);
            this.Controls.Add(this.wynikOdsetkiLab);
            this.Controls.Add(this.wynikDniLab);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.wynikRazemLab);
            this.Controls.Add(this.obliczButt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "Odsetki";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Odsetki";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button obliczButt;
        private System.Windows.Forms.Label wynikRazemLab;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dataDo;
        private System.Windows.Forms.TextBox wplataBox;
        private System.Windows.Forms.DateTimePicker dataOd;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox zaFvBox;
        private System.Windows.Forms.Label wynikDniLab;
        private System.Windows.Forms.Label wynikOdsetkiLab;
        private System.Windows.Forms.Label wynikCzynszLab;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox procentNum;
    }
}

