﻿using System;
using System.Windows.Forms;
using Administracja.KlasyStatic;

namespace Administracja.InneOkna
{
    public partial class Odsetki : Form
    {
        public Odsetki()
        {
            InitializeComponent();
            dataOd.Value = DateTimeHelper.GetDateTenthDayOfMonth(dataOd.Value);
        }

        private void ObliczClick(object sender, EventArgs e)
        {
            double procent = Convert.ToDouble(procentNum.Text.Replace('.', ','));
            double wplata = Convert.ToDouble(wplataBox.Text.Replace('.', ','));
            double zaFv = Convert.ToDouble(zaFvBox.Text.Replace('.', ','));

            OdsetkiKlasa wynik = new OdsetkiKlasa(dataOd.Value, dataDo.Value, procent, wplata, false, true, zaFv, 0);

            wynikDniLab.Text = "Liczba dni: " + wynik.LiczbaDni;
            wynikCzynszLab.Text = "Czynsz: " + wynik.ZaFv.ToString("0.00");
            wynikOdsetkiLab.Text = "Odsetki: " + wynik.OdsetkiOp.ToString("0.00");
            wynikRazemLab.Text = "Razem: " + (wynik.ZaFv + wynik.OdsetkiOp).ToString("0.00");
        }

        private void CheckKeys(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                ObliczClick(sender, e);
                e.KeyChar = (char)0;
                e.Handled = true;
            }
        }

        private void SelectAll(object sender, EventArgs e)
        {
            TextBox pole = (TextBox)sender;
            pole.SelectAll();
        }
    }
}