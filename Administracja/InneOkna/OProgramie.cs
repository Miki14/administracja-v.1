﻿using System;
using System.Windows.Forms;

namespace Administracja.InneOkna
{
    public partial class OProgramie : Form
    {
        public OProgramie()
        {
            InitializeComponent();

            wersja.Text = "Wersja: " + Application.ProductVersion;

        }

        private void MailLinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            mail.LinkVisited = true;
            System.Diagnostics.Process.Start("mailto:michalgol4@gmail.com");
        }

        private void OkClick(object sender, EventArgs e)
        {
            Close();
        }
    }
}
