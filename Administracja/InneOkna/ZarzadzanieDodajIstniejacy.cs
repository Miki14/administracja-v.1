﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Administracja.Model;

namespace Administracja.InneOkna
{
    public partial class ZarzadzanieDodajIstniejacy : Form
    {
        private BazaJson exisingDb;

        public ZarzadzanieDodajIstniejacy()
        {
            InitializeComponent();
        }

        public ZarzadzanieDodajIstniejacy(int Id)
        {
            InitializeComponent();

            exisingDb = (from x in Baza.ModelData.BazaJson
                         where x.Id == Id
                         select x).FirstOrDefault();


            nazwaBox.Text = exisingDb.Name;
            sciezkaBox.Text = exisingDb.Path;
            if (exisingDb.Server != "localhost")
            {
                serverZdalnyRad.Checked = true;
                serverBox.Text = exisingDb.Server;
            }
        }

        private void WyborClick(object sender, EventArgs e)
        {
            var okienko = new OpenFileDialog
            {
                Filter = "Pliki Bazy (*.fdb)|*.fdb|All Files (*.*)|*.*",
                InitialDirectory = Application.StartupPath + "\\Baza"
            };
            if (okienko.ShowDialog() == DialogResult.OK)
            {
                string nazwa = new FileInfo(okienko.FileName).Name;
                nazwaBox.Text = nazwa.Substring(0, nazwa.Length - 4);
                sciezkaBox.Text = okienko.FileName;
            }
        }

        private void ZapiszClick(object sender, EventArgs e)
        {
            if (exisingDb != null)
            {
                exisingDb.Name = nazwaBox.Text;
                exisingDb.Path = sciezkaBox.Text;
                exisingDb.Server = serverBox.Text;
                Close();
            }
            else
            {
                bool rokIstnieje = (from x in Baza.ModelData.BazaJson
                                    where x.Name == nazwaBox.Text
                                    select x).Any();

                if (rokIstnieje)
                {
                    Wyjatki.Komunikat("Rok o tej nazwe istnieje", "Rok istnieje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (sciezkaBox.Text.Length > 0)
                    {
                        Baza.ModelData.BazaJson.Add(new BazaJson(nazwaBox.Text, sciezkaBox.Text, "T", serverBox.Text));

                        Close();
                    }
                    else
                    {
                        Wyjatki.Komunikat("Nie wybrano pliku", "Wybierz plik", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void AnulujButtClick(object sender, EventArgs e)
        {
            Close();
        }

        private void ServerRadCheckedChanged(object sender, EventArgs e)
        {
            serverBox.Enabled = serverZdalnyRad.Checked;
            serverLab.Enabled = serverZdalnyRad.Checked;
            wyborButt.Enabled = !serverZdalnyRad.Checked;

            serverBox.Text = serverZdalnyRad.Checked ? "" : "localhost";
        }
    }
}
