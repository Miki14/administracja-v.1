﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Windows.Forms;
using Administracja.KlasyStatic;

namespace Administracja.InneOkna
{
    public partial class Salda : Form
    {
        private class Stany
        {
            public Stany()
            {
                Stary = 0;
                Wplaty = 0;
                Wplaty_Fv = 0;
                Wplaty_InneObroty = 0;
                Wyplaty = 0;
                Wyplaty_Zakupy = 0;
                Wyplaty_InneObroty = 0;
                Saldo = 0;
            }

            public double Stary;
            public double Wplaty;
            public double Wplaty_Fv;
            public double Wplaty_InneObroty;
            public double Wyplaty;
            public double Wyplaty_Zakupy;
            public double Wyplaty_InneObroty;
            public double Saldo;
        }

        private Stany _konto = new Stany();
        private Stany _kasa = new Stany();
        private string _dataPopSalda = "";
        private PrintDocument _pd;
        private readonly bool _oknoUruchomione;

        public Salda()
        {
            InitializeComponent();
            data_od.Value = DateTimeHelper.GetDateFirstDayOfMonth(DateTime.Today.AddMonths(-1));
            data_do.Value = DateTimeHelper.GetDateLastDayOfMonth(DateTime.Today.AddMonths(-1));
            _oknoUruchomione = true;
            Wyswietl();
        }

        private void Wyswietl()
        {
            JedenMiesiacChCheckedChanged(this, EventArgs.Empty);

            List<string> reader1D;

            _konto = new Stany();
            _kasa = new Stany();

            double staraKasa = 0;
            double stareKonto = 0;
            bool brakPopMiesiaca = false;

            try
            {
                DateTime dataPopMies = data_od.Value.AddMonths(-1);
                reader1D = Baza.Wczytaj1D("SELECT KONTO, KASA, DATA FROM SALDA WHERE EXTRACT(MONTH FROM DATA) = '"
                    + dataPopMies.Month + "' AND EXTRACT(YEAR FROM DATA) = '" + dataPopMies.Year + "'");
                if (reader1D[0].Length != 0) stareKonto = Convert.ToDouble(reader1D[0]);
                if (reader1D[1].Length != 0) staraKasa = Convert.ToDouble(reader1D[1]);
                if (reader1D[2].Length != 0) _dataPopSalda = OperacjeTekstowe.TrymujDate(reader1D[2]);
            }
            catch
            {
                reader1D = Baza.Wczytaj1D("SELECT KONTO, KASA, DATA FROM SALDA WHERE ID IN (SELECT MAX(ID) FROM SALDA WHERE DATA < '" + data_do.Value.ToString("dd.MM.yyyy") + "')");
                if (reader1D.Count != 0 && reader1D[0].Length != 0) stareKonto = Convert.ToDouble(reader1D[0]);
                if (reader1D.Count != 0 && reader1D[1].Length != 0) staraKasa = Convert.ToDouble(reader1D[1]);
                if (reader1D[2].Length != 0) _dataPopSalda = OperacjeTekstowe.TrymujDate(reader1D[2]);

                brakPopMiesiaca = true;
            }

            poprzednie_saldo_ch.Text = "Uwzględnij ostatnie saldo z dnia: " + Environment.NewLine + _dataPopSalda;

            if (poprzednie_saldo_ch.Checked)
            {
                if (brakPopMiesiaca)
                {
                    Wyjatki.Komunikat("Brak salda z poprzedniego miesiąca, używam salda z dnia " + _dataPopSalda + " .", "Brak poprzedniego salda", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                stary_stan_konta.Enabled = true;
                stary_stan_kasy.Enabled = true;

                _konto.Stary = stareKonto;
                _kasa.Stary = staraKasa;
            }
            else
            {
                stary_stan_kasy.Enabled = false;
                stary_stan_konta.Enabled = false;
            }

            try
            {
                //wplaty i odsetki na konto
                reader1D = Baza.Wczytaj1D("SELECT SUM(KWOTA), SUM(ODSETKI) FROM WPLATY WHERE SPOSOB = 'P' AND DATA >= '"
                    + data_od.Value.ToString("dd.MM.yyyy") + "' AND DATA <= '" + data_do.Value.ToString("dd.MM.yyyy") + "'");
                if (reader1D.Count > 0 && reader1D[0].Length > 0)
                {
                    _konto.Wplaty += Convert.ToDouble(reader1D[0]);
                    _konto.Wplaty_Fv += Convert.ToDouble(reader1D[0]);
                }
                if (reader1D.Count > 1 && reader1D[1].Length > 0)
                {
                    _konto.Wplaty += Convert.ToDouble(reader1D[1]);
                    _konto.Wplaty_Fv += Convert.ToDouble(reader1D[1]);
                }

                //wplaty i odsetki do kasy
                reader1D = Baza.Wczytaj1D("SELECT SUM(KWOTA), SUM(ODSETKI) FROM WPLATY WHERE SPOSOB = 'G' AND DATA >= '"
                    + data_od.Value.ToString("dd.MM.yyyy") + "' AND DATA <= '" + data_do.Value.ToString("dd.MM.yyyy") + "'");
                if (reader1D.Count > 0 && reader1D[0].Length > 0)
                {
                    _kasa.Wplaty += Convert.ToDouble(reader1D[0]);
                    _kasa.Wplaty_Fv += Convert.ToDouble(reader1D[0]);
                }
                if (reader1D.Count > 1 && reader1D[1].Length > 0)
                {
                    _kasa.Wplaty += Convert.ToDouble(reader1D[1]);
                    _kasa.Wplaty_Fv += Convert.ToDouble(reader1D[1]);
                }

                string data_do_string = data_do.Value.ToString("dd.MM.yyyy");

                //inne obroty na konto
                var reader = Baza.Wczytaj("SELECT SUM(KWOTA) FROM INNE_OBROTY WHERE DATA >= '" + data_od.Value.ToString("dd.MM.yyyy")
                    + "' AND DATA <= '" + data_do.Value.ToString("dd.MM.yyyy") + "' " +
                    "AND (TYP = 'kasa -> konto' OR TYP = 'inne -> konto')");
                _konto.Wplaty += Convert.ToDouble(reader);
                _konto.Wplaty_InneObroty += Convert.ToDouble(reader);

                //inne obroty do kasy
                reader = Baza.Wczytaj("SELECT SUM(KWOTA) FROM INNE_OBROTY WHERE DATA >= '"
                    + data_od.Value.ToString("dd.MM.yyyy") + "' AND DATA <= '" + data_do_string + "' "
                    + "AND (TYP = 'konto -> kasa' OR TYP = 'inne -> kasa')");
                _kasa.Wplaty += Convert.ToDouble(reader);
                _kasa.Wplaty_InneObroty += Convert.ToDouble(reader);

                //zakupy z konta
                reader = Baza.Wczytaj("SELECT SUM(WPLACONA_KWOTA) FROM ZAKUPY WHERE SPOSOB_ZAPLATY = 'P' AND Data_wplaty >= '"
                    + data_od.Value.ToString("dd.MM.yyyy") + "' AND Data_wplaty <= '" + data_do_string + "'");
                _konto.Wyplaty += Convert.ToDouble(reader);
                _konto.Wyplaty_Zakupy += Convert.ToDouble(reader);

                //zakupy z kasy
                reader = Baza.Wczytaj("SELECT SUM(WPLACONA_KWOTA) FROM ZAKUPY WHERE SPOSOB_ZAPLATY = 'G' AND Data_wplaty >= '"
                    + data_od.Value.ToString("dd.MM.yyyy") + "' AND Data_wplaty <= '" + data_do_string + "'");
                _kasa.Wyplaty += Convert.ToDouble(reader);
                _kasa.Wyplaty_Zakupy += Convert.ToDouble(reader);

                //inne obroty z konta
                reader = Baza.Wczytaj("SELECT SUM(KWOTA) FROM INNE_OBROTY WHERE DATA >= '"
                    + data_od.Value.ToString("dd.MM.yyyy") + "' AND DATA <= '" + data_do_string + "' "
                    + "AND (TYP = 'konto -> kasa' OR TYP = 'konto -> inne')");
                _konto.Wyplaty += Convert.ToDouble(reader);
                _konto.Wyplaty_InneObroty += Convert.ToDouble(reader);

                //inne obroty z kasy
                reader = Baza.Wczytaj("SELECT SUM(KWOTA) FROM INNE_OBROTY WHERE DATA >= '"
                    + data_od.Value.ToString("dd.MM.yyyy") + "' AND DATA <= '" + data_do_string + "' "
                    + "AND (TYP = 'kasa -> konto' OR TYP = 'kasa -> inne')");
                _kasa.Wyplaty += Convert.ToDouble(reader);
                _kasa.Wyplaty_InneObroty += Convert.ToDouble(reader);

                _konto.Saldo = _konto.Wplaty - _konto.Wyplaty;
                _kasa.Saldo = _kasa.Wplaty - _kasa.Wyplaty;

                if (poprzednie_saldo_ch.Checked)
                {
                    _konto.Saldo += _konto.Stary;
                    _kasa.Saldo += _kasa.Stary;
                }

                stary_stan_konta.Text = "Poprzednie saldo: " + string.Format("{0:N2}", _konto.Stary);

                wplaty_konto.Text = "Wplaty: " + string.Format("{0:N2}", _konto.Wplaty);
                wplaty_konto_fv.Text = "Wplaty na FV: " + string.Format("{0:N2}", _konto.Wplaty_Fv);
                wplaty_konto_inneObroty.Text = "Wplaty na inne obr.: " + string.Format("{0:N2}", _konto.Wplaty_InneObroty);

                wyplaty_konto.Text = "Wyplaty: " + string.Format("{0:N2}", _konto.Wyplaty);
                wyplaty_konto_zakupy.Text = "Wyplaty na zakupy: " + string.Format("{0:N2}", _konto.Wyplaty_Zakupy);
                wyplaty_konto_inneObroty.Text = "Wyplaty na inne obr.: " + string.Format("{0:N2}", _konto.Wyplaty_InneObroty);

                saldo_konta.Text = "Saldo konta: " + string.Format("{0:N2}", _konto.Saldo);

                stary_stan_kasy.Text = "Poprzednie saldo: " + string.Format("{0:N2}", _kasa.Stary);

                wplaty_kasa.Text = "Wplaty: " + string.Format("{0:N2}", _kasa.Wplaty);
                wplaty_kasa_fv.Text = "Wplaty na FV: " + string.Format("{0:N2}", _kasa.Wplaty_Fv);
                wplaty_kasa_inneObroty.Text = "Wplaty na inne obr.: " + string.Format("{0:N2}", _kasa.Wplaty_InneObroty);

                wyplaty_kasa.Text = "Wyplaty: " + string.Format("{0:N2}", _kasa.Wyplaty);
                wyplaty_kasa_zakupy.Text = "Wyplaty na zakupy: " + string.Format("{0:N2}", _kasa.Wyplaty_Zakupy);
                wyplaty_kasa_inneObroty.Text = "Wyplaty na inne obr.: " + string.Format("{0:N2}", _kasa.Wyplaty_InneObroty);

                saldo_kasy.Text = "Saldo kasy: " + string.Format("{0:N2}", _kasa.Saldo);


                saldo_konto_kasa.Text = "Saldo konta i kasy: " + string.Format("{0:N2}", _konto.Saldo + _kasa.Saldo);
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
        }

        private void ZapiszClick(object sender, EventArgs e)
        {
            if (Wyjatki.Komunikat("Czy na pewno chcesz zapisać saldo?", "Zapisać?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (DateTime.Compare(DateTime.Parse(Baza.Wczytaj("SELECT MAX(DATA) FROM SALDA")), data_do.Value) > 0)
                {
                    Wyjatki.Komunikat("Istnieje nowsze saldo, niż to które chcesz zapisać. Aby edytować starsze saldo najpierw usuń nowsze.", "Istnieje nowsze saldo.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Baza.Zapisz("DELETE FROM SALDA WHERE EXTRACT(MONTH FROM DATA) = '" + data_do.Value.Month +
                                "' AND EXTRACT(YEAR FROM DATA) = '" + data_do.Value.Year + "' AND ID <> 0");
                    Baza.Ustaw("Salda");
                    Baza.Zapisz("INSERT INTO SALDA VALUES (GEN_ID(NUMERACJA_SALDA,1),'" + data_do.Value.ToString("dd.MM.yyyy") + "','"
                        + _konto.Saldo.ToString("0.00", CultureInfo.InvariantCulture) + "','" +
                        _kasa.Saldo.ToString("0.00", CultureInfo.InvariantCulture) + "')");
                    Close();
                }
            }
        }

        private void AnulujClick(object sender, EventArgs e)
        {
            Close();
        }

        private void DrukujClick(object sender, EventArgs e)
        {
            _pd = new PrintDocument();
            _pd.PrintPage += PdPrintPage;
            print_dia.Document = _pd;
            if (print_dia.ShowDialog() != DialogResult.OK) return;
            try
            {
                _pd.Print();
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
        }

        private void PodgladClick(object sender, EventArgs e)
        {
            _pd = new PrintDocument();
            _pd.PrintPage += PdPrintPage;
            podglad_dia.Document = _pd;
            ((Form)podglad_dia).WindowState = FormWindowState.Maximized;
            podglad_dia.PrintPreviewControl.Zoom = 1.5;
            podglad_dia.ShowDialog();
        }

        private void PdPrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            int pozY = 50;
            const int lewyMargX = 50;
            const int xOpis = 100;
            const int xKonto = 450;
            const int xKasa = 700;

            g.DrawString("Saldo", Fonty.Header, Brushes.Black, new PointF(375, pozY));
            g.DrawString("Za okres od  " + data_od.Value.ToString("dd.MM.yyyy") + " do  " + data_do.Value.ToString("dd.MM.yyyy"), Fonty.Period, Brushes.Black, new PointF(lewyMargX, pozY + 30));

            var reader1D = Baza.Wczytaj1D("SELECT Nazwa_1, Nazwa_2, Adres_1, Adres_2 FROM DANE_SPRZEDAWCY");
            g.DrawString(reader1D[0] + " " + reader1D[1] + " " + reader1D[2] + " " + reader1D[3], Fonty.Normal, Brushes.Black, new PointF(lewyMargX, pozY + 50));

            pozY += 125;

            g.DrawString("Konto", Fonty.Period, Brushes.Black, new PointF(xKonto - 75, pozY));
            g.DrawString("Kasa", Fonty.Period, Brushes.Black, new PointF(xKasa - 75, pozY));

            pozY += 40;

            if (poprzednie_saldo_ch.Checked)
            {
                g.DrawString("Poprzednie saldo:", Fonty.Period, Brushes.Black, new PointF(xOpis, pozY));
                g.DrawString(string.Format("{0:N2}", _kasa.Stary), Fonty.Period, Brushes.Black, new PointF(xKasa, pozY), Fonty.Left);
                g.DrawString(string.Format("{0:N2}", _konto.Stary), Fonty.Period, Brushes.Black, new PointF(xKonto, pozY), Fonty.Left);
            }
            pozY += 30;

            g.DrawString("Wplaty:", Fonty.Period, Brushes.Black, new PointF(xOpis, pozY));
            g.DrawString(string.Format("{0:N2}", _kasa.Wplaty), Fonty.Period, Brushes.Black, new PointF(xKasa, pozY), Fonty.Left);
            g.DrawString(string.Format("{0:N2}", _konto.Wplaty), Fonty.Period, Brushes.Black, new PointF(xKonto, pozY), Fonty.Left);
            pozY += 30;

            g.DrawString("Wyplaty:", Fonty.Period, Brushes.Black, new PointF(xOpis, pozY));
            g.DrawString(string.Format("{0:N2}", _kasa.Wyplaty), Fonty.Period, Brushes.Black, new PointF(xKasa, pozY), Fonty.Left);
            g.DrawString(string.Format("{0:N2}", _konto.Wyplaty), Fonty.Period, Brushes.Black, new PointF(xKonto, pozY), Fonty.Left);
            pozY += 30;

            g.DrawString("Saldo:", Fonty.Period, Brushes.Black, new PointF(xOpis, pozY));
            g.DrawString(string.Format("{0:N2}", _kasa.Saldo), Fonty.Header, Brushes.Black, new PointF(xKasa, pozY), Fonty.Left);
            g.DrawString(string.Format("{0:N2}", _konto.Saldo), Fonty.Header, Brushes.Black, new PointF(xKonto, pozY), Fonty.Left);
            pozY += 30;

            g.DrawString("Saldo konta i kasy:", Fonty.Period, Brushes.Black, new PointF(xOpis, pozY));
            g.DrawString(string.Format("{0:N2}", (_konto.Saldo + _kasa.Saldo)), Fonty.Header, Brushes.Black, new PointF(600, pozY), Fonty.Left);
        }

        private void UsunClick(object sender, EventArgs e)
        {
            var reader1D = Baza.Wczytaj1D("SELECT MAX(ID) FROM SALDA");
            if (reader1D[0].Length > 0 && reader1D[0] != "0")
            {
                if (Wyjatki.Komunikat("Czy na pewno usunąć saldo z dnia " + _dataPopSalda + " ?", "Usunąć?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Baza.Zapisz("DELETE FROM SALDA WHERE ID IN (SELECT MAX(ID) FROM SALDA)");
                    Baza.Ustaw("Salda");
                    Wyswietl();
                }
            }
            else
            {
                Wyjatki.Komunikat("Brak sald do usnięcia", "Brak sald", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void WyswietlHandler(object sender, EventArgs e)
        {
            if (_oknoUruchomione)
            {
                Wyswietl();
            }
        }

        private void JedenMiesiacChCheckedChanged(object sender, EventArgs e)
        {
            if (jedenMiesiacCh.Checked)
            {
                dataOdGroup.Text = "Miesiąc Salda";
                data_od.CustomFormat = "MMMM yyyy";
                data_do.Enabled = false;
                data_do.ValueChanged -= new EventHandler(WyswietlHandler);
            }
            else
            {
                dataOdGroup.Text = "Data Od";
                data_od.CustomFormat = "dd MMMM yyyy";
                data_do.Enabled = true;
                data_do.ValueChanged += new EventHandler(WyswietlHandler);
            }

            PelnyMiesiac();
        }

        private void DataOdValueChanged(object sender, EventArgs e)
        {
            if (_oknoUruchomione)
            {
                Wyswietl();
            }

            PelnyMiesiac();
        }

        private void PelnyMiesiac()
        {
            if (jedenMiesiacCh.Checked)
            {
                data_od.Value = DateTimeHelper.GetDateFirstDayOfMonth(data_od.Value);
                data_do.Value = DateTimeHelper.GetDateLastDayOfMonth(data_od.Value);
            }
        }
    }
}
