﻿namespace Administracja.InneOkna
{
    partial class Inne
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.podglad = new System.Windows.Forms.Button();
            this.zarzadzanie = new System.Windows.Forms.Button();
            this.sprzedawca = new System.Windows.Forms.Button();
            this.aktualizacja = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // podglad
            // 
            this.podglad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.podglad.Location = new System.Drawing.Point(65, 47);
            this.podglad.Name = "podglad";
            this.podglad.Size = new System.Drawing.Size(244, 135);
            this.podglad.TabIndex = 9;
            this.podglad.Text = "Podglad zawartości bazy";
            this.podglad.UseVisualStyleBackColor = true;
            this.podglad.Click += new System.EventHandler(this.podglad_Click);
            // 
            // zarzadzanie
            // 
            this.zarzadzanie.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zarzadzanie.Location = new System.Drawing.Point(315, 47);
            this.zarzadzanie.Name = "zarzadzanie";
            this.zarzadzanie.Size = new System.Drawing.Size(244, 135);
            this.zarzadzanie.TabIndex = 10;
            this.zarzadzanie.Text = "Zarządzanie budynkami";
            this.zarzadzanie.UseVisualStyleBackColor = true;
            this.zarzadzanie.Click += new System.EventHandler(this.zarzadzanie_Click);
            // 
            // sprzedawca
            // 
            this.sprzedawca.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sprzedawca.Location = new System.Drawing.Point(65, 188);
            this.sprzedawca.Name = "sprzedawca";
            this.sprzedawca.Size = new System.Drawing.Size(244, 135);
            this.sprzedawca.TabIndex = 11;
            this.sprzedawca.Text = "Dane sprzedawcy";
            this.sprzedawca.UseVisualStyleBackColor = true;
            this.sprzedawca.Click += new System.EventHandler(this.sprzedawca_Click);
            // 
            // aktualizacja
            // 
            this.aktualizacja.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.aktualizacja.Location = new System.Drawing.Point(315, 188);
            this.aktualizacja.Name = "aktualizacja";
            this.aktualizacja.Size = new System.Drawing.Size(244, 135);
            this.aktualizacja.TabIndex = 12;
            this.aktualizacja.Text = "Aktualizacja programu";
            this.aktualizacja.UseVisualStyleBackColor = true;
            this.aktualizacja.Click += new System.EventHandler(this.aktualizacja_Click);
            // 
            // Inne
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.aktualizacja);
            this.Controls.Add(this.sprzedawca);
            this.Controls.Add(this.zarzadzanie);
            this.Controls.Add(this.podglad);
            this.Name = "Inne";
            this.Size = new System.Drawing.Size(1066, 555);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button podglad;
        private System.Windows.Forms.Button zarzadzanie;
        private System.Windows.Forms.Button sprzedawca;
        private System.Windows.Forms.Button aktualizacja;
    }
}
