﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Administracja.InneOkna
{
    internal class AktualizacjaLogika
    {
        public Thread Watek;
        private readonly WebClient _webClient = new WebClient();
        public readonly Version Aktualna;
        public readonly Version AppVersion = Version.Parse(Application.ProductVersion);
        private const string Link = "https://bitbucket.org/Miki14/administracja-v.1/raw/master/UpToDate/";
        private readonly Aktualizacja _interface;

        public AktualizacjaLogika(Aktualizacja aktInterface)
        {
            _interface = aktInterface;

            try
            {
                Aktualna = Version.Parse(_webClient.DownloadString(Link + "Version.txt"));
            }
            catch (WebException)
            {
                Wyjatki.Komunikat("Prawdopodobnie nie masz połączenia z internetem lub na serwerze nie znajduje się plik aktualizacji.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
            }
        }

        public void Aktualizuj()
        {
            bool aktualizujF;

            if (Aktualna.CompareTo(AppVersion) <= 0)
            {
                aktualizujF = Wyjatki.Komunikat("Brak nowszej wersji na serwerze, czy na pewno chcesz próbować aktualizować?",
                    "Aktualizować?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
            }
            else
            {
                aktualizujF = true;
            }

            if (aktualizujF)
            {
                Process[] processes = Process.GetProcessesByName("Administracja");

                if (processes.Count() > 1)
                {
                    Wyjatki.Komunikat(
                        "Jest otwartych kilka programów \"Administracja\". Zamknij je i spróbuj ponownie.",
                        "Wiele instancji programu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    _interface.AktualizacjaZaczeta();

                    Watek = new Thread(PobieranieTask) { IsBackground = true };
                    Watek.Start();
                }
            }
        }

        private void PobieranieTask()
        {
            Baza.CloseConnection();
            Thread.Sleep(2000);

            var files = new List<string>(ConfigurationManager.AppSettings["FilesToUpdate"].Split(';'));
            var filesToUpdateApp = new List<string>(ConfigurationManager.AppSettings["FilesToUpdateInUpdateApp"].Split(';'));

            int procenty = 5;

            try
            {
                foreach (string file in files)
                {
                    PobierzPlik(file);
                    _interface.OdswiezPasek(procenty);
                    procenty += 10;
                }

                foreach (string plik in filesToUpdateApp)
                {
                    PobierzPlik("_" + plik, plik);
                    _interface.OdswiezPasek(procenty);
                    procenty += 10;
                }

                _interface.OdswiezPasek(100);

                Program.Zamykam = true;

                //Listę plików podmienianych przez udpate zapisujemy jako jeden string oddzielony spacjami
                //i otwieramy Update.exe z tym stringiem jako parametr
                StringBuilder sb = new StringBuilder();
                sb.Append(filesToUpdateApp[0]);
                for (int i = 1; i < filesToUpdateApp.Count; i++)
                {
                    sb.Append(" " + filesToUpdateApp[i]);
                }

                Process.Start("Update.exe", sb.ToString());
                Application.Exit();
            }
            catch (IOException)
            {
                Wyjatki.Komunikat("Prawdopodobnie jakiś plik jest używany, uruchom program ponownie.", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _interface.OdswiezPasek(0);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                Wyjatki.Error(ex);
                _interface.OdswiezPasek(0);
            }
        }

        private void PobierzPlik(string nazwa)
        {
            PobierzPlik(nazwa, nazwa);
        }

        private void PobierzPlik(string nazwaDysk, string nazwaSerwer)
        {
            if (File.Exists(nazwaDysk)) File.Delete(nazwaDysk);
            Raport.Instance.Raportuj("Downloading " + nazwaDysk);
            _webClient.DownloadFile(Link + nazwaSerwer, nazwaDysk);
        }
    }
}
