﻿namespace Administracja.InneOkna
{
    partial class Sprzedawca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sprzedawca));
            this.zapiszButt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.nazwa1Box = new System.Windows.Forms.TextBox();
            this.nazwa2Box = new System.Windows.Forms.TextBox();
            this.adresBox = new System.Windows.Forms.TextBox();
            this.nipBox = new System.Windows.Forms.TextBox();
            this.regonBox = new System.Windows.Forms.TextBox();
            this.telefonBox = new System.Windows.Forms.TextBox();
            this.kontoBox = new System.Windows.Forms.TextBox();
            this.adresCdBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.folder_l = new System.Windows.Forms.Label();
            this.wyborButt = new System.Windows.Forms.Button();
            this.numerFvBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.anulujButt = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.oprocOdsetkiNum = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // zapiszButt
            // 
            this.zapiszButt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zapiszButt.Location = new System.Drawing.Point(300, 383);
            this.zapiszButt.Name = "zapiszButt";
            this.zapiszButt.Size = new System.Drawing.Size(200, 70);
            this.zapiszButt.TabIndex = 12;
            this.zapiszButt.Text = "Zapisz";
            this.zapiszButt.UseVisualStyleBackColor = true;
            this.zapiszButt.Click += new System.EventHandler(this.ZapiszClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(14, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nazwa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nazwa cd.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(12, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Adres";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(12, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "NIP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(12, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "Regon";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(12, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "Telefon";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(12, 240);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 20);
            this.label8.TabIndex = 8;
            this.label8.Text = "Numer konta";
            // 
            // nazwa1Box
            // 
            this.nazwa1Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwa1Box.Location = new System.Drawing.Point(118, 13);
            this.nazwa1Box.MaxLength = 40;
            this.nazwa1Box.Name = "nazwa1Box";
            this.nazwa1Box.Size = new System.Drawing.Size(388, 26);
            this.nazwa1Box.TabIndex = 0;
            this.nazwa1Box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // nazwa2Box
            // 
            this.nazwa2Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazwa2Box.Location = new System.Drawing.Point(118, 45);
            this.nazwa2Box.MaxLength = 40;
            this.nazwa2Box.Name = "nazwa2Box";
            this.nazwa2Box.Size = new System.Drawing.Size(388, 26);
            this.nazwa2Box.TabIndex = 1;
            this.nazwa2Box.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // adresBox
            // 
            this.adresBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.adresBox.Location = new System.Drawing.Point(118, 77);
            this.adresBox.MaxLength = 20;
            this.adresBox.Name = "adresBox";
            this.adresBox.Size = new System.Drawing.Size(388, 26);
            this.adresBox.TabIndex = 2;
            this.adresBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // nipBox
            // 
            this.nipBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nipBox.Location = new System.Drawing.Point(118, 141);
            this.nipBox.MaxLength = 15;
            this.nipBox.Name = "nipBox";
            this.nipBox.Size = new System.Drawing.Size(388, 26);
            this.nipBox.TabIndex = 4;
            this.nipBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // regonBox
            // 
            this.regonBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.regonBox.Location = new System.Drawing.Point(118, 173);
            this.regonBox.MaxLength = 15;
            this.regonBox.Name = "regonBox";
            this.regonBox.Size = new System.Drawing.Size(388, 26);
            this.regonBox.TabIndex = 5;
            this.regonBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // telefonBox
            // 
            this.telefonBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.telefonBox.Location = new System.Drawing.Point(118, 205);
            this.telefonBox.MaxLength = 20;
            this.telefonBox.Name = "telefonBox";
            this.telefonBox.Size = new System.Drawing.Size(388, 26);
            this.telefonBox.TabIndex = 6;
            this.telefonBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // kontoBox
            // 
            this.kontoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.kontoBox.Location = new System.Drawing.Point(118, 237);
            this.kontoBox.MaxLength = 40;
            this.kontoBox.Name = "kontoBox";
            this.kontoBox.Size = new System.Drawing.Size(388, 26);
            this.kontoBox.TabIndex = 8;
            this.kontoBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // adresCdBox
            // 
            this.adresCdBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.adresCdBox.Location = new System.Drawing.Point(118, 109);
            this.adresCdBox.MaxLength = 20;
            this.adresCdBox.Name = "adresCdBox";
            this.adresCdBox.Size = new System.Drawing.Size(388, 26);
            this.adresCdBox.TabIndex = 3;
            this.adresCdBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(12, 112);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "Adres cd.";
            // 
            // folder_l
            // 
            this.folder_l.AutoSize = true;
            this.folder_l.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.folder_l.Location = new System.Drawing.Point(14, 309);
            this.folder_l.Name = "folder_l";
            this.folder_l.Size = new System.Drawing.Size(49, 20);
            this.folder_l.TabIndex = 20;
            this.folder_l.Text = "Logo:";
            // 
            // wyborButt
            // 
            this.wyborButt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wyborButt.Location = new System.Drawing.Point(337, 309);
            this.wyborButt.Name = "wyborButt";
            this.wyborButt.Size = new System.Drawing.Size(166, 32);
            this.wyborButt.TabIndex = 10;
            this.wyborButt.Text = "Wybierz plik logo";
            this.wyborButt.UseVisualStyleBackColor = true;
            this.wyborButt.Click += new System.EventHandler(this.WyborClick);
            // 
            // numerFvBox
            // 
            this.numerFvBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numerFvBox.Location = new System.Drawing.Point(118, 269);
            this.numerFvBox.MaxLength = 3;
            this.numerFvBox.Name = "numerFvBox";
            this.numerFvBox.Size = new System.Drawing.Size(388, 26);
            this.numerFvBox.TabIndex = 9;
            this.numerFvBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckKeys);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(12, 272);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 20);
            this.label10.TabIndex = 21;
            this.label10.Text = "Numer FV";
            // 
            // anulujButt
            // 
            this.anulujButt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.anulujButt.Location = new System.Drawing.Point(94, 383);
            this.anulujButt.Name = "anulujButt";
            this.anulujButt.Size = new System.Drawing.Size(200, 70);
            this.anulujButt.TabIndex = 13;
            this.anulujButt.Text = "Anuluj";
            this.anulujButt.UseVisualStyleBackColor = true;
            this.anulujButt.Click += new System.EventHandler(this.AnulujClick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(12, 346);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(190, 20);
            this.label7.TabIndex = 22;
            this.label7.Text = "Oprocentowanie odsetek:";
            // 
            // oprocOdsetkiNum
            // 
            this.oprocOdsetkiNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.oprocOdsetkiNum.Location = new System.Drawing.Point(204, 342);
            this.oprocOdsetkiNum.Name = "oprocOdsetkiNum";
            this.oprocOdsetkiNum.Size = new System.Drawing.Size(60, 26);
            this.oprocOdsetkiNum.TabIndex = 11;
            this.oprocOdsetkiNum.Text = "8";
            // 
            // Sprzedawca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 464);
            this.ControlBox = false;
            this.Controls.Add(this.oprocOdsetkiNum);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.anulujButt);
            this.Controls.Add(this.numerFvBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.folder_l);
            this.Controls.Add(this.wyborButt);
            this.Controls.Add(this.adresCdBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.kontoBox);
            this.Controls.Add(this.telefonBox);
            this.Controls.Add(this.regonBox);
            this.Controls.Add(this.nipBox);
            this.Controls.Add(this.adresBox);
            this.Controls.Add(this.nazwa2Box);
            this.Controls.Add(this.nazwa1Box);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.zapiszButt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Sprzedawca";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sprzedawca";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button zapiszButt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox nazwa1Box;
        private System.Windows.Forms.TextBox nazwa2Box;
        private System.Windows.Forms.TextBox adresBox;
        private System.Windows.Forms.TextBox nipBox;
        private System.Windows.Forms.TextBox regonBox;
        private System.Windows.Forms.TextBox telefonBox;
        private System.Windows.Forms.TextBox kontoBox;
        private System.Windows.Forms.TextBox adresCdBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label folder_l;
        private System.Windows.Forms.Button wyborButt;
        private System.Windows.Forms.TextBox numerFvBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button anulujButt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox oprocOdsetkiNum;
    }
}