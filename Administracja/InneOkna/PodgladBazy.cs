﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Administracja.InneOkna
{
    public partial class PodgladBazy : Form
    {
        private readonly List<ColumnHeader> _naglowki = new List<ColumnHeader>();

        public PodgladBazy()
        {
            InitializeComponent();

            _naglowki.Add(columnHeader1);
            _naglowki.Add(columnHeader2);
            _naglowki.Add(columnHeader3);
            _naglowki.Add(columnHeader4);
            _naglowki.Add(columnHeader5);
            _naglowki.Add(columnHeader6);
            _naglowki.Add(columnHeader7);
            _naglowki.Add(columnHeader8);
            _naglowki.Add(columnHeader9);
            _naglowki.Add(columnHeader10);
            _naglowki.Add(columnHeader11);
            _naglowki.Add(columnHeader12);
            _naglowki.Add(columnHeader13);
            _naglowki.Add(columnHeader14);
            _naglowki.Add(columnHeader15);
            _naglowki.Add(columnHeader16);
            _naglowki.Add(columnHeader17);
            _naglowki.Add(columnHeader18);
            _naglowki.Add(columnHeader19);

            var reader1D = Baza.Wczytaj1Dp("SELECT RDB$RELATION_NAME FROM RDB$RELATIONS WHERE RDB$SYSTEM_FLAG = 0 AND RDB$VIEW_BLR IS NULL ORDER BY RDB$RELATION_NAME");
            foreach (var reader in reader1D)
            {
                tabele.Items.Add(reader);
            }

            tabele.SelectedIndex = 0;
        }

        private void Wyswietl()
        {
            lista.Items.Clear();

            foreach (ColumnHeader naglowek in _naglowki)
            {
                naglowek.Text = "-";
            }

            var reader2D = Baza.Wczytaj2D("SELECT * FROM " + tabele.Text + " ORDER BY ID");
            foreach (var reader1D in reader2D)
            {
                ListViewItem element = lista.Items.Add(reader1D[0]);
                for (int i = 1; i < reader1D.Count; i++)
                {
                    element.SubItems.Add(reader1D[i]);
                }
            }

            var reader1Dp = Baza.Wczytaj1Dp("SELECT RDB$FIELD_NAME FROM RDB$RELATION_FIELDS WHERE RDB$RELATION_NAME='" + tabele.Text + "'");
            for (int i = 0; i < reader1Dp.Count; i++)
            {
                _naglowki[i].Text = reader1Dp[i];
            }
        }

        private void UsunClick(object sender, EventArgs e)
        {
            if (Wyjatki.Komunikat("Czy chcesz usunąć ten rekord?", "Usunąć?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Baza.Zapisz("DELETE FROM " + tabele.Text + " WHERE ID='" + lista.SelectedItems[0].Text + "';");
            }
            Wyswietl();
        }

        private void ZamknijClick(object sender, EventArgs e)
        {
            Close();
        }

        private void TabeleSelectedIndexChanged(object sender, EventArgs e)
        {
            Wyswietl();
        }
    }
}
