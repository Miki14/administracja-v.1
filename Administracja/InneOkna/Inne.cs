﻿using System;
using System.Windows.Forms;

namespace Administracja.InneOkna
{
    public partial class Inne : UserControl
    {
        public Inne()
        {
            InitializeComponent();
        }

        private void podglad_Click(object sender, EventArgs e)
        {
            var z = new PodgladBazy { MdiParent = ParentWindow.ParentStatic };
            z.Show();
        }

        private void zarzadzanie_Click(object sender, EventArgs e)
        {
            var z = new Zarzadzanie { MdiParent = ParentWindow.ParentStatic };
            z.Show();
        }

        private void sprzedawca_Click(object sender, EventArgs e)
        {
            new Sprzedawca().ShowDialog();
        }

        private void aktualizacja_Click(object sender, EventArgs e)
        {
            new Aktualizacja().ShowDialog();
        }
    }
}
