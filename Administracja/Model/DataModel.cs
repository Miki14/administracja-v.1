﻿using System.Collections.Generic;

namespace Administracja.Model
{
    public class DataModel
    {
        public List<BazaJson> BazaJson { get; set; }
        public double Interest { get; set; }
    }
}