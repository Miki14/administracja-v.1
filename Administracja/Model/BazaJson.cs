﻿using System.Linq;

namespace Administracja.Model
{
    public class BazaJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Show { get; set; }
        public string Server { get; set; }

        public BazaJson(string name, string path, string show, string server)
        {
            int maxId = (from x in Baza.ModelData.BazaJson
                         select x.Id).Max();

            Id = maxId + 1;
            Name = name;
            Path = path;
            Show = show;
            Server = server;
        }

        public BazaJson()
        {
        }
    }
}