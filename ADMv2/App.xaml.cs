﻿using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Windows;
using ADMv2.Customers;
using ADMv2.Customers.CustomerEditInvoice;
using ADMv2.Customers.CustomerIngredients;
using ADMv2.Customers.CustomerIngredientsEdit;
using ADMv2.Customers.CustomersList;
using ADMv2.DAL.Entities;
using ADMv2.MainWindowComponents;
using ADMv2.MainWindowComponents.ChangePasswordWindow;
using ADMv2.MainWindowComponents.LoginWindow;
using ADMv2.Other.Balance;
using ADMv2.Other.OtherTransactions;
using ADMv2.Printing.SummaryWindow;
using ADMv2.Purchases.PurchaseEdit;
using ADMv2.Purchases.PurchaseIngredients;
using ADMv2.Purchases.PurchaseIngredientsEdit;
using ADMv2.Purchases.PurchasesList;
using ADMv2.Purchases.PurchasesSuppliersList;
using ADMv2.SystemWindows.CompanyData;
using Microsoft.Extensions.DependencyInjection;

namespace ADMv2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static bool isClosing = false;

        public App()
        {
            Services = ConfigureServices();

            this.InitializeComponent();
        }

        /// <summary>
        /// Gets the current <see cref="App"/> instance in use
        /// </summary>
        public new static App Current => (App)Application.Current;

        /// <summary>
        /// Gets the <see cref="IServiceProvider"/> instance to resolve application services.
        /// </summary>
        public ServiceProvider Services { get; }

        public static void Close()
        {
            isClosing = true;
            Close();
        }

        /// <summary>
        /// Configures the services for the application.
        /// </summary>
        private static ServiceProvider ConfigureServices()
        {
            var services = new ServiceCollection();

            // Services
            //services.AddSingleton<IFilesService, FilesService>();

            // Viewmodels
            services.AddTransient<MainViewModel>();
            services.AddTransient<CustomersListViewModel>();
            services.AddTransient<Customer>();
            services.AddTransient<CustomerCardViewModel>();
            services.AddTransient<CustomerEditInvoiceViewModel>();
            services.AddTransient<CustomerIngredientsViewModel>();
            services.AddTransient<Payment>();
            services.AddTransient<PurchasesListViewModel>();
            services.AddTransient<PurchaseEditViewModel>();
            services.AddTransient<CustomerIngredientsEditViewModel>();
            services.AddTransient<PurchasesSuppliersListViewModel>();
            services.AddTransient<PurchasesSupplierEditViewModel>();
            services.AddTransient<PurchasesIngredientsViewModel>();
            services.AddTransient<CompanyDataViewModel>();
            services.AddTransient<OtherTransactionsViewModel>();
            services.AddTransient<OtherTransactionsEditViewModel>();
            services.AddTransient<PurchasesIngredientsEditViewModel>();
            services.AddTransient<SummaryWindowViewModel>();
            services.AddTransient<LoginWindowViewModel>();
            services.AddTransient<BalanceViewModel>();
            services.AddTransient<ChangePasswordWindowViewModel>();

            //Po dodaniu tu dodać też w ViewModelLocator

            DbProviderFactories.RegisterFactory("System.Data.SqlClient", SqlClientFactory.Instance);

            return services.BuildServiceProvider();
        }
    }
}
