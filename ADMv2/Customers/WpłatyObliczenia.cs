﻿namespace ADMv2
{
    static class Wpłaty_Obliczenia
    {
        /// <summary>
        /// Funkcja obliczająca kwotę do zapłaty.
        /// </summary>
        /// <param name="wplata">Ilość pieniędzy do rozliczenia.</param>
        /// <param name="procent">Oprocentowanie odsetek w skali roku.</param>
        /// <param name="odsetkiD">Referencja na odsetki do zapłaty.</param>
        /// <param name="nadplataB">Flaga czy uwzględniać nadpłatę.</param>
        /// <param name="odsetkiB">Flaga czy liczyć odsetki.</param>
        /// <returns>Kwota do zapłaty za fakturę bez odsetek.</returns>
        public static double LiczWplate(double wplata, int procent, ref double odsetkiD, bool nadplataB, bool odsetkiB)
        {
            if (!nadplataB)
            {
                //double suma_b = 0;

                //var reader1D = Baza.Wczytaj1D("SELECT Suma_B, Oplata FROM FAKTURY WHERE ID = '" + ID_Fak + "'");
                //suma_b = Convert.ToDouble(reader1D[0].Replace(".", ","));
                //do_oplacenia = Convert.ToDouble(reader1D[1].Replace(".", ","));

                //if (do_oplacenia < suma_b)
                //{
                //    do_oplacenia = suma_b - do_oplacenia;
                //}
                //else
                //{
                //    wplata = 0;
                //    do_oplacenia = 0;
                //    f_bilans = true;
                //}
            }
            else
            {
                odsetkiB = false;
            }





            //Stary sposób
            /*string sposob = "G";
            string dokument = "FV";
            bool f_bilans;
            odsetki = 0;

            if (propozycja == true)
            {
                wplata = 10000000;
            }
            else wplata = Convert.ToDouble(wplata_box.Text.Replace(".", ","));

            Baza.Ustaw("WPLATY");

            if (nadplata_ch.Checked == false)
            {
                double suma_b = 0;

                var reader1D = Baza.Wczytaj1D("SELECT Suma_B, Oplata FROM FAKTURY WHERE ID = '" + ID_Fak + "'");
                suma_b = Convert.ToDouble(reader1D[0].Replace(".", ","));
                do_oplacenia = Convert.ToDouble(reader1D[1].Replace(".", ","));

                if (do_oplacenia >= suma_b)
                {
                    wplata = 0;
                    do_oplacenia = 0;
                    f_bilans = true;
                }
                else do_oplacenia = suma_b - do_oplacenia;
            }
            else odsetki_ch.Checked = false;

            if (odsetki_ch.Checked == false && nadplata_ch.Checked == false)
            {
                kasa = 0;
                if (do_oplacenia < wplata)
                {
                    wplata = do_oplacenia;
                }
            }

            if (odsetki_ch.Checked == true)
            {
                DateTime data_faktury = new DateTime();   //data faktury, ktora oplacamy

                bool licz_dalej = true;         //czy program ma dalej naliczac odsetki czy wszystkie miesoiace są zliczone

                double procent = 13;            //oprocentowanie, 13 lub wybrane przez uzytkownika
                double suma = 0;                //oplata + odsetki

                liczba_dni_i = 0;               //suma dni ze wszystkich miesiecy
                kasa = wplata;                  //kasa do podzialu

                data_faktury = DateTime.Parse(Baza.Wczytaj("SELECT Data FROM Faktury WHERE ID = '" + ID_Fak + "'"));

                if (data_faktury.CompareTo(data_wplaty.Value) >= 0)
                {
                    MessageBox.Show("Błąd obliczania odsetek", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    odsetki = -1;
                }

                while (licz_dalej)
                {
                    if (data_wplaty.Value.Year == data_faktury.Year && data_faktury.Month == data_wplaty.Value.Month)
                    {
                        licz_dalej = false;
                    }
                    else
                    {
                        liczba_dni_i += DateTime.DaysInMonth(data_faktury.Year, data_faktury.Month);
                        data_faktury = data_faktury.AddMonths(1);
                    }
                }
                liczba_dni_i = liczba_dni_i - data_faktury.Day + data_wplaty.Value.Day;

                odsetki = Math.Round(do_oplacenia * (procent / 100) / 365 * liczba_dni_i, 2);

                suma = do_oplacenia + odsetki;

                if (do_oplacenia <= kasa - odsetki)
                {
                    kasa -= suma;
                }
                else
                {
                    do_oplacenia = Math.Round(do_oplacenia * kasa / suma, 2);
                    odsetki = Math.Round(odsetki * kasa / suma, 2);
                    kasa = 0;
                }
                wplata = do_oplacenia;
            }

            if (odsetki == -1)
            {
                wplata = 0;
                odsetki = 0;
                liczba_dni_i = 0;
                f_bilans = true;
            }

            if (propozycja == true) kasa = odsetki + wplata;

            za_fv.Text = "Za FV: " + String.Format("{0:N2}", Math.Round(wplata, 2));
            odsetki_l.Text = "Odsetki : " + String.Format("{0:N2}", Math.Round(odsetki, 2));
            razem_l.Text = "Razem : " + String.Format("{0:N2}", Math.Round(odsetki + wplata, 2));
            pozostalo_l.Text = "Pozostało : " + String.Format("{0:N2}", Math.Round(kasa, 2));
            liczba_dni.Text = "Liczba dni : " + liczba_dni_i.ToString();
            wplata_box.Text = Math.Round(kasa, 2).ToString("0.00");

            f_bilans = false;

            if (f_bilans == false && odsetki != -1 && propozycja == false)
            {
                if (przelew_rad.Checked == true)
                {
                    sposob = "P";
                    dokument = "W";
                }
                if (kp_rad.Checked == true) dokument = "KP";

                Baza.Zapisz("INSERT INTO WPLATY VALUES (GEN_ID(NUMERACJA_WPLATY,1),'" +
                    ID_Fak.ToString() + "','" +
                    data_wplaty.Value.Day + "." + data_wplaty.Value.Month + "." + data_wplaty.Value.Year + "','" +
                    wplata.ToString().Replace(",", ".") + "','" +
                    odsetki.ToString().Replace(",", ".") + "','" +
                    sposob + "','" +
                    dokument + "','" +
                    numer_dok_box.Text + "');");
            */


            return 0;
        }
    }
}
