﻿using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.MainWindowComponents;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Customers.CustomerIngredientsEdit
{
    public class CustomerIngredientsEditViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Customers;
        public ICommand CancelCommand => new RelayCommand(Cancel);
        public ICommand SaveCommand => new RelayCommand(Save);

        public bool IsVatRateNull { get; set; }

        public IngredientSale IngredientSale
        {
            get => _ingredientSale;
            set => SetProperty(ref _ingredientSale, value);
        }
        private IngredientSale _ingredientSale;

        public decimal BruttoValue
        {
            get => _bruttoValue;
            set => SetProperty(ref _bruttoValue, value);
        }
        private decimal _bruttoValue;

        public CustomerIngredientsEditViewModel() { }

        public CustomerIngredientsEditViewModel(IngredientSale ingredientSale)
        {
            IngredientSale = ingredientSale;
        }

        public override void Refresh(int? returnedValue = null) { } //TODO ?

        public void Save()
        {
            if (IsVatRateNull)
                IngredientSale.VatRate = null;

            new InvoiceIngredientAdapter().AddOrUpdate(IngredientSale);
        }

        public void Cancel()
        {
            //TODO: Przejść na
        }
    }
}
