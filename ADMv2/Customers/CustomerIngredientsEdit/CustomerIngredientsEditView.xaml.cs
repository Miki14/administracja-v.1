﻿using System.Windows;
using System.Windows.Controls;
using ADMv2.Common;
using ADMv2.DAL.Entities;

namespace ADMv2.Customers.CustomerIngredientsEdit
{
    public partial class CustomerIngredientsEditView : Window, IUserControlRefreshable
    {
        private readonly CustomerIngredientsEditViewModel _viewModel;

        public CustomerIngredientsEditView(IngredientSale ingredientSale = null)
        {
            ingredientSale ??= new IngredientSale()
            {
                VatRate = 0,
            };

            InitializeComponent();
            _viewModel = new CustomerIngredientsEditViewModel(ingredientSale);
            DataContext = _viewModel;
            if (ingredientSale.VatRate == null)
                VatZwCh.IsChecked = true;
        }

        public void Refresh(int? returnedValue = null)
        {
            ((CustomerIngredientsEditViewModel)DataContext).Refresh(returnedValue);
        }

        private void VatZwChChecked(object sender, RoutedEventArgs e)
        {
            //TODO: Powinno zmieniać ViewModel, a nie UI, ale nie działa. Można kiedyś zmienić
            if (VatZwCh.IsChecked == true)
            {
                _viewModel.IngredientSale.VatRate = null;
                _viewModel.IsVatRateNull = true;
            }
            else
            {
                _viewModel.IngredientSale.VatRate = 0;
                _viewModel.IsVatRateNull = false;
            }
            VatBox.Text = "0";
        }

        private void PriceBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            BruttoLabel.Content = string.Format("Brutto: {0:C2}", _viewModel.IngredientSale.Brutto);
        }

        private void SaveButt_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.Save();
            this.Close();
        }

        private void CancelButt_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
