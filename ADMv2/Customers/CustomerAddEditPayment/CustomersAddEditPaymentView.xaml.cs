﻿using System.Windows;
using System.Windows.Controls;
using ADMv2.Common;
using ADMv2.CommonProj;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;
using ADMv2.MainWindowComponents;

namespace ADMv2.Customers.CustomerAddEditPayment
{
    public partial class CustomersAddEditPaymentView : UserControl, IUserControlRefreshable
    {
        //TODO: Przenieść na ViewModel wszystko

        private readonly Payment _payment;
        private readonly SaleInvoice _invoice;
        private readonly bool Edit;
        private readonly PaymentAdapter paymentAdapter = new();

        public CustomersAddEditPaymentView(int paymentId, bool edit)
        {
            InitializeComponent();
            Edit = edit;
            _payment = paymentAdapter.Get(paymentId);
            DataContext = _payment;
            int invoiceId = paymentAdapter.GetInvoiceId(_payment.Id);
            _invoice = new InvoicesAdapter().Get(invoiceId);

            ShowSelectedDocument();

            dokumentRadioInvoice.Checked += DokumentRadioChecked;
            dokumentRadioKp.Checked += DokumentRadioChecked;
            dokumentRadioStatement.Checked += DokumentRadioChecked;
        }

        public void Refresh(int? returnedValue = null) { } //TODO ?

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            if (!Edit)
            {
                paymentAdapter.Remove(_payment);
            }
            MainWindow.Instance.CloseWindow(WindowType.Customers);
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            if (_payment.DocumentType == PaymentDocument.Invoice
                && !_invoice.Date.IsSameMonthYear(_payment.Date))
            {
                MessageBox.Show("Nie można dodać wpłaty na FV w innym miesiącu, niż data wystawienia FV. Dodaj wpłatę na KP");
            }
            else
            {
                paymentAdapter.AddOrUpdate((Payment)DataContext);

                MainWindow.Instance.CloseWindow(WindowType.Customers);
            }
        }

        private void CopyFromInvoiceButtClick(object sender, RoutedEventArgs e)
        {
            _payment.Amount = _invoice.Owe;
            AmountTextBox.Text = _payment.Amount.ToString();
        }

        private void DokumentRadioChecked(object sender, RoutedEventArgs e)
        {
            if (dokumentRadioKp.IsChecked == true)
            {
                _payment.DocumentType = PaymentDocument.KP;
                _payment.Method = PaymentMethod.Cash;
            }
            else if (dokumentRadioStatement.IsChecked == true)
            {
                _payment.DocumentType = PaymentDocument.Statement;
                _payment.Method = PaymentMethod.BankTransfer;
            }
            else
            {
                _payment.DocumentType = PaymentDocument.Invoice;
                _payment.Method = PaymentMethod.Cash;
            }
        }

        private void ShowSelectedDocument()
        {
            if (_payment.DocumentType == PaymentDocument.KP)
                dokumentRadioKp.IsChecked = true;
            else if (_payment.DocumentType == PaymentDocument.Statement)
                dokumentRadioStatement.IsChecked = true;
            else
                dokumentRadioInvoice.IsChecked = true;
        }
    }
}
