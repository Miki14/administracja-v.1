using ADMv2.Common;
using ADMv2.DAL.Entities;
using ADMv2.MainWindowComponents;

namespace ADMv2.Customers.CustomerAddEditPayment
{
    public class CustomersAddEditPaymentViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Customers;

        Payment Payment { get; set; }

        bool IsInvoiceChecked { get; set; }
        bool IsKpChecked { get; set; }
        bool IsBankTransferChecked { get; set; }

        public CustomersAddEditPaymentViewModel(Payment payment)
        {
            Payment = payment;
        }

        public override void Refresh(int? returnedValue = null) { } //TODO ?
    }
}