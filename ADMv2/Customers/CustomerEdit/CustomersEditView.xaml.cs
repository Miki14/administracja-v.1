﻿using System.Windows;
using ADMv2.Common;
using ADMv2.DAL.Enums;

namespace ADMv2.Customers
{
    public partial class CustomersEditView : Window, IUserControlRefreshable
    {
        private readonly CustomersEditViewModel _viewModel;

        public CustomersEditView(int customerId, bool edit)
        {
            InitializeComponent();
            _viewModel = new CustomersEditViewModel(customerId, edit);
            DataContext = _viewModel;
            GrupComboBox.ItemsSource = CustomerGroup.Inhabitants.GetDictionary();
            GrupComboBox.SelectedValue = _viewModel.Customer.Group;
        }

        public void Refresh(int? returnedValue = null) { } //TODO ?

        private async void GusButtonClick(object sender, RoutedEventArgs e)
        {
            //TODO: Przenieść na ViewModel
            var gusResult = await GusHelper.GetGusData(NipBox.Text);

            if (gusResult != null)
            {
                ImieBox.Text = gusResult.Name;
                Address1Box.Text = gusResult.Address1;
                Address2Box.Text = gusResult.Address2;
            }
        }

        private void SaveButtClick(object sender, RoutedEventArgs e)
        {
            _viewModel.Save();
            this.Close();
        }

        private void CancelButtClick(object sender, RoutedEventArgs e)
        {
            _viewModel.Back();
            this.Close();
        }
    }
}
