using System;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;
using ADMv2.MainWindowComponents;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Customers
{
    public class CustomersEditViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Customers;

        private Customer _customer;
        public Customer Customer
        {
            get { return _customer; }
            set { SetProperty(ref _customer, value); }
        }

        public int CustomerGroup
        {
            get
            {
                return (int)Customer.Group;
            }
            set
            {
                Customer.Group = (CustomerGroup)value;
            }
        }

        public bool IsEdit;
        private readonly CustomersAdapter customersAccess = new();

        public ICommand SaveCustomerCommand => new RelayCommand(Save);
        public ICommand BackCommand => new RelayCommand(Back);
        //public ICommand GusCommand => new RelayCommand(Gus);

        public bool IsEndlessContract
        {
            get { return _isEndlessContract; }
            set
            {
                DateTime? contractEndDateToSet = value ? null : Customer.ContractEndDate;
                Customer.ContractEndDate = contractEndDateToSet;
                SetProperty(ref _isEndlessContract, value);
            }
        }

        public bool NotEndlessContract
        {
            get { return !_isEndlessContract; }
        }

        private bool _isEndlessContract;

        public CustomersEditViewModel(int customerId, bool isEdit)
        {
            Customer = customersAccess.Get(customerId);
            IsEndlessContract = !Customer.ContractEndDate.HasValue;
            IsEdit = isEdit;
        }

        public void Save()
        {
            customersAccess.AddOrUpdate(Customer);
            //MainWindow.Instance.CloseWindow(WindowType);
        }

        public void Back()
        {
            if (!IsEdit)
            {
                customersAccess.Remove(Customer.Id);
            }
            //MainWindow.Instance.CloseWindow(WindowType);
        }

        public override void Refresh(int? returnedValue = null) { } //TODO ?

        //private async void Gus()
        //{
        //TODO: Odkomentowa�, ale naprawi� od�wie�anie UI
        //}
    }
}