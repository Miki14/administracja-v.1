﻿using System.Windows.Controls;
using System.Windows.Input;
using ADMv2.Common;

namespace ADMv2.Customers.CustomerIngredients
{
    public partial class CustomerIngredientsView : UserControl, IUserControlRefreshable
    {
        public CustomerIngredientsView()
        {
            InitializeComponent();
        }

        public void Refresh(int? returnedValue = null)
        {
            ((CustomerIngredientsViewModel)DataContext).Refresh(returnedValue);
        }

        private void ListViewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((CustomerIngredientsViewModel)DataContext).EditCommand.Execute(null);
        }

        private void ShowHiddenBoxChecked(object sender, System.Windows.RoutedEventArgs e)
        {
            Refresh();
        }
    }
}
