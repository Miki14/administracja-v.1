﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.Customers.CustomerIngredientsEdit;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.MainWindowComponents;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Customers.CustomerIngredients
{
    public class CustomerIngredientsViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Customers;

        public ObservableCollection<IngredientSale> Ingredients
        {
            get => _ingredients;
            set => SetProperty(ref _ingredients, value);
        }
        private ObservableCollection<IngredientSale> _ingredients = new();

        public IngredientSale SelectedIngredient
        {
            get => _selectedIngredient;
            set => SetProperty(ref _selectedIngredient, value);
        }
        private IngredientSale _selectedIngredient;

        public bool ShowHidden { get; set; }

        public ICommand EditCommand => new RelayCommand(Edit);
        public ICommand AddCommand => new RelayCommand(Add);
        public ICommand BackCommand => new RelayCommand(Back);

        public CustomerIngredientsViewModel()
        {
            Refresh();
        }

        public override void Refresh(int? returnedValue = null)
        {
            int? selectedSupplierId = SelectedIngredient?.Id;
            Ingredients = new ObservableCollection<IngredientSale>(new InvoiceIngredientAdapter().GetAll(ShowHidden));

            if (Ingredients.Count > 0)
                SelectedIngredient = Ingredients.FirstOrDefault(x => x.Id == selectedSupplierId) ?? Ingredients.FirstOrDefault();
        }

        private void Edit()
        {
            if (SelectedIngredient != null)
            {
                new CustomerIngredientsEditView(SelectedIngredient)
                {
                    Owner = MainWindow.Instance
                }.ShowDialog();
            }
            Refresh();
        }

        private void Add()
        {
            new CustomerIngredientsEditView()
            {
                Owner = MainWindow.Instance
            }.ShowDialog();
        }

        private void Back()
        {
            MainWindow.Instance.CloseWindow(WindowType);
        }
    }
}
