﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.Customers.CustomerIngredients;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.MainWindowComponents;
using ADMv2.Printing;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Customers.CustomerEditInvoice
{
    public class CustomerEditInvoiceViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Customers;

        public InvoicePrintingDto Invoice { get; set; }

        public ObservableCollection<IngredientSale> IngredientsSale
        {
            get => _ingredientsSale;
            set => SetProperty(ref _ingredientsSale, value);
        }
        private ObservableCollection<IngredientSale> _ingredientsSale = new();

        public ObservableCollection<SaleInvoiceIngredient> InvoiceIngredients
        {
            get => _invoiceIngredients;
            set => SetProperty(ref _invoiceIngredients, value);
        }
        private ObservableCollection<SaleInvoiceIngredient> _invoiceIngredients = new();

        public SaleInvoiceIngredient SelectedInvoiceIngredient
        {
            get => _selectedInvoiceIngredient;
            set => SetProperty(ref _selectedInvoiceIngredient, value);
        }
        private SaleInvoiceIngredient _selectedInvoiceIngredient;

        public IngredientSale SelectedIngredientSale
        {
            get => _selectedIngredientSale;
            set => SetProperty(ref _selectedIngredientSale, value);
        }
        private IngredientSale _selectedIngredientSale;

        public ICommand AddCommand => new RelayCommand(AddIngredient);
        public ICommand RemoveCommand => new RelayCommand(RemoveIngredient);
        public ICommand MoveUpCommand => new RelayCommand(MoveUpIngredient);
        public ICommand MoveDownCommand => new RelayCommand(MoveDownIngredient);
        public ICommand IngredientsCommand => new RelayCommand(Ingredients);
        public ICommand PrintPrevievCommand => new RelayCommand(PrintPreviev);
        public ICommand PrintCommand => new RelayCommand(Print);
        public ICommand CancelCommand => new RelayCommand(Cancel);
        public ICommand BackCommand => new RelayCommand(Back);

        public CustomerEditInvoiceViewModel()
        {
            //InvoiceIngredients = new();
            //IngredientsSale = new();
        }

        public CustomerEditInvoiceViewModel(SaleInvoice invoice)
        {
            Invoice = new InvoicePrintingDto(invoice);
            Refresh();
        }

        public override void Refresh(int? returnedValue = null)
        {
            InvoiceIngredientAdapter iia = new();

            InvoiceIngredients = new ObservableCollection<SaleInvoiceIngredient>(iia.GetInvoiceIngredients(Invoice.Id));
            IngredientsSale = new ObservableCollection<IngredientSale>(iia.GetAll());

            if (InvoiceIngredients.Any())
            {
                SelectedInvoiceIngredient = InvoiceIngredients.FirstOrDefault();
            }

            if (IngredientsSale.Any())
            {
                SelectedIngredientSale = IngredientsSale.FirstOrDefault();
            }
        }

        private void AddIngredient()
        {
            InvoiceIngredients.Add(new SaleInvoiceIngredient(SelectedIngredientSale));
        }

        private void RemoveIngredient()
        {
            if (SelectedInvoiceIngredient != null)
            {
                InvoiceIngredients.Remove(SelectedInvoiceIngredient);
            }
        }

        private void MoveUpIngredient()
        {
            if (SelectedInvoiceIngredient != null)
            {
                int oldIndex = InvoiceIngredients.IndexOf(SelectedInvoiceIngredient);
                if (oldIndex > 0)
                {
                    InvoiceIngredients.Move(oldIndex, oldIndex - 1);
                }
            }
        }

        private void MoveDownIngredient()
        {
            if (SelectedInvoiceIngredient != null)
            {
                int oldIndex = InvoiceIngredients.IndexOf(SelectedInvoiceIngredient);
                if (oldIndex < InvoiceIngredients.Count - 1)
                {
                    InvoiceIngredients.Move(oldIndex, oldIndex + 1);
                }
            }
        }

        private void Ingredients()
        {
            MainWindow.Instance.NewWindow(WindowType, new CustomerIngredientsView());
        }

        private void PrintPreviev()
        {
            SaveInvoice();
            new InvoicePrinting(Invoice).PrintPreview();
        }

        private void Print()
        {
            Invoice.Printed = true;
            SaveInvoice();
            new InvoicePrinting(Invoice).Print();
            MainWindow.Instance.CloseWindow(WindowType);
        }

        private void Cancel()
        {
            MainWindow.Instance.CloseWindow(WindowType);
        }

        private void Back()
        {
            SaveInvoice();
            MainWindow.Instance.CloseWindow(WindowType);
        }

        private bool SaveInvoice()
        {
            //TODO: Sprawdzać czy można zapisać FV - dobra data itp.
            new InvoicesAdapter().UpdateInvoice(Invoice, InvoiceIngredients);
            return true;
        }
    }
}
