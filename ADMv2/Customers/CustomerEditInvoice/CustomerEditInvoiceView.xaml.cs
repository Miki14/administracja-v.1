﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Entities;

namespace ADMv2.Customers.CustomerEditInvoice
{
    public partial class CustomerEditInvoiceView : UserControl, IUserControlRefreshable
    {
        private readonly CustomerEditInvoiceViewModel ViewModel;

        public CustomerEditInvoiceView(SaleInvoice invoice)
        {
            InitializeComponent();
            ViewModel = new CustomerEditInvoiceViewModel(invoice);
            DataContext = ViewModel;
            PaymentMethodCombo.ItemsSource = new List<string> { "przelew", "zapłacono gotówką", "gotówka / przelew", "..........." };
            PaymentDateCombo.ItemsSource = new List<string> { DateTime.Today.ToString("dd.MM.yyyy"), "...............", "7 dni", "14 dni", "30 dni" };
        }

        public void Refresh(int? returnedValue = null)
        {
            ViewModel.Refresh(returnedValue);
        }

        private void IngredientsMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var currentTextBox = (TextBox)sender;
            currentTextBox.IsReadOnly = !currentTextBox.IsReadOnly;
            //currentTextBox.BorderThickness = new System.Windows.Thickness(0, 0, 0, 0);
            //IsReadOnly="True" BorderThickness="0" Background="Transparent"
        }

        private void NettoLostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            //ViewModel.Refresh();
            //TODO: Po zmianie wartości netto nie przelicza brutto
        }
    }
}
