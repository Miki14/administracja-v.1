﻿using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;

namespace ADMv2.Customers.CustomersList
{
    public class CustomerListModel : Customer
    {
        public CustomerListModel() { }

        public CustomerListModel(Customer customer)
        {
            Id = customer.Id;
            LastName = customer.LastName;
            FirstName = customer.FirstName;
            Address1 = customer.Address1;
            Address2 = customer.Address2;
            PersonsCount = customer.PersonsCount;
            Meters = customer.Meters;
            Parking = customer.Parking;
            NIP = customer.NIP;
            Group = customer.Group;
            IdCard = customer.IdCard;
            Phone = customer.Phone;
            ContractEndDate = customer.ContractEndDate;
            Note = customer.Note;
            Email = customer.Email;
            //TODO: Wolne, poszukać szybszego sposobu?
            Balance = new CustomersAdapter().GetBalanceOfCustomer(customer);
            Month = new CustomersAdapter().GetMaxInvoiceMonth(customer);
        }

        public string GroupStr => Group.GetString();

        public decimal Balance { get; set; }

        public int Month { get; set; }
    }
}
