﻿using System.Windows.Controls;
using System.Windows.Input;
using ADMv2.Common;

namespace ADMv2.Customers.CustomersList
{
    public partial class CustomersView : UserControl, IUserControlRefreshable
    {
        public CustomersView()
        {
            InitializeComponent();
            DataContext = new CustomersListViewModel();
        }

        public void Refresh(int? returnedValue = null)
        {
            ((CustomersListViewModel)DataContext).Refresh(returnedValue);
        }

        private void CustomersListMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((CustomersListViewModel)DataContext).CustomerCardCommand.Execute(new object());
        }
    }
}
