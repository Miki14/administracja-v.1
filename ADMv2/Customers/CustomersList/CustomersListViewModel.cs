using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.MainWindowComponents;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Customers.CustomersList
{
    public class CustomersListViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Customers;

        private ObservableCollection<CustomerListModel> _customers;
        public ObservableCollection<CustomerListModel> Customers
        {
            get => _customers;
            set => SetProperty(ref _customers, value);
        }

        private CustomerListModel _selectedCustomer;
        public CustomerListModel SelectedCustomer
        {
            get => _selectedCustomer;
            set => SetProperty(ref _selectedCustomer, value);
        }

        private bool _showHidden;
        public bool ShowHidden
        {
            get => _showHidden;
            set
            {
                SetProperty(ref _showHidden, value);
                Refresh();
            }
        }

        private bool _showNotPrintedInvoices;
        public bool ShowNotPrintedInvoices
        {
            get => _showNotPrintedInvoices;
            set
            {
                SetProperty(ref _showNotPrintedInvoices, value);
                Refresh();
            }
        }

        private string _searchText;
        public string SearchText
        {
            get => _searchText;
            set
            {
                SetProperty(ref _searchText, value);
                Refresh();
            }
        }

        public ICommand AddCommand => new RelayCommand(AddCustomer);
        public ICommand EditCommand => new RelayCommand(EditCustomer);
        public ICommand CustomerCardCommand => new RelayCommand(CustomerCard);
        public ICommand BackCommand => new RelayCommand(Back);

        public CustomersListViewModel()
        {
            Refresh();
        }

        public override void Refresh(int? returnedValue = null)
        {
            int? selectedCustomerId = SelectedCustomer?.Id;

            Customers = new ObservableCollection<CustomerListModel>();
            List<Customer> dbCustomers = new CustomersAdapter().Get(SearchText, ShowHidden, ShowNotPrintedInvoices);

            foreach (Customer customer in dbCustomers)
            {
                Customers.Add(new CustomerListModel(customer));
            }

            if (Customers.Count > 0)
                SelectedCustomer = Customers.FirstOrDefault(x => x.Id == selectedCustomerId) ?? Customers.FirstOrDefault();
        }

        private void AddCustomer()
        {
            var customersAccess = new CustomersAdapter();

            int idNew = customersAccess.AddOrUpdate(new Customer
            {
                ContractEndDate = DateTime.Now,
                ContractStartDate = DateTime.Now
            });

            new CustomersEditView(SelectedCustomer.Id, true)
            {
                Owner = MainWindow.Instance,
            }.ShowDialog();

            Refresh();
        }

        private void EditCustomer()
        {
            if (SelectedCustomer != null)
            {
                new CustomersEditView(SelectedCustomer.Id, true)
                {
                    Owner = MainWindow.Instance,
                }.ShowDialog();
            }
            Refresh();
        }

        private void CustomerCard()
        {
            if (SelectedCustomer != null)
            {
                MainWindow.Instance.NewWindow(WindowType, new CustomerCardView(SelectedCustomer.Id));
            }
            Refresh();
        }

        private void Back()
        {
            MainWindow.Instance.CloseWindow(WindowType);
        }
    }
}