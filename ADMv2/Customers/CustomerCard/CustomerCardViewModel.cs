using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using ADMv2.Common;
using ADMv2.Customers.CustomerAddEditPayment;
using ADMv2.Customers.CustomerEditInvoice;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;
using ADMv2.MainWindowComponents;
using ADMv2.Printing;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Customers
{
    public class CustomerCardViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Customers;

        public ObservableCollection<SaleInvoice> Invoices
        {
            get => _invoices;
            set => SetProperty(ref _invoices, value);
        }
        private ObservableCollection<SaleInvoice> _invoices = new();

        public ObservableCollection<Payment> Payments
        {
            get => _payments;
            set => SetProperty(ref _payments, value);
        }
        private ObservableCollection<Payment> _payments = new();

        public SaleInvoice SelectedInvoice
        {
            get => _selectedInvoice;
            set
            {
                SetProperty(ref _selectedInvoice, value);
                RefreshPayments();
            }
        }
        private SaleInvoice _selectedInvoice;

        public Payment SelectedPayment
        {
            get => _selectedPayment;
            set => SetProperty(ref _selectedPayment, value);
        }
        private Payment _selectedPayment;

        public string CustomerBalance
        {
            get => _customerBalance;
            set => SetProperty(ref _customerBalance, value);
        }
        private string _customerBalance;

        public SolidColorBrush CustomerBalanceColor
        {
            get => _customerBalanceColor;
            set => SetProperty(ref _customerBalanceColor, value);
        }
        private SolidColorBrush _customerBalanceColor;

        public string PersonsCount => "Os�b: " + Customer.PersonsCount;

        public Customer Customer { get; set; }

        public ICommand RemoveInvoiceCommand => new RelayCommand(RemoveInvoice);
        public ICommand AddInvoiceCommand => new RelayCommand(AddInvoice);
        public ICommand EditInvoiceCommand => new RelayCommand(EditInvoice);
        public ICommand RemovePaymentCommand => new RelayCommand(RemovePayment);
        public ICommand AddPaymentCommand => new RelayCommand(AddPayment);
        public ICommand EditPaymentCommand => new RelayCommand(EditPayment);
        public ICommand RefreshInvoicesCommand => new RelayCommand(RefreshInvoices);
        public ICommand RefreshPaymentsCommand => new RelayCommand(RefreshPayments);
        public ICommand BackCommand => new RelayCommand(Back);
        public ICommand PrintKpCommand => new RelayCommand(PrintKp);
        public ICommand PreviewKpCommand => new RelayCommand(PreviewKp);

        private readonly CustomersAdapter customersAdapter = new();
        private readonly InvoicesAdapter invoicesAdapter = new();
        private readonly PaymentAdapter paymentAdapter = new();

        public CustomerCardViewModel()
        {
            Customer = new();
        }

        public CustomerCardViewModel(int customerId)
        {
            Customer = customersAdapter.Get(customerId);

            RefreshInvoices();
        }

        private void RemoveInvoice()
        {
            if (SelectedInvoice == null)
                return;

            switch (invoicesAdapter.CanDeleteInvoice(SelectedInvoice.Id))
            {
                case CanDeleteInvoiceResponse.InvoiceHavePayments:
                    MessageBox.Show("Do faktury s� dodane wp�aty. Przed usuni�ciem faktury usu� wp�aty.",
                    "B��d usuwania faktury - wp�aty", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case CanDeleteInvoiceResponse.InvoiceDoNotHaveLastNumber:
                    MessageBox.Show("Faktura nie jest ostatnia w danym miesi�cu. Mo�na usun�� tylko faktur� o najwy�szym numerze w danym miesi�cu.",
                    "B��d usuwania faktury - numer", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case CanDeleteInvoiceResponse.InvoiceSentToKsef:
                    MessageBox.Show("Faktura zosta�a wys�ana do systemu KSeF. Nie jest mo�liwe usuni�cie takiej faktury.",
                    "B��d usuwania faktury - KSeF", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case CanDeleteInvoiceResponse.Can:
                    var invoiceForDelete = invoicesAdapter.Get(SelectedInvoice.Id);

                    string invoiceNo = new CompanyDataAdapter().Get().InvoiceNo;
                    var result = MessageBox.Show(string.Format("Czy na pewno chcesz usun�� FV numer {0} / {1} / {2} / {3}?",
                        invoiceForDelete.Number, invoiceNo, invoiceForDelete.Date.Month, invoiceForDelete.Date.Year),
                        "Czy na pewno usun�� t� FV?", MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (result == MessageBoxResult.Yes)
                        invoicesAdapter.RemoveInvoice(invoiceForDelete.Id);
                    RefreshInvoices();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void AddInvoice()
        {
            if (SelectedInvoice == null)
            {
                invoicesAdapter.AddInvoice(null, Customer.Id);
            }
            else
            {
                invoicesAdapter.AddInvoiceBaseOn(SelectedInvoice, Customer.Id);
                RefreshInvoices();
            }
        }

        private void EditInvoice()
        {
            if (SelectedInvoice != null)
            {
                if (SelectedInvoice.Number == 0)
                {
                    MessageBox.Show("Nie mo�na edytowa� bilansu otwarcia.", "B��d edycji", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    var invoice = invoicesAdapter.Get(SelectedInvoice.Id);
                    MainWindow.Instance.NewWindow(WindowType, new CustomerEditInvoiceView(invoice));
                    RefreshInvoices();
                }
            }
        }

        private void RemovePayment()
        {

            if (SelectedPayment != null &&
                MessageBox.Show(string.Format("Czy na pewno chcesz usun�� wp�at� z dnia {0} ?", SelectedPayment.Date.ToString("dd.MM.yyyy")),
                "Usun��?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                paymentAdapter.Remove(SelectedPayment);
            }

            RefreshInvoices();
        }

        private void AddPayment()
        {
            //TODO: Tworzy� obiekt wp�aty w bazie przy zapisie edycji, a nie tu
            int paymentId = paymentAdapter.Create(SelectedInvoice.Id);
            MainWindow.Instance.NewWindow(WindowType, new CustomersAddEditPaymentView(paymentId, false));

            RefreshInvoices();
        }

        private void EditPayment()
        {
            if (SelectedPayment != null)
            {
                var payment = paymentAdapter.Get(SelectedPayment.Id);
                MainWindow.Instance.NewWindow(WindowType, new CustomersAddEditPaymentView(payment.Id, true));
            }

            RefreshInvoices();
        }

        private void RefreshInvoices()
        {
            int? selectedInvoiceId = SelectedInvoice?.Id;

            Invoices = new ObservableCollection<SaleInvoice>(invoicesAdapter.GetInvoicesDesc(Customer.Id));
            RefreshCustomerTotalBalance();

            if (Invoices.Count > 0)
            {
                SelectedInvoice = Invoices.FirstOrDefault(x => x.Id == selectedInvoiceId) ?? Invoices.FirstOrDefault();
                RefreshPayments();
            }
        }

        private void RefreshPayments()
        {
            if (SelectedInvoice != null)
            {
                int? selectedPaymentId = SelectedPayment?.Id;
                Payments = new ObservableCollection<Payment>(paymentAdapter.GetByInvoiceId(SelectedInvoice.Id));

                if (Payments.Count > 0)
                {
                    SelectedPayment = Payments.FirstOrDefault(x => x.Id == selectedPaymentId) ?? Payments.FirstOrDefault();
                }
            }
        }

        private void RefreshCustomerTotalBalance()
        {
            decimal customerBalance = customersAdapter.GetBalanceOfCustomer(Customer);
            CustomerBalance = string.Format("Bilans: {0:C2}", customerBalance);
            CustomerBalanceColor = customerBalance >= 0 ? Brushes.Green : Brushes.Red;
        }

        private void Back()
        {
            MainWindow.Instance.CloseWindow(WindowType);
        }

        private void PrintKp()
        {
            if (SelectedPayment != null)
            {
                new KpPrinting(SelectedPayment.Id).Print();
            }
        }

        private void PreviewKp()
        {
            if (SelectedPayment != null)
            {
                new KpPrinting(SelectedPayment.Id).PrintPreview();
            }
        }

        public override void Refresh(int? returnedValue = null)
        {
            RefreshInvoices();
        }
    }
}