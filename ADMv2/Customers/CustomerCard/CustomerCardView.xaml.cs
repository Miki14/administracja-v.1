﻿using System.Windows.Controls;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Entities;

namespace ADMv2.Customers
{
    /// <summary>
    /// Interaction logic for CustomerCardView.xaml
    /// </summary>
    public partial class CustomerCardView : UserControl, IUserControlRefreshable
    {
        readonly CustomerCardViewModel ViewModel;

        public CustomerCardView(int customerId)
        {
            InitializeComponent();
            ViewModel = new CustomerCardViewModel(customerId);
            DataContext = ViewModel;
        }

        public void Refresh(int? returnedValue = null)
        {
            ViewModel.RefreshInvoicesCommand.Execute(null);
        }

        private void InvoiceslistViewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ViewModel.EditInvoiceCommand.Execute(null);
        }

        private void PaymentslistViewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ViewModel.EditPaymentCommand.Execute(null);
        }
    }
}
