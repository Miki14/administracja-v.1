﻿using System;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.MainWindowComponents;
using ADMv2.Other.OtherTransactionsEdit;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Other.OtherTransactions
{
    public class OtherTransactionsEditViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Other;
        public OtherTransactionsEditWindow Window;

        public OtherTransaction Transaction
        {
            get => _transaction;
            set => SetProperty(ref _transaction, value);
        }
        private OtherTransaction _transaction;

        public ICommand SaveCommand => new RelayCommand(Save);
        public ICommand BackCommand => new RelayCommand(Back);

        public OtherTransactionsEditViewModel(OtherTransaction transaction = null)
        {
            if (transaction == null)
            {
                Transaction = new OtherTransaction()
                {
                    Date = DateTime.Today,
                };
            }
            else
                Transaction = new OtherTransactionAdapter().Get(transaction.Id);
        }

        public override void Refresh(int? returnedValue = null) { } //TODO ?

        private void Save()
        {
            new OtherTransactionAdapter().AddOrUpdate(Transaction);
            Window.Close();
        }

        private void Back()
        {
            Window.Close();
        }
    }
}
