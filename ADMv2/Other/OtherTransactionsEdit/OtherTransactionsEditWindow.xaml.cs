﻿using System.Windows;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;
using ADMv2.Other.OtherTransactions;

namespace ADMv2.Other.OtherTransactionsEdit
{
    /// <summary>
    /// Interaction logic for OtherTransactionsEditWindow.xaml
    /// </summary>
    public partial class OtherTransactionsEditWindow : Window
    {
        private OtherTransactionsEditViewModel _viewModel;
        public OtherTransactionsEditWindow(OtherTransaction otherTransaction = null)
        {
            InitializeComponent();
            _viewModel = new OtherTransactionsEditViewModel(otherTransaction);
            DataContext = _viewModel;
            _viewModel.Window = this;
            typeCombo.ItemsSource = OtherTransactionType.AccountToOther.GetDictionaryString();
            typeCombo.SelectedIndex = otherTransaction != null ? (int)_viewModel.Transaction.Type : 0;
        }

        private void DatePickerValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

        }

        private void typeCombo_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            _viewModel.Transaction.Type = (OtherTransactionType)typeCombo.SelectedIndex;
        }
    }
}
