﻿using System.Windows;
using System.Windows.Controls;
using ADMv2.Common;
using ADMv2.CommonProj;
using Xceed.Wpf.Toolkit;

namespace ADMv2.Other.Balance
{
    public partial class BalanceView : UserControl, IUserControlRefreshable
    {
        private readonly BalanceViewModel _viewModel;

        public BalanceView()
        {
            InitializeComponent();
            _viewModel = new BalanceViewModel();
            DataContext = _viewModel;

            oneMonthBox.Checked += OneMonthBoxChecked;
            oneMonthBox.Unchecked += OneMonthBoxChecked;
            DateFromPicker.ValueChanged += DateFromPickerValueChanged;
        }

        public void Refresh(int? returnedValue = null)
        {
            ((BalanceViewModel)DataContext).Refresh(returnedValue);
        }

        private void OneMonthBoxChecked(object sender, RoutedEventArgs e)
        {
            DateToPicker.IsEnabled = oneMonthBox.IsChecked != true;

            if (oneMonthBox.IsChecked == true)
            {
                DateFromPicker.Format = DateTimeFormat.YearMonth;
                DateFromPicker.Value = DateFromPicker.Value.Value.GetFirstDayOfMonth();
                RefreshDateToPicker();
            }
            else
            {
                DateFromPicker.Format = DateTimeFormat.Custom;
                DateFromPicker.FormatString = "dd MMMM yyyy";
            }

            Refresh();
        }

        private void RefreshDateToPicker()
        {
            if (oneMonthBox.IsChecked == true)
                DateToPicker.Value = DateFromPicker.Value.Value.GetLastDayOfMonth();
        }

        private void PreviousBalanceBoxChecked(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private void DateFromPickerValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            RefreshDateToPicker();
            Refresh();
        }

        private void DateToPickerValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Refresh();
        }
    }
}
