﻿using System;
using System.Windows.Forms;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.CommonProj;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Models;
using ADMv2.MainWindowComponents;
using ADMv2.Printing;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Other.Balance
{
    public class BalanceViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Balance;

        public DateTime DateFrom { get; set; } = DateTime.Today.AddMonths(-1).GetFirstDayOfMonth();
        public DateTime DateTo { get; set; } = DateTime.Today.AddMonths(-1).GetLastDayOfMonth();

        private BalanceModel _model;
        public BalanceModel Model
        {
            get => _model;
            set => SetProperty(ref _model, value);
        }

        public bool IsPreviousBalance { get; set; } = true;

        public ICommand RemoveCommand => new RelayCommand(Remove);
        public ICommand PreviewCommand => new RelayCommand(Preview);
        public ICommand PrintCommand => new RelayCommand(Print);
        public ICommand CancelCommand => new RelayCommand(Cancel);
        public ICommand SaveCommand => new RelayCommand(Save);

        public BalanceViewModel()
        {
            Refresh();
        }

        public override void Refresh(int? returnedValue = null)
        {
            Model = null;
            Model = new BalanceAdapter().Calculate(DateFrom, DateTo, IsPreviousBalance);
        }

        private void Remove()
        {
            var result = MessageBox.Show("Czy na pewno usunąć ostatnie saldo z dnia " + _model.PreviousBalanceDate.ToStringDMY(), "Usuń ostatnie saldo.",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                new BalanceAdapter().RemoveLastBalance(DateFrom);
                //To odświeżanie coś nie działa
                Refresh();
            }
        }

        private void Preview()
        {
            //TODO:
            //if (SaveBalanceQestion())
            {
                new PrintingFactory().BalancePreview(Model);
            }
        }

        private void Print()
        {
            //TODO:
            //if (SaveBalanceQestion())
            {
                new PrintingFactory().BalancePrint(Model);
            }
        }

        private void Cancel()
        {
            MainWindow.Instance.CloseWindow(WindowType);
        }

        private void Save()
        {
            var result = MessageBox.Show("Czy na pewno zapisać saldo?", "Zapisz saldo.",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                new BalanceAdapter().AddOrUpdate(Model);
                MainWindow.Instance.CloseWindow(WindowType);
            }
        }

        private bool SaveBalanceQestion()
        {
            var result = MessageBox.Show("Przed wydrukowaniem salda należy je zapisać. Czy chcesz to zrobić teraz?", "Zapisać saldo?",
                               MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            bool ret = result == DialogResult.Yes;
            if (ret)
                new BalanceAdapter().AddOrUpdate(Model);

            return ret;
        }
    }
}
