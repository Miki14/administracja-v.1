﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Enums;

namespace ADMv2.Other.OtherTransactions
{
    public partial class OtherTransactionsView : UserControl, IUserControlRefreshable
    {
        private readonly OtherTransactionsViewModel _viewModel;

        public OtherTransactionsView()
        {
            InitializeComponent();
            _viewModel = new OtherTransactionsViewModel();
            DataContext = _viewModel;
            TypeComboBox.ItemsSource = OtherTransactionType.AccountToCashdesk.GetDictionaryStringWithAll();
        }

        public void Refresh(int? returnedValue = null)
        {
            ((OtherTransactionsViewModel)DataContext).Refresh(returnedValue);
        }

        private void ListViewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((OtherTransactionsViewModel)DataContext).EditCommand.Execute(null);
        }

        private void TypeComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Refresh();
        }

        private void YearUpDownValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Refresh();
        }
    }
}
