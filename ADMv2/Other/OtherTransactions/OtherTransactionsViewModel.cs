﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;
using ADMv2.MainWindowComponents;
using ADMv2.Other.OtherTransactionsEdit;
using ADMv2.Printing.SummaryWindow;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Other.OtherTransactions
{
    public class OtherTransactionsViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.OtherTransaction;

        public OtherTransaction SelectedTransaction
        {
            get => _selectedTransaction;
            set => SetProperty(ref _selectedTransaction, value);
        }
        private OtherTransaction _selectedTransaction;

        private ObservableCollection<OtherTransaction> _transactions;
        public ObservableCollection<OtherTransaction> Transactions
        {
            get => _transactions;
            set => SetProperty(ref _transactions, value);
        }

        public int SelectedYear { get; set; } = DateTime.Now.Year;

        public int SelectedTypeId { get; set; } = -1;
        public OtherTransactionType? SelectedType
        {
            get
            {
                return SelectedTypeId == -1 ? null : (OtherTransactionType)SelectedTypeId;
            }
        }

        public ICommand PrintCommand => new RelayCommand(Print);
        public ICommand DeleteCommand => new RelayCommand(Delete);
        public ICommand EditCommand => new RelayCommand(Edit);
        public ICommand AddCommand => new RelayCommand(Add);
        public ICommand BackCommand => new RelayCommand(Back);

        public OtherTransactionsViewModel()
        {
            Refresh();
        }

        public override void Refresh(int? returnedValue = null)
        {
            int? SelectedTransactionId = SelectedTransaction?.Id;
            Transactions = new ObservableCollection<OtherTransaction>(new OtherTransactionAdapter().GetAll(SelectedYear, SelectedType));

            SelectedTransaction = SelectedTransactionId.HasValue
                ? Transactions.FirstOrDefault(x => x.Id == SelectedTransactionId.Value)
                : Transactions.FirstOrDefault();
        }

        private void Print()
        {
            new SummaryWindowView(true).ShowDialog();
        }

        private void Delete()
        {
            if (SelectedTransaction != null
                && MessageBox.Show("Czy na pewno usunąć ten element?", "Usunąć?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                new OtherTransactionAdapter().Remove(SelectedTransaction.Id);
            }

            Refresh();
        }

        private void Edit()
        {
            if (SelectedTransaction != null)
            {
                new OtherTransactionsEditWindow(SelectedTransaction).ShowDialog();
                Refresh();
            }
        }

        private void Add()
        {
            new OtherTransactionsEditWindow().ShowDialog();
            Refresh();
        }

        private void Back()
        {
            MainWindow.Instance.CloseWindow(WindowType);
        }
    }
}
