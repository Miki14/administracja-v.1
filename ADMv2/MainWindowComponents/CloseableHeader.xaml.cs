﻿using System.Windows.Controls;

namespace ADMv2.MainWindowComponents
{
    /// <summary>
    /// Interaction logic for CloseableHeader.xaml
    /// </summary>
    public partial class CloseableHeader : UserControl
    {
        public CloseableHeader()
        {
            InitializeComponent();
        }
    }
}
