﻿using System;
using ADMv2.Common;
using ADMv2.DAL;

namespace ADMv2.MainWindowComponents.LoginWindow
{
    public class LoginWindowViewModel : ViewModel
    {
        public override WindowType WindowType => throw new NotImplementedException();

        public string UserName { get; set; }
        public string Password { get; set; }
        public int SelectedDbKey { get; set; }

        public bool _isLoginSucessful = false;
        public bool IsLoginSucessful => _isLoginSucessful;

        public override void Refresh(int? returnedValue = null) { } //TODO ?

        public bool Login()
        {
            _isLoginSucessful = CurrentUserManager.LoginUser(UserName, Password);

            if (_isLoginSucessful)
                ConnectionStringsManager.SelectConnectionString(SelectedDbKey);

            return _isLoginSucessful;
        }
    }
}
