﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using ADMv2.DAL;

namespace ADMv2.MainWindowComponents.LoginWindow
{
    public partial class LoginWindowView : Window
    {
        private readonly LoginWindowViewModel _viewModel;
        private readonly Dictionary<int, string> _databases;

        public LoginWindowView()
        {
            InitializeComponent();

            _viewModel = new LoginWindowViewModel();
            DataContext = _viewModel;
            _databases = ConnectionStringsManager.GetDatabases();
            databasesCombo.ItemsSource = _databases;
            databasesCombo.SelectedValue = ConnectionStringsManager.SelectedConfig.Id;

#if DEBUG
            loginBox.Text = "ela";
            _viewModel.UserName = "ela";
            passwordBox.Password = "asia";
            _viewModel.Password = "asia";
#endif
        }

        private void LoginButtClick(object sender, RoutedEventArgs e)
        {
            _viewModel.Login();
            if (_viewModel.IsLoginSucessful)
            {
                this.Close();
            }
            else
            {
                MessageBox.Show("Błędne dane logowania", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void CancelButtClick(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_viewModel.IsLoginSucessful)
                Environment.Exit(0);
        }

        private void PasswordBoxPasswordChanged(object sender, RoutedEventArgs e)
        {
            _viewModel.Password = passwordBox.Password;
        }

        private void TextBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                LoginButtClick(this, null);
                e.Handled = true;
            }
        }
    }
}
