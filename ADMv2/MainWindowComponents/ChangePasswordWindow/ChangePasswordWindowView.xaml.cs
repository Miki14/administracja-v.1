﻿using System.Windows;

namespace ADMv2.MainWindowComponents.ChangePasswordWindow
{
    public partial class ChangePasswordWindowView : Window
    {
        private readonly ChangePasswordWindowViewModel _viewModel;

        public ChangePasswordWindowView()
        {
            InitializeComponent();

            _viewModel = new ChangePasswordWindowViewModel();
            DataContext = _viewModel;
        }

        private void CancelButtClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void PasswordBoxPasswordChanged(object sender, RoutedEventArgs e)
        {
            _viewModel.Password = passwordBox.Password;
        }

        private void NewPasswordBoxPasswordChanged(object sender, RoutedEventArgs e)
        {
            _viewModel.NewPassword = newPasswordBox.Password;
        }

        private void SaveButtClick(object sender, RoutedEventArgs e)
        {
            if (_viewModel.Save())
                this.Close();
        }
    }
}
