﻿using System;
using System.Windows;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;

namespace ADMv2.MainWindowComponents.ChangePasswordWindow
{
    public class ChangePasswordWindowViewModel : ViewModel
    {
        public override WindowType WindowType => throw new NotImplementedException();

        public User _currentUser { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string Name { get; set; }

        public override void Refresh(int? returnedValue = null) { } //TODO ?

        public ChangePasswordWindowViewModel()
        {
            _currentUser = CurrentUserManager.CurrentUser;
            User = _currentUser.Login;
            Name = _currentUser.Name;
        }

        public bool Save()
        {
            if (NewPassword != null && CurrentUserManager.LoginUser(_currentUser.Login, Password))
            {
                _currentUser.PasswordHash = CurrentUserManager.CreateHashForUser(_currentUser, NewPassword);
                _currentUser.Name = Name;
                new UserAdapter().AddOrUpdate(_currentUser);
                return true;
            }
            else
            {
                MessageBox.Show("Nowe hasło nie może być puste lub aktualne hasło jest niepoprawne", "Błędne hasło", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }
    }
}