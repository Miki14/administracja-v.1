﻿using System;

namespace ADMv2.MainWindowComponents
{
    public enum WindowType
    {
        Customers,
        Purchase,
        OtherTransaction,
        CustomerIngredients,
        Balance,
        Summary,
        IngredientsSummary,
        ManageBuildings,
        CompanyData,
        Update,
        About,
        Other,
    }

    public static class CustomerGroupExtensions
    {
        public static string GetString(this WindowType windowType)
        {
            return windowType switch
            {
                WindowType.Customers => "Osoby",
                WindowType.Purchase => "Zakupy",
                WindowType.OtherTransaction => "Inne obroty",
                WindowType.CustomerIngredients => "Składniki sprzedaży",
                WindowType.Balance => "Saldo",
                WindowType.Summary => "Zestawienia",
                WindowType.IngredientsSummary => "Zestawienie składników",
                WindowType.ManageBuildings => "Zarządzanie budynkami",
                WindowType.CompanyData => "Dane sprzedawcy",
                WindowType.Update => "Aktualizacja",
                WindowType.About => "O programie",
                WindowType.Other => "Inne",
                _ => throw new NotImplementedException(),
            };
        }
    }
}
