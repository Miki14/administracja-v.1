﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace ADMv2.MainWindowComponents
{
    public class ClosableTab : TabItem
    {
        // Constructor
        public ClosableTab()
        {
            // Create an instance of the usercontrol
            CloseableHeader closableTabHeader = new CloseableHeader();

            // Assign the usercontrol to the tab header
            Header = closableTabHeader;

            //this.Content = content;

            // Attach to the CloseableHeader events (Mouse Enter/Leave, Button Click, and Label resize)
            closableTabHeader.button_close.MouseEnter += ButtonCloseMouseEnter;
            closableTabHeader.button_close.MouseLeave += ButtonCloseMouseLeave;
            closableTabHeader.button_close.Click += ButtonCloseClick;
            closableTabHeader.label_TabTitle.SizeChanged += LabelTabTitleSizeChanged;
            this.FontSize = 16;
            //this.FontFamily = new FontFamily("Microsoft Sans Serif");
        }

        /// <summary>
        /// Property - Set the Title of the Tab
        /// </summary>
        public string Title
        {
            set
            {
                ((CloseableHeader)Header).label_TabTitle.Content = value;
            }
        }

        //
        // - - - Overrides  - - -
        //

        // Override OnSelected - Show the Close Button
        protected override void OnSelected(RoutedEventArgs e)
        {
            base.OnSelected(e);
            ((CloseableHeader)Header).button_close.Visibility = Visibility.Visible;
        }

        // Override OnUnSelected - Hide the Close Button
        protected override void OnUnselected(RoutedEventArgs e)
        {
            base.OnUnselected(e);
            ((CloseableHeader)Header).button_close.Visibility = Visibility.Hidden;
        }

        // Override OnMouseEnter - Show the Close Button
        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            ((CloseableHeader)Header).button_close.Visibility = Visibility.Visible;
        }

        // Override OnMouseLeave - Hide the Close Button (If it is NOT selected)
        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            if (!IsSelected)
            {
                ((CloseableHeader)Header).button_close.Visibility = Visibility.Hidden;
            }
        }

        //
        // - - - Event Handlers  - - -
        //


        // Button MouseEnter - When the mouse is over the button - change color to Red
        void ButtonCloseMouseEnter(object sender, MouseEventArgs e)
        {
            ((CloseableHeader)Header).button_close.Foreground = Brushes.Red;
        }

        // Button MouseLeave - When mouse is no longer over button - change color back to black
        void ButtonCloseMouseLeave(object sender, MouseEventArgs e)
        {
            ((CloseableHeader)Header).button_close.Foreground = Brushes.Black;
        }

        // Button Close Click - Remove the Tab - (or raise an event indicating a "CloseTab" event has occurred)
        void ButtonCloseClick(object sender, RoutedEventArgs e)
        {
            ((TabControl)Parent).Items.Remove(this);
        }

        // Label SizeChanged - When the Size of the Label changes (due to setting the Title) set position of button properly
        void LabelTabTitleSizeChanged(object sender, SizeChangedEventArgs e)
        {
            ((CloseableHeader)Header).button_close.Margin = new Thickness(((CloseableHeader)Header).label_TabTitle.ActualWidth + 5, 3, 4, 0);
        }
    }
}