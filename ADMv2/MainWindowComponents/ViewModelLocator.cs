/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:ADMv2"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using System;
using ADMv2.Customers;
using ADMv2.Customers.CustomerEditInvoice;
using ADMv2.Customers.CustomerIngredients;
using ADMv2.Customers.CustomerIngredientsEdit;
using ADMv2.Customers.CustomersList;
using ADMv2.DAL.Entities;
using ADMv2.MainWindowComponents.ChangePasswordWindow;
using ADMv2.MainWindowComponents.LoginWindow;
using ADMv2.Other.Balance;
using ADMv2.Other.OtherTransactions;
using ADMv2.Printing.SummaryWindow;
using ADMv2.Purchases.PurchaseEdit;
using ADMv2.Purchases.PurchaseIngredients;
using ADMv2.Purchases.PurchaseIngredientsEdit;
using ADMv2.Purchases.PurchasesList;
using ADMv2.Purchases.PurchasesSuppliersList;
using ADMv2.SystemWindows.CompanyData;
using Microsoft.Extensions.DependencyInjection;

namespace ADMv2.MainWindowComponents
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator() { }

        public static MainViewModel Main => App.Current.Services.GetService<MainViewModel>();
        public static CustomersListViewModel Customers => App.Current.Services.GetService<CustomersListViewModel>();
        public static Customer Customer => App.Current.Services.GetService<Customer>();
        public static CustomersEditViewModel CustomerEdit => App.Current.Services.GetService<CustomersEditViewModel>();
        public static CustomerCardViewModel CustomerCard => App.Current.Services.GetService<CustomerCardViewModel>();
        public static CustomerEditInvoiceViewModel CustomerEditInvoiceViewModel => App.Current.Services.GetService<CustomerEditInvoiceViewModel>();
        public static CustomerIngredientsViewModel CustomerIngredientsViewModel => App.Current.Services.GetService<CustomerIngredientsViewModel>();
        public static Payment Payment => App.Current.Services.GetService<Payment>();
        public static PurchasesListViewModel PurchasesListViewModel => App.Current.Services.GetService<PurchasesListViewModel>();
        public static PurchaseEditViewModel PurchaseEditViewModel => App.Current.Services.GetService<PurchaseEditViewModel>();
        public static CustomerIngredientsEditViewModel CustomerIngredientsEditViewModel => App.Current.Services.GetService<CustomerIngredientsEditViewModel>();
        public static PurchasesSuppliersListViewModel PurchasesSuppliersListViewModel => App.Current.Services.GetService<PurchasesSuppliersListViewModel>();
        public static PurchasesSupplierEditViewModel PurchasesSupplierEditViewModel => App.Current.Services.GetService<PurchasesSupplierEditViewModel>();
        public static PurchasesIngredientsViewModel PurchasesIngredientsViewModel => App.Current.Services.GetService<PurchasesIngredientsViewModel>();
        public static CompanyDataViewModel CompanyDataViewModel => App.Current.Services.GetService<CompanyDataViewModel>();
        public static OtherTransactionsViewModel OtherTransactionsViewModel => App.Current.Services.GetService<OtherTransactionsViewModel>();
        public static OtherTransactionsEditViewModel OtherTransactionsEditViewModel => App.Current.Services.GetService<OtherTransactionsEditViewModel>();
        public static PurchasesIngredientsEditViewModel PurchasesIngredientsEditViewModel => App.Current.Services.GetService<PurchasesIngredientsEditViewModel>();
        public static SummaryWindowViewModel SummaryWindowViewModel => App.Current.Services.GetService<SummaryWindowViewModel>();
        public static LoginWindowViewModel LoginWindowViewModel => App.Current.Services.GetService<LoginWindowViewModel>();
        public static BalanceViewModel BalanceViewModel => App.Current.Services.GetService<BalanceViewModel>();
        public static ChangePasswordWindowViewModel ChangePasswordWindowViewModel => App.Current.Services.GetService<ChangePasswordWindowViewModel>();

        //Po dodaniu tu doda� te� w App.xaml.cs
    }
}