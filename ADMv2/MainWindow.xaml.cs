﻿using System;
using System.Collections;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ADMv2.Common;
using ADMv2.Customers.CustomerIngredients;
using ADMv2.Customers.CustomersList;
using ADMv2.DAL;
using ADMv2.DAL.Firebird;
using ADMv2.MainWindowComponents;
using ADMv2.MainWindowComponents.ChangePasswordWindow;
using ADMv2.MainWindowComponents.LoginWindow;
using ADMv2.Other.Balance;
using ADMv2.Other.OtherTransactions;
using ADMv2.Printing.SummaryWindow;
using ADMv2.Purchases.PurchaseIngredients;
using ADMv2.Purchases.PurchasesList;
using ADMv2.Purchases.PurchasesSuppliersList;
using ADMv2.SystemWindows.About;
using ADMv2.SystemWindows.CompanyData;

namespace ADMv2
{
    public partial class MainWindow : Window
    {
        private static MainWindow _instance;
        public static MainWindow Instance => _instance;
        private readonly Stack _stackWindowCustomers = new();
        private readonly Stack _stackWindowPurchase = new();
        private readonly Stack _stackWindowCustomerData = new();

        private static int counterMs = 0;
        private static int counterFirebird = 0;

        public MainWindow()
        {
            InitializeComponent();
            _instance = this;

            //TODO: Przejść na common.CurretnVersion, żeby mieć tylko 2 cyfry
            Title = "Administracja v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public void NewWindow(WindowType windowType, UserControl newWindow)
        {
            TabItem currentTab = SearchCard(windowType);

            if (currentTab == null)
            {
                string name = windowType.GetString();
                currentTab = new ClosableTab
                {
                    Title = name,
                    Name = name.Replace(" ", ""),
                    Content = newWindow
                };

                tabcontrol.Items.Add(currentTab);
            }
            else
            {
                var stack = GetStack(windowType);
                stack.Push(currentTab.Content);
                currentTab.Content = newWindow;
            }

            currentTab.Focus();
        }

        public void CloseWindow(WindowType windowType, int? returnedValue = null)
        {
            var stack = GetStack(windowType);

            TabItem previousWindow = SearchCard(windowType);
            if (stack != null && stack.Count != 0)
            {
                previousWindow.Content = stack.Pop();
                try
                {
                    ((IUserControlRefreshable)previousWindow.Content).Refresh(returnedValue);
                }
                catch { }
            }
            else
            {
                tabcontrol.Items.Remove(previousWindow);
            }
        }

        private TabItem SearchCard(WindowType windowType)
        {
            return tabcontrol.Items.Cast<TabItem>().FirstOrDefault(item => string.CompareOrdinal(item.Name, windowType.GetString().Replace(" ", "")) == 0);
        }

        private Stack GetStack(WindowType windowType)
        {
            return windowType switch
            {
                WindowType.Purchase => _stackWindowPurchase,
                WindowType.Customers => _stackWindowCustomers,
                WindowType.CompanyData => _stackWindowCustomerData,
                _ => null,
            };
        }

        private void CustomersClick(object sender, RoutedEventArgs e)
        {
            NewWindow(WindowType.Customers, new CustomersView());
        }

        private void CustomersIngredientsButtClick(object sender, RoutedEventArgs e)
        {
            NewWindow(WindowType.Customers, new CustomerIngredientsView());
        }

        private void PurchaseClick(object sender, RoutedEventArgs e)
        {
            NewWindow(WindowType.Purchase, new PurchasesListView());
        }

        private void PurchaseIngredientsButtClick(object sender, RoutedEventArgs e)
        {
            NewWindow(WindowType.Purchase, new PurchaseIngredientsView());
        }

        private void PurchaseSuppliersButtClick(object sender, RoutedEventArgs e)
        {
            NewWindow(WindowType.Purchase, new PurchasesSuppliersListView());
        }

        private void SummaryButtClick(object sender, RoutedEventArgs e)
        {
            new SummaryWindowView()
            {
                Owner = this
            }.ShowDialog();
        }

        private void SummaryIngredientsButtClick(object sender, RoutedEventArgs e)
        {

        }

        private void DłuznicyButtClick(object sender, RoutedEventArgs e)
        {

        }

        private void BalancesButtClick(object sender, RoutedEventArgs e)
        {
            NewWindow(WindowType.Balance, new BalanceView());
        }

        private void OtherTransactionsButtClick(object sender, RoutedEventArgs e)
        {
            NewWindow(WindowType.OtherTransaction, new OtherTransactionsView());
        }

        private void SettingsButtClick(object sender, RoutedEventArgs e)
        {
            NewWindow(WindowType.CompanyData, new CompanyDataView());
        }

        private void BuildingsManagerButtClick(object sender, RoutedEventArgs e)
        {

        }

        private void ManageUsersButtClick(object sender, RoutedEventArgs e)
        {

        }

        private void AboutButtClick(object sender, RoutedEventArgs e)
        {
            new AboutWindow()
            {
                Owner = this
            }.ShowDialog();
        }

        private void CopyMsDbClick(object sender, RoutedEventArgs e)
        {
            int maxCounter = 2;
            new ConverterFrebirdToMsSql().ConvertDb(counterMs);
            MessageBox.Show(string.Format("Baza skopiowana. Licznik = {0}/{1}.", counterMs, maxCounter));
            counterMs++;
        }

        private void CopyFbDbClick(object sender, RoutedEventArgs e)
        {
            int maxCounter = 5;
            new ConverterFrebirdToFirebird().ConvertDb(counterFirebird);
            MessageBox.Show(string.Format("Baza skopiowana. Licznik = {0}/{1}.", counterFirebird, maxCounter));
            counterFirebird++;
        }

        private void CloseClick(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void LogoutButtClick(object sender, RoutedEventArgs e)
        {
            CurrentUserManager.LogOut();
            ClearTabs();
            ShowLoginWindow();
        }

        private void ClearTabs()
        {
            //Wszytskie zakładki oprócz Głównej
            int count = tabcontrol.Items.Count;
            for (int i = 1; i < count; i++)
            {
                tabcontrol.Items.RemoveAt(1);
            }
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            ShowLoginWindow();
        }

        private void ShowLoginWindow()
        {
            dbLabel.Content = "";

            new LoginWindowView()
            {
                Owner = this
            }.ShowDialog();

            dbLabel.Content = "Baza: " + ConnectionStringsManager.SelectedConfig.Name;
        }

        private void ChangePassButtClick(object sender, RoutedEventArgs e)
        {
            new ChangePasswordWindowView()
            {
                Owner = this
            }.ShowDialog();
        }
    }
}
