﻿using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.MainWindowComponents;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Purchases.PurchasesList
{
    public class PurchasesSupplierEditViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Purchase;

        public ICommand GusCommand => new RelayCommand(Gus);
        public ICommand SaveCommand => new RelayCommand(Save);
        public ICommand BackCommand => new RelayCommand(Back);

        public Supplier Supplier { get; set; }

        public PurchasesSupplierEditViewModel() { }

        public PurchasesSupplierEditViewModel(int? supplierId)
        {
            if (supplierId.HasValue)
                Supplier = new SupplierAdapter().Get(supplierId.Value);
            else
                Supplier = new Supplier();
        }

        public override void Refresh(int? returnedValue = null) { } //TODO ?

        private void Gus()
        {
            //TODO: Przenieść z Code behind
        }

        private void Save()
        {
            new SupplierAdapter().AddOrUpdate(Supplier);
            MainWindow.Instance.CloseWindow(WindowType);
        }

        private void Back()
        {
            MainWindow.Instance.CloseWindow(WindowType);
        }
    }
}
