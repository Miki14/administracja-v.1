﻿using System.Windows.Controls;
using ADMv2.Common;
using ADMv2.Purchases.PurchasesList;

namespace ADMv2.Purchases.PurchasesSupplierEdit
{
    public partial class PurchasesSupplierEditView : UserControl, IUserControlRefreshable
    {
        private readonly PurchasesSupplierEditViewModel _model;

        public PurchasesSupplierEditView(int? supplierId = null)
        {
            InitializeComponent();
            _model = new PurchasesSupplierEditViewModel(supplierId);
            DataContext = _model;
        }

        public void Refresh(int? returnedValue = null)
        {
            ((PurchasesSupplierEditViewModel)DataContext).Refresh(returnedValue);
        }

        private async void GusButtClick(object sender, System.Windows.RoutedEventArgs e)
        {
            //TODO: Przenieść na ViewModel
            var gusResult = await GusHelper.GetGusData(NipBox.Text);

            if (gusResult != null)
            {
                Name1Box.Text = gusResult.Name;
                AddressBox.Text = gusResult.Address1 + " " + gusResult.Address2;
            }
        }
    }
}
