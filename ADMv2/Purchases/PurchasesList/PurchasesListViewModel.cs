﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Models;
using ADMv2.MainWindowComponents;
using ADMv2.Purchases.PurchaseEdit;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Purchases.PurchasesList
{
    public class PurchasesListViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Purchase;

        public ICommand RemoveCommand => new RelayCommand(Remove);
        public ICommand CopyCommand => new RelayCommand(Copy);
        public ICommand AddCommand => new RelayCommand(Add);
        public ICommand EditCommand => new RelayCommand(Edit);
        public ICommand BackCommand => new RelayCommand(Back);

        private ObservableCollection<PurchaseListModel> _purchases;
        public ObservableCollection<PurchaseListModel> Purchases
        {
            get => _purchases;
            set => SetProperty(ref _purchases, value);
        }

        public PurchaseListModel _selectedPurchase;
        public PurchaseListModel SelectedPurchase
        {
            get => _selectedPurchase;
            set => SetProperty(ref _selectedPurchase, value);
        }

        public DateTime SelectedMonth { get; set; }

        public bool IsOnlyOneMonth { get; set; } = true;

        public PurchasesListViewModel()
        {
            Refresh();
        }

        public override void Refresh(int? returnedValue = null)
        {
            int? selectedPurchaseId = SelectedPurchase?.Id;

            var dbPurchases = IsOnlyOneMonth ? new PurchaseAdapter().GetAll(SelectedMonth) : new PurchaseAdapter().GetAll(null);

            Purchases = new ObservableCollection<PurchaseListModel>(dbPurchases);

            if (Purchases.Count > 0)
                SelectedPurchase = Purchases.FirstOrDefault(x => x.Id == selectedPurchaseId) ?? Purchases.FirstOrDefault();
        }

        private void Edit()
        {
            if (SelectedPurchase != null)
            {
                MainWindow.Instance.NewWindow(WindowType, new PurchasesEditView(SelectedPurchase.Id, PurchaseEditMode.Edit));
            }

            Refresh();
        }

        private void Back()
        {
            MainWindow.Instance.CloseWindow(WindowType);
        }

        private void Remove()
        {
            if (SelectedPurchase != null
                && MessageBox.Show("Czy na pewno usunąć ten zakup?", "Usunąć?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                new PurchaseAdapter().Remove(SelectedPurchase.Id);
            }

            Refresh();
        }

        private void Add()
        {
            //TODO: Tworzyć zakup przy zapisie, a nie tu
            var newId = new PurchaseAdapter().Create();
            MainWindow.Instance.NewWindow(WindowType, new PurchasesEditView(newId, PurchaseEditMode.Add));

            Refresh();
        }

        private void Copy()
        {
            var newId = new PurchaseAdapter().Copy(SelectedPurchase.Id);
            MainWindow.Instance.NewWindow(WindowType, new PurchasesEditView(newId, PurchaseEditMode.Copy));

            Refresh();
        }
    }
}
