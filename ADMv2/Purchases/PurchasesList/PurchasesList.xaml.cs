﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ADMv2.Common;

namespace ADMv2.Purchases.PurchasesList
{
    public partial class PurchasesListView : UserControl, IUserControlRefreshable
    {
        private readonly PurchasesListViewModel _viewModel;

        public PurchasesListView()
        {
            InitializeComponent();
            _viewModel = (PurchasesListViewModel)DataContext;
            MonthPicker.Value = DateTime.Today;

            MonthBox.Checked += MonthBoxChecked;
            MonthBox.Unchecked += MonthBoxChecked;
        }

        public void Refresh(int? returnedValue = null)
        {
            ((PurchasesListViewModel)DataContext).Refresh(returnedValue);
        }

        private void InvoiceslistViewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            _viewModel.EditCommand.Execute(null);
        }

        private void MonthPickerValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Refresh();
        }

        private void MonthBoxChecked(object sender, RoutedEventArgs e)
        {
            _viewModel.IsOnlyOneMonth = MonthBox.IsChecked.Value;
            MonthPicker.IsEnabled = _viewModel.IsOnlyOneMonth;
            Refresh();
        }
    }
}
