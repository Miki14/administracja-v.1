﻿using System;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;
using ADMv2.MainWindowComponents;
using ADMv2.Purchases.PurchaseIngredients;
using ADMv2.Purchases.PurchasesSuppliersList;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Purchases.PurchaseEdit
{
    public class PurchaseEditViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Purchase;

        public ICommand SuppliersCommand => new RelayCommand(Suppliers);
        public ICommand IngerdientsCommand => new RelayCommand(Ingerdients);
        public ICommand SaveCommand => new RelayCommand(Save);
        public ICommand BackCommand => new RelayCommand(Back);

        public Purchase Purchase { get; set; }
        private int _purchaseId { get; set; }

        private Supplier _supplier;
        public Supplier Supplier
        {
            get
            {
                return _supplier;
            }
            set
            {
                SetProperty(ref _supplier, value);
            }
        }

        public bool IsCashSelected
        {
            get
            {
                return Purchase?.PaymentMethod == PaymentMethod.Cash;
            }
            set
            {
                Purchase.PaymentMethod = value ? PaymentMethod.Cash : PaymentMethod.BankTransfer;
            }
        }
        private readonly PurchaseEditMode _editMode;

        public PurchaseEditViewModel() { }

        public PurchaseEditViewModel(int purchaseId, PurchaseEditMode editMode)
        {
            _purchaseId = purchaseId;
            _editMode = editMode;
            Refresh();
        }

        public override void Refresh(int? returnedValue = null)
        {
            Purchase = new PurchaseAdapter().Get(_purchaseId);
            Supplier = Purchase.Supplier;
        }

        private void Ingerdients()
        {
            MainWindow.Instance.NewWindow(WindowType, new PurchaseIngredientsView(Purchase.Id));
        }

        private void Suppliers()
        {
            new PurchaseAdapter().AddOrUpdate(Purchase);
            MainWindow.Instance.NewWindow(WindowType, new PurchasesSuppliersListView(true, Purchase.Id));
        }

        private void Save()
        {
            new PurchaseAdapter().AddOrUpdate(Purchase);
            MainWindow.Instance.CloseWindow(WindowType);
        }

        private void Back()
        {
            if (_editMode == PurchaseEditMode.Add || _editMode == PurchaseEditMode.Copy)
            {
                new PurchaseAdapter().Remove(Purchase.Id);
            }
            MainWindow.Instance.CloseWindow(WindowType);
        }
    }
}
