﻿using System.Windows;
using System.Windows.Controls;
using ADMv2.Common;
using ADMv2.CommonProj;
using ADMv2.DAL.Enums;

namespace ADMv2.Purchases.PurchaseEdit
{
    public partial class PurchasesEditView : UserControl, IUserControlRefreshable
    {
        private readonly PurchaseEditViewModel _model;

        public PurchasesEditView(int purchaseId, PurchaseEditMode editMode)
        {
            InitializeComponent();
            _model = new PurchaseEditViewModel(purchaseId, editMode);
            DataContext = _model;

            if (editMode == PurchaseEditMode.Add)
            {
                AllDatesSameBox.IsChecked = true;
            }

            PaidChBox.IsChecked = _model.Purchase.AmountPaid > 0;

            if (_model.Purchase.PaymentMethod == PaymentMethod.BankTransfer)
                BankTraRadioButton.IsChecked = true;
            else
                CashRadio.IsChecked = true;
        }

        public void Refresh(int? returnedValue = null)
        {
            ((PurchaseEditViewModel)DataContext).Refresh(returnedValue);
        }

        private void AllDatesSameBoxCheckedChanged(object sender, RoutedEventArgs e)
        {
            DateIssuePicker.IsEnabled = AllDatesSameBox.IsChecked == false;
            DatePaymentDeadlinePicker.IsEnabled = AllDatesSameBox.IsChecked == false;
            SetAllDatesSame();
        }

        private void PaidChBoxChecked(object sender, RoutedEventArgs e)
        {
            bool isPaidBoxIsChecked = PaidChBox.IsChecked == true;
            DatePaymentPicker.IsEnabled = isPaidBoxIsChecked;
            CashRadio.IsEnabled = isPaidBoxIsChecked;
            BankTraRadioButton.IsEnabled = isPaidBoxIsChecked;
            PaidAmountBox.IsEnabled = isPaidBoxIsChecked;

            decimal toPaid = isPaidBoxIsChecked ? _model.Purchase.SumBrutto : 0;
            PaidAmountBox.Text = toPaid.ToStringTwoDigits();
            _model.Purchase.AmountPaid = toPaid;
        }

        private void ReceiveDatePickerValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            SetAllDatesSame();
        }

        private void SetAllDatesSame()
        {
            if (AllDatesSameBox.IsChecked == true)
            {
                DateIssuePicker.Value = ReceiveDatePicker.Value;
                DatePaymentDeadlinePicker.Value = ReceiveDatePicker.Value;
            }
        }
    }
}
