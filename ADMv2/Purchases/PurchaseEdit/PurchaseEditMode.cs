﻿namespace ADMv2.Purchases.PurchaseEdit
{
    public enum PurchaseEditMode
    {
        Add = 0,
        Edit = 1,
        Copy = 2,
    }
}
