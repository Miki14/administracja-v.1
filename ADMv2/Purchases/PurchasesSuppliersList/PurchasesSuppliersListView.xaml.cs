﻿using System.Windows.Controls;
using ADMv2.Common;

namespace ADMv2.Purchases.PurchasesSuppliersList
{
    public partial class PurchasesSuppliersListView : UserControl, IUserControlRefreshable
    {
        private readonly PurchasesSuppliersListViewModel _model;

        public PurchasesSuppliersListView(bool isSelect = false, int? purchaseId = null)
        {
            InitializeComponent();
            _model = new PurchasesSuppliersListViewModel(isSelect, purchaseId);
            DataContext = _model;

            suppliersListView.ScrollIntoView(suppliersListView.SelectedItem);
        }

        public void Refresh(int? returnedValue = null)
        {
            ((PurchasesSuppliersListViewModel)DataContext).Refresh();
        }

        private void SearchBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            Refresh();
        }

        private void ListViewMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _model.SelectCommand.Execute(null);
        }
    }
}
