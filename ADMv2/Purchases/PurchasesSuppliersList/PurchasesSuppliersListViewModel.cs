﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.MainWindowComponents;
using ADMv2.Purchases.PurchasesSupplierEdit;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Purchases.PurchasesSuppliersList
{
    public class PurchasesSuppliersListViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Purchase;

        public ICommand DeleteCommand => new RelayCommand(Delete);
        public ICommand AddCommand => new RelayCommand(Add);
        public ICommand EditCommand => new RelayCommand(Edit);
        public ICommand SelectCommand => new RelayCommand(Select);
        public ICommand BackCommand => new RelayCommand(Back);

        private ObservableCollection<Supplier> _suppliers;
        public ObservableCollection<Supplier> Suppliers
        {
            get => _suppliers;
            set => SetProperty(ref _suppliers, value);
        }

        public Supplier _selectedSupplier;
        public Supplier SelectedSupplier
        {
            get => _selectedSupplier;
            set => SetProperty(ref _selectedSupplier, value);
        }

        public string SearchName { get; set; }
        public string SearchNip { get; set; }
        private bool IsSelect { get; set; }
        private readonly int? _purchaseId;
        public Visibility SelectButtVisibility => IsSelect ? Visibility.Visible : Visibility.Hidden;


        public PurchasesSuppliersListViewModel(bool isSelect = false, int? purchaseId = null)
        {
            IsSelect = isSelect;
            _purchaseId = purchaseId;

            if (_purchaseId.HasValue)
            {
                var purchase = new PurchaseAdapter().Get(_purchaseId.Value);
                if (purchase.Supplier != null)
                {
                    SelectedSupplier = new SupplierAdapter().Get(purchase.Supplier.Id);
                }
            }

            Refresh();
        }

        public override void Refresh(int? returnedValue = null)
        {
            int? selectedSupplierId = SelectedSupplier?.Id;
            Suppliers = new ObservableCollection<Supplier>(new SupplierAdapter().GetAll(SearchName, SearchNip));

            if (Suppliers.Count > 0)
                SelectedSupplier = Suppliers.FirstOrDefault(x => x.Id == selectedSupplierId) ?? Suppliers.FirstOrDefault();
        }

        private void Delete()
        {
            if (SelectedSupplier != null)
            {
                new SupplierAdapter().Remove(SelectedSupplier.Id);
                Refresh();
            }
        }

        private void Add()
        {
            MainWindow.Instance.NewWindow(WindowType, new PurchasesSupplierEditView());
        }

        private void Edit()
        {
            if (SelectedSupplier != null)
            {
                MainWindow.Instance.NewWindow(WindowType, new PurchasesSupplierEditView(SelectedSupplier.Id));
            }
        }

        private void Select()
        {
            if (SelectedSupplier != null && _purchaseId != null)
            {
                new PurchaseAdapter().UpdatePurchaseSupplier(_purchaseId.Value, SelectedSupplier.Id);
                MainWindow.Instance.CloseWindow(WindowType, SelectedSupplier?.Id);
            }
        }

        private void Back()
        {
            MainWindow.Instance.CloseWindow(WindowType);
        }
    }
}
