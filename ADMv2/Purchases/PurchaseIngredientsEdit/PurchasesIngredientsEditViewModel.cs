﻿using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.MainWindowComponents;

namespace ADMv2.Purchases.PurchaseIngredientsEdit
{
    public class PurchasesIngredientsEditViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Purchase;
        private readonly IngredientPurchaseAdapter ingredientPurchaseAdapter = new();

        //public ICommand SaveCommand => new RelayCommand(Save);
        //public ICommand BackCommand => new RelayCommand(Back);

        private IngredientPurchase _ingredientPurchase;
        public IngredientPurchase IngredientPurchase
        {
            get => _ingredientPurchase;
            set => SetProperty(ref _ingredientPurchase, value);
        }
        public IngredientPurchase SelectedIngredientPurchase { get; set; }

        public bool ShowHidden { get; set; }
        public int SelectedTypeId { get; set; }

        public PurchasesIngredientsEditViewModel(int? ingredientId = null)
        {
            if (ingredientId != null)
                IngredientPurchase = ingredientPurchaseAdapter.Get(ingredientId.Value);
            else
                IngredientPurchase = new IngredientPurchase();
        }

        public override void Refresh(int? returnedValue = null) { } //TODO ?

        //private void Save()
        //{
        //    new PurchaseAdapter().AddOrUpdate(Purchase);
        //    MainWindow.Instance.CloseWindow(WindowType);
        //}

        //private void Back()
        //{
        //    MainWindow.Instance.CloseWindow(WindowType);
        //}
    }
}
