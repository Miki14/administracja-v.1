﻿using System.Windows;
using System.Windows.Controls;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Enums;

namespace ADMv2.Purchases.PurchaseIngredientsEdit
{
    public partial class PurchaseIngredientsEditView : Window, IUserControlRefreshable
    {
        private readonly PurchasesIngredientsEditViewModel _viewModel;

        public PurchaseIngredientsEditView(int? ingredientId = null)
        {
            InitializeComponent();
            _viewModel = new PurchasesIngredientsEditViewModel(ingredientId);
            DataContext = _viewModel;
            groupCombo.ItemsSource = PurchaseGroup.Purchases.GetDictionaryString();
            groupCombo.SelectedIndex = (int)_viewModel.IngredientPurchase.Group;
        }

        public void Refresh(int? returnedValue = null)
        {
            ((PurchasesIngredientsEditViewModel)DataContext).Refresh();
        }

        private void SaveButtClick(object sender, RoutedEventArgs e)
        {
            new IngredientPurchaseAdapter().AddOrUpdate(_viewModel.IngredientPurchase);
            this.Close();
        }

        private void CancelButtClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void PriceBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            if (_viewModel != null)
                bruttoValLab.Content = _viewModel.IngredientPurchase.Brutto;
        }
    }
}
