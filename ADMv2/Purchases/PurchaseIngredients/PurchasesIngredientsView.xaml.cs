﻿using System.Windows;
using System.Windows.Controls;
using ADMv2.Common;
using ADMv2.DAL.Enums;

namespace ADMv2.Purchases.PurchaseIngredients
{
    public partial class PurchaseIngredientsView : UserControl, IUserControlRefreshable
    {
        private readonly PurchasesIngredientsViewModel _viewModel;

        public PurchaseIngredientsView(int? purchaseId = null)
        {
            InitializeComponent();
            _viewModel = new PurchasesIngredientsViewModel(purchaseId);
            DataContext = _viewModel;
            typeCombo.ItemsSource = PurchaseGroup.Purchases.GetDictionaryString();
            typeCombo.SelectedIndex = 2;
        }

        public void Refresh(int? returnedValue = null)
        {
            ((PurchasesIngredientsViewModel)DataContext).Refresh();
        }

        private void TypeComboSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Refresh();
        }

        private void ShowHidenChecked(object sender, RoutedEventArgs e)
        {
            Refresh();
        }
    }
}
