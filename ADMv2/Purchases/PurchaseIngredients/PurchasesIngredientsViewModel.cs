﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;
using ADMv2.MainWindowComponents;
using ADMv2.Purchases.PurchaseIngredientsEdit;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Purchases.PurchaseIngredients
{
    public class PurchasesIngredientsViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.Purchase;
        private readonly IngredientPurchaseAdapter ingredientPurchaseAdapter = new();
        private readonly PurchaseIngredientAdapter purchaseIngredientAdapter = new();
        private readonly int? _purchaseId;

        public bool CanEdit => _purchaseId.HasValue;
        public ICommand AddCommand => new RelayCommand(Add);
        public ICommand EditCommand => new RelayCommand(Edit);
        public ICommand AddToPurchaseCommand => new RelayCommand(AddToPurchase);
        public ICommand RemoveFromPurchaseCommand => new RelayCommand(RemoveFromPurchase);
        public ICommand SaveCommand => new RelayCommand(Save);
        public ICommand BackCommand => new RelayCommand(Back);

        public PurchasesIngredientsViewModel(int? purchaseId = null)
        {
            _purchaseId = purchaseId;
            Refresh();
        }

        private ObservableCollection<IngredientPurchase> _ingredientPurchases;
        public ObservableCollection<IngredientPurchase> IngredientPurchases
        {
            get => _ingredientPurchases;
            set => SetProperty(ref _ingredientPurchases, value);
        }

        public IngredientPurchase _selectedIngredientPurchase;
        public IngredientPurchase SelectedIngredientPurchase
        {
            get => _selectedIngredientPurchase;
            set => SetProperty(ref _selectedIngredientPurchase, value);
        }

        private ObservableCollection<PurchaseIngredient> _purchaseIngredientsAssigned;
        public ObservableCollection<PurchaseIngredient> PurchaseIngredientsAssigned
        {
            get => _purchaseIngredientsAssigned;
            set => SetProperty(ref _purchaseIngredientsAssigned, value);
        }

        public PurchaseIngredient _selectedPurchaseIngredient;
        public PurchaseIngredient SelectedPurchaseIngredient
        {
            get => _selectedPurchaseIngredient;
            set => SetProperty(ref _selectedPurchaseIngredient, value);
        }

        public int _selectedPurchaseIngredientIndex;
        public int SelectedPurchaseIngredientIndex
        {
            get => _selectedPurchaseIngredientIndex;
            set => SetProperty(ref _selectedPurchaseIngredientIndex, value);
        }

        public bool ShowHidden { get; set; }
        public int SelectedTypeId { get; set; }

        public override void Refresh(int? returnedValue = null)
        {
            int? selectedIngredientPurchaseId = SelectedIngredientPurchase?.Id;

            IngredientPurchases = new ObservableCollection<IngredientPurchase>(ingredientPurchaseAdapter.GetAll(ShowHidden, (PurchaseGroup)SelectedTypeId));

            if (IngredientPurchases.Count > 0)
                SelectedIngredientPurchase = IngredientPurchases.FirstOrDefault(x => x.Id == selectedIngredientPurchaseId) ?? IngredientPurchases.FirstOrDefault();


            int? selectedPurchaseIngredientId = SelectedPurchaseIngredient?.Id;

            if (_purchaseId.HasValue)
                PurchaseIngredientsAssigned = new ObservableCollection<PurchaseIngredient>(purchaseIngredientAdapter.GetAll(_purchaseId.Value));
            else
                PurchaseIngredientsAssigned = new ObservableCollection<PurchaseIngredient>();

            if (PurchaseIngredientsAssigned.Count > 0)
                SelectedPurchaseIngredient = PurchaseIngredientsAssigned.FirstOrDefault(x => x.Id == selectedPurchaseIngredientId) ?? PurchaseIngredientsAssigned.FirstOrDefault();
        }

        private void Add()
        {
            new PurchaseIngredientsEditView()
            {
                Owner = MainWindow.Instance,
            }.ShowDialog();
            Refresh();
        }

        private void Edit()
        {
            //TODO: Ownej się działa
            new PurchaseIngredientsEditView(SelectedIngredientPurchase.Id)
            {
                Owner = MainWindow.Instance,
            }.ShowDialog();
            Refresh();
        }

        private void AddToPurchase()
        {
            PurchaseIngredientsAssigned.Add(new PurchaseIngredient(SelectedIngredientPurchase));
        }

        private void RemoveFromPurchase()
        {
            int selectedInex = PurchaseIngredientsAssigned.IndexOf(SelectedPurchaseIngredient);
            PurchaseIngredientsAssigned.Remove(SelectedPurchaseIngredient);

            try
            {
                SelectedPurchaseIngredient = PurchaseIngredientsAssigned.ElementAt(selectedInex);
            }
            catch
            {
                SelectedPurchaseIngredient = PurchaseIngredientsAssigned.FirstOrDefault();
            }
        }

        private void Save()
        {
            //Zapisać skłądniki
            purchaseIngredientAdapter.Update(_purchaseId.Value, PurchaseIngredientsAssigned);
            MainWindow.Instance.CloseWindow(WindowType);
        }

        private void Back()
        {
            MainWindow.Instance.CloseWindow(WindowType);
        }
    }
}
