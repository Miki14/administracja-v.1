﻿namespace ADMv2.Printing
{
    public struct Price
    {
        public decimal Netto;
        public decimal Vat;
        public readonly decimal Brutto => Netto + Vat;
    }
}
