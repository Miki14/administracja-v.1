﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Windows.Forms;
using ADMv2.Common;
using ADMv2.CommonProj;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Enums;
using LiczbyNaSlowaNETCore;

namespace ADMv2.Printing
{
    public class KpPrinting
    {
        private const int LewyMargX = 50;
        private int _paymentId;
        private PaymentAdapter _paymentAdapter = new();
        private readonly PrintDocument _printDocument;

        public KpPrinting(int paymentId)
        {
            _paymentId = paymentId;
            _printDocument = new PrintDocument();
            _printDocument.PrintPage += KpPrintPage;
        }

        public void Print()
        {
            PrintDialog printDialog = new() { Document = _printDocument };

            if (printDialog.ShowDialog() != DialogResult.OK) return;
            try
            {
                _printDocument.Print();
            }
            catch (Exception ex)
            {
                Report.Error(ex);
            }
        }

        public void PrintPreview()
        {
            PrintPreviewDialog podgladDialog = new() { Document = _printDocument };
            ((Form)podgladDialog).WindowState = FormWindowState.Maximized;
            podgladDialog.PrintPreviewControl.Zoom = 1.5;
            podgladDialog.ShowDialog();
        }


        public void KpPrintPage(object sender, PrintPageEventArgs e)
        {
            int polowaY = 580;

            //Pierwsza część od 0
            Graphics g = KpSkladanie(e.Graphics, 0);
            //Linia połowy kartki
            g.DrawLine(Fonts.DashPen, new PointF(LewyMargX - 20, polowaY), new PointF(800, polowaY));
            //Druga część od połowy
            KpSkladanie(g, polowaY);
        }

        private Graphics KpSkladanie(Graphics g, int przesuniecieY)
        {
            var payment = _paymentAdapter.Get(_paymentId);

            int pozY = 50 + przesuniecieY;

            g.DrawString("Dowód wpłaty", Fonts.Header, Brushes.Black, new PointF(315, pozY));

            //DANE SPRZEDAWCY
            int pozSprzedY = 150 + przesuniecieY;
            var companyData = new CompanyDataAdapter().Get();

            g.DrawString("Sprzedawca:", Fonts.Normal, Brushes.Black, new PointF(LewyMargX - 10, pozSprzedY - 5));
            g.DrawString(companyData.Name1, Fonts.Dane, Brushes.Black, new PointF(LewyMargX, pozSprzedY + 20));
            g.DrawString(companyData.Name2, Fonts.Dane, Brushes.Black, new PointF(LewyMargX, pozSprzedY + 40));
            g.DrawString(companyData.Address1, Fonts.Dane, Brushes.Black, new PointF(LewyMargX, pozSprzedY + 60));
            g.DrawString(companyData.Address2, Fonts.Dane, Brushes.Black, new PointF(LewyMargX, pozSprzedY + 80));

            if (!string.IsNullOrWhiteSpace(companyData.Logo))
            {
                g.DrawImage(Image.FromFile(@"Loga\" + companyData.Logo), 50, pozSprzedY - 60, 150, 50);
            }

            //DATA
            int dataY = 80 + przesuniecieY;
            g.DrawString("Data: " + payment.Date.ToString("dd / MM / yyyy", CultureInfo.InvariantCulture), Fonts.Normal, Brushes.Black, new PointF(630, dataY));

            //NUMER
            g.DrawString("KP " + payment.DocumentNumber + " / " + companyData.InvoiceNo + " / " + payment.Date.Year, Fonts.Header, Brushes.Black, new PointF(310, pozY + 30));

            //DANE NABYWCY
            const int pozNabX = 480;
            int pozNabY = 150 + przesuniecieY;
            var customer = new CustomersAdapter().GetByPayment(_paymentId);
            g.DrawString("Nabywca:", Fonts.Normal, Brushes.Black, new PointF(pozNabX - 10, pozNabY - 5));
            g.DrawString(customer.LastName, Fonts.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 20));
            g.DrawString(customer.FirstName, Fonts.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 40));
            g.DrawString(customer.Address1, Fonts.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 60));
            g.DrawString(customer.Address2, Fonts.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 80));

            int przesuniecieNip = 0;
            if (!string.IsNullOrWhiteSpace(customer.NIP))
            {
                przesuniecieNip = 20;
                g.DrawString("NIP: " + customer.NIP, Fonts.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 100));
            }

            //OPIS
            int pozTowarY = 260 + przesuniecieNip + przesuniecieY;

            int PozXFv = 550;
            int PozXOdsetki = 630;
            int PozXFvIOdsetki = 750;

            g.DrawString("Kwota", Fonts.Normal, Brushes.Black, new PointF(PozXFv - 10, pozTowarY), Fonts.Left);
            g.DrawString("Odsetki", Fonts.Normal, Brushes.Black, new PointF(PozXOdsetki - 10, pozTowarY), Fonts.Left);
            g.DrawString("Razem", Fonts.Normal, Brushes.Black, new PointF(PozXFvIOdsetki - 10, pozTowarY), Fonts.Left);

            decimal sumaFv = 0;
            decimal sumaOds = 0;
            pozTowarY += 20;

            //Po wszystkich KP
            var paymentsAll = _paymentAdapter.GetByNumber(payment.DocumentNumber, PaymentDocument.KP, payment.Date);
            foreach (var paymentAll in paymentsAll)
            {
                g.DrawString(paymentAll.Description, Fonts.Normal, Brushes.Black, new PointF(LewyMargX, pozTowarY));
                g.DrawString(paymentAll.Amount.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(PozXFv, pozTowarY), Fonts.Left);
                g.DrawString(paymentAll.Interest.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(PozXOdsetki, pozTowarY), Fonts.Left);
                g.DrawString(paymentAll.TotalAmount.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(PozXFvIOdsetki, pozTowarY), Fonts.Left);

                sumaFv += paymentAll.Amount;
                sumaOds += paymentAll.Interest;
                pozTowarY += 15;
            }

            //KWOTA
            pozTowarY += 5;
            g.DrawLine(Fonts.NormalPen, LewyMargX + 400, pozTowarY, LewyMargX + 700, pozTowarY);
            pozTowarY += 5;
            g.DrawString(sumaFv.ToStringTwoDigits(), Fonts.Header, Brushes.Black, new PointF(PozXFv, pozTowarY), Fonts.Left);
            g.DrawString(sumaOds.ToStringTwoDigits(), Fonts.Header, Brushes.Black, new PointF(PozXOdsetki, pozTowarY), Fonts.Left);
            g.DrawString(sumaFv + sumaOds.ToStringTwoDigits(), Fonts.Header, Brushes.Black, new PointF(PozXFvIOdsetki, pozTowarY), Fonts.Left);
            g.DrawString("Słownie: " + NumberToText.Convert(sumaFv + sumaOds, TextOperations.LiczbyNaSlowaOptions), Fonts.Normal, Brushes.Black, new PointF(LewyMargX, pozTowarY + 30));

            //PODPIS
            int pozPodpisX = 70;
            int pozPodpisY = 500 + przesuniecieY;
            g.DrawString(".........................................", Fonts.Normal, Brushes.Black, new PointF(pozPodpisX, pozPodpisY));
            g.DrawString("Podpis osoby upoważnionej", Fonts.Small, Brushes.Black, new PointF(pozPodpisX + 15, pozPodpisY + 20));
            g.DrawString("do wystawienia dokumentu", Fonts.Small, Brushes.Black, new PointF(pozPodpisX + 20, pozPodpisY + 40));

            return g;
        }
    }
}
