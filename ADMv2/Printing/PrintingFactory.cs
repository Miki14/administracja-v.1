﻿using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Models;

namespace ADMv2.Printing
{
    public class PrintingFactory
    {
        private readonly CompanyData _companyData;

        public PrintingFactory()
        {
            _companyData = new CompanyDataAdapter().Get();
        }

        public void BalancePrint(BalanceModel balanceModel)
        {
            new BalancePrinting(balanceModel, _companyData).Print();
        }

        public void BalancePreview(BalanceModel balanceModel)
        {
            new BalancePrinting(balanceModel, _companyData).Preview();
        }
    }
}
