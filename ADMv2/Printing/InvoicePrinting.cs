﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using LiczbyNaSlowaNETCore;

namespace ADMv2.Printing
{
    public class InvoicePrinting
    {
        //private bool _orgKop;
        private readonly PrintDialog printDialog = new();
        private readonly PrintPreviewDialog printPreviewDialog = new();
        private readonly InvoicePrintingDto invoice;
        private readonly CompanyData companyData;
        private readonly CompanyDataAdapter cda = new();
        private readonly InvoiceIngredientAdapter iia = new();
        private readonly CustomersAdapter ca = new();
        private readonly List<SaleInvoiceIngredient> invoiceIngredients;
        private readonly Customer customer;

        public InvoicePrinting(InvoicePrintingDto invoiceParam)
        {
            invoice = invoiceParam;
            invoiceIngredients = iia.GetInvoiceIngredients(invoice.Id);
            companyData = cda.Get();
            customer = ca.Get(invoice);
        }

        public bool Print()
        {
            try
            {
                //wydrukowanoCH.Checked = true;

                //if (ZapiszFakture())
                {
                    var pd = new PrintDocument();
                    pd.PrintPage += PdPrintPage;
                    printDialog.Document = pd;
                    if (printDialog.ShowDialog() != DialogResult.OK) { return false; };

                    for (int i = 0; i < invoice.CopyNumber; i++)
                    {
                        pd.Print();
                    }
                }
            }
            catch (Exception ex)
            {
                Report.Error(ex);
                return false;
            }

            return true;
        }

        public void PrintPreview()
        {
            var podgladDruku = new PrintDocument();
            podgladDruku.PrintPage += PdPrintPage;
            printPreviewDialog.Document = podgladDruku;
            ((Form)printPreviewDialog).WindowState = FormWindowState.Maximized;
            printPreviewDialog.PrintPreviewControl.Zoom = 1.5;
            printPreviewDialog.ShowDialog();
        }

        private void PdPrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;


            const int pozNabX = 480;
            const int pozNabY = 200;

            int lewyMargX = 75;
            const int pozSprzedY = 200;

            //TODO: przygotowane do przenoszenia składników na kolejne strony
            //bool fStop = false;

            //DANE NABYWCY
            g.DrawString("Nabywca:", Fonts.Normal, Brushes.Black, new PointF(pozNabX - 10, pozNabY - 5));
            g.DrawString(customer.LastName, Fonts.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 20));
            g.DrawString(customer.FirstName, Fonts.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 40));


            g.DrawString(customer.Address1, Fonts.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 60));

            g.DrawString(customer.Address2, Fonts.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 80));
            if (!string.IsNullOrWhiteSpace(customer.NIP))
            {
                g.DrawString("NIP: " + customer.NIP, Fonts.Dane, Brushes.Black, new PointF(pozNabX, pozNabY + 100));
            }

            //DANE SPRZEDAWCY
            g.DrawString("Sprzedawca:", Fonts.Normal, Brushes.Black, new PointF(lewyMargX - 10, pozSprzedY - 5));
            g.DrawString(companyData.Name1, Fonts.Dane, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 20));
            g.DrawString(companyData.Name2, Fonts.Dane, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 40));
            g.DrawString(companyData.Address1, Fonts.Dane, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 60));
            g.DrawString(companyData.Address2, Fonts.Dane, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 80));
            g.DrawString("NIP: " + companyData.NIP, Fonts.Dane, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 100));
            g.DrawString("Regon: " + companyData.Regon, Fonts.Dane, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 120));
            g.DrawString("Telefon: " + companyData.Phone, Fonts.Normal, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 150));
            g.DrawString("Nr. konta: " + companyData.BankAccountNo, Fonts.Normal, Brushes.Black, new PointF(lewyMargX, pozSprzedY + 170));

            if (!string.IsNullOrWhiteSpace(companyData.Logo))
            {
                g.DrawImage(Image.FromFile(Environment.CurrentDirectory + @"\Loga\" + companyData.Logo), 50, 140, 150, 50);
            }

            //NR. FAKTURY
            string numerFv = companyData.InvoiceNo;
            if (!string.IsNullOrWhiteSpace(numerFv)) numerFv += " / ";
            else numerFv = "";

            g.DrawString("Faktura VAT nr: " + invoice.Number + " / " + numerFv + invoice.Date.ToString("MM / yyyy", CultureInfo.InvariantCulture), Fonts.Header, Brushes.Black, new PointF(50, 100));

            //SKLADANIE DO KOPERTY
            g.DrawLine(Fonts.NormalPen, new PointF(lewyMargX - 20, 390), new PointF(lewyMargX - 15, 390));
            g.DrawLine(Fonts.NormalPen, new PointF(795, 390), new PointF(800, 390));
            g.DrawLine(Fonts.NormalPen, new PointF(lewyMargX - 20, 780), new PointF(lewyMargX - 15, 780));
            g.DrawLine(Fonts.NormalPen, new PointF(795, 780), new PointF(800, 780));

            //DUPLIKAT
            if (invoice.Duplicate) g.DrawString("Duplikat z dnia: " + invoice.DuplicateDate.ToString("dd.MM.yyyy"), Fonts.Duplicate, Brushes.Black, new PointF(lewyMargX, 50));

            //ORYGINAŁ / KOPIA
            //g.DrawString(_orgKop ? "Oryginał" : "Kopia", Fonts.Header, Brushes.Black, new PointF(530, 50));

            //DATY
            string dataTmp = invoice.Date.ToString("dd / MM / yyyy", CultureInfo.InvariantCulture);
            g.DrawString("Data wystawienia:", Fonts.Normal, Brushes.Black, new PointF(500, 80));
            g.DrawString(dataTmp, Fonts.Normal, Brushes.Black, new PointF(650, 80));
            g.DrawString("Data wykonania usługi:", Fonts.Normal, Brushes.Black, new PointF(500, 100));
            g.DrawString(dataTmp, Fonts.Normal, Brushes.Black, new PointF(650, 100));

            //Sposób i termin
            g.DrawString("Sposób zapłaty: " + invoice.PaymentMethod, Fonts.Normal, Brushes.Black, new PointF(pozNabX, 350));
            g.DrawString("Termin płatności: " + invoice.PaymentDeadline, Fonts.Normal, Brushes.Black, new PointF(pozNabX, 370));

            int pozTowarY = 415;

            //TABELA TOWAROW
            g.DrawString("Lp.", Fonts.Goods, Brushes.Black, new PointF(lewyMargX - 10, pozTowarY));
            g.DrawString("Nazwa", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 20, pozTowarY));
            g.DrawString("PKWiU", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 260, pozTowarY));
            g.DrawString("Cena Netto", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 350, pozTowarY));
            g.DrawString("% VAT", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 450, pozTowarY));
            g.DrawString("Kwota VAT", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 510, pozTowarY));
            g.DrawString("Cena Brutto", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 600, pozTowarY));
            g.DrawLine(Fonts.NormalPen, lewyMargX, 435, 750, 435);

            pozTowarY += 5;

            decimal sumaNetto = 0;
            decimal sumaVat = 0;

            if (invoiceIngredients.Count > 18)
            {
                //TODO: Dzielić FV na strony
                Report.MessageError("Za dużo składników. Nie mieszczą się na fakturze. Skontaktuj się z autorem oprogramowania.", "Za dużo składników.");
            }
            else
            {
                //LISTA TOWAROW
                for (int i = 0; i < invoiceIngredients.Count; i++)
                {
                    //if (!fStop)
                    //{
                    pozTowarY += 20;
                    g.DrawString((i + 1).ToString(), Fonts.Goods, Brushes.Black, new PointF(lewyMargX - 10, pozTowarY));
                    //NAZWA
                    g.DrawString(invoiceIngredients[i].Name, Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 20, pozTowarY));
                    //PKWiU
                    g.DrawString(invoiceIngredients[i].Symbol, Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 260, pozTowarY));

                    decimal netto = invoiceIngredients[i].Netto;
                    sumaNetto += netto;
                    g.DrawString(string.Format("{0:N2}", netto), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 420, pozTowarY), Fonts.Left);

                    if (invoiceIngredients[i].VatRate == 0) g.DrawString("zw", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 470, pozTowarY));
                    else g.DrawString(invoiceIngredients[i].VatRate.ToString(), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 470, pozTowarY));

                    decimal vat = Math.Round(netto * invoiceIngredients[i].VatRateValue / 100, 2);
                    sumaVat += vat;
                    g.DrawString(string.Format("{0:N2}", vat), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 580, pozTowarY), Fonts.Left);

                    g.DrawString(string.Format("{0:N2}", netto + vat), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 670, pozTowarY), Fonts.Left);
                    //}
                    //else
                    //{
                    //if (i < 10) e.HasMorePages = true;
                    //}
                }
            }

            //PODSUMOWANIE TOWAROW
            g.DrawLine(Fonts.NormalPen, lewyMargX + 100, pozTowarY + 30, lewyMargX + 700, pozTowarY + 30);
            pozTowarY += 40;
            g.DrawString("Razem:", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 250, pozTowarY));
            g.DrawString(string.Format("{0:N2}", sumaNetto), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 420, pozTowarY), Fonts.Left);
            g.DrawString(string.Format("{0:N2}", sumaVat), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 580, pozTowarY), Fonts.Left);
            var sumaBrutto = sumaVat + sumaNetto;
            g.DrawString(string.Format("{0:N2}", sumaBrutto), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 670, pozTowarY), Fonts.Left);

            //WYPISYWANIE POSZCZEGOLNYCH STAWEK VAT
            pozTowarY += 30;
            g.DrawString("W tym:", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 254, pozTowarY));

            var vatRates = invoiceIngredients.Select(x => x.VatRate).Distinct().OrderBy(y => y);

            foreach (int? vat in vatRates)
            {
                decimal sumaCenNetto = 0;
                decimal sumaCenVat = 0;

                var ingredientsVat = invoiceIngredients.Where(x => x.VatRate == vat);
                foreach (var ingredient in ingredientsVat)
                {
                    sumaCenNetto += ingredient.Netto;
                    sumaCenVat += ingredient.Netto * ingredient.VatRateValue / 100;
                }

                if (sumaCenNetto != 0)
                {
                    g.DrawString(string.Format("{0:N2}", sumaCenNetto), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 420, pozTowarY), Fonts.Left);
                    g.DrawString(vat == 0 ? "zw" : vat.ToString(), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 470, pozTowarY));
                    g.DrawString(string.Format("{0:N2}", sumaCenVat), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 580, pozTowarY), Fonts.Left);
                    g.DrawString(string.Format("{0:N2}", sumaCenNetto + sumaCenVat), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 670, pozTowarY), Fonts.Left);
                    pozTowarY += 20;
                }
            }

            //SUMA DO ZAPLATY I KWOTA SLOWNIE
            g.DrawString("Do zapłaty: " + string.Format("{0:N2}", sumaBrutto) + " zł", Fonts.Header, Brushes.Black, new PointF(lewyMargX, pozTowarY));
            g.DrawString("Słownie: " + NumberToText.Convert(sumaBrutto, TextOperations.LiczbyNaSlowaOptions), Fonts.InWords, Brushes.Black, new PointF(lewyMargX, pozTowarY + 30));
            g.DrawString(invoice.Note, Fonts.Normal, Brushes.Black, new PointF(lewyMargX, pozTowarY + 60));

            //MIEJSCA NA PODPISY
            pozTowarY += 150;
            lewyMargX += 10;

            g.DrawString("Michał Gołacki", Fonts.Normal, Brushes.Black, new PointF(lewyMargX + 50, pozTowarY - 10));
            g.DrawString(".................................................", Fonts.Normal, Brushes.Black, new PointF(lewyMargX, pozTowarY));
            g.DrawString("Osoba upoważniona do wystawienia faktury", Fonts.Small, Brushes.Black, new PointF(lewyMargX - 10, pozTowarY + 20));

            g.DrawString("..........................................", Fonts.Normal, Brushes.Black, new PointF(580, pozTowarY));
            g.DrawString("Podpis osoby upoważnionej", Fonts.Small, Brushes.Black, new PointF(580 + 15, pozTowarY + 20));
            g.DrawString("do otrzymania faktury", Fonts.Small, Brushes.Black, new PointF(580 + 35, pozTowarY + 40));
        }
    }
}
