﻿using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Enums;
using ADMv2.MainWindowComponents;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.Printing.SummaryWindow
{
    public class SummaryWindowViewModel : ViewModel
    {
        public ICommand PreviewCommand => new RelayCommand(Preview);
        public ICommand PrintCommand => new RelayCommand(Print);
        public ICommand BackCommand => new RelayCommand(Back);

        public override WindowType WindowType => WindowType.Summary;

        public int SelectedTypeId { get; set; } = -1;
        public OtherTransactionType? SelectedType
        {
            get
            {
                return SelectedTypeId == -1 ? null : (OtherTransactionType)SelectedTypeId;
            }
        }

        public override void Refresh(int? returnedValue = null) 
        {
            //TODO: Wypełnić tym dla innych obrotów
        }

        private void Preview()
        {
            ////TODO: Przenieść z CodeBehind
            ////UstawTyp();

            //if (faktury_rad.Checked)
            //{
            //    _printDok.PrintPage += druk.FakturyZestawieniePrintPage;
            //}
            //else
            //{
            //    if (wplaty_rad.Checked)
            //    {
            //        _printDok.PrintPage += druk.WplatyZestawieniePrintPage;
            //    }
            //    else
            //    {
            //        if (wydatki_rad.Checked)
            //        {
            //            _printDok.PrintPage += druk.WydatkiZestawieniePrintPage;
            //        }
            //        else
            //        {
            //            _printDok.PrintPage += druk.OdsetkiZestawieniePrintPage;
            //        }
            //    }
            //}




            //PrintPreviewDialog printPreviewDialog = new PrintPreviewDialog();
            //printPreviewDialog.Document = _printDok;
            //((Form)printPreviewDialog).WindowState = FormWindowState.Maximized;
            //printPreviewDialog.PrintPreviewControl.Zoom = 1.5;
            //printPreviewDialog.ShowDialog();
        }

        private void Print()
        {
            //TODO: Przenieść z Code behind
        }

        private void Back()
        {
            //TODO: Przenieść z Code behind
        }
    }
}
