﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using ADMv2.Common;
using ADMv2.CommonProj;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;

namespace ADMv2.Printing.SummaryWindow
{
    class SummaryPrinting
    {
        private int _licznikStron;
        private int _licznikPozycji;
        private int _iloscPozycji;
        private bool _fPodsumowanie;
        private const int LewyMargX = 50;

        private readonly DateTime _dateFrom;
        private readonly DateTime _dateTo;
        private readonly OtherTransactionType? _otherTransactionType;

        private readonly CompanyDataAdapter companyDataAdapter = new();
        private readonly InvoicesAdapter invoicesAdapter = new();

        public SummaryPrinting(DateTime dateFrom, DateTime dateTo, OtherTransactionType? otherTransactionType = null)
        {
            _dateFrom = dateFrom;
            _dateTo = dateTo;
            _otherTransactionType = otherTransactionType;
        }

        private void OkresSprzedawcaZestawienie(ref Graphics g)
        {
            g.DrawString("Za okres od  " + _dateFrom.ToStringDMY() + " do  " + _dateTo.ToStringDMY(), Fonts.Period, Brushes.Black, new PointF(LewyMargX, 80));

            var companyData = companyDataAdapter.Get();
            g.DrawString(companyData.InvoiceNo, Fonts.Normal, Brushes.Black, new PointF(750, 30));
            g.DrawString(companyData.Name1 + " " + companyData.Name2 + " " + companyData.Address1 + " " + companyData.Address2, Fonts.Normal, Brushes.Black, new PointF(LewyMargX, 100));
        }

        public void FakturyZestawieniePrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            int pozY = 50;
            const int pozycjiNaStronie = 11;

            const int xNrD = LewyMargX + 30; //80
            const int xDaneN = xNrD + 150; //230
            const int xNetto = xDaneN + 275; //425
            const int xPVat = xNetto + 80; //505
            const int xVat = xPVat + 50; //555
            const int xBrutto = xVat + 90; //645

            var stawkiVat = new List<int>();

            e.HasMorePages = false;

            g.DrawString("Zestawienie faktur", Fonts.Header, Brushes.Black, new PointF(315, pozY));

            OkresSprzedawcaZestawienie(ref g);

            if (!_fPodsumowanie)
            {
                //NAGLOWEK
                pozY += 80;
                g.DrawString("Lp.", Fonts.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                g.DrawString("Nr/Data", Fonts.Normal, Brushes.Black, new PointF(xNrD, pozY));
                g.DrawString("Dane nabywcy", Fonts.Normal, Brushes.Black, new PointF(xDaneN, pozY));
                g.DrawString("Netto", Fonts.Normal, Brushes.Black, new PointF(xNetto + 10, pozY));
                g.DrawString("%", Fonts.Normal, Brushes.Black, new PointF(xPVat, pozY));
                g.DrawString("Vat", Fonts.Normal, Brushes.Black, new PointF(xVat + 20, pozY));
                g.DrawString("Brutto", Fonts.Normal, Brushes.Black, new PointF(xBrutto, pozY));

                pozY += 20;

                //LISTA FAKTUR CZYTANA Z BAZY
                _iloscPozycji = invoicesAdapter.GetInvoicesCountForSummary(_dateFrom, _dateTo);

                string numerFv = companyDataAdapter.Get().InvoiceNo;
                if (!string.IsNullOrWhiteSpace(numerFv)) numerFv = "/" + numerFv;

                var invoices = invoicesAdapter.GetInvoicesForSummary(_dateFrom, _dateTo, pozycjiNaStronie * _licznikStron);

                foreach (var invoice in invoices)
                {
                    invoice.Customer = new CustomersAdapter().Get(invoice);
                    if (_licznikPozycji >= (_licznikStron + 1) * pozycjiNaStronie)
                    {
                        _licznikStron++;
                        e.HasMorePages = true;
                        break;
                    }
                    pozY += 10;
                    _licznikPozycji++;
                    stawkiVat.Clear();

                    g.DrawLine(Fonts.DashPen, new PointF(50, pozY - 5), new PointF(775, pozY - 5));
                    g.DrawString(_licznikPozycji.ToString(), Fonts.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                    string numer = invoice.Number + numerFv + invoice.Date.ToString("/MM/yyyy", CultureInfo.InvariantCulture);
                    g.DrawString(numer, Fonts.Normal, Brushes.Black, new PointF(xNrD, pozY));
                    g.DrawString(invoice.Date.ToStringDMY(), Fonts.Normal, Brushes.Black, new PointF(xNrD, pozY + 20));
                    g.DrawString(invoice.Customer.LastName, Fonts.Normal, Brushes.Black, new PointF(xDaneN, pozY));
                    g.DrawString(invoice.Customer.FirstName, Fonts.Normal, Brushes.Black, new PointF(xDaneN, pozY + 20));
                    g.DrawString(invoice.Customer.Address1, Fonts.Normal, Brushes.Black, new PointF(xDaneN, pozY + 40));
                    if (!string.IsNullOrWhiteSpace(invoice.Customer.NIP))
                    {
                        g.DrawString("NIP: " + invoice.Customer.NIP, Fonts.Normal, Brushes.Black, new PointF(xDaneN, pozY + 60));
                    }

                    Price cenaFv;
                    cenaFv.Netto = 0;
                    cenaFv.Vat = 0;

                    Price sumaFv;
                    sumaFv.Netto = 0;
                    sumaFv.Vat = 0;

                    var invoiceIngredients = new InvoiceIngredientAdapter().GetInvoiceIngredients(invoice.Id);
                    var vatRates = invoiceIngredients.Select(x => x.VatRate).Distinct().OrderBy(y => y);

                    int podkreślenie = 0;

                    foreach (int vatRate in vatRates)
                    {
                        var ingredientsForVatRate = invoiceIngredients.Where(x => x.VatRate == vatRate);
                        cenaFv.Netto = ingredientsForVatRate.Sum(x => x.Netto);
                        cenaFv.Vat = ingredientsForVatRate.Sum(x => x.VatValue);

                        if (cenaFv.Netto != 0)
                        {
                            g.DrawString(cenaFv.Netto.ToStringTwoDigits(), Fonts.Small, Brushes.Black, new PointF(xNetto + 50, pozY), Fonts.Left);
                            if (vatRate == 0) g.DrawString("zw", Fonts.Small, Brushes.Black, new PointF(xPVat, pozY));
                            else g.DrawString(vatRate.ToString(), Fonts.Small, Brushes.Black, new PointF(xPVat, pozY));
                            g.DrawString(cenaFv.Vat.ToStringTwoDigits(), Fonts.Small, Brushes.Black, new PointF(xVat + 50, pozY), Fonts.Left);
                            g.DrawString((cenaFv.Netto + cenaFv.Vat).ToStringTwoDigits(), Fonts.Small, Brushes.Black, new PointF(xBrutto + 50, pozY), Fonts.Left);
                            pozY += 15;
                            podkreślenie++;
                        }
                    }

                    //Podsumowanie danej faktury
                    g.DrawLine(Fonts.NormalPen, new PointF(480, pozY), new PointF(775, pozY));
                    pozY += 5;

                    sumaFv.Netto = invoiceIngredients.Sum(x => x.Netto);
                    sumaFv.Vat = invoiceIngredients.Sum(x => x.VatValue);

                    g.DrawString(sumaFv.Netto.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xNetto + 50, pozY), Fonts.Left);
                    g.DrawString(sumaFv.Vat.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xVat + 50, pozY), Fonts.Left);
                    g.DrawString((sumaFv.Netto + sumaFv.Vat).ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xBrutto + 50, pozY), Fonts.Left);

                    pozY += 35;

                    if (string.IsNullOrWhiteSpace(invoice.Customer.NIP))
                    {
                        pozY -= 20;
                    }

                    if (podkreślenie < 2) pozY += 20 * (2 - podkreślenie);
                }

                if (pozY < 920)
                {
                    PodsumowanieFv(ref g, pozY, ref e);
                }
                else
                {
                    if (_licznikPozycji >= _iloscPozycji)
                    {
                        e.HasMorePages = true;
                        _fPodsumowanie = true;
                    }
                }
            }
            else
            {
                _licznikStron++;
                pozY += 40;
                PodsumowanieFv(ref g, pozY, ref e);
            }
        }

        public void WplatyZestawieniePrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            var paymentAdapter = new PaymentAdapter();
            string companyDataNumber = new CompanyDataAdapter().Get().InvoiceNo;
            string companyDataSlashNumber = string.IsNullOrWhiteSpace(companyDataNumber) ? string.Empty : "/" + companyDataNumber;

            int pozY = 50;
            const int pozycjiNaStronie = 30;

            const int xDaneN = LewyMargX + 30; //80
            const int xDokument = xDaneN + 300; //380
            const int xData = xDokument + 140; //550
            const int xKwotaOds = xData + 50; //560
            const int xKwota = xKwotaOds + 70; //630
            const int xOdsetki = xKwota + 50; //680

            int previousCustomerId = 0;

            e.HasMorePages = false;

            g.DrawString("Zestawienie wpłat", Fonts.Header, Brushes.Black, new PointF(315, pozY));
            OkresSprzedawcaZestawienie(ref g);

            if (!_fPodsumowanie)
            {
                //NAGLOWEK
                pozY += 80;
                g.DrawString("Lp.", Fonts.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                g.DrawString("Dane", Fonts.Normal, Brushes.Black, new PointF(xDaneN + 100, pozY));
                g.DrawString("Dokument Nr.", Fonts.Normal, Brushes.Black, new PointF(xDokument, pozY));
                g.DrawString("Data", Fonts.Normal, Brushes.Black, new PointF(xData + 20, pozY));
                g.DrawString("Wplata", Fonts.Normal, Brushes.Black, new PointF(xKwotaOds + 40, pozY));
                g.DrawString("Kwota", Fonts.Normal, Brushes.Black, new PointF(xKwota + 40, pozY));
                g.DrawString("Odsetki", Fonts.Normal, Brushes.Black, new PointF(xOdsetki + 60, pozY));

                pozY += 20;

                //USTAWIENIE SPODZIEWANEJ LICZBY WIERSZY NA WYDRUKU
                _iloscPozycji = paymentAdapter.GetPaymentsCountForSummary(_dateFrom, _dateTo);

                //LISTA WPLAT CZYTANA Z BAZY
                //var reader2D = Baza.Wczytaj2D("SELECT SKIP " + pozycjiNaStronie * _licznikStron + " Osoby.ID, Nazwisko, Imie, Dokument, Numer_Dok, Wplaty.Data, Kwota, Odsetki, (Kwota + Odsetki) as"
                //    + " Kwota_ods FROM WPLATY JOIN FAKTURY ON WPLATY.ID_F = FAKTURY.ID JOIN OSOBY ON FAKTURY.ID_N = OSOBY.ID WHERE WPLATY.DATA >= '" + _dataOd.ToString("dd.MM.yyyy")
                //    + "' AND WPLATY.DATA <= '" + _dataDo.ToString("dd.MM.yyyy") + "' ORDER BY Grupa, Nazwisko, Imie");
                var paymentsList = paymentAdapter.GetPaymentsForSummary(_dateFrom, _dateTo, pozycjiNaStronie * _licznikStron);


                foreach (Payment payment in paymentsList)
                {
                    if (_licznikPozycji >= (_licznikStron + 1) * pozycjiNaStronie)
                    {
                        _licznikStron++;
                        e.HasMorePages = true;
                        break;
                    }
                    pozY += 10;
                    _licznikPozycji++;

                    var invoice = new InvoicesAdapter().Get(payment);
                    var customer = new CustomersAdapter().Get(invoice);

                    g.DrawLine(Fonts.DashPen, new PointF(50, pozY - 5), new PointF(785, pozY - 5));
                    g.DrawString(_licznikPozycji.ToString(), Fonts.Normal, Brushes.Black, new PointF(LewyMargX, pozY));

                    if (previousCustomerId != customer.Id)
                    {
                        const int maxLenght = 41;
                        string nazwa = customer.FullName.Length > maxLenght ? customer.FullName.Substring(0, maxLenght) : customer.FullName;
                        g.DrawString(nazwa, Fonts.Normal, Brushes.Black, new PointF(xDaneN, pozY));
                        previousCustomerId = customer.Id;
                    }
                    else
                    {
                        g.DrawString("j. w.", Fonts.Normal, Brushes.Black, new PointF(xDaneN + 50, pozY));
                    }

                    //Numer
                    string numer = payment.DocumentType.GetString(true) + " " + payment.DocumentNumber;
                    if (payment.DocumentType == PaymentDocument.Invoice)
                    {
                        numer = numer + companyDataSlashNumber + payment.Date.ToString("/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    else if (payment.DocumentType == PaymentDocument.KP)
                    {
                        numer = numer + companyDataSlashNumber + "/" + payment.Date.Year;
                    }

                    g.DrawString(numer, Fonts.Normal, Brushes.Black, new PointF(xDokument, pozY));
                    g.DrawString(payment.Date.ToStringDMY(), Fonts.Normal, Brushes.Black, new PointF(xData, pozY));
                    g.DrawString((payment.Amount + payment.Interest).ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xKwotaOds + 100, pozY), Fonts.Left);
                    g.DrawString(payment.Amount.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xKwota + 100, pozY), Fonts.Left);
                    g.DrawString(payment.Interest.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xOdsetki + 100, pozY), Fonts.Left);

                    pozY += 20;
                }

                if (pozY < 900)
                {
                    PodsumowanieWplaty(ref g, pozY, ref e);
                }
                else
                {
                    if (_licznikPozycji >= _iloscPozycji)
                    {
                        e.HasMorePages = true;
                        _fPodsumowanie = true;
                    }
                }
            }
            else
            {
                _licznikStron++;
                pozY += 40;
                PodsumowanieWplaty(ref g, pozY, ref e);
            }
        }

        public void WydatkiZestawieniePrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            PurchaseAdapter purchaseAdapter = new();

            int pozY = 50;
            const int pozycjiNaStronie = 19;

            const int xNrDat = LewyMargX + 30; //80
            const int xDaneOpis = xNrDat + 120; //200
            const int xKwota = xDaneOpis + 460; //660
            const int xZaplata = xKwota + 10; //670
            const int xSposob = xZaplata + 90; //760

            e.HasMorePages = false;

            g.DrawString("Zestawienie wydatków", Fonts.Header, Brushes.Black, new PointF(315, pozY));
            OkresSprzedawcaZestawienie(ref g);

            if (!_fPodsumowanie)
            {
                //NAGLOWEK
                pozY += 80;
                g.DrawString("Lp.", Fonts.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                g.DrawString("Nr. FV", Fonts.Normal, Brushes.Black, new PointF(xNrDat + 20, pozY));
                g.DrawString("Data", Fonts.Normal, Brushes.Black, new PointF(xNrDat + 25, pozY + 20));
                g.DrawString("Dane dostawcy", Fonts.Normal, Brushes.Black, new PointF(xDaneOpis, pozY));
                g.DrawString("Opis", Fonts.Normal, Brushes.Black, new PointF(xDaneOpis, pozY + 20));
                g.DrawString("Kwota", Fonts.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonts.Left);
                g.DrawString("Zapłacono", Fonts.Normal, Brushes.Black, new PointF(xZaplata, pozY));
                g.DrawString("Sposób", Fonts.Normal, Brushes.Black, new PointF(xSposob - 20, pozY));

                pozY += 40;


                //USTAWIENIE SPODZIEWANEJ LICZBY WIERSZY NA WYDRUKU
                _iloscPozycji = purchaseAdapter.GetPurchasesCountForSummary(_dateFrom, _dateTo);

                ////LISTA WPLAT CZYTANA Z BAZY
                var purchases = purchaseAdapter.GetPurchasesForSummary(_dateFrom, _dateTo, pozycjiNaStronie * _licznikStron);

                foreach (var purchase in purchases)
                {
                    if (_licznikPozycji >= (_licznikStron + 1) * pozycjiNaStronie)
                    {
                        _licznikStron++;
                        e.HasMorePages = true;
                        break;
                    }
                    pozY += 10;
                    _licznikPozycji++;

                    var supplier = new SupplierAdapter().Get(purchase);

                    g.DrawLine(Fonts.DashPen, new PointF(50, pozY - 5), new PointF(785, pozY - 5));
                    g.DrawString(_licznikPozycji.ToString(), Fonts.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                    g.DrawString(purchase.Number, Fonts.Normal, Brushes.Black, new PointF(xNrDat, pozY));
                    g.DrawString(purchase.DateIssue.ToStringDMY(), Fonts.Normal, Brushes.Black, new PointF(xNrDat, pozY + 20));
                    g.DrawString(supplier.FullName, Fonts.Normal, Brushes.Black, new PointF(xDaneOpis, pozY));
                    g.DrawString(purchase.Description, Fonts.Normal, Brushes.Black, new PointF(xDaneOpis, pozY + 20));
                    g.DrawString(purchase.AmountPaid.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonts.Left);
                    g.DrawString(purchase.DatePayment.ToStringDMY(), Fonts.Normal, Brushes.Black, new PointF(xZaplata, pozY));
                    g.DrawString(purchase.PaymentMethod.GetShortString(), Fonts.Normal, Brushes.Black, new PointF(xSposob, pozY));

                    pozY += 40;
                }

                if (pozY < 950)
                {
                    PodsumowanieWydatki(ref g, pozY, ref e);
                }
                else
                {
                    if (_licznikPozycji >= _iloscPozycji)
                    {
                        e.HasMorePages = true;
                        _fPodsumowanie = true;
                    }
                }
            }
            else
            {
                _licznikStron++;
                pozY += 40;
                PodsumowanieWydatki(ref g, pozY, ref e);
            }
        }

        public void OdsetkiZestawieniePrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            PaymentAdapter paymentAdapter = new();

            int pozY = 50;
            const int pozycjiNaStronie = 20;

            const int xDaneN = LewyMargX + 50; //80
            const int xOdsetki = xDaneN + 550; //660

            e.HasMorePages = false;

            g.DrawString("Zestawienie odsetek", Fonts.Header, Brushes.Black, new PointF(315, pozY));
            OkresSprzedawcaZestawienie(ref g);

            if (!_fPodsumowanie)
            {
                //NAGLOWEK
                pozY += 80;
                g.DrawString("Lp.", Fonts.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                g.DrawString("Dane najemcy", Fonts.Normal, Brushes.Black, new PointF(xDaneN + 200, pozY));
                g.DrawString("Odsetki", Fonts.Normal, Brushes.Black, new PointF(xOdsetki + 60, pozY));

                pozY += 20;

                //USTAWIENIE SPODZIEWANEJ LICZBY WIERSZY NA WYDRUKU
                _iloscPozycji = paymentAdapter.GetInterestsCountForSummary(_dateFrom, _dateTo);

                //LISTA ODSETEK CZYTANA Z BAZY
                var odsetkiLista = paymentAdapter.GetInterestsForSummary(_dateFrom, _dateTo, pozycjiNaStronie * _licznikStron);

                foreach (var payment in odsetkiLista)
                {
                    if (_licznikPozycji >= (_licznikStron + 1) * pozycjiNaStronie)
                    {
                        _licznikStron++;
                        e.HasMorePages = true;
                        break;
                    }
                    pozY += 10;
                    _licznikPozycji++;

                    var invoice = new InvoicesAdapter().Get(payment);
                    var customer = new CustomersAdapter().Get(invoice);

                    g.DrawLine(Fonts.DashPen, new PointF(50, pozY - 5), new PointF(775, pozY - 5));
                    g.DrawString(_licznikPozycji.ToString(), Fonts.Normal, Brushes.Black, new PointF(LewyMargX, pozY));
                    g.DrawString(customer.FullName, Fonts.Normal, Brushes.Black, new PointF(xDaneN, pozY));
                    g.DrawString(payment.Interest.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xOdsetki + 100, pozY), Fonts.Left);

                    pozY += 20;
                }

                if (pozY < 820)
                {
                    PodsumowanieOdsetki(ref g, pozY, ref e);
                }
                else
                {
                    if (_licznikPozycji >= _iloscPozycji)
                    {
                        e.HasMorePages = true;
                        _fPodsumowanie = true;
                    }
                }
            }
            else
            {
                _licznikStron++;
                pozY += 40;
                PodsumowanieOdsetki(ref g, pozY, ref e);
            }

        }

        public void InneObrotyPrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            OtherTransactionAdapter otherTransactionAdapter = new();

            e.HasMorePages = false;
            const int lewyMargX = 75;
            const int pozNaglowka = 50;
            const int pozycjiNaStronie = 45;
            int pozY = 70;

            g.DrawString("Zestawienie innych obrotów", Fonts.Header, Brushes.Black, new PointF(275, pozNaglowka));
            OkresSprzedawcaZestawienie(ref g);

            string typeString = _otherTransactionType.HasValue ? _otherTransactionType.Value.GetString() : "Wszystkie";
            g.DrawString("Typ: " + typeString, Fonts.Normal, Brushes.Black, new PointF(550, pozNaglowka + 30));

            if (!_fPodsumowanie)
            {
                pozY += 70;

                //NAGLOWKI
                g.DrawString("Lp.", Fonts.Goods, Brushes.Black, new PointF(lewyMargX, pozY));
                g.DrawString("Data", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 30, pozY));
                g.DrawString("Typ", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 130, pozY));
                g.DrawString("Kwota", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 280, pozY));
                g.DrawString("Opis", Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 350, pozY));
                g.DrawLine(Fonts.DashPen, lewyMargX, pozY + 25, 750, pozY + 25);
                pozY += 10;

                try
                {
                    var otherTransactions = otherTransactionAdapter.GetOtherTransactionsForSummary(_dateFrom, _dateTo, pozycjiNaStronie * _licznikStron, _otherTransactionType);

                    foreach (var transaction in otherTransactions)
                    {
                        if (_licznikPozycji >= (_licznikStron + 1) * pozycjiNaStronie)
                        {
                            _licznikStron++;
                            e.HasMorePages = true;
                        }
                        else
                        {
                            pozY += 20;
                            _licznikPozycji++;

                            g.DrawString(_licznikPozycji.ToString(), Fonts.Goods, Brushes.Black, new PointF(lewyMargX, pozY));
                            g.DrawString(transaction.Date.ToStringDMY(), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 30, pozY));
                            g.DrawString(transaction.Type.GetString(), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 130, pozY));
                            g.DrawString(transaction.Amount.ToStringTwoDigits(), Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 340, pozY), Fonts.Left);
                            g.DrawString(transaction.Description, Fonts.Goods, Brushes.Black, new PointF(lewyMargX + 350, pozY));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Report.Error(ex);
                }

                if (pozY < 820)
                {
                    PodsumowanieInneObroty(ref g, pozY, ref e);
                }
                else
                {
                    if (_licznikPozycji >= _iloscPozycji)
                    {
                        e.HasMorePages = true;
                        _fPodsumowanie = true;
                    }
                }
            }
            else
            {
                _licznikStron++;

                PodsumowanieInneObroty(ref g, pozY, ref e);
            }
        }



        private void PodsumowanieFv(ref Graphics g, int pozY, ref PrintPageEventArgs e)
        {
            const int xNetto = LewyMargX + 375; //425
            const int xPVat = xNetto + 80; //505
            const int xVat = xPVat + 50; //555
            const int xBrutto = xVat + 90; //645

            pozY += 50;
            g.DrawString("Podsumowanie", Fonts.Dane, Brushes.Black, new PointF(520, pozY));
            pozY += 20;
            g.DrawString("Netto", Fonts.Normal, Brushes.Black, new PointF(xNetto + 10, pozY));
            g.DrawString("%", Fonts.Normal, Brushes.Black, new PointF(xPVat, pozY));
            g.DrawString("Vat", Fonts.Normal, Brushes.Black, new PointF(xVat + 20, pozY));
            g.DrawString("Brutto", Fonts.Normal, Brushes.Black, new PointF(xBrutto, pozY));
            pozY += 15;
            g.DrawLine(Fonts.NormalPen, xNetto - 20, pozY, xBrutto + 50, pozY);
            pozY += 5;

            var vatRates = new InvoiceIngredientAdapter().GetVatRatesForSummary();

            try
            {
                decimal sumaNetto = 0;
                decimal sumaVat = 0;

                foreach (int vat in vatRates)
                {
                    var sumaCenNetto = new InvoiceIngredientAdapter().GetSumOfVatRate(_dateFrom, _dateTo, vat, false);
                    var sumaCenVat = new InvoiceIngredientAdapter().GetSumOfVatRate(_dateFrom, _dateTo, vat, true);

                    if (sumaCenNetto != 0)
                    {
                        g.DrawString(sumaCenNetto.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xNetto + 50, pozY), Fonts.Left);
                        g.DrawString(vat == 0 ? "zw" : vat.ToString(), Fonts.Normal, Brushes.Black, new PointF(xPVat, pozY));
                        g.DrawString(sumaCenVat.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xVat + 50, pozY), Fonts.Left);
                        g.DrawString((sumaCenNetto + sumaCenVat).ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xBrutto + 50, pozY), Fonts.Left);
                        pozY += 20;

                        sumaNetto += sumaCenNetto;
                        sumaVat += sumaCenVat;
                    }
                }

                g.DrawLine(Fonts.NormalPen, xNetto - 20, pozY, xBrutto + 50, pozY);
                pozY += 5;
                g.DrawString(sumaNetto.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xNetto + 50, pozY), Fonts.Left);
                g.DrawString(sumaVat.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xVat + 50, pozY), Fonts.Left);
                g.DrawString((sumaNetto + sumaVat).ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xBrutto + 50, pozY), Fonts.Left);
            }
            catch (Exception ex)
            {
                Report.Error(ex);
            }

            e.HasMorePages = false;
            _fPodsumowanie = false;
            _licznikPozycji = 0;
            _licznikStron = 0;
        }

        private void PodsumowanieWplaty(ref Graphics g, int pozY, ref PrintPageEventArgs e)
        {
            const int xLabel = LewyMargX + 600; //660
            const int xKwota = xLabel + 120; //660

            pozY += 50;
            g.DrawString("Podsumowanie", Fonts.Dane, Brushes.Black, new PointF(xLabel - 30, pozY));
            pozY += 20;
            g.DrawLine(Fonts.DashPen, xLabel - 35, pozY, xKwota + 5, pozY);
            pozY += 5;

            g.DrawString("Gotówka: ", Fonts.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            decimal sumCash = new PaymentAdapter().GetPaymentsSumForSummary(_dateFrom, _dateTo, PaymentMethod.Cash);
            g.DrawString(sumCash.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonts.Left);

            pozY += 20;

            g.DrawString("Przelew: ", Fonts.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            decimal sumTransfer = new PaymentAdapter().GetPaymentsSumForSummary(_dateFrom, _dateTo, PaymentMethod.BankTransfer);
            g.DrawString(sumTransfer.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonts.Left);

            pozY += 25;

            g.DrawLine(Fonts.DashPen, xLabel - 35, pozY - 5, xKwota + 5, pozY - 5);
            g.DrawString("Suma:  ", Fonts.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString((sumCash + sumTransfer).ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonts.Left);

            e.HasMorePages = false;
            _fPodsumowanie = false;
            _licznikPozycji = 0;
            _licznikStron = 0;
        }

        private void PodsumowanieWydatki(ref Graphics g, int pozY, ref PrintPageEventArgs e)
        {
            const int xLabel = LewyMargX + 600; //660
            const int xKwota = xLabel + 120; //660

            PurchaseAdapter purchaseAdapter = new();

            pozY += 50;
            g.DrawString("Podsumowanie", Fonts.Dane, Brushes.Black, new PointF(xLabel - 30, pozY));
            pozY += 20;
            g.DrawLine(Fonts.DashPen, xLabel - 35, pozY, xKwota + 5, pozY);
            pozY += 5;

            var sumCash = purchaseAdapter.GetPurchaseSumForSummary(_dateFrom, _dateTo, PaymentMethod.Cash, AmountType.Brutto);
            g.DrawString("Gotówka: ", Fonts.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString(sumCash.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonts.Left);

            pozY += 20;

            var sumTransfer = purchaseAdapter.GetPurchaseSumForSummary(_dateFrom, _dateTo, PaymentMethod.BankTransfer, AmountType.Brutto);
            g.DrawString("Przelew: ", Fonts.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString(sumTransfer.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonts.Left);

            pozY += 20;

            // Netto,Vat, brutto
            var sumNetto = purchaseAdapter.GetPurchaseSumForSummary(_dateFrom, _dateTo, null, AmountType.Netto);
            g.DrawLine(Fonts.DashPen, xLabel - 35, pozY - 3, xKwota + 5, pozY - 3);
            g.DrawString("Netto: ", Fonts.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString(sumNetto.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonts.Left);

            pozY += 20;

            var sumVat = purchaseAdapter.GetPurchaseSumForSummary(_dateFrom, _dateTo, null, AmountType.Vat);
            g.DrawString("Vat:", Fonts.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString(sumVat.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonts.Left);

            pozY += 20;

            g.DrawLine(Fonts.DashPen, xLabel - 35, pozY - 3, xKwota + 5, pozY - 3);
            g.DrawString("Suma:  ", Fonts.Normal, Brushes.Black, new PointF(xLabel - 30, pozY));
            g.DrawString((sumVat + sumNetto).ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xKwota, pozY), Fonts.Left);

            e.HasMorePages = false;
            _fPodsumowanie = false;
            _licznikPozycji = 0;
            _licznikStron = 0;
        }

        private void PodsumowanieOdsetki(ref Graphics g, int pozY, ref PrintPageEventArgs e)
        {
            const int xOdsetki = LewyMargX + 610; //660

            pozY += 50;
            g.DrawString("Podsumowanie", Fonts.Dane, Brushes.Black, new PointF(xOdsetki - 30, pozY));
            pozY += 20;
            g.DrawLine(Fonts.DashPen, xOdsetki - 35, pozY, xOdsetki + 110, pozY);
            pozY += 5;

            var interest = new PaymentAdapter().GetInterestsSumForSummary(_dateFrom, _dateTo);
            g.DrawString("Suma odsetek: " + interest.ToStringTwoDigits(), Fonts.Normal, Brushes.Black, new PointF(xOdsetki - 30, pozY));

            e.HasMorePages = false;
            _fPodsumowanie = false;
            _licznikPozycji = 0;
            _licznikStron = 0;
        }

        private void PodsumowanieInneObroty(ref Graphics g, int pozY, ref PrintPageEventArgs e)
        {
            const int pozOpisu = 250;
            const int pozSum = 500;
            OtherTransactionAdapter otherTransactionAdapter = new();

            pozY += 100;
            g.DrawString("Podsumowanie", Fonts.Dane, Brushes.Black, new PointF(325, pozY));

            if (_otherTransactionType.HasValue)
            {
                pozY += 30;
                g.DrawString(_otherTransactionType.Value.GetString(), Fonts.Normal, Brushes.Black, new PointF(pozOpisu, pozY));
                var sum = otherTransactionAdapter.GetOtherTransactionsSumForSummary(_dateFrom, _dateTo, _otherTransactionType.Value);
                g.DrawString(sum.ToStringWithCurrencySymbol(), Fonts.Normal, Brushes.Black, new PointF(pozSum, pozY), Fonts.Left);
            }
            else
            {
                var types = OtherTransactionType.AccountToOther.GetDictionary();

                foreach (var type in types)
                {
                    pozY += 30;
                    g.DrawString(type.Value.GetString(), Fonts.Normal, Brushes.Black, new PointF(pozOpisu, pozY));
                    var sum = otherTransactionAdapter.GetOtherTransactionsSumForSummary(_dateFrom, _dateTo, type.Value);
                    g.DrawString(sum.ToStringWithCurrencySymbol(), Fonts.Normal, Brushes.Black, new PointF(pozSum, pozY), Fonts.Left);
                }
            }

            e.HasMorePages = false;
            _fPodsumowanie = false;
            _licznikPozycji = 0;
            _licznikStron = 0;
        }
    }
}
