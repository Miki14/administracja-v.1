﻿using System;
using System.Drawing.Printing;
using System.Windows;
using System.Windows.Forms;
using ADMv2.Common;
using ADMv2.CommonProj;
using ADMv2.DAL.Enums;

namespace ADMv2.Printing.SummaryWindow
{
    public partial class SummaryWindowView : Window
    {
        private readonly SummaryWindowViewModel _viewModel;

        public SummaryWindowView(bool isSelectedOtherOperations = false)
        {
            InitializeComponent();

            _viewModel = new SummaryWindowViewModel();
            DataContext = _viewModel;

            if (isSelectedOtherOperations)
                OtherOperationsRad.IsChecked = true;
            else
                InvoicesRad.IsChecked = true;

            startDatePicker.Value = DateTime.Today.GetFirstDayOfMonth();
            endDatePicker.Value = DateTime.Today.GetLastDayOfMonth();
            OneMonthBox.Checked += OneMonthBoxChecked;
            OneMonthBox.Unchecked += OneMonthBoxChecked;

            OtherOperationsTypeCombo.ItemsSource = OtherTransactionType.AccountToCashdesk.GetDictionaryStringWithAll();
            OtherOperationsTypeCombo.SelectedIndex = 0;

            OneMonthBoxChecked(this, null);
        }

        private void OneMonthBoxChecked(object sender, RoutedEventArgs e)
        {
            DatePickerValueChanged(this, null);
            if (OneMonthBox.IsChecked == true)
            {
                endDatePicker.IsEnabled = false;
                startDatePicker.Format = Xceed.Wpf.Toolkit.DateTimeFormat.YearMonth;
            }
            else
            {
                endDatePicker.IsEnabled = true;
                startDatePicker.Format = Xceed.Wpf.Toolkit.DateTimeFormat.Custom;
                startDatePicker.FormatString = "dd MMMM yyyy";
                startDatePicker.Value = startDatePicker.Value.Value.GetFirstDayOfMonth();
            }
        }

        private void DatePickerValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (OneMonthBox.IsChecked == true)
            {
                endDatePicker.Value = startDatePicker.Value?.GetLastDayOfMonth();
            }
        }

        private void BackButtClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void PreviewButt_Click(object sender, RoutedEventArgs e)
        {
            PrintPreviewDialog printPreviewDialog = new PrintPreviewDialog();
            printPreviewDialog.Document = GetPrintDocument();
            ((Form)printPreviewDialog).WindowState = FormWindowState.Maximized;
            printPreviewDialog.PrintPreviewControl.Zoom = 1.5;
            printPreviewDialog.ShowDialog();
        }

        private void PrintButt_Click(object sender, RoutedEventArgs e)
        {
            var printDok = GetPrintDocument();
            var printDialog = new PrintDialog()
            {
                Document = printDok,
            };

            if (printDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

            try
            {
                printDok.Print();
            }
            catch (Exception ex)
            {
                Report.Error(ex);
            }
        }

        private PrintDocument GetPrintDocument()
        {
            var printDok = new PrintDocument();
            var summaryPrinting = new SummaryPrinting(startDatePicker.Value.Value, endDatePicker.Value.Value);

            if (InvoicesRad.IsChecked == true)
            {
                printDok.PrintPage += summaryPrinting.FakturyZestawieniePrintPage;
            }
            else if (PaymentsRad.IsChecked == true)
            {
                printDok.PrintPage += summaryPrinting.WplatyZestawieniePrintPage;
            }
            else if (PurchasesRad.IsChecked == true)
            {
                printDok.PrintPage += summaryPrinting.WydatkiZestawieniePrintPage;
            }
            else if (InterestsRad.IsChecked == true)
            {
                printDok.PrintPage += summaryPrinting.OdsetkiZestawieniePrintPage;
            }
            else
            {
                summaryPrinting = new SummaryPrinting(startDatePicker.Value.Value, endDatePicker.Value.Value, _viewModel.SelectedType);
                printDok.PrintPage += summaryPrinting.InneObrotyPrintPage;
            }

            return printDok;
        }
    }
}