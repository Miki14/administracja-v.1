﻿using System.Drawing;
using System.Globalization;

namespace ADMv2.Printing
{
    static class Fonts
    {
        public static Font Header = new("Arial", 14, FontStyle.Bold);
        public static Font Period = new("Arial", 14);
        public static Font Normal = new("Arial", 10);
        public static Font Dane = new("Arial", 11, FontStyle.Bold);
        public static Font Goods = new("Arial", 11);
        public static Font Small = new("Arial", 8);
        public static Font Duplicate = new("Arial", 15, FontStyle.Bold);
        public static Font InWords = new("Arial", 10, FontStyle.Bold);

        public static Pen NormalPen = new(Brushes.Black);
        public static Pen DashPen = new(Brushes.Black);

        public static StringFormat Left = new()
        {
            Alignment = StringAlignment.Far
        };

        //liczba.ToString i w nawiasie podajemy jaki ma być separator, przecinek czy kropka
        public static CultureInfo Kropka = CultureInfo.InvariantCulture;
        public static CultureInfo Przecinek = new("pl-PL");

        static Fonts()
        {
            const float tmpLength = 10;
            DashPen.DashPattern = [tmpLength, tmpLength, tmpLength, tmpLength];
        }
    }
}
