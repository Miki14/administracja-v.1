﻿using System;
using ADMv2.DAL.Entities;

namespace ADMv2.Printing
{
    public class InvoicePrintingDto : SaleInvoice
    {
        public InvoicePrintingDto() { }
        public InvoicePrintingDto(SaleInvoice invoice)
        {
            Id = invoice.Id;
            Number = invoice.Number;
            Date = invoice.Date;
            PaymentMethod = invoice.PaymentMethod;
            PaymentDeadline = invoice.PaymentDeadline;
            Note = invoice.Note;
            Printed = invoice.Printed;
            Paid = invoice.Paid;
            SumBrutto = invoice.SumBrutto;

            CopyNumber = 2;
            Duplicate = false;
            DuplicateDate = DateTime.Today;
        }

        public int? CopyNumber { get; set; }
        public bool Duplicate { get; set; }
        public DateTime DuplicateDate { get; set; }
    }
}
