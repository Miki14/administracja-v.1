﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using ADMv2.Common;
using ADMv2.CommonProj;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Models;

namespace ADMv2.Printing
{
    public class BalancePrinting
    {
        private readonly PrintDocument _printDocument = new();
        private readonly BalanceModel _balanceModel;
        private readonly CompanyData _companyData;

        public BalancePrinting(BalanceModel balanceModel, CompanyData companyData)
        {
            _printDocument.PrintPage += PdPrintPage;
            _balanceModel = balanceModel;
            _companyData = companyData;
        }

        public void Print()
        {
            PrintDialog print_dia = new()
            {
                Document = _printDocument
            };

            if (print_dia.ShowDialog() != DialogResult.OK) return;

            try
            {
                _printDocument.Print();
            }
            catch (Exception ex)
            {
                Report.Error(ex);
            }
        }

        public void Preview()
        {
            PrintPreviewDialog podglad_dia = new()
            {
                Document = _printDocument
            };

            ((Form)podglad_dia).WindowState = FormWindowState.Maximized;
            podglad_dia.PrintPreviewControl.Zoom = 1.5;
            podglad_dia.ShowDialog();
        }

        private void PdPrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;

            int pozY = 50;
            const int lewyMargX = 50;
            const int xOpis = 100;
            const int xKonto = 450;
            const int xKasa = 700;

            g.DrawString("Saldo", Fonts.Header, Brushes.Black, new PointF(375, pozY));
            g.DrawString("Za okres od " + _balanceModel.DateFrom.ToStringDMY() + " do " + _balanceModel.DateTo.ToStringDMY(), Fonts.Period, Brushes.Black, new PointF(lewyMargX, pozY + 30));

            g.DrawString(_companyData.InvoiceNo, Fonts.Normal, Brushes.Black, new PointF(750, 30));
            g.DrawString(_companyData.Name1 + " " + _companyData.Name2 + " " + _companyData.Address1 + " " + _companyData.Address2, Fonts.Normal, Brushes.Black, new PointF(lewyMargX, pozY + 50));

            pozY += 125;

            g.DrawString("Konto", Fonts.Period, Brushes.Black, new PointF(xKonto - 75, pozY));
            g.DrawString("Kasa", Fonts.Period, Brushes.Black, new PointF(xKasa - 75, pozY));

            pozY += 40;

            if (_balanceModel.IncludePreviousBalance)
            {
                g.DrawString("Poprzednie saldo:", Fonts.Period, Brushes.Black, new PointF(xOpis, pozY));
                g.DrawString(_balanceModel.PreviousCashBalance.ToStringWithCurrencySymbol(), Fonts.Period, Brushes.Black, new PointF(xKasa, pozY), Fonts.Left);
                g.DrawString(_balanceModel.PreviousAccountBalance.ToStringWithCurrencySymbol(), Fonts.Period, Brushes.Black, new PointF(xKonto, pozY), Fonts.Left);
            }
            pozY += 30;

            g.DrawString("Wplaty:", Fonts.Period, Brushes.Black, new PointF(xOpis, pozY));
            g.DrawString(_balanceModel.InputsCash.ToStringWithCurrencySymbol(), Fonts.Period, Brushes.Black, new PointF(xKasa, pozY), Fonts.Left);
            g.DrawString(_balanceModel.InputsAccount.ToStringWithCurrencySymbol(), Fonts.Period, Brushes.Black, new PointF(xKonto, pozY), Fonts.Left);
            pozY += 30;

            g.DrawString("Wyplaty:", Fonts.Period, Brushes.Black, new PointF(xOpis, pozY));
            g.DrawString(_balanceModel.OutputsCash.ToStringWithCurrencySymbol(), Fonts.Period, Brushes.Black, new PointF(xKasa, pozY), Fonts.Left);
            g.DrawString(_balanceModel.OutputsAccount.ToStringWithCurrencySymbol(), Fonts.Period, Brushes.Black, new PointF(xKonto, pozY), Fonts.Left);
            pozY += 30;

            g.DrawString("Saldo:", Fonts.Period, Brushes.Black, new PointF(xOpis, pozY));
            g.DrawString(_balanceModel.CurrentCashBalance.ToStringWithCurrencySymbol(), Fonts.Header, Brushes.Black, new PointF(xKasa, pozY), Fonts.Left);
            g.DrawString(_balanceModel.CurrentAccountBalance.ToStringWithCurrencySymbol(), Fonts.Header, Brushes.Black, new PointF(xKonto, pozY), Fonts.Left);
            pozY += 30;

            g.DrawString("Saldo konta i kasy:", Fonts.Period, Brushes.Black, new PointF(xOpis, pozY));
            g.DrawString(_balanceModel.TotalBalance.ToStringWithCurrencySymbol(), Fonts.Header, Brushes.Black, new PointF(600, pozY), Fonts.Left);
        }
    }
}
