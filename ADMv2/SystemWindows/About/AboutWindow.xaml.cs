﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;

namespace ADMv2.SystemWindows.About
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        public AboutWindow()
        {
            InitializeComponent();
            //TODO: Przejść na common.CurretnVersion, żeby mieć tylko 2 cyfry
            verLabel.Content = "Wersja: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        private void BackButtClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri) { UseShellExecute = true });
            e.Handled = true;
        }
    }
}
