﻿using System.Windows.Input;
using ADMv2.Common;
using ADMv2.DAL.Adapters;
using ADMv2.MainWindowComponents;
using CommunityToolkit.Mvvm.Input;

namespace ADMv2.SystemWindows.CompanyData
{
    public class CompanyDataViewModel : ViewModel
    {
        public override WindowType WindowType => WindowType.CompanyData;
        public ICommand SelectLogoCommand => new RelayCommand(SelectLogo);
        public ICommand SaveCommand => new RelayCommand(Save);
        public ICommand BackCommand => new RelayCommand(Back);

        public DAL.Entities.CompanyData _companyData;
        public DAL.Entities.CompanyData CompanyData
        {
            get => _companyData;
            set => SetProperty(ref _companyData, value);
        }

        public CompanyDataViewModel()
        {
            CompanyData = new CompanyDataAdapter().Get();
        }

        public override void Refresh(int? returnedValue = null) { }

        private void SelectLogo()
        {
            //TODO: Po wybraniu nie odświeża UI

            //var dialogWindow = new OpenFileDialog
            //{
            //    Filter = "Image Files(*.BMP;*.JPG;*.JPEG;*.PNG)|*.BMP;*.JPG;*.JPEG;*.PNG|All Files (*.*)|*.*",
            //    InitialDirectory = Application.StartupPath + "Loga"
            //};

            //if (dialogWindow.ShowDialog() == DialogResult.OK)
            //{
            //    CompanyData.Logo = new FileInfo(dialogWindow.FileName).Name;
            //}
        }

        private void Save()
        {
            new CompanyDataAdapter().AddOrUpdate(CompanyData);
            MainWindow.Instance.CloseWindow(WindowType);
        }

        private void Back()
        {
            MainWindow.Instance.CloseWindow(WindowType);
        }
    }
}
