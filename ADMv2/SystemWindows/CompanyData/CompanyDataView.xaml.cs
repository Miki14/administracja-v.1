﻿using System.IO;
using System.Windows.Forms;
using ADMv2.Common;

namespace ADMv2.SystemWindows.CompanyData
{
    public partial class CompanyDataView : System.Windows.Controls.UserControl, IUserControlRefreshable
    {
        private CompanyDataViewModel _model;
        public CompanyDataView()
        {
            InitializeComponent();
            _model = new CompanyDataViewModel();
            DataContext = _model;
        }

        public void Refresh(int? returnedValue = null)
        {
            ((CompanyDataViewModel)DataContext).Refresh();
        }

        private void SelectLogoButt_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //TODO: Przenieśćna ViewModel. Jest tu bo nie odświeżał się UI

            var dialogWindow = new OpenFileDialog
            {
                Filter = "Image Files(*.BMP;*.JPG;*.JPEG;*.PNG)|*.BMP;*.JPG;*.JPEG;*.PNG|All Files (*.*)|*.*",
                InitialDirectory = Application.StartupPath + "Loga"
            };

            if (dialogWindow.ShowDialog() == DialogResult.OK)
            {
                string fileName = new FileInfo(dialogWindow.FileName).Name;
                _model.CompanyData.Logo = fileName;
                FileNameLab.Content = fileName;
            }
        }
    }
}
