﻿using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using Microsoft.AspNetCore.Identity;

namespace ADMv2.Common
{
    public class CurrentUserManager
    {
        public static User CurrentUser => _currentUser;
        private static User _currentUser;

        private CurrentUserManager() { }

        public static bool LoginUser(string login, string password)
        {
            UserAdapter userAdapter = new();
            var tmpUser = userAdapter.Get(login);

            var _hasher = new PasswordHasher<User>();
            var result = _hasher.VerifyHashedPassword(tmpUser, tmpUser.PasswordHash, password);

            if (result == PasswordVerificationResult.Success)
            {
                _currentUser = tmpUser;
                return true;
            }
            return false;
        }

        public static string CreateHashForUser(User user, string password)
        {
            var _hasher = new PasswordHasher<User>();
            return _hasher.HashPassword(user, password);
        }

        public static void LogOut()
        {
            _currentUser = null;
        }
    }
}
