﻿using ADMv2.MainWindowComponents;
using CommunityToolkit.Mvvm.ComponentModel;

namespace ADMv2.Common
{
    public abstract class ViewModel : ObservableRecipient
    {
        abstract public WindowType WindowType { get; }

        abstract public void Refresh(int? returnedValue = null);
    }
}
