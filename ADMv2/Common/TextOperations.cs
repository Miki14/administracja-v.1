﻿using System;
using System.Windows.Forms;
using LiczbyNaSlowaNETCore;

namespace ADMv2.Common
{
    static class TextOperations
    {
        public static NumberToTextOptions LiczbyNaSlowaOptions = new()
        {
            Currency = Currency.PLN,
            Stems = true, //ąę
            SplitDecimal = "i",
        };

        public static void WordsToUpper(TextBox textBox)
        {
            int poz = textBox.SelectionStart;

            string[] slowa = textBox.Text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            string nazwaBoxText = "";

            for (int i = 0; i < slowa.Length; i++)
            {
                nazwaBoxText += slowa[i].Substring(0, 1).ToUpper() + slowa[i].Substring(1) + " ";
            }
            textBox.Text = nazwaBoxText;

            textBox.SelectionStart = poz;
        }

        public static void FirstToUpper(TextBox textBox)
        {
            int poz = textBox.SelectionStart;

            textBox.Text = textBox.Text.Substring(0, 1).ToUpper() + textBox.Text.Substring(1);

            textBox.SelectionStart = poz;
        }

        public static string TrimDate(string data)
        {
            return DateTime.Parse(data).ToString("dd.MM.yyyy");
        }

        public static bool CheckKeys(char key)
        {
            return key == (char)Keys.Enter;
            //najlepiej zrobione:
            //if (e.KeyChar == (char)Keys.Enter)
            //{
            //    zaloguj_Click(this, EventArgs.Empty);
            //    e.KeyChar = (char)0;          //jak nie to 4 razy wywoła funkcje
            //    e.Handled = true;
            //}
        }
    }
}
