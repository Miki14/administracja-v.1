﻿using System.Linq;
using System.Threading.Tasks;
using SolidCompany.Interop.Gus;
using SolidCompany.Interop.Gus.Connected_Services;

namespace ADMv2.Common
{
    public class GusHelper
    {
        private static readonly string GusKey = "ba2342425d7c40a68e6f";
        public GusHelper() { }

        public static async Task<GusResult> GetGusData(string nip)
        {
            await using var client = new GusBirClient(GusKey, GusEnironment.Production);

            var result = await client.FindByNipAsync(nip);

            if (result.Success)
            {
                var found = result.Entities.FirstOrDefault();

                var gusResult = new GusResult();
                gusResult.Name = found.Name;
                var adres1 = string.Format("{0} {1}", found.Street, found.BuildingNumber);
                gusResult.Address1 = !string.IsNullOrWhiteSpace(found.LocalNumber) ? string.Format("{0}/{1}", adres1, found.LocalNumber) : adres1;
                gusResult.Address2 = string.Format("{0} {1}", found.PostalCode, found.City);

                return gusResult;
            }
            return null;
        }
    }

    public class GusResult
    {
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
    }
}
