﻿namespace ADMv2.Common
{
    public interface IUserControlRefreshable
    {
        void Refresh(int? returnedValue = null);
    }
}
