﻿using System.ComponentModel.DataAnnotations;

namespace ADMv2.DAL.Entities
{
    public interface IBaseEntity
    {
        //TODO: Bazowa encja: konstruktor, konstruktor przyjmujący id, pole id

        [Required]
        int Id { get; set; }
    }
}
