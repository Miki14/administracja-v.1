﻿using System;
using System.ComponentModel.DataAnnotations;
using ADMv2.DAL.Enums;

namespace ADMv2.DAL.Entities
{
    public class PurchaseIngredient : IBaseEntity
    {
        //SKŁADNIK PRZYPISANY DO ZAKUPU
        //TODO: Dziedziczyć po IngredientPurchase?
        public int Id { get; set; }

        [MaxLength(60)]
        public string Name { get; set; }

        public decimal Netto { get; set; }

        public int VatRate { get; set; }

        public PurchaseGroup Group { get; set; }

        public virtual Purchase Purchase { get; set; }

        public int Count { get; set; }


        public decimal VatValue => Math.Round(Netto * VatRate / 100, 2);

        public decimal Brutto => Netto + VatValue;

        public decimal SumNetto => Netto * Count;

        public decimal SumVatValue => VatValue * Count;

        public decimal SumBrutto => (Netto + VatValue) * Count;


        public PurchaseIngredient() { }

        public PurchaseIngredient(IngredientPurchase ingredientPurchase, int count = 1)
        {
            Name = ingredientPurchase.Name;
            Netto = ingredientPurchase.Netto;
            VatRate = ingredientPurchase.VatRate;
            Group = ingredientPurchase.Group;
            Count = count;
        }
    }
}
