﻿using System;
using System.ComponentModel.DataAnnotations;
using ADMv2.DAL.Enums;

namespace ADMv2.DAL.Entities
{
    public class IngredientPurchase : IBaseEntity
    {
        //BAZOWY ZAKUP DO WYORU Z LISTY
        public int Id { get; set; }

        [MaxLength(60)]
        public string Name { get; set; }

        public decimal Netto { get; set; }

        public int VatRate { get; set; }

        public PurchaseGroup Group { get; set; }

        public bool IsHidden { get; set; }


        public decimal VatValue => Math.Round(Netto * VatRate / 100, 2);

        public string VatRateStr => VatRate + "%";

        public decimal Brutto => Netto + VatValue;
    }
}
