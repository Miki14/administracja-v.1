﻿using System;
using System.ComponentModel.DataAnnotations;
using ADMv2.DAL.Enums;

namespace ADMv2.DAL.Entities
{
    public class OtherTransaction : IBaseEntity
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public OtherTransactionType Type { get; set; }

        public decimal Amount { get; set; }

        [MaxLength(40)]
        public string Description { get; set; }


        public string TypeStr => Type.GetString();
    }
}
