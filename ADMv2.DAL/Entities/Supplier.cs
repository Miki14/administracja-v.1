﻿using System.ComponentModel.DataAnnotations;

namespace ADMv2.DAL.Entities
{
    public class Supplier : IBaseEntity
    {
        public int Id { get; set; }

        [MaxLength(40)]
        public string Name1 { get; set; }

        [MaxLength(40)]
        public string Name2 { get; set; }

        [MaxLength(50)]
        public string Address { get; set; }

        [MaxLength(15)]
        public string NIP { get; set; }

        [MaxLength(20)]
        public string Phone { get; set; }

        public bool IsHidden { get; set; }


        public string FullName => Name1 + " " + Name2;
    }
}
