﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ADMv2.DAL.Entities
{
    public class SaleInvoice : IBaseEntity
    {
        public int Id { get; set; }

        public int Number { get; set; }

        public DateTime Date { get; set; }

        [MaxLength(25)]
        public string PaymentMethod { get; set; }

        [MaxLength(15)]
        public string PaymentDeadline { get; set; }

        [MaxLength(60)]
        public string Note { get; set; }

        public bool Printed { get; set; }

        public decimal Paid { get; set; }

        public decimal SumBrutto { get; set; }

        public bool SentToKsef { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual ICollection<Payment> Payments { get; set; }

        public virtual ICollection<SaleInvoiceIngredient> Ingredients { get; set; }


        public SaleInvoice Clone()
        {
            return (SaleInvoice)MemberwiseClone();
        }

        public decimal Owe => SumBrutto - Paid;

        public void RefreshSumBruttoAndPaid()
        {
            Paid = Payments.Sum(x => x.Amount);
            SumBrutto = Ingredients.Sum(x => x.Brutto);
        }
    }
}