﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ADMv2.DAL.Entities
{
    public class Note : IBaseEntity
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        [MaxLength(100)]
        public string NoteText { get; set; }
    }
}
