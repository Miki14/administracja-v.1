﻿using System.ComponentModel.DataAnnotations;

namespace ADMv2.DAL.Entities
{
    public class User : IBaseEntity
    {
        public int Id { get; set; }

        [MaxLength(40)]
        public string Login { get; set; }

        public string PasswordHash { get; set; }

        [MaxLength(40)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public bool IsAuthorizedToSale { get; set; }
        public bool IsAuthorizedToPurchases { get; set; }
        public bool IsAuthorizedToSummaryAndPrinting { get; set; }
        public bool IsAuthorizedToSystem { get; set; }
    }
}
