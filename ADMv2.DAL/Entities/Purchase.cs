﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ADMv2.DAL.Enums;

namespace ADMv2.DAL.Entities
{
    public class Purchase : IBaseEntity
    {
        public int Id { get; set; }

        [MaxLength(40)]
        public string Number { get; set; }

        public decimal SumNetto { get; set; }

        public decimal SumVat { get; set; }

        public DateTime DateReceive { get; set; }

        public DateTime DateIssue { get; set; }

        public DateTime DatePaymentDeadline { get; set; }

        [MaxLength(80)]
        public string Description { get; set; }

        public DateTime DatePayment { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public decimal AmountPaid { get; set; }


        public virtual Supplier Supplier { get; set; }

        public virtual ICollection<PurchaseIngredient> PurchaseIngredient { get; set; }


        public decimal SumBrutto => SumNetto + SumVat;

        public Purchase() { }

        public Purchase(Purchase purchase)
        {
            Id = purchase.Id;
            Number = purchase.Number;
            SumNetto = purchase.SumNetto;
            SumVat = purchase.SumVat;
            DateReceive = purchase.DateReceive;
            DateIssue = purchase.DateIssue;
            DatePaymentDeadline = purchase.DatePaymentDeadline;
            Description = purchase.Description;
            DatePayment = purchase.DatePayment;
            PaymentMethod = purchase.PaymentMethod;
            AmountPaid = purchase.AmountPaid;
            Supplier = purchase.Supplier;
        }

        public void RefreshSumNettoVat()
        {
            SumNetto = PurchaseIngredient.Sum(x => x.SumNetto);
            SumVat = PurchaseIngredient.Sum(x => x.SumVatValue);
        }
    }
}