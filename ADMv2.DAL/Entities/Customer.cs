﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ADMv2.DAL.Enums;

namespace ADMv2.DAL.Entities
{
    public class Customer : IBaseEntity
    {
        public int Id { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string Address1 { get; set; }

        [MaxLength(50)]
        public string Address2 { get; set; }

        public int PersonsCount { get; set; }

        public decimal Meters { get; set; }

        public bool Parking { get; set; }

        [MaxLength(15)]
        public string NIP { get; set; }

        public CustomerGroup Group { get; set; }

        [MaxLength(20)]
        public string IdCard { get; set; }

        [MaxLength(15)]
        public string Phone { get; set; }

        public DateTime? ContractStartDate { get; set; }

        public DateTime? ContractEndDate { get; set; }

        [MaxLength(50)]
        public string Note { get; set; }

        [MaxLength(30)]
        public string Email { get; set; }

        [MaxLength(15)]
        public string Pesel { get; set; }



        public virtual ICollection<SaleInvoice> Invoices { get; set; }


        public string FullName => LastName + " " + FirstName;
    }
}