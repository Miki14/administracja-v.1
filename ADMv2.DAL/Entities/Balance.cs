﻿using System;

namespace ADMv2.DAL.Entities
{
    public class Balance : IBaseEntity
    {
        public int Id { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public decimal BankAccount { get; set; }

        public decimal Cash { get; set; }

        public bool IsPreviousBalanceIncluded { get; set; }
    }
}
