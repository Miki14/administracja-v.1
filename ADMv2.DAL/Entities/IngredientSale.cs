﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ADMv2.DAL.Entities
{
    public class IngredientSale : IBaseEntity
    {
        public int Id { get; set; }

        [MaxLength(60)]
        public string Name { get; set; }

        [MaxLength(10)]
        public string Symbol { get; set; }

        public decimal Netto { get; set; }

        public int? VatRate { get; set; }

        public bool IsHidden { get; set; }


        public int VatRateValue => VatRate ?? 0;

        public decimal VatValue => Math.Round(Netto * VatRateValue / 100, 2);

        public string VatString => VatRate.HasValue ? VatRate.ToString() : "zw";

        public decimal Brutto => Netto + VatValue;
    }
}
