﻿using System.ComponentModel.DataAnnotations;

namespace ADMv2.DAL.Entities
{
    public class CompanyData : IBaseEntity
    {
        public int Id { get; set; }

        [MaxLength(40)]
        public string Name1 { get; set; }

        [MaxLength(40)]
        public string Name2 { get; set; }

        [MaxLength(20)]
        public string Address1 { get; set; }

        [MaxLength(20)]
        public string Address2 { get; set; }

        [MaxLength(15)]
        public string NIP { get; set; }

        [MaxLength(15)]
        public string Regon { get; set; }

        [MaxLength(20)]
        public string Phone { get; set; }

        [MaxLength(40)]
        public string BankAccountNo { get; set; }

        [MaxLength(20)]
        public string Logo { get; set; }

        [MaxLength(3)]
        public string InvoiceNo { get; set; }

        public decimal Interest { get; set; }
    }
}
