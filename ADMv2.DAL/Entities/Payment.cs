﻿using ADMv2.DAL.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace ADMv2.DAL.Entities
{
    public class Payment : IBaseEntity
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        public decimal Interest { get; set; }

        public PaymentMethod Method { get; set; }

        public PaymentDocument DocumentType { get; set; }

        public int DocumentNumber { get; set; }

        [MaxLength(55)]
        public string Description { get; set; }

        public virtual SaleInvoice Invoice { get; set; }


        public string MethodStr => Method.GetString();
        public string DocumentTypeStr => DocumentType.GetString();
        public decimal TotalAmount => Amount + Interest;
    }
}
