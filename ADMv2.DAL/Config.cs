﻿namespace ADMv2.DAL
{
    public class Config
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ConnectionString { get; set; }
        public string Show { get; set; }
    }
}
