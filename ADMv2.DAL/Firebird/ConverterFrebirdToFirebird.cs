﻿using System;
using System.IO;
using System.Text;
using Administracja;
using ADMv2.DAL.Enums;

namespace ADMv2.DAL.Firebird
{
    public class ConverterFrebirdToFirebird
    {
        private string ConnectionStringAllPio = @"D:\Drive\Programowanie\VS\Administracja\AdministracjaGit\Administracja\bin\Debug\Baza\PiotrkowskaAll.FDB";
        private string ConnectionStringAllKil = @"D:\Drive\Programowanie\VS\Administracja\AdministracjaGit\Administracja\bin\Debug\Baza\KilinskiegoAll.FDB";

        private string ConnectionStringPio = @"D:\Drive\Programowanie\VS\Administracja\AdministracjaGit\Administracja\bin\Debug\Baza\Piotrkowska.FDB";
        private string ConnectionStringKil = @"D:\Drive\Programowanie\VS\Administracja\AdministracjaGit\Administracja\bin\Debug\Baza\Kilinskiego.FDB";

        private string ConnectionString2014Pio = @"D:\Drive\Programowanie\VS\Administracja\AdministracjaGit\Administracja\bin\Debug\Baza\PoprzednieLata\2014-PIO.FDB";
        private string ConnectionString2015Pio = @"D:\Drive\Programowanie\VS\Administracja\AdministracjaGit\Administracja\bin\Debug\Baza\PoprzednieLata\2015-PIO.FDB";
        private string ConnectionString2016Pio = @"D:\Drive\Programowanie\VS\Administracja\AdministracjaGit\Administracja\bin\Debug\Baza\PoprzednieLata\2016-PIO.FDB";

        private string ConnectionString2014Kil = @"D:\Drive\Programowanie\VS\Administracja\AdministracjaGit\Administracja\bin\Debug\Baza\PoprzednieLata\2014-KIL.FDB";
        private string ConnectionString2015Kil = @"D:\Drive\Programowanie\VS\Administracja\AdministracjaGit\Administracja\bin\Debug\Baza\PoprzednieLata\2015-KIL.FDB";
        private string ConnectionString2016Kil = @"D:\Drive\Programowanie\VS\Administracja\AdministracjaGit\Administracja\bin\Debug\Baza\PoprzednieLata\2016-KIL.FDB";

        public void ConvertDb(int counter)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            //CzyscNumeryWplat(ConnectionString2014Pio, 2014);
            //return;

            if (counter == 0)
            {
                if (!File.Exists(ConnectionStringAllPio))
                {
                    File.Copy(ConnectionStringPio, ConnectionStringAllPio);
                }

                Copy(ConnectionStringAllPio, ConnectionString2014Pio, 2014, true);
            }
            if (counter == 1)
            {
                Copy(ConnectionStringAllPio, ConnectionString2015Pio, 2015, true);
            }
            if (counter == 2)
            {
                Copy(ConnectionStringAllPio, ConnectionString2016Pio, 2016, true);
            }

            if (counter == 3)
            {
                if (!File.Exists(ConnectionStringAllKil))
                {
                    File.Copy(ConnectionStringKil, ConnectionStringAllKil);
                }

                Copy(ConnectionStringAllKil, ConnectionString2014Kil, 2014, false);
            }
            if (counter == 4)
            {
                Copy(ConnectionStringAllKil, ConnectionString2015Kil, 2015, false);
            }
            if (counter == 5)
            {
                Copy(ConnectionStringAllKil, ConnectionString2016Kil, 2016, false);
            }
        }

        private void Copy(string dbNameAkt, string dbNameOld, int rok, bool isPio)
        {
            KopiujOsoby(dbNameAkt, dbNameOld, "OSOBY", "NUMERACJA_OSOBY", 15, rok);
            KopiujFaktury(dbNameAkt, dbNameOld, "FAKTURY", "NUMERACJA_FAKTURY", 9, rok, isPio);
            KopiujWplaty(dbNameAkt, dbNameOld, "WPLATY", "NUMERACJA_WPLATY", 8, rok, isPio);
            KopiujSkladniki(dbNameAkt, dbNameOld, "SKLADNIKI", "NUMERACJA_SKLADNIKI", 4);
            KopiujFakturySkladniki(dbNameAkt, dbNameOld, "FAKTURY_SKLADNIKI", "NUMERACJA_FAKTURY_SKLADNIKI", 3);

            KopiujDostawcy(dbNameAkt, dbNameOld, "DOSTAWCY", "NUMERACJA_DOSTAWCY", 5);
            KopiujSkladniki_Z(dbNameAkt, dbNameOld, "SKLADNIKI_Z", "NUMERACJA_SKLADNIKI_Z", 4);
            KopiujZakupy(dbNameAkt, dbNameOld, "ZAKUPY", "NUMERACJA_ZAKUPY", 11);
            KopiujZakupySkladniki(dbNameAkt, dbNameOld, "ZAKUPY_SKLADNIKI", "NUMERACJA_ZAKUPY_SKLADNIKI", 4);

            KopiujSalda(dbNameAkt, dbNameOld, "SALDA", "NUMERACJA_SALDA", 3);
            KopiujInneObroty(dbNameAkt, dbNameOld, "INNE_OBROTY", "NUMERACJA_INNE_OBROTY", 4);
        }

        private void KopiujOsoby(string dbNameAkt, string dbNameOld, string tableName, string generator, int columnCount, int rok)
        {
            OpenConn(dbNameOld);
            var readerOld = Baza.Wczytaj2D("SELECT * FROM " + tableName);
            Baza.CloseConnection();

            OpenConn(dbNameAkt);
            var readerNew = Baza.Wczytaj2D("SELECT * FROM " + tableName);

            foreach (var elemNew in readerNew)
            {
                if (string.IsNullOrWhiteSpace(elemNew[11]))
                {
                    readerOld.RemoveAll(x => x[1].Equals(elemNew[1])
                    && x[2].Equals(elemNew[2])
                    && x[3].Equals(elemNew[3])
                    && x[4].Equals(elemNew[4])
                    && x[5].Equals(elemNew[5]));
                }
                else
                {
                    readerOld.RemoveAll(x => x[11].Equals(elemNew[11]));
                }
            }

            int counter = 0;
            foreach (var item in readerOld)
            {
                //OSOBY
                string toUpdate = "INSERT INTO " + tableName + " VALUES (GEN_ID(" + generator + ",1),'";
                for (int i = 1; i <= columnCount; i++)
                {
                    if (i == columnCount)
                        toUpdate += item[i] + "')";
                    else if (i == 12) //Ustawienie Grupa = Ukryci
                        toUpdate += "100','";
                    else
                        toUpdate += item[i] + "','";
                }

                //FV JAKO BILANS OTWARCIA
                Baza.Zapisz("INSERT INTO FAKTURY VALUES(GEN_ID(NUMERACJA_FAKTURY,1),'0','" + rok + ".01.01',GEN_ID(NUMERACJA_OSOBY,0),'0','0','0','0','','T')");

                Baza.Zapisz(toUpdate);

                counter++;
            }

            Baza.CloseConnection();
        }

        private void KopiujFaktury(string dbNameAkt, string dbNameOld, string tableName, string generator, int columnCount, int rok, bool isPio)
        {
            OpenConn(dbNameOld);
            var readerOld = Baza.Wczytaj2D("SELECT f.ID, f.NUMER, f.DATA, f.ID_N, f.METODA_PLATNOSCI, f.TERMIN_PLATNOSCI, f.SUMA_B, f.OPLATA, f.NOTATKA, f.WYDRUKOWANO, o.Nazwisko, o.Imie, o.Nip " +
                "FROM " + tableName + " f JOIN OSOBY o ON f.ID_N = o.ID");
            Baza.CloseConnection();

            OpenConn(dbNameAkt);

            //Usuń wszytkie faktury - bilanse w 2017 roku 
            //if (rok == 2014)
            //{
            //    Baza.Zapisz("DELETE FROM FAKTURY f WHERE f.NUMER = 0");
            //}

            foreach (var readerOldItem in readerOld)
            {
                string Id_N = "0";
                if (!string.IsNullOrWhiteSpace(readerOldItem[12]))
                    Id_N = Baza.Wczytaj("SELECT Id FROM OSOBY WHERE Nip = '" + readerOldItem[12] + "'");
                if (string.IsNullOrWhiteSpace(readerOldItem[12]) || Id_N == "0")
                    Id_N = Baza.Wczytaj("SELECT Id FROM OSOBY WHERE Nazwisko = '" + readerOldItem[10] + "' AND Imie = '" + readerOldItem[11] + "'");

                //if (Id_N == "71" || readerOldItem[10] == "Rybak")
                //{
                //    int aa = 15;
                //}

                string toUpdate = "INSERT INTO " + tableName + " VALUES (GEN_ID(" + generator + ",1),'";
                for (int i = 1; i <= columnCount; i++)
                {
                    if (i == columnCount)
                        toUpdate += readerOldItem[i] + "')";
                    else if (i == 2)
                        toUpdate += DateTimeToDateTime(readerOldItem[i]) + "','";
                    else if (i == 3)
                        toUpdate += Id_N + "','";
                    else if (i == 6 || i == 7)
                        toUpdate += readerOldItem[i].Replace(",", ".") + "','";
                    else
                        toUpdate += readerOldItem[i] + "','";
                }

                Baza.Zapisz(toUpdate);
            }

            if (rok == 2016)
            {
                var Id_N_List = Baza.Wczytaj1Dp("SELECT DISTINCT Id_N FROM FAKTURY");

                foreach (var id in Id_N_List)
                {
                    Baza.Zapisz("delete from FAKTURY f where id_n = '" + id + "' and f.NUMER = '0' and f.DATA not in(select min(f.DATA) from faktury f where id_n = '" + id + "')");
                }
            }


            //if (rok == 2014)
            //{
            //    Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '0', OPLATA = '0' WHERE NUMER = '0'");
            //    if (isPio)
            //    {
            //        //PIO
            //        //Rusek
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '620.62', OPLATA = '535.38' WHERE NUMER = '0' AND ID_N = '1'");
            //        //Signum
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '719.73', OPLATA = '196.12' WHERE NUMER = '0' AND ID_N = '12'");
            //    }
            //    else
            //    {
            //        //KIL
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '578.79', OPLATA = '578.79' WHERE NUMER = '0' AND ID_N = '4'"); //Kapusta Iwona
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '0.00', OPLATA = '40.26' WHERE NUMER = '0' AND ID_N = '9'");
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '186.65', OPLATA = '186.65' WHERE NUMER = '0' AND ID_N = '10'");
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '307.50', OPLATA = '307.50' WHERE NUMER = '0' AND ID_N = '13'");
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '504.30', OPLATA = '504.30' WHERE NUMER = '0' AND ID_N = '14'"); // Mielecki
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '1072.31', OPLATA = '1072.31' WHERE NUMER = '0' AND ID_N = '16'");
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '478.69', OPLATA = '478.69' WHERE NUMER = '0' AND ID_N = '20'");
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '101.67', OPLATA = '101.67' WHERE NUMER = '0' AND ID_N = '21'");
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '397.19', OPLATA = '397.19' WHERE NUMER = '0' AND ID_N = '22'");

            //        string Id_N = Baza.Wczytaj("SELECT ID FROM OSOBY WHERE NAZWISKO = 'Celoch' AND IMIE ='Dagmara'"); //8
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '526.29', OPLATA = '526.29' WHERE NUMER = '0' AND ID_N = '" + Id_N + "'");
            //        Id_N = Baza.Wczytaj("SELECT ID FROM OSOBY WHERE NAZWISKO = 'Przedsiębiorstwo Remontowo-' AND IMIE ='Budowlane Janicki Andrzej'"); //12
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '184.50', OPLATA = '184.50' WHERE NUMER = '0' AND ID_N = '" + Id_N + "'");
            //        Id_N = Baza.Wczytaj("SELECT ID FROM OSOBY WHERE NAZWISKO = 'Polskie Towarzystwo Badań nad' AND IMIE ='Powikłaniami Cukrzycy'"); //15
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '331.56', OPLATA = '331.56' WHERE NUMER = '0' AND ID_N = '" + Id_N + "'");
            //        Id_N = Baza.Wczytaj("SELECT ID FROM OSOBY WHERE NAZWISKO = 'Inwestpol Sp.z o.o.'"); //17
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '404.32', OPLATA = '404.32' WHERE NUMER = '0' AND ID_N = '" + Id_N + "'");
            //        Id_N = Baza.Wczytaj("SELECT ID FROM OSOBY WHERE NAZWISKO = 'Polskie Towarzystwo ' AND IMIE ='Diabetologiczne'"); //18
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '1108.20', OPLATA = '1108.20' WHERE NUMER = '0' AND ID_N = '" + Id_N + "'");
            //        Id_N = Baza.Wczytaj("SELECT ID FROM OSOBY WHERE NAZWISKO = 'Sikorski' AND IMIE ='Mariusz'"); //23
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '287.96', OPLATA = '287.96' WHERE NUMER = '0' AND ID_N = '" + Id_N + "'");
            //        Id_N = Baza.Wczytaj("SELECT ID FROM OSOBY WHERE NAZWISKO = 'Rybak' AND IMIE ='Dariusz'"); //24
            //        Baza.Zapisz("UPDATE FAKTURY SET DATA = '" + rok + ".01.01', SUMA_B = '1329.16', OPLATA = '1329.16' WHERE NUMER = '0' AND ID_N = '" + Id_N + "'");
            //    }
            //}

            Baza.CloseConnection();
        }

        private void KopiujWplaty(string dbNameAkt, string dbNameOld, string tableName, string generator, int columnCount, int rok, bool isPio)
        {
            OpenConn(dbNameOld);

            string numerZero = "";
            if (rok != 2014)
            {
                numerZero = " WHERE f.NUMER <> 0";
            }

            var readerOld = Baza.Wczytaj2D("SELECT w.ID, w.ID_F, w.DATA, w.KWOTA, w.ODSETKI, w.SPOSOB, w.DOKUMENT, w.NUMER_DOK, w.OPIS, o.Nazwisko, o.Imie, f.Data, f.Numer, o.Nip FROM "
                + tableName + " w " +
                "JOIN FAKTURY f ON w.ID_F = f.ID " +
                "JOIN OSOBY o ON f.ID_N = o.ID" + numerZero);
            Baza.CloseConnection();

            OpenConn(dbNameAkt);

            //Usuń wszytkie wpłaty na bilans w 2017 roku 
            if (rok == 2014)
            {
                Baza.Zapisz("DELETE FROM WPLATY w WHERE W.ID IN (SELECT w.ID FROM WPLATY w JOIN FAKTURY f ON w.ID_F = f.ID WHERE f.NUMER = 0)");
            }

            foreach (var readerOldItem in readerOld)
            {
                //var Id_N = Baza.Wczytaj("SELECT Id FROM OSOBY WHERE Nazwisko = '" + readerOldItem[9] + "' AND Imie = '" + readerOldItem[10] + "'");
                string Id_N = "0";
                if (!string.IsNullOrWhiteSpace(readerOldItem[13]))
                    Id_N = Baza.Wczytaj("SELECT Id FROM OSOBY WHERE Nip = '" + readerOldItem[13] + "'");
                if (string.IsNullOrWhiteSpace(readerOldItem[13]) || Id_N == "0")
                    Id_N = Baza.Wczytaj("SELECT Id FROM OSOBY WHERE Nazwisko = '" + readerOldItem[9] + "' AND Imie = '" + readerOldItem[10] + "'");

                string Id_F;
                if (readerOldItem[12].Equals("0")) //jeśli numer FV == 0
                    Id_F = Baza.Wczytaj("SELECT Id FROM FAKTURY WHERE ID_N = '" + Id_N + "' AND Numer = '0'");
                else
                    Id_F = Baza.Wczytaj("SELECT Id FROM FAKTURY WHERE ID_N = '" + Id_N + "' AND Data = '" + DateTimeToDateTime(readerOldItem[11]) + "'");

                string toUpdate = "INSERT INTO " + tableName + " VALUES (GEN_ID(" + generator + ",1),'";
                for (int i = 1; i <= columnCount; i++)
                {
                    if (i == columnCount)
                        toUpdate += readerOldItem[i] + "')";
                    else if (i == 1)
                        toUpdate += Id_F + "','";
                    else if (i == 2)
                        toUpdate += DateTimeToDateTime(readerOldItem[i]) + "','";
                    else if (i == 3 || i == 4)
                        toUpdate += readerOldItem[i].Replace(",", ".") + "','";
                    else
                        toUpdate += readerOldItem[i] + "','";
                }

                Baza.Zapisz(toUpdate);
            }

            if (rok == 2016 && !isPio)
            {
                //Dębicka, wpłata na bilans
                Baza.Zapisz("INSERT INTO WPLATY VALUES (GEN_ID(NUMERACJA_WPLATY,1),'511','2017.01.12','278.58','0','G','KP','3','Za czynsz i media XII 2016')");
            }

            //Pierwszy IF powinien rozwiązać problem wpłat na bilans
            //if (rok == 2014)
            //{
            //    //Przy bilansach usunąć wszystkie wplaty i zrobić tylko Rusek i Pek
            //    //Baza.Zapisz("UPDATE WPLATY SET KWOTA = '0' WHERE ID_F IN (SELECT ID FROM FAKTURY WHERE NUMER = '0')");
            //    if (isPio)
            //    {
            //        //PIO
            //        Baza.Zapisz("DELETE FROM WPLATY WHERE ID_F IN (SELECT ID FROM FAKTURY WHERE NUMER = '0')");
            //        //Rusek
            //        Baza.Zapisz("INSERT INTO WPLATY VALUES (GEN_ID(NUMERACJA_WPLATY,1),'1','2014.01.24','535.38','0','P','W','1','')");
            //        //Signum
            //        Baza.Zapisz("INSERT INTO WPLATY VALUES (GEN_ID(NUMERACJA_WPLATY,1),'12','2014.02.21','196.12','0','P','W','2','')");
            //    }
            //    else
            //    {
            //        //TODO: KIL
            //        // Napisać ręczne query do ustawienia bilanów otwarcia tym co trzeba, reszta na 0
            //    }
            //}

            Baza.CloseConnection();
        }

        private void KopiujSkladniki(string dbNameAkt, string dbNameOld, string tableName, string generator, int columnCount)
        {
            OpenConn(dbNameOld);
            var readerOld = Baza.Wczytaj2D("SELECT * FROM " + tableName);
            Baza.CloseConnection();

            OpenConn(dbNameAkt);
            var readerNew = Baza.Wczytaj2D("SELECT * FROM " + tableName);

            foreach (var elemNew in readerNew)
            {
                readerOld.RemoveAll(x => x[0].Equals(elemNew[0]));
            }

            foreach (var readerOldItem in readerOld)
            {
                string toUpdate = "INSERT INTO " + tableName + " VALUES ('";
                for (int i = 0; i <= columnCount; i++)
                {
                    if (i == columnCount)
                        toUpdate += readerOldItem[i] + "')";
                    else
                        toUpdate += readerOldItem[i] + "','";
                }

                Baza.Zapisz(toUpdate);
            }

            Baza.CloseConnection();
        }

        private void KopiujFakturySkladniki(string dbNameAkt, string dbNameOld, string tableName, string generator, int columnCount)
        {
            OpenConn(dbNameOld);
            var readerOld = Baza.Wczytaj2D("SELECT w.ID, w.ID_F, w.ID_S, w.CENA, o.Nazwisko, o.Imie, f.Data, o.Nip " +
            "FROM " + tableName + " w " +
            "JOIN FAKTURY f ON w.ID_F = f.ID " +
            "JOIN OSOBY o ON f.ID_N = o.ID WHERE f.NUMER <> 0");
            Baza.CloseConnection();

            OpenConn(dbNameAkt);

            //Kopiujemy wszystkie skladniki, prócz tych na bilans
            foreach (var readerOldItem in readerOld)
            {
                //var Id_N = Baza.Wczytaj("SELECT Id FROM OSOBY WHERE Nazwisko = '" + readerOldItem[4] + "' AND Imie = '" + readerOldItem[5] + "'");
                string Id_N = "0";
                if (!string.IsNullOrWhiteSpace(readerOldItem[7]))
                    Id_N = Baza.Wczytaj("SELECT Id FROM OSOBY WHERE Nip = '" + readerOldItem[7] + "'");
                if (string.IsNullOrWhiteSpace(readerOldItem[7]) || Id_N == "0")
                    Id_N = Baza.Wczytaj("SELECT Id FROM OSOBY WHERE Nazwisko = '" + readerOldItem[4] + "' AND Imie = '" + readerOldItem[5] + "'");

                var Id_F = Baza.Wczytaj("SELECT Id FROM FAKTURY WHERE ID_N = '" + Id_N + "' AND Data = '" + DateTimeToDateTime(readerOldItem[6]) + "'");

                string toUpdate = "INSERT INTO " + tableName + " VALUES (GEN_ID(" + generator + ",1),'";
                for (int i = 1; i <= columnCount; i++)
                {
                    if (i == columnCount)
                        toUpdate += readerOldItem[i].Replace(",", ".") + "')";
                    else if (i == 1)
                        toUpdate += Id_F + "','";
                    else
                        toUpdate += readerOldItem[i] + "','";
                }

                Baza.Zapisz(toUpdate);
            }

            Baza.CloseConnection();
        }

        private void KopiujDostawcy(string dbNameAkt, string dbNameOld, string tableName, string generator, int columnCount)
        {
            OpenConn(dbNameOld);
            var readerOld = Baza.Wczytaj2D("SELECT * FROM " + tableName + " WHERE ID <> 0");
            Baza.CloseConnection();

            OpenConn(dbNameAkt);
            var readerNew = Baza.Wczytaj2D("SELECT * FROM " + tableName);

            foreach (var elemNew in readerNew)
            {
                readerOld.RemoveAll(x => x[0].Equals(elemNew[0]));
            }

            foreach (var readerOldItem in readerOld)
            {
                string toUpdate = "INSERT INTO " + tableName + " VALUES ('";
                for (int i = 0; i <= columnCount; i++)
                {
                    if (i == columnCount)
                        toUpdate += readerOldItem[i] + "')";
                    else
                        toUpdate += readerOldItem[i] + "','";
                }

                Baza.Zapisz(toUpdate);
            }

            Baza.CloseConnection();
        }

        private void KopiujSkladniki_Z(string dbNameAkt, string dbNameOld, string tableName, string generator, int columnCount)
        {
            OpenConn(dbNameOld);
            var readerOld = Baza.Wczytaj2D("SELECT * FROM " + tableName);
            Baza.CloseConnection();

            OpenConn(dbNameAkt);
            var readerNew = Baza.Wczytaj2D("SELECT * FROM " + tableName);

            foreach (var elemNew in readerNew)
            {
                readerOld.RemoveAll(x => x[0].Equals(elemNew[0]));
            }

            foreach (var readerOldItem in readerOld)
            {
                string toUpdate = "INSERT INTO " + tableName + " VALUES ('";
                for (int i = 0; i <= columnCount; i++)
                {
                    if (i == columnCount)
                        toUpdate += readerOldItem[i] + "')";
                    else if (i == 2)
                        toUpdate += readerOldItem[i].Replace(",", ".") + "','";
                    else
                        toUpdate += readerOldItem[i] + "','";
                }

                Baza.Zapisz(toUpdate);
            }

            Baza.CloseConnection();
        }

        private void KopiujZakupy(string dbNameAkt, string dbNameOld, string tableName, string generator, int columnCount)
        {
            OpenConn(dbNameOld);
            var readerOld = Baza.Wczytaj2D("SELECT * FROM " + tableName);
            Baza.CloseConnection();

            OpenConn(dbNameAkt);

            foreach (var readerOldItem in readerOld)
            {
                string toUpdate = "INSERT INTO " + tableName + " VALUES (GEN_ID(" + generator + ",1),'";
                for (int i = 1; i <= columnCount; i++)
                {
                    if (i == columnCount)
                        toUpdate += readerOldItem[i].Replace(",", ".") + "')";
                    else if (i == 5 || i == 6 || i == 7 || i == 9)
                        toUpdate += DateTimeToDateTime(readerOldItem[i]) + "','";
                    else if (i == 3 || i == 4)
                        toUpdate += readerOldItem[i].Replace(",", ".") + "','";
                    else
                        toUpdate += readerOldItem[i] + "','";
                }

                Baza.Zapisz(toUpdate);
            }

            Baza.CloseConnection();
        }

        private void KopiujZakupySkladniki(string dbNameAkt, string dbNameOld, string tableName, string generator, int columnCount)
        {
            OpenConn(dbNameOld);
            var readerOld = Baza.Wczytaj2D("SELECT r.ID, r.ID_Z, r.ID_S, r.ILOSC, r.NETTO, z.ID_D, z.NUMER, z.SUMA_NETTO, z.SUMA_VAT, z.OPIS " +
            "FROM " + tableName + " r JOIN ZAKUPY Z ON r.ID_Z = z.ID");
            Baza.CloseConnection();

            OpenConn(dbNameAkt);
            int counter = 0;

            foreach (var readerOldItem in readerOld)
            {
                var Id_Z = Baza.Wczytaj("SELECT Id FROM ZAKUPY WHERE ID_D = '" + readerOldItem[5]
                + "' AND NUMER = '" + readerOldItem[6]
                + "' AND SUMA_NETTO = '" + readerOldItem[7].Replace(",", ".")
                + "' AND SUMA_VAT = '" + readerOldItem[8].Replace(",", ".")
                + "' AND OPIS = '" + readerOldItem[9] + "'");

                string toUpdate = "INSERT INTO " + tableName + " VALUES (GEN_ID(" + generator + ",1),'";
                for (int i = 1; i <= columnCount; i++)
                {
                    if (i == columnCount)
                        toUpdate += readerOldItem[i].Replace(",", ".") + "')";
                    else if (i == 1)
                        toUpdate += Id_Z + "','";
                    else
                        toUpdate += readerOldItem[i] + "','";
                }

                Baza.Zapisz(toUpdate);

                counter++;
            }

            Baza.CloseConnection();
        }

        private void KopiujSalda(string dbNameAkt, string dbNameOld, string tableName, string generator, int columnCount)
        {
            OpenConn(dbNameOld);
            var readerOld = Baza.Wczytaj2D("SELECT * FROM " + tableName + " WHERE DATA < '2016.12.30'");
            Baza.CloseConnection();

            OpenConn(dbNameAkt);

            foreach (var readerOldItem in readerOld)
            {
                string toUpdate = "INSERT INTO " + tableName + " VALUES (GEN_ID(" + generator + ",1),'";
                for (int i = 1; i <= columnCount; i++)
                {
                    if (i == columnCount)
                        toUpdate += readerOldItem[i].Replace(",", ".") + "')";
                    else if (i == 1)
                        toUpdate += DateTimeToDateTime(readerOldItem[i]) + "','";
                    else if (i == 2)
                        toUpdate += readerOldItem[i].Replace(",", ".") + "','";
                    else
                        toUpdate += readerOldItem[i] + "','";
                }

                Baza.Zapisz(toUpdate);
            }

            Baza.CloseConnection();
        }

        private void KopiujInneObroty(string dbNameAkt, string dbNameOld, string tableName, string generator, int columnCount)
        {
            OpenConn(dbNameOld);
            var readerOld = Baza.Wczytaj2D("SELECT * FROM " + tableName);
            Baza.CloseConnection();

            OpenConn(dbNameAkt);

            foreach (var readerOldItem in readerOld)
            {
                string toUpdate = "INSERT INTO " + tableName + " VALUES (GEN_ID(" + generator + ",1),'";
                for (int i = 1; i <= columnCount; i++)
                {
                    if (i == columnCount)
                        toUpdate += readerOldItem[i].Replace(",", ".") + "')";
                    else if (i == 1)
                        toUpdate += DateTimeToDateTime(readerOldItem[i]) + "','";
                    else if (i == 3)
                        toUpdate += readerOldItem[i].Replace(",", ".") + "','";
                    else
                        toUpdate += readerOldItem[i] + "','";
                }

                Baza.Zapisz(toUpdate);
            }

            Baza.CloseConnection();
        }

        private void CzyscNumeryWplat(string dbNameAkt, int rok)
        {
            OpenConn(dbNameAkt);
            var readerOld = Baza.Wczytaj2D("SELECT Id, NUMER_DOK FROM WPLATY");
            foreach (var readerOldItem in readerOld)
            {
                //string newNumer = readerOldItem[1].Replace(rok.ToString(), "").Replace("k", "", StringComparison.OrdinalIgnoreCase);
                string newNumer = readerOldItem[1].Replace(@"/", "");

                Baza.Zapisz("UPDATE WPLATY SET NUMER_DOK = '" + newNumer + "' WHERE ID = '" + readerOldItem[0] + "'");
            }

            Baza.CloseConnection();
        }

        //Funkcje pomocnicze

        private void OpenConn(string connStr)
        {
            Baza.ConnectionStringSet(connStr);
            Baza.OpenConnection();
        }

        private int ConvertToInt(string value)
        {
            if (string.IsNullOrEmpty(value)) return 0;
            if (value.Contains('s')) value = value.Replace("s", "");
            if (value.Contains('n')) value = value.Replace("n", "");

            return Convert.ToInt32(value);
        }

        private int? ConvertToNullableInt(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;
            if (value.Contains('s')) value = value.Replace("s", "");
            if (value.Contains('n')) value = value.Replace("n", "");

            return Convert.ToInt32(value);
        }

        private double ConvertToDouble(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return 0;
            }
            return Convert.ToDouble(value);
        }

        private decimal ConvertToDecimal(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return 0;
            }
            return Convert.ToDecimal(value);
        }

        private DateTime ConvertToDateTime(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return DateTime.MinValue;
            }
            return Convert.ToDateTime(value);
        }

        private DateTime? ConvertToDateTimeNull(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            return Convert.ToDateTime(value);
        }

        private CustomerGroup ConvertOldGroup(string oldGroup)
        {
            switch (oldGroup)
            {
                case "1": return CustomerGroup.Inhabitants;
                case "2": return CustomerGroup.Commercial;
                case "3": return CustomerGroup.Long_Ap;
                case "4": return CustomerGroup.Short_Ap;
                case "5": return CustomerGroup.Parking;
                default: return CustomerGroup.Hidden;
            }
        }

        private PaymentMethod ConvertToPaymentMethod(string oldPaymentMethod)
        {
            return oldPaymentMethod == "G" ? PaymentMethod.Cash : PaymentMethod.BankTransfer;
        }

        private PaymentDocument ConvertToPaymentDocument(string oldPaymentDocument)
        {
            switch (oldPaymentDocument)
            {
                case "FV": return PaymentDocument.Invoice;
                case "W": return PaymentDocument.Statement;
                default: return PaymentDocument.KP;
            }
        }

        private OtherTransactionType ConvertToOtherTransactionType(string oldOtherTransactionType)
        {
            switch (oldOtherTransactionType)
            {
                case "konto -> kasa": return OtherTransactionType.AccountToCashdesk;
                case "konto -> inne": return OtherTransactionType.AccountToOther;
                case "kasa -> konto": return OtherTransactionType.CashdeskToAccount;
                case "kasa -> inne": return OtherTransactionType.CashdeskToOther;
                case "inne -> konto": return OtherTransactionType.OtherToAccount;
                default: return OtherTransactionType.OtherToCashdesk;
            }
        }

        private PurchaseGroup ConvertToPurchaseGroup(string oldPurchaseGroup)
        {
            return (PurchaseGroup)ConvertToInt(oldPurchaseGroup);
        }

        private string DateTimeToDateTime(string oldDateTime)
        {
            return ConvertToDateTime(oldDateTime).ToString("yyyy.MM.dd");
        }
    }
}
