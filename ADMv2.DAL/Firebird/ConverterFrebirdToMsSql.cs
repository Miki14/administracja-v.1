﻿using System;
using System.Collections.Generic;
using System.Text;
using Administracja;
using ADMv2.CommonProj;
using ADMv2.DAL.Adapters;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;

namespace ADMv2.DAL.Firebird
{
    public class ConverterFrebirdToMsSql
    {
        private readonly CustomersAdapter customersAdapter = new();
        private readonly InvoicesAdapter invoicesAdapter = new();
        private readonly PaymentAdapter paymentAdapter = new();
        private readonly InvoiceIngredientAdapter invoiceIngredientAdapter = new();
        private readonly CompanyDataAdapter companyDataAdapter = new();
        private readonly SupplierAdapter supplierAdapter = new();
        private readonly OtherTransactionAdapter otherTransactionAdapter = new();
        private readonly BalanceAdapter balanceAdapter = new();
        private readonly PurchaseAdapter purchaseAdapter = new();
        private readonly PurchaseIngredientAdapter purchaseIngredientAdapter = new();
        private readonly IngredientPurchaseAdapter ingredientPurchaseAdapter = new();

        public void ConvertDb(int counter)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            string dbName = ConnectionStringsManager.SelectedConfig.Name;

            Baza.ConnectionStringSet(@"D:\Drive\Programowanie\VS\Administracja\AdministracjaGit\Administracja\bin\Debug\Baza\" + dbName + ".FDB");
            Baza.OpenConnection();

            if (counter == 0)
            {
                CopyCustomers();
                CopyInvoices();
                CopyIndergredientsSale();
                CopyInvoiceIngredient();
            }

            if (counter == 1)
            {
                CopyPayments();
                CopyCompanyData();
                CopyOtherTransaction();
                CopyBalance();
            }

            if (counter == 2)
            {
                CopySupplier();
                CopyPurchase();
                CopyPurchaseIngredient();
                CopyIngredientPurchase();
                SeedUsers();
            }

            Baza.CloseConnection();
        }

        private void CopyCustomers()
        {
            var reader = Baza.Wczytaj2D("SELECT * FROM OSOBY ORDER BY GRUPA, NAZWISKO, IMIE");
            var toAdd = new List<Customer>();

            foreach (var item in reader)
            {
                toAdd.Add(new Customer()
                {
                    LastName = item[1],
                    FirstName = item[2],
                    Address1 = item[4] + " " + item[5] + " " + item[6],
                    Address2 = item[7] + " " + item[3],
                    PersonsCount = ConvertToInt(item[8]),
                    Meters = ConvertToDecimal(item[9]),
                    Parking = item[10] == "t",
                    NIP = NipTrim(item[11]),
                    Group = ConvertOldGroup(item[12]),
                    IdCard = item[13],
                    Phone = "",
                    ContractStartDate = null,
                    ContractEndDate = ConvertToDateTimeNull(item[14]),
                    Note = item[15],
                    Email = "",
                    Pesel = ""
                });

            }

            customersAdapter.AddRange(toAdd);
        }

        private void CopyInvoices()
        {
            var reader = Baza.Wczytaj2D("SELECT * FROM FAKTURY ORDER BY DATA");

            foreach (var item in reader)
            {
                var customerOld = Baza.Wczytaj1D("SELECT nazwisko, imie FROM OSOBY WHERE ID = '" + item[3] + "'");

                if (!(item[1].Equals("0") && item[6].Equals("0") && item[7].Equals("0")))
                {
                    invoicesAdapter.AddInvoice(new SaleInvoice()
                    {
                        Number = ConvertToInt(item[1]),
                        Date = ConvertToDateTime(item[2]),
                        PaymentMethod = item[4],
                        PaymentDeadline = item[5],
                        Note = item[8],
                        Printed = item[9] == "T",
                        SumBrutto = ConvertToDecimal(item[6]),
                        Paid = ConvertToDecimal(item[7]),
                        SentToKsef = false,
                    },
                    customersAdapter.Get(customerOld[0], customerOld[1]).Id);
                }
            }
        }

        private void CopyPayments()
        {
            var reader = Baza.Wczytaj2D("SELECT ID,ID_F,DATA,KWOTA,ODSETKI,SPOSOB,DOKUMENT,NUMER_DOK,OPIS FROM WPLATY ORDER BY DATA");

            foreach (var item in reader)
            {
                var faktura = Baza.Wczytaj1D("SELECT f.NUMER, f.DATA, f.SUMA_B, o.Nazwisko, o.Imie FROM FAKTURY f JOIN OSOBY o ON o.Id = f.Id_N WHERE f.ID=" + item[1]);

                paymentAdapter.Add(new Payment()
                {
                    Date = ConvertToDateTime(item[2]),
                    Amount = ConvertToDecimal(item[3]),
                    Interest = ConvertToDecimal(item[4]),
                    Method = ConvertToPaymentMethod(item[5]),
                    DocumentType = ConvertToPaymentDocument(item[6]),
                    DocumentNumber = ConvertToInt(item[7]),
                    Description = item[8],
                },
                invoicesAdapter.Get(ConvertToInt(faktura[0]), ConvertToDateTime(faktura[1]), ConvertToDecimal(faktura[2]), faktura[3], faktura[4]).Id);
            }
        }

        private void CopyIndergredientsSale()
        {
            var reader = Baza.Wczytaj2D("SELECT r.ID, r.NAZWA, r.SYMBOL, r.CENA_NETTO, r.VAT FROM SKLADNIKI r ORDER BY ID");

            foreach (var item in reader)
            {
                invoiceIngredientAdapter.AddOrUpdate(new IngredientSale()
                {
                    Name = item[1],
                    Symbol = item[2],
                    Netto = ConvertToDecimal(item[3]),
                    VatRate = ConvertToNullableInt(item[4]),
                    IsHidden = false,
                });
            }
        }

        private void CopyInvoiceIngredient()
        {
            var reader = Baza.Wczytaj2D("SELECT fs.ID_F, s.NAZWA, s.SYMBOL, fs.CENA, s.VAT"
                + " FROM FAKTURY_SKLADNIKI fs"
                + " JOIN SKLADNIKI s on fs.ID_S = s.ID");

            foreach (var item in reader)
            {
                var faktura = Baza.Wczytaj1D("SELECT f.NUMER, f.DATA, f.SUMA_B, o.Nazwisko, o.Imie FROM FAKTURY f JOIN OSOBY o ON o.Id = f.Id_N WHERE f.ID=" + item[0]);

                invoiceIngredientAdapter.AddInvoiceIngredient(new SaleInvoiceIngredient()
                {
                    Name = item[1],
                    Symbol = item[2],
                    Netto = ConvertToDecimal(item[3]),
                    VatRate = ConvertToNullableInt(item[4]),
                    Order = 0,
                },
                invoicesAdapter.Get(ConvertToInt(faktura[0]), ConvertToDateTime(faktura[1]), ConvertToDecimal(faktura[2]), faktura[3], faktura[4]).Id);
            }
        }

        private void CopyCompanyData()
        {
            var reader = Baza.Wczytaj2D("SELECT r.ID, r.NAZWA_1, r.NAZWA_2, r.ADRES_1, r.ADRES_2, r.NIP, r.REGON,"
                + " r.TELEFON, r.KONTO, r.LOGO, r.NUMER_FV FROM DANE_SPRZEDAWCY r");

            foreach (var item in reader)
            {
                companyDataAdapter.AddOrUpdate(new CompanyData()
                {
                    Name1 = item[1],
                    Name2 = item[2],
                    Address1 = item[3],
                    Address2 = item[4],
                    NIP = item[5],
                    Regon = item[6],
                    Phone = item[7],
                    BankAccountNo = item[8],
                    Logo = item[9],
                    InvoiceNo = item[10]
                });
            }
        }

        private void CopySupplier()
        {
            var reader = Baza.Wczytaj2D("SELECT r.ID, r.NAZWA, r.NAZWA_CD, r.ADRES, r.NIP, r.TELEFON FROM DOSTAWCY r ORDER BY ID");

            foreach (var item in reader)
            {
                supplierAdapter.AddOrUpdate(new Supplier()
                {
                    Name1 = item[1],
                    Name2 = item[2],
                    Address = item[3],
                    NIP = NipTrim(item[4]),
                    Phone = item[5],
                    IsHidden = false,
                });
            }
        }

        private void CopyOtherTransaction()
        {
            var reader = Baza.Wczytaj2D("SELECT r.ID, r.DATA, r.TYP, r.KWOTA, r.OPIS FROM INNE_OBROTY r ORDER BY DATA");

            foreach (var item in reader)
            {
                otherTransactionAdapter.AddOrUpdate(new OtherTransaction()
                {
                    Date = ConvertToDateTime(item[1]),
                    Type = ConvertToOtherTransactionType(item[2]),
                    Amount = ConvertToDecimal(item[3]),
                    Description = item[4],
                });
            }
        }

        private void CopyBalance()
        {
            var reader = Baza.Wczytaj2D("SELECT r.ID, r.DATA, r.KONTO, r.KASA FROM SALDA r ORDER BY DATA");

            foreach (var item in reader)
            {
                balanceAdapter.AddOrUpdate(new Balance()
                {
                    DateFrom = ConvertToDateTime(item[1]).GetFirstDayOfMonth(),
                    DateTo = ConvertToDateTime(item[1]),
                    BankAccount = ConvertToDecimal(item[2]),
                    Cash = ConvertToDecimal(item[3]),
                    IsPreviousBalanceIncluded = true,
                });
            }
        }

        private void CopyPurchase()
        {
            var reader = Baza.Wczytaj2D("SELECT r.ID, r.ID_D, r.NUMER, r.SUMA_NETTO, r.SUMA_VAT, r.DATA_OTRZYMANIA, "
                + "r.DATA_WYSTAWIENIA, r.TERMIN_ZAPLATY, r.OPIS, r.DATA_WPLATY, r.SPOSOB_ZAPLATY, r.WPLACONA_KWOTA FROM ZAKUPY r ORDER BY DATA_WYSTAWIENIA");

            foreach (var item in reader)
            {
                var supplierOld = Baza.Wczytaj1D("SELECT nazwa, nip FROM DOSTAWCY WHERE ID = '" + item[1] + "'");

                purchaseAdapter.Add(new Purchase()
                {
                    Number = item[2],
                    SumNetto = ConvertToDecimal(item[3]),
                    SumVat = ConvertToDecimal(item[4]),
                    DateReceive = ConvertToDateTime(item[5]),
                    DateIssue = ConvertToDateTime(item[6]),
                    DatePaymentDeadline = ConvertToDateTime(item[7]),
                    Description = item[8],
                    DatePayment = ConvertToDateTime(item[9]),
                    PaymentMethod = ConvertToPaymentMethod(item[10]),
                    AmountPaid = ConvertToDecimal(item[11]),
                },
                supplierAdapter.Get(supplierOld[0], NipTrim(supplierOld[1])).Id);
            }
        }

        private void CopyIngredientPurchase()
        {
            var reader = Baza.Wczytaj2D("SELECT r.ID, r.NAZWA, r.NETTO, r.VAT, r.GRUPA FROM SKLADNIKI_Z r");

            foreach (var item in reader)
            {
                ingredientPurchaseAdapter.Add(new IngredientPurchase()
                {
                    Name = item[1],
                    Netto = ConvertToDecimal(item[2]),
                    VatRate = ConvertToInt(item[3]),
                    Group = ConvertToPurchaseGroup(item[4]),
                    IsHidden = false,
                });
            }
        }

        private void CopyPurchaseIngredient()
        {
            var reader = Baza.Wczytaj2D("SELECT zs.ID_Z, s.NAZWA, zs.NETTO, s.VAT, s.GRUPA, z.NUMER, z.DATA_WYSTAWIENIA, zs.ILOSC "
                + "FROM ZAKUPY_SKLADNIKI zs "
                + "JOIN SKLADNIKI_Z s on zs.ID_S = s.ID "
                + "JOIN ZAKUPY z on zs.ID_Z = z.ID");

            foreach (var item in reader)
            {
                purchaseIngredientAdapter.Add(new PurchaseIngredient()
                {
                    Name = item[1],
                    Netto = ConvertToDecimal(item[2]),
                    VatRate = ConvertToInt(item[3]),
                    Group = ConvertToPurchaseGroup(item[4]),
                    Count = ConvertToInt(item[7]),
                },
                purchaseAdapter.Get(item[5], ConvertToDateTime(item[6])).Id);
            }
        }

        private void SeedUsers()
        {
            UserAdapter userAdapter = new();
            userAdapter.AddOrUpdate(new User()
            {
                Login = "admin",
                PasswordHash = "AQAAAAIAAYagAAAAEE9EOM67Ew1MZExALK8/Sqp7IK73o46zbL7u6+Pic4fthu+uy4VnXRLNeG+AEu7RNg==",
                Name = "Administrator",
                IsActive = true,
                IsAuthorizedToSale = true,
                IsAuthorizedToPurchases = true,
                IsAuthorizedToSummaryAndPrinting = true,
                IsAuthorizedToSystem = true,
            });

            userAdapter.AddOrUpdate(new User()
            {
                Login = "ela",
                PasswordHash = "AQAAAAIAAYagAAAAEJa2nxwcSRTCVt2t+yCfqUFW27Zo6JAjrluxBhtFDSGmf53TrwCJJCMCrZs1yPYGag==",
                Name = "Elżbieta Gołacka",
                IsActive = true,
                IsAuthorizedToSale = true,
                IsAuthorizedToPurchases = true,
                IsAuthorizedToSummaryAndPrinting = true,
                IsAuthorizedToSystem = true,
            });

            userAdapter.AddOrUpdate(new User()
            {
                Login = "monika",
                PasswordHash = "AQAAAAIAAYagAAAAEADQws98+SVqsyvk1sccPDpiivHhBfQaIJQYZsTkIQu8DWwvJDvmRPpX738aGg04Gg==",
                Name = "Monika",
                IsActive = true,
                IsAuthorizedToSale = false,
                IsAuthorizedToPurchases = true,
                IsAuthorizedToSummaryAndPrinting = false,
                IsAuthorizedToSystem = false,
            });
        }

        #region Funkcje pomocnicze
        private int ConvertToInt(string value)
        {
            return ConvertToNullableInt(value) ?? 0;
        }

        private int? ConvertToNullableInt(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;
            if (value.Contains('s')) value = value.Replace("s", "");
            if (value.Contains('n')) value = value.Replace("n", "");

            return Convert.ToInt32(value);
        }

        private decimal ConvertToDecimal(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return 0;
            }
            return Convert.ToDecimal(value);
        }

        private DateTime ConvertToDateTime(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return DateTime.MinValue;
            }
            return Convert.ToDateTime(value);
        }

        private DateTime? ConvertToDateTimeNull(string value)
        {
            if (string.IsNullOrEmpty(value) || value.Equals("nieokreślony") || value.Equals("nieokreslony"))
                return null;
            return Convert.ToDateTime(value);
        }

        private CustomerGroup ConvertOldGroup(string oldGroup)
        {
            return oldGroup switch
            {
                "1" => CustomerGroup.Inhabitants,
                "2" => CustomerGroup.Commercial,
                "3" => CustomerGroup.Long_Ap,
                "4" => CustomerGroup.Short_Ap,
                "5" => CustomerGroup.Parking,
                _ => CustomerGroup.Hidden,
            };
        }

        private PaymentMethod ConvertToPaymentMethod(string oldPaymentMethod)
        {
            return oldPaymentMethod == "G" ? PaymentMethod.Cash : PaymentMethod.BankTransfer;
        }

        private PaymentDocument ConvertToPaymentDocument(string oldPaymentDocument)
        {
            return oldPaymentDocument switch
            {
                "FV" => PaymentDocument.Invoice,
                "W" => PaymentDocument.Statement,
                _ => PaymentDocument.KP,
            };
        }

        private OtherTransactionType ConvertToOtherTransactionType(string oldOtherTransactionType)
        {
            return oldOtherTransactionType switch
            {
                "konto -> kasa" => OtherTransactionType.AccountToCashdesk,
                "konto -> inne" => OtherTransactionType.AccountToOther,
                "kasa -> konto" => OtherTransactionType.CashdeskToAccount,
                "kasa -> inne" => OtherTransactionType.CashdeskToOther,
                "inne -> konto" => OtherTransactionType.OtherToAccount,
                _ => OtherTransactionType.OtherToCashdesk,
            };
        }

        private PurchaseGroup ConvertToPurchaseGroup(string oldPurchaseGroup)
        {
            return (PurchaseGroup)(ConvertToInt(oldPurchaseGroup) - 1);
        }

        private string NipTrim(string Nip)
        {
            return Nip.Trim().Replace(" ", null).Replace("-", null).Replace("PL", null);
        }
        #endregion
    }
}
