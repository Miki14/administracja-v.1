﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ADMv2.DAL.Enums
{
    public enum CustomerGroup
    {
        [Description("Mieszkańcy")]
        Inhabitants,
        [Description("Uzytkowe")]
        Commercial,
        [Description("Stałe Ap.")]
        Long_Ap,
        [Description("Krótko. Ap.")]
        Short_Ap,
        [Description("Parking")]
        Parking,
        [Description("Ukryci")]
        Hidden
    }

    public static class CustomerGroupExtensions
    {
        public static string GetString(this CustomerGroup customerGroup)
        {
            return new EnumDescriptionConverter().GetEnumDescription(customerGroup);
        }

        public static Dictionary<int, string> GetDictionary(this CustomerGroup customerGroup)
        {
            Dictionary<int, string> ret = new();

            foreach (var t in (CustomerGroup[])Enum.GetValues(typeof(CustomerGroup)))
            {
                ret.Add((int)t, t.GetString());
            }

            return ret;
        }
    }
}
