﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ADMv2.DAL.Enums
{
    public enum OtherTransactionType
    {
        [Description("Konto -> Kasa")]
        AccountToCashdesk = 0,
        [Description("Konto -> Inne")]
        AccountToOther = 1,
        [Description("Kasa -> Konto")]
        CashdeskToAccount = 2,
        [Description("Kasa -> Inne")]
        CashdeskToOther = 3,
        [Description("Inne -> Konto")]
        OtherToAccount = 4,
        [Description("Inne -> Kasa")]
        OtherToCashdesk = 5,
    }

    public static class OtherTransactionTypeExtensions
    {
        public static string GetString(this OtherTransactionType otherTransactionType)
        {
            return new EnumDescriptionConverter().GetEnumDescription(otherTransactionType);
        }

        public static Dictionary<int, string> GetDictionaryString(this OtherTransactionType otherTransactionType)
        {
            Dictionary<int, string> ret = new();

            foreach (var t in (OtherTransactionType[])Enum.GetValues(typeof(OtherTransactionType)))
            {
                ret.Add((int)t, t.GetString());
            }

            return ret;
        }

        public static Dictionary<int, OtherTransactionType> GetDictionary(this OtherTransactionType otherTransactionType)
        {
            Dictionary<int, OtherTransactionType> ret = new();

            foreach (var t in (OtherTransactionType[])Enum.GetValues(typeof(OtherTransactionType)))
            {
                ret.Add((int)t, t);
            }
            return ret;
        }

        public static SortedDictionary<int, string> GetDictionaryStringWithAll(this OtherTransactionType otherTransactionType)
        {
            return new SortedDictionary<int, string>(otherTransactionType.GetDictionaryString())
            {
                { -1, "Wszystkie" }
            };
        }
    }
}

