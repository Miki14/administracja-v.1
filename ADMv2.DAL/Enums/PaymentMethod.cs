﻿using System;
using System.ComponentModel;

namespace ADMv2.DAL.Enums
{
    public enum PaymentMethod
    {
        [Description("Gotówka")]
        Cash = 0,
        [Description("Przelew")]
        BankTransfer = 1
    }

    public static class PaymentMethodExtensions
    {
        public static string GetString(this PaymentMethod paymentMethod)
        {
            return new EnumDescriptionConverter().GetEnumDescription(paymentMethod);
        }

        public static string GetShortString(this PaymentMethod paymentMethod)
        {
            return paymentMethod switch
            {
                PaymentMethod.Cash => "G",
                PaymentMethod.BankTransfer => "P",
                _ => throw new NotImplementedException(),
            };
        }
    }
}
