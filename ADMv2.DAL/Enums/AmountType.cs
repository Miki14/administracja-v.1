﻿namespace ADMv2.DAL.Enums
{
    public enum AmountType
    {
        Netto,
        Vat,
        Brutto
    }
}
