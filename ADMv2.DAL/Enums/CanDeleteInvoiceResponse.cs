﻿namespace ADMv2.DAL.Enums
{
    public enum CanDeleteInvoiceResponse
    {
        Can,
        InvoiceHavePayments,
        InvoiceDoNotHaveLastNumber,
        InvoiceSentToKsef,
    }
}
