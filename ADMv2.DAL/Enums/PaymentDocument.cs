﻿using System;
using System.ComponentModel;

namespace ADMv2.DAL.Enums
{
    public enum PaymentDocument
    {
        [Description("FV")]
        Invoice,
        [Description("KP")]
        KP,
        [Description("Wyciąg")]
        Statement
    }

    public static class PaymentDocumentExtensions
    {
        public static string GetString(this PaymentDocument purchaseGroup, bool isShortString = false)
        {
            return purchaseGroup switch
            {
                PaymentDocument.Invoice => "FV",
                PaymentDocument.KP => "KP",
                PaymentDocument.Statement => isShortString ? "W" : "Wyciąg",
                _ => throw new NotImplementedException(),
            };
        }
    }
}
