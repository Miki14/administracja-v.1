﻿using System.Collections.Generic;
using System;
using System.ComponentModel;

namespace ADMv2.DAL.Enums
{
    public enum PurchaseGroup
    {
        [Description("Wynagrodzenia")]
        Rewards,
        [Description("Remonty")]
        Repairs,
        [Description("Zakupy")]
        Purchases,
        [Description("ZUS")]
        Zus,
        [Description("Podatki")]
        Taxes,
        [Description("Opłaty")]
        Charges
    }

    public static class PurchaseGroupExtensions
    {
        public static string GetString(this PurchaseGroup purchaseGroup)
        {
            return new EnumDescriptionConverter().GetEnumDescription(purchaseGroup);
        }

        public static Dictionary<int, string> GetDictionaryString(this PurchaseGroup purchaseGroup)
        {
            Dictionary<int, string> ret = new();

            foreach (var t in (PurchaseGroup[])Enum.GetValues(typeof(PurchaseGroup)))
            {
                ret.Add((int)t, t.GetString());
            }

            return ret;
        }

        public static Dictionary<int, PurchaseGroup> GetDictionary(this PurchaseGroup purchaseGroup)
        {
            Dictionary<int, PurchaseGroup> ret = new();

            foreach (var t in (PurchaseGroup[])Enum.GetValues(typeof(PurchaseGroup)))
            {
                ret.Add((int)t, t);
            }
            return ret;
        }
    }
}
