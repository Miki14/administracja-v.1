﻿using ADMv2.DAL.Entities;
using System.Data.Entity;

namespace ADMv2.DAL
{
    public class AdmContext : DbContext
    {
        public AdmContext(string connectionString) : base(connectionString) { }

        public DbSet<Balance> Balances { get; set; }
        public DbSet<CompanyData> CompanyData { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<IngredientSale> IngredientSales { get; set; }
        public DbSet<SaleInvoice> Invoices { get; set; }
        public DbSet<SaleInvoiceIngredient> InvoiceIngredients { get; set; }
        public DbSet<IngredientPurchase> IngredientPurchases { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<OtherTransaction> OtherTransactions { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<PurchaseIngredient> PurchaseIngredients { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<User> Users { get; set; }


        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        //}
    }
}
