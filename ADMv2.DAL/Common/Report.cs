﻿//using System;
//using System.IO;
//using System.Windows;

//namespace ADMv2.Common
//{
//    class Report
//    {
//        private const int maxReportsCount = 50;
//        private static string _logPath = @"Logs";
//        private const string logFilename = @"\Log_";
//        private const string logExtension = ".log";

//        private static Report _instance;
//        private static readonly object Locked = new();

//        public Report() { }

//        public static void Log(string log)
//        {
//            InitializeInstance();

//            lock (Locked)
//            {
//                using StreamWriter sw = File.AppendText(_logPath);
//                sw.WriteLine(DateTime.Now + " : " + log);
//            }
//        }

//        public static void Error(Exception ex)
//        {
//            if (!App.isClosing)
//            {
//                string message = ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace;
//                MessageBox.Show(message, @"Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
//                Log(message);

//                App.Close();
//            }
//        }

//        public static MessageBoxResult Message(string message, string header, MessageBoxButton button, MessageBoxImage image, string logMessage = "")
//        {
//            if (!App.isClosing)
//            {
//                Log($"{logMessage}: {message}({header})");
//                return MessageBox.Show(message, header, button, image);
//            }
//            return MessageBoxResult.Cancel;
//        }

//        public static MessageBoxResult MessageInfo(string message, string header)
//        {
//            return Message(message, header, MessageBoxButton.OK, MessageBoxImage.Information, "Information");
//        }

//        public static MessageBoxResult MessageError(string message, string header)
//        {
//            return Message(message, header, MessageBoxButton.OK, MessageBoxImage.Error, "Error");
//        }

//        public static MessageBoxResult MessageQuestion(string message, string header)
//        {
//            return Message(message, header, MessageBoxButton.YesNo, MessageBoxImage.Question, "Question");
//        }

//        private static void InitializeInstance()
//        {
//            if (_instance == null)
//            {
//                if (!Directory.Exists(_logPath)) Directory.CreateDirectory(_logPath);

//                //czyszczenie folderu raportów
//                var pliki = Directory.GetFiles(_logPath);
//                for (int i = 0; i < pliki.Length - maxReportsCount; i++)
//                {
//                    File.Delete(pliki[i]);
//                }

//                //Ustawienie pełnej ściezki dla tej sesji 
//                _logPath += logFilename + DateTime.Now.ToString("yyyy.MM.dd HH_mm_ss") + logExtension;
//                _logPath = _logPath.Replace(":", "_");

//                _instance = new Report();
//            }
//        }
//    }
//}
