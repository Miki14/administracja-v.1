﻿using System;

namespace ADMv2.DAL.Models
{
    public class PurchaseListModel
    {
        public int Id { get; set; }

        public string SupplierName { get; set; }

        public decimal SumBrutto { get; set; }

        public DateTime DatePayment { get; set; }

        public decimal AmountPaid { get; set; }
    }
}