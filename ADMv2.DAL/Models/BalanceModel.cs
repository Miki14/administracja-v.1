﻿using System;

namespace ADMv2.DAL.Models
{
    public class BalanceModel
    {
        public DateTime DateFrom { get; set; } = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
        public DateTime DateTo { get; set; }
        public DateTime PreviousBalanceDate { get; set; }

        public bool IncludePreviousBalance { get; set; }

        public decimal PreviousAccountBalance { get; set; }
        public decimal PreviousCashBalance { get; set; }

        public decimal InputsAccount => InputsAccountInvoices + InputsAccountOtherTransactions;
        public decimal InputsAccountInvoices { get; set; }
        public decimal InputsAccountOtherTransactions { get; set; }
        public decimal InputsCash => InputsCashInvoices + InputsCashOtherTransactions;
        public decimal InputsCashInvoices { get; set; }
        public decimal InputsCashOtherTransactions { get; set; }

        public decimal OutputsAccount => OutputsAccountPurchases + OutputsAccountOtherTransactions;
        public decimal OutputsAccountPurchases { get; set; }
        public decimal OutputsAccountOtherTransactions { get; set; }
        public decimal OutputsCash => OutputsCashPurchases + OutputsCashOtherTransactions;
        public decimal OutputsCashPurchases { get; set; }
        public decimal OutputsCashOtherTransactions { get; set; }

        public decimal CurrentAccountBalance => PreviousAccountBalance + InputsAccount - OutputsAccount;
        public decimal CurrentCashBalance => PreviousCashBalance + InputsCash - OutputsCash;

        public decimal TotalBalance => CurrentAccountBalance + CurrentCashBalance;

        public BalanceModel()
        {
            DateTo = DateFrom.AddMonths(1).AddDays(-1);
        }
    }
}
