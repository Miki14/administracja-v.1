﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Windows.UI.Popups;

namespace ADMv2.DAL
{
    public static class ConnectionStringsManager
    {
        private static readonly List<Config> _admConfigs;
        private const string ConfigFilePath = @"Config.json";

        public static Config SelectedConfig => _selectedConfig;
        private static Config _selectedConfig;

        static ConnectionStringsManager()
        {
            try
            {
                _admConfigs = JsonConvert.DeserializeObject<List<Config>>(File.ReadAllText(ConfigFilePath));
            }
            catch (Exception ex)
            {
                new MessageDialog("Brak pliku konfiguracyjnego baz danych", "Błąd konfiguracji").ShowAsync();
            }
            _selectedConfig = _admConfigs.FirstOrDefault();
        }

        public static AdmContext GetAdmContext()
        {
            return new AdmContext(SelectedConfig.ConnectionString);
        }

        public static void SelectConnectionString(int key)
        {
            _selectedConfig = _admConfigs.FirstOrDefault(x => x.Id == key);
        }

        public static Dictionary<int, string> GetDatabases()
        {
            var ret = new Dictionary<int, string>();

            foreach (var c in _admConfigs.Where(x => x.Show == "T"))
            {
                ret.Add(c.Id, c.Name);
            }

            return ret;
        }
    }
}
