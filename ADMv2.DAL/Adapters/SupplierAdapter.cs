﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using ADMv2.DAL.Entities;

namespace ADMv2.DAL.Adapters
{
    public class SupplierAdapter
    {
        public Supplier Get(int supplierId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Suppliers.FirstOrDefault(x => x.Id == supplierId);
        }

        public Supplier Get(Purchase purchase)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            var supplierId = context.Purchases.FirstOrDefault(x => x.Id == purchase.Id).Supplier.Id;
            return context.Suppliers.FirstOrDefault(x => x.Id == supplierId);
        }

        public List<Supplier> GetAll(string name, string nip)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            IQueryable<Supplier> searchedSuppliers;

            if (string.IsNullOrEmpty(name) && string.IsNullOrEmpty(nip))
            {
                searchedSuppliers = context.Suppliers;
            }
            else if (!string.IsNullOrEmpty(name) && string.IsNullOrEmpty(nip))
            {
                searchedSuppliers = context.Suppliers.Where(x => x.Name1.Contains(name) || x.Name2.Contains(name));
            }
            else if (string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(nip))
            {
                searchedSuppliers = context.Suppliers.Where(x => x.NIP.Contains(nip));
            }
            else
            {
                searchedSuppliers = context.Suppliers
                    .Where(x => (x.Name1.Contains(name)
                    || x.Name2.Contains(name))
                    && x.NIP.Contains(nip));
            }

            return searchedSuppliers.OrderBy(x => x.Name1).ToList();
        }

        public Supplier Get(string name, string nip)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Suppliers.FirstOrDefault(x => x.Name1 == name && x.NIP == nip);
        }

        public int AddOrUpdate(Supplier supplier)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.Suppliers.AddOrUpdate(supplier);
            context.SaveChanges();
            return supplier.Id;
        }

        public void Remove(int supplierId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.Suppliers.Remove(
                context.Suppliers.FirstOrDefault(x => x.Id == supplierId));
            context.SaveChanges();
        }
    }
}
