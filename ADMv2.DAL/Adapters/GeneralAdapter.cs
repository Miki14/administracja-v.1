﻿namespace ADMv2.DAL.Adapters
{
    public class GeneralAdapter
    {
        public void ExecuteSqlCommand(string command)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.Database.ExecuteSqlCommand(command);
        }

        //public void ClearDb()
        //{
        //    using AdmContext context = ConnectionStringsManager.GetAdmContext();

        //    //TODO: Czyścić całą bazę
        //    context.Database.ExecuteSqlCommand("DELETE FROM Notes");
        //    context.Database.ExecuteSqlCommand("DELETE FROM Users");
        //    context.Database.ExecuteSqlCommand("DELETE FROM InvoiceIngredients");
        //    context.Database.ExecuteSqlCommand("DELETE FROM PurchaseIngredients");
        //    context.Database.ExecuteSqlCommand("DELETE FROM Purchases");
        //    context.SaveChanges();
        //    context.Database.ExecuteSqlCommand("DELETE FROM Suppliers");
        //    context.Database.ExecuteSqlCommand("DELETE FROM Balances");
        //    context.Database.ExecuteSqlCommand("DELETE FROM OtherTransactions");
        //    context.Database.ExecuteSqlCommand("DELETE FROM CompanyDatas");
        //    context.Database.ExecuteSqlCommand("DELETE FROM IngredientSales");
        //    context.Database.ExecuteSqlCommand("DELETE FROM Payments");
        //    context.SaveChanges();
        //    context.Database.ExecuteSqlCommand("DELETE FROM Invoices");
        //    context.SaveChanges();
        //    context.Database.ExecuteSqlCommand("DELETE FROM Customers");

        //    context.SaveChanges();
        //}
    }
}
