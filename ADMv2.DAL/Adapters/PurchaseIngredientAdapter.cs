﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using ADMv2.DAL.Entities;

namespace ADMv2.DAL.Adapters
{
    public class PurchaseIngredientAdapter : IBaseAdapter<PurchaseIngredient>
    {
        public PurchaseIngredient Get(int Id)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.PurchaseIngredients.FirstOrDefault(x => x.Id == Id);
        }
        public List<PurchaseIngredient> GetAll(int purchaseId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.PurchaseIngredients.Where(x => x.Purchase.Id == purchaseId).OrderBy(x => x.Id).ToList();
        }

        public int AddOrUpdate(PurchaseIngredient element)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.PurchaseIngredients.AddOrUpdate(element);
            context.SaveChanges();
            return element.Id;
        }

        public void Remove(PurchaseIngredient element)
        {
            //TODO: Usunąć tą metodę
            throw new System.NotImplementedException();
        }

        public int Add(PurchaseIngredient purchaseIngredient, int purchaseId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            purchaseIngredient.Purchase = context.Purchases.FirstOrDefault(x => x.Id == purchaseId);
            context.PurchaseIngredients.Add(purchaseIngredient);
            context.SaveChanges();
            return purchaseIngredient.Id;
        }

        public void Update(int purchaseId, IEnumerable<PurchaseIngredient> purchaseIngredients)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            var purchase = context.Purchases.FirstOrDefault(x => x.Id == purchaseId);

            foreach (var purchaseIngredient in purchaseIngredients)
            {
                purchaseIngredient.Purchase = purchase;
            }

            //TODO: Wszystkie składniki są wyrzucane i dodawane od nowa, przy każdym zapisie!!!
            context.PurchaseIngredients.RemoveRange(context.PurchaseIngredients.Where(x => x.Purchase.Id == purchaseId));
            context.PurchaseIngredients.AddRange(purchaseIngredients);
            context.SaveChanges();

            purchase.RefreshSumNettoVat();
            context.SaveChanges();
        }
    }
}
