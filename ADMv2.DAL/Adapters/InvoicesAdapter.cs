﻿using System;
using System.Collections.Generic;
using System.Linq;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;

namespace ADMv2.DAL.Adapters
{
    public class InvoicesAdapter
    {
        public List<SaleInvoice> GetInvoices(int customerId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Invoices.Where(x => x.Customer.Id == customerId).OrderBy(x => x.Date).ThenBy(x => x.Number).ToList();
        }

        public List<SaleInvoice> GetInvoicesDesc(int customerId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Invoices.Where(x => x.Customer.Id == customerId).OrderByDescending(x => x.Date).ThenByDescending(x => x.Number).ToList();
        }

        public SaleInvoice Get(int Id)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Invoices.FirstOrDefault(x => x.Id == Id);
        }

        public SaleInvoice Get(int number, DateTime date, decimal suma_b, string nazwisko, string imie)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            var customer = context.Customers.FirstOrDefault(x => x.LastName == nazwisko && x.FirstName == imie);
            return context.Invoices.FirstOrDefault(x => x.Number == number && x.Date.Month == date.Month
                && x.Date.Year == date.Year && x.Customer.Id == customer.Id);
        }

        public SaleInvoice Get(Payment paymentParam)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            var payment = context.Payments.FirstOrDefault(x => x.Id.Equals(paymentParam.Id));
            return context.Invoices.FirstOrDefault(x => x.Id.Equals(payment.Invoice.Id));
        }

        public int AddInvoiceBaseOn(SaleInvoice invoice, int customerId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            invoice.Date = invoice.Date.AddMonths(1);
            invoice.Printed = false;
            invoice.Number = GetNextFvNumber(invoice.Date);
            invoice.Paid = 0;
            invoice.Customer = context.Customers.FirstOrDefault(x => x.Id == customerId);

            context.Invoices.Add(invoice);
            context.SaveChanges();
            return invoice.Id;
        }

        public int AddInvoice(SaleInvoice invoice, int customerId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            if (invoice == null)
            {
                invoice = new SaleInvoice()
                {
                    Date = DateTime.Today,
                    Printed = false,
                    Number = GetNextFvNumber(DateTime.Today),
                    Paid = 0,
                };
            }

            invoice.Customer = context.Customers.FirstOrDefault(x => x.Id == customerId);
            context.Invoices.Add(invoice);
            context.SaveChanges();
            return invoice.Id;
        }

        public void RemoveInvoice(int invoiceId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            context.Payments.RemoveRange(context.Payments.Where(x => x.Invoice.Id == invoiceId));
            context.InvoiceIngredients.RemoveRange(context.InvoiceIngredients.Where(x => x.Invoice.Id == invoiceId));
            context.Invoices.Remove(context.Invoices.FirstOrDefault(x => x.Id == invoiceId));
            context.SaveChanges();
        }

        public int GetNextFvNumber(DateTime dateFv/*, bool checkDay*/)
        {
            int maxNumber = GetMaxInvoiceNumber(dateFv);

            //TODO: Dodać ?sprawdzanie dnia?
            //if (checkDay)
            //{
            //    string dayOfmaxNumberStr = Wczytaj("SELECT EXTRACT(DAY FROM DATA) FROM FAKTURY WHERE EXTRACT(MONTH FROM DATA) = " +
            //        dataFv.Month + " AND EXTRACT(YEAR FROM DATA) = " + dataFv.Year + "AND NUMER = " + maxNumber);

            //    int dayOfmaxNumber = Convert.ToInt32(dayOfmaxNumberStr);

            //    if (dayOfmaxNumber > dataFv.Day)
            //    {
            //        throw new IndexOutOfRangeException("Day of new invoice is lover than existing invoice");
            //    }
            //}

            return maxNumber + 1;
        }

        public void UpdateInvoice(SaleInvoice invoiceNew, IEnumerable<SaleInvoiceIngredient> invoiceIngredientsNew)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            var invoice = context.Invoices.Find(invoiceNew.Id);
            if (invoice == null)
            {
                return;
            }

            context.Entry(invoice).CurrentValues.SetValues(invoiceNew);
            context.SaveChanges();

            var invoiceNew2 = context.Invoices.FirstOrDefault(x => x.Id == invoiceNew.Id);

            for (int i = 0; i < invoiceIngredientsNew.Count(); i++)
            {
                var invoiceIngredient = invoiceIngredientsNew.ElementAt(i);
                invoiceIngredient.Invoice = invoiceNew2;
                invoiceIngredient.Order = i;
            }

            //TODO: Wszystkie składniki są wyrzucane i dodawane od nowa, przy każdym zapisie!!!
            context.InvoiceIngredients.RemoveRange(context.InvoiceIngredients.Where(x => x.Invoice.Id == invoiceNew.Id));
            context.InvoiceIngredients.AddRange(invoiceIngredientsNew);
            context.SaveChanges();

            invoiceNew2.RefreshSumBruttoAndPaid();
            context.SaveChanges();
        }

        public SaleInvoice GetInvoiceForDelete()
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Invoices.FirstOrDefault(x => x.Id == context.Invoices.Where(y => y.Number != 0).Max(z => z.Id));
        }

        public CanDeleteInvoiceResponse CanDeleteInvoice(int invoiceId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            var invoiceToDelete = context.Invoices.FirstOrDefault(x => x.Id == invoiceId);

            if (invoiceToDelete.SentToKsef)
                return CanDeleteInvoiceResponse.InvoiceSentToKsef;

            if (context.Payments.Any(x => x.Invoice.Id == invoiceToDelete.Id))
                return CanDeleteInvoiceResponse.InvoiceHavePayments;

            int maxNumber = GetMaxInvoiceNumber(invoiceToDelete.Date);
            if (maxNumber != invoiceToDelete.Number)
                return CanDeleteInvoiceResponse.InvoiceDoNotHaveLastNumber;

            return CanDeleteInvoiceResponse.Can;
        }

        public int GetInvoicesCountForSummary(DateTime dateFrom, DateTime dateTo)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Invoices.Where(x => x.Date >= dateFrom && x.Date <= dateTo).Count();
        }

        public List<SaleInvoice> GetInvoicesForSummary(DateTime dateFrom, DateTime dateTo, int skipCounter)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            //var reader2D = Baza.Wczytaj2D("SELECT SKIP " + pozycjiNaStronie * _licznikStron +
            //        " FAKTURY.ID, NUMER, DATA, NAZWISKO, IMIE, MIASTO, ULICA, DOM, MIESZKANIE, NIP FROM FAKTURY" +
            //        " JOIN OSOBY ON FAKTURY.ID_N = OSOBY.ID WHERE DATA >= '" + _dataOd.ToString("dd.MM.yyyy") +
            //        "' AND DATA <= '" + _dataDo.ToString("dd.MM.yyyy") + "' AND NUMER <> 0 ORDER BY NUMER");

            return context.Invoices.Where(x => x.Date >= dateFrom && x.Date <= dateTo && x.Number != 0)
                .OrderBy(x => x.Number)
                .Skip(skipCounter)
                .ToList();
        }

        private int GetMaxInvoiceNumber(DateTime dateFv)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            var allFv = context.Invoices.Where(x => x.Date.Month == dateFv.Month && x.Date.Year == dateFv.Year);
            return allFv.Any() ? allFv.Max(x => x.Number) : 0;
        }
    }
}
