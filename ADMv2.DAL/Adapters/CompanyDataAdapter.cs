﻿using System.Data.Entity.Migrations;
using System.Linq;
using ADMv2.DAL.Entities;

namespace ADMv2.DAL.Adapters
{
    public class CompanyDataAdapter
    {
        public CompanyData Get()
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.CompanyData.FirstOrDefault();
        }

        public void AddOrUpdate(CompanyData companyData)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            context.CompanyData.AddOrUpdate(companyData);
            context.SaveChanges();
        }
    }
}
