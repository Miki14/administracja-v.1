﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Linq;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;

namespace ADMv2.DAL.Adapters
{
    public class OtherTransactionAdapter
    {
        public OtherTransaction Get(int Id)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.OtherTransactions.FirstOrDefault(x => x.Id == Id);
        }

        public List<OtherTransaction> GetAll(int year, OtherTransactionType? otherTransactionType)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            if (!otherTransactionType.HasValue)
                return context.OtherTransactions.Where(x => x.Date.Year == year).OrderByDescending(x => x.Date).ToList();
            return context.OtherTransactions.Where(x => x.Date.Year == year && x.Type == otherTransactionType).OrderByDescending(x => x.Date).ToList();
        }

        public int AddOrUpdate(OtherTransaction otherTransaction)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.OtherTransactions.AddOrUpdate(otherTransaction);
            context.SaveChanges();
            return otherTransaction.Id;
        }

        public void Remove(int otherTransactionId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.OtherTransactions.Remove(
                context.OtherTransactions.FirstOrDefault(x => x.Id == otherTransactionId));
            context.SaveChanges();
        }

        public List<OtherTransaction> GetOtherTransactionsForSummary(DateTime dateFrom, DateTime dateTo, int skipCounter, OtherTransactionType? otherTransactionType)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            if (otherTransactionType.HasValue)
            {
                return context.OtherTransactions.Where(x => x.Date >= dateFrom && x.Date <= dateTo && x.Type == otherTransactionType.Value)
                    .OrderBy(x => x.Date)
                    .Skip(skipCounter)
                    .ToList();
            }
            else
            {
                return context.OtherTransactions.Where(x => x.Date >= dateFrom && x.Date <= dateTo)
                    .OrderBy(x => x.Date)
                    .Skip(skipCounter)
                    .ToList();
            }
        }

        public decimal GetOtherTransactionsSumForSummary(DateTime dateFrom, DateTime dateTo, OtherTransactionType otherTransactionType)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            var ret = context.OtherTransactions.Where(x => x.Date >= dateFrom && x.Date <= dateTo && x.Type == otherTransactionType);
            if (!ret.Any())
                return 0;
            return ret.Sum(x => x.Amount);
        }
    }
}
