﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using ADMv2.DAL.Entities;

namespace ADMv2.DAL.Adapters
{
    public class InvoiceIngredientAdapter : IBaseAdapter<IngredientSale>
    {
        public IngredientSale Get(int ingredientId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.IngredientSales.FirstOrDefault(x => x.Id == ingredientId);
        }

        public List<IngredientSale> GetAll(bool showHidden = false)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            if (showHidden)
                return context.IngredientSales.ToList();
            return context.IngredientSales.Where(x => x.IsHidden == false).ToList();
        }

        public int AddOrUpdate(IngredientSale ingredientSale)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            context.IngredientSales.AddOrUpdate(ingredientSale);
            context.SaveChanges();
            return ingredientSale.Id;
        }

        public void Remove(IngredientSale ingredientSale)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            context.IngredientSales.Remove(context.IngredientSales.FirstOrDefault(x => x.Id == ingredientSale.Id));
            context.SaveChanges();
        }








        //Funkcje do przeniesienia do nowego adaptera odpowiadającego za połącznie składników z FV?

        //Pobiera składniki sprzedaży na podstawie Id FV
        public List<SaleInvoiceIngredient> GetInvoiceIngredients(int invoiceId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            return context.InvoiceIngredients.Where(x => x.Invoice.Id == invoiceId).OrderBy(x=>x.Order).ToList();
        }

        public int AddInvoiceIngredient(SaleInvoiceIngredient invoiceIngredient, int invoiceId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            invoiceIngredient.Invoice = context.Invoices.FirstOrDefault(x => x.Id == invoiceId);
            context.InvoiceIngredients.Add(invoiceIngredient);
            context.SaveChanges();
            return invoiceIngredient.Id;
        }

        public List<int?> GetVatRatesForSummary()
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.InvoiceIngredients.Select(x => x.VatRate).Distinct().OrderBy(x => x.Value).ToList();
        }

        public decimal GetSumOfVatRate(DateTime dateFrom, DateTime dateTo, int vatRate, bool isVat = false)
        {
            //string sumaS = Baza.Wczytaj("SELECT SUM(CENA) FROM Faktury_Skladniki JOIN SKLADNIKI ON FAKTURY_SKLADNIKI.ID_S = SKLADNIKI.ID WHERE VAT = '" + vat + "' AND ID_F IN " +
            //                 "( SELECT ID FROM FAKTURY WHERE DATA >= '" + _dataOd.ToString("dd.MM.yyyy") + "' AND DATA <= '" + _dataDo.ToString("dd.MM.yyyy") + "')").Replace(".", ",");

            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            var query = from ii in context.InvoiceIngredients
                        join i in context.Invoices
                        on ii.Invoice.Id equals i.Id
                        where i.Date >= dateFrom && i.Date <= dateTo && ii.VatRate == vatRate
                        select new { ii.Netto };

            if (isVat)
            {
                decimal sumVatValues = 0;
                foreach (var item in query)
                {
                    sumVatValues += Math.Round(item.Netto * vatRate / 100, 2);
                }
                return sumVatValues;
            }
            else
            {
                var aa = query.ToList();
                return aa.Sum(x => x.Netto);
            }
        }
    }
}
