﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;

namespace ADMv2.DAL.Adapters
{
    public class IngredientPurchaseAdapter : IBaseAdapter<IngredientPurchase>
    {
        public IngredientPurchase Get(int Id)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.IngredientPurchases.FirstOrDefault(x => x.Id == Id);
        }

        public List<IngredientPurchase> GetAll(bool ShowHidden = false, PurchaseGroup? purchaseGroup = null)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            if (ShowHidden && purchaseGroup == null)
                return context.IngredientPurchases.OrderBy(x => x.Name).ToList();
            else if (!ShowHidden && purchaseGroup == null)
                return context.IngredientPurchases.Where(x => x.IsHidden == false).OrderBy(x => x.Name).ToList();
            else if (ShowHidden && purchaseGroup != null)
                return context.IngredientPurchases.Where(x => x.Group == purchaseGroup).OrderBy(x => x.Name).ToList();
            else
                return context.IngredientPurchases.Where(x => x.IsHidden == false && x.Group == purchaseGroup).OrderBy(x => x.Name).ToList();
        }

        public int AddOrUpdate(IngredientPurchase element)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.IngredientPurchases.AddOrUpdate(element);
            context.SaveChanges();
            return element.Id;
        }

        public void Remove(IngredientPurchase element)
        {
            //TODO: Usunąć tą metodę
            throw new System.NotImplementedException();
        }

        public int Add(IngredientPurchase ingredientPurchase)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.IngredientPurchases.Add(ingredientPurchase);
            context.SaveChanges();
            return ingredientPurchase.Id;
        }
    }
}
