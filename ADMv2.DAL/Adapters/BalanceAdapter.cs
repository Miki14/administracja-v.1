﻿using System;
using System.Data;
using System.Data.Entity.Migrations;
using System.Linq;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Models;

namespace ADMv2.DAL.Adapters
{
    public class BalanceAdapter : IBaseAdapter<Balance>
    {
        public Balance Get(int Id)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Balances.Where(x => x.Id == Id).FirstOrDefault();
        }

        public int AddOrUpdate(Balance balance)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.Balances.AddOrUpdate(balance);
            context.SaveChanges();
            return balance.Id;
        }

        public int AddOrUpdate(BalanceModel balanceModel)
        {
            var balance = new Balance()
            {
                BankAccount = balanceModel.CurrentAccountBalance,
                Cash = balanceModel.CurrentCashBalance,
                DateTo = balanceModel.DateTo
                //TODO: Dodać dateFrom i czy poprzednie saldo
            };
            return AddOrUpdate(balance);
        }

        public void Remove(Balance balance)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.Balances.Remove(context.Balances.FirstOrDefault(x => x.Id == balance.Id));
            context.SaveChanges();
        }

        public void RemoveLastBalance(DateTime dateFrom)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            Balance previousBalance = context.Balances.Where(x => x.DateTo <= dateFrom).ToList().MaxBy(x => x.DateTo);
            context.Balances.Remove(previousBalance);
            context.SaveChanges();
        }

        public BalanceModel Calculate(DateTime dateFrom, DateTime dateTo, bool includePreviousBalance)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            Balance previousBalance = context.Balances.Where(x => x.DateTo <= dateFrom).ToList().MaxBy(x => x.DateTo);

            var ret = new BalanceModel();

            ret.DateFrom = dateFrom;
            ret.DateTo = dateTo;
            ret.IncludePreviousBalance = includePreviousBalance;
            ret.PreviousBalanceDate = previousBalance.DateTo;
            ret.PreviousAccountBalance = includePreviousBalance ? previousBalance.BankAccount : 0;
            ret.PreviousCashBalance = includePreviousBalance ? previousBalance.Cash : 0;

            ret.InputsCashInvoices = context.Payments.Where(x => x.Date >= dateFrom && x.Date <= dateTo && x.Method == Enums.PaymentMethod.Cash).ToList()?.Sum(x => x.Amount + x.Interest) ?? 0;
            ret.InputsCashOtherTransactions = context.OtherTransactions.Where(x => x.Date >= dateFrom && x.Date <= dateTo &&
                (x.Type == Enums.OtherTransactionType.OtherToCashdesk || x.Type == Enums.OtherTransactionType.AccountToCashdesk)).ToList()?.Sum(x => x.Amount) ?? 0;

            ret.OutputsCashPurchases = context.Purchases.Where(x => x.DatePayment >= dateFrom && x.DatePayment <= dateTo
                && x.PaymentMethod == Enums.PaymentMethod.Cash).ToList()?.Sum(x => x.AmountPaid) ?? 0;
            ret.OutputsCashOtherTransactions = context.OtherTransactions.Where(x => x.Date >= dateFrom && x.Date <= dateTo &&
                (x.Type == Enums.OtherTransactionType.CashdeskToAccount || x.Type == Enums.OtherTransactionType.CashdeskToOther)).ToList()?.Sum(x => x.Amount) ?? 0; ;

            ret.InputsAccountInvoices = context.Payments.Where(x => x.Date >= dateFrom && x.Date <= dateTo && x.Method == Enums.PaymentMethod.BankTransfer).ToList()?.Sum(x => x.Amount + x.Interest) ?? 0;
            ret.InputsAccountOtherTransactions = context.OtherTransactions.Where(x => x.Date >= dateFrom && x.Date <= dateTo &&
                (x.Type == Enums.OtherTransactionType.OtherToAccount || x.Type == Enums.OtherTransactionType.CashdeskToAccount)).ToList()?.Sum(x => x.Amount) ?? 0;

            ret.OutputsAccountPurchases = context.Purchases.Where(x => x.DatePayment >= dateFrom && x.DatePayment <= dateTo
                && x.PaymentMethod == Enums.PaymentMethod.BankTransfer).ToList()?.Sum(x => x.AmountPaid) ?? 0;
            ret.OutputsAccountOtherTransactions = context.OtherTransactions.Where(x => x.Date >= dateFrom && x.Date <= dateTo &&
                (x.Type == Enums.OtherTransactionType.AccountToCashdesk || x.Type == Enums.OtherTransactionType.AccountToOther)).ToList()?.Sum(x => x.Amount) ?? 0;

            return ret;
        }

        //public List<Balance> GetAll()
        //{
        //    throw new System.NotImplementedException();
        //}
    }
}
