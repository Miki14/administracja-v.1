﻿using System.Collections.Generic;

namespace ADMv2.DAL.Adapters
{
    public interface IBaseAdapter<T>
    {
        //TODO: ZmienićGet na GetById
        public T Get(int Id);

        //public List<T> GetAll();

        public int AddOrUpdate(T element);

        public void Remove(T element);
    }
}
