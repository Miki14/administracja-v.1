﻿using System.Data.Entity.Migrations;
using System.Linq;
using ADMv2.DAL.Entities;
using System.Security.Cryptography;
using System.Text;

namespace ADMv2.DAL.Adapters
{
    public class UserAdapter
    {
        public User Get(int userId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Users.FirstOrDefault(x => x.Id == userId);
        }

        public User Get(string login)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Users.FirstOrDefault(x => x.Login == login);
        }

        //public List<Supplier> GetAll(string name, string nip)
        //{
        //    using AdmContext context = ConnectionStringsManager.GetAdmContext();

        //    IQueryable<Supplier> searchedSuppliers;

        //    if (string.IsNullOrEmpty(name) && string.IsNullOrEmpty(nip))
        //    {
        //        searchedSuppliers = context.Suppliers;
        //    }
        //    else if (!string.IsNullOrEmpty(name) && string.IsNullOrEmpty(nip))
        //    {
        //        searchedSuppliers = context.Suppliers.Where(x => x.Name1.Contains(name) || x.Name2.Contains(name));
        //    }
        //    else if (string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(nip))
        //    {
        //        searchedSuppliers = context.Suppliers.Where(x => x.NIP.Contains(nip));
        //    }
        //    else
        //    {
        //        searchedSuppliers = context.Suppliers
        //            .Where(x => (x.Name1.Contains(name)
        //            || x.Name2.Contains(name))
        //            && x.NIP.Contains(nip));
        //    }

        //    return searchedSuppliers.OrderBy(x => x.Name1).ToList();
        //}

        public int AddOrUpdate(User user)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.Users.AddOrUpdate(user);
            context.SaveChanges();
            return user.Id;
        }

        public void Remove(int userId)
        {
            //using AdmContext context = ConnectionStringsManager.GetAdmContext();
            //context.Users.Remove(
            //    context.Users.FirstOrDefault(x => x.Id == userId));
            //context.SaveChanges();

            //TODO: Nie używać
        }

        public string GetPasswordHashForUser(string login)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Users.FirstOrDefault(x => x.Login == login).PasswordHash;
        }
    }
}
