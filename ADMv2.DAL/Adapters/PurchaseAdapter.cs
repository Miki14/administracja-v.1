﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;
using ADMv2.DAL.Models;

namespace ADMv2.DAL.Adapters
{
    public class PurchaseAdapter
    {
        public Purchase Get(int Id)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            var purchase = context.Purchases.FirstOrDefault(x => x.Id == Id);
            if (purchase.Supplier != null)
                purchase.Supplier = context.Suppliers.FirstOrDefault(x => x.Id == purchase.Supplier.Id);
            return purchase;
        }

        public IEnumerable<PurchaseListModel> GetAll(DateTime? monthYear)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            List<Purchase> purchases;

            if (monthYear == null)
                purchases = context.Purchases.ToList();
            else
                purchases = context.Purchases.Where(x => x.DatePayment.Month == monthYear.Value.Month
                && x.DatePayment.Year == monthYear.Value.Year).ToList();

            List<PurchaseListModel> ret = [];

            foreach (Purchase purchase in purchases)
            {
                var purchaseDb = context.Purchases.FirstOrDefault(x => x.Id == purchase.Id);
                if (purchaseDb != null)
                {
                    var supplierToRet = "";
                    if (purchase.Supplier != null)
                    {
                        var supplier = context.Suppliers.FirstOrDefault(x => x.Id == purchaseDb.Supplier.Id);
                        supplierToRet = supplier.Name1 + " " + supplier.Name2;
                    }

                    ret.Add(new PurchaseListModel
                    {
                        Id = purchaseDb.Id,
                        SupplierName = supplierToRet,
                        SumBrutto = purchaseDb.SumNetto + purchaseDb.SumVat,
                        DatePayment = purchaseDb.DatePayment,
                        AmountPaid = purchaseDb.AmountPaid,
                    });
                }
            }

            return ret;
        }

        public Purchase Get(string number, DateTime dateIssue)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Purchases.FirstOrDefault(x => x.Number == number && x.DateIssue == dateIssue);
        }

        public int Add(Purchase purchase, int supplierId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            purchase.Supplier = context.Suppliers.FirstOrDefault(x => x.Id == supplierId);
            context.Purchases.Add(purchase);
            context.SaveChanges();
            return purchase.Id;
        }

        public int Create()
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            Purchase purchase = new()
            {
                DateIssue = DateTime.Today,
                DateReceive = DateTime.Today,
                DatePayment = DateTime.Today,
                DatePaymentDeadline = DateTime.Today,
            };
            context.Purchases.Add(purchase);
            context.SaveChanges();
            return purchase.Id;
        }

        public int Copy(int sourceId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            var sourcePurchase = context.Purchases.FirstOrDefault(x => x.Id == sourceId);
            var targetPurchase = new Purchase(sourcePurchase);
            context.Purchases.Add(targetPurchase);
            context.SaveChanges();
            return targetPurchase.Id;
        }

        public void Remove(int purchaseId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.PurchaseIngredients.RemoveRange(context.PurchaseIngredients.Where(x => x.Purchase.Id == purchaseId));
            context.Purchases.Remove(context.Purchases.FirstOrDefault(x => x.Id == purchaseId));
            context.SaveChanges();
        }

        public void AddOrUpdate(Purchase purchaseNew)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.Purchases.AddOrUpdate(purchaseNew);
            context.SaveChanges();
        }

        public void UpdatePurchaseSupplier(int purchaseId, int supplierId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            var purchase = context.Purchases.FirstOrDefault(x => x.Id == purchaseId);
            purchase.Supplier = context.Suppliers.FirstOrDefault(x => x.Id == supplierId);
            context.Purchases.AddOrUpdate(purchase);
            context.SaveChanges();
        }

        public int GetPurchasesCountForSummary(DateTime dateFrom, DateTime dateTo)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Purchases.Count(x => x.DatePayment >= dateFrom && x.DatePayment <= dateTo && x.AmountPaid > 0);
        }

        public List<Purchase> GetPurchasesForSummary(DateTime dateFrom, DateTime dateTo, int skipCounter)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            return context.Purchases.Where(x => x.DatePayment >= dateFrom && x.DatePayment <= dateTo && x.AmountPaid > 0)
                .OrderBy(x => x.DatePayment)
                .Skip(skipCounter)
                .ToList();
        }

        public decimal GetPurchaseSumForSummary(DateTime dateFrom, DateTime dateTo, PaymentMethod? paymentMethod, AmountType amountType)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            IQueryable<Purchase> ret;
            if (paymentMethod == null)
                ret = context.Purchases.Where(x => x.DatePayment >= dateFrom && x.DatePayment <= dateTo && x.AmountPaid > 0);
            else
                ret = context.Purchases.Where(x => x.DatePayment >= dateFrom && x.DatePayment <= dateTo && x.AmountPaid > 0 && x.PaymentMethod == paymentMethod);

            if (!ret.Any())
                return 0;
            if (amountType == AmountType.Netto)
                return ret.Sum(x => x.SumNetto);
            if (amountType == AmountType.Vat)
                return ret.Sum(x => x.SumVat);

            return ret.Sum(x => x.SumNetto + x.SumVat);
        }
    }
}
