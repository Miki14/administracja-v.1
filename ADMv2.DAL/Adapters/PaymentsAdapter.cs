﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;

namespace ADMv2.DAL.Adapters
{
    public class PaymentAdapter
    {
        public Payment Get(int paymentId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Payments.FirstOrDefault(x => x.Id == paymentId);
        }

        public List<Payment> GetByInvoiceId(int invoiceId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Payments.Where(x => x.Invoice.Id == invoiceId).ToList();
        }

        public List<Payment> GetByNumber(int dok_number, PaymentDocument paymentDocument, DateTime paymentDate)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Payments.Where(x => x.DocumentNumber == dok_number && x.DocumentType == paymentDocument
            && x.Date == paymentDate).OrderBy(x => x.Date).ToList();
        }

        public int Add(Payment payment, int invoiceId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            payment.Invoice = context.Invoices.FirstOrDefault(x => x.Id == invoiceId);
            context.Payments.Add(payment);
            context.SaveChanges();
            if (payment.Invoice.Number != 0)
            {
                payment.Invoice.RefreshSumBruttoAndPaid();
                context.SaveChanges();
            }
            return payment.Id;
        }

        public int Create(int invoiceId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            var payment = new Payment()
            {
                Date = DateTime.Today,
                Invoice = context.Invoices.FirstOrDefault(x => x.Id == invoiceId)
            };

            context.Payments.Add(payment);
            context.SaveChanges();
            return payment.Id;
        }

        public void Remove(Payment payment)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            var toDel = context.Payments.FirstOrDefault(x => x.Id == payment.Id);
            var invoice = toDel.Invoice;
            context.Payments.Remove(toDel);
            invoice.RefreshSumBruttoAndPaid();
            context.SaveChanges();
        }

        public void AddOrUpdate(Payment payment)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            context.Payments.AddOrUpdate(payment);
            context.SaveChanges();

            context.Payments.FirstOrDefault(x => x.Id == payment.Id).Invoice.RefreshSumBruttoAndPaid();
            context.SaveChanges();
        }

        public int GetInvoiceId(int paymentId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Payments.FirstOrDefault(x => x.Id == paymentId).Invoice.Id;
        }

        public int GetPaymentsCountForSummary(DateTime dateFrom, DateTime dateTo)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Payments.Where(x => x.Date >= dateFrom && x.Date <= dateTo).Count();
        }

        public List<Payment> GetPaymentsForSummary(DateTime dateFrom, DateTime dateTo, int skipCounter)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            //var reader2D = Baza.Wczytaj2D("SELECT SKIP " + pozycjiNaStronie * _licznikStron + " Osoby.ID, Nazwisko, Imie, Dokument, Numer_Dok, Wplaty.Data, Kwota, Odsetki, (Kwota + Odsetki) as"
            //                    + " Kwota_ods FROM WPLATY JOIN FAKTURY ON WPLATY.ID_F = FAKTURY.ID JOIN OSOBY ON FAKTURY.ID_N = OSOBY.ID WHERE WPLATY.DATA >= '" + _dataOd.ToString("dd.MM.yyyy")
            //                    + "' AND WPLATY.DATA <= '" + _dataDo.ToString("dd.MM.yyyy") + "' ORDER BY Grupa, Nazwisko, Imie");

            return context.Payments.Where(x => x.Date >= dateFrom && x.Date <= dateTo)
                .OrderBy(x => x.Invoice.Customer.Group)
                .ThenBy(x => x.Invoice.Customer.LastName)
                .ThenBy(x => x.Invoice.Customer.FirstName)
                .Skip(skipCounter)
                .ToList();
        }

        public decimal GetPaymentsSumForSummary(DateTime dateFrom, DateTime dateTo, PaymentMethod paymentMethod)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            var ret = context.Payments.Where(x => x.Date >= dateFrom && x.Date <= dateTo && x.Method == paymentMethod);
            if (!ret.Any())
                return 0;
            return ret.Sum(x => x.Amount + x.Interest);
        }

        public int GetInterestsCountForSummary(DateTime dateFrom, DateTime dateTo)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            return context.Payments.Where(x => x.Date >= dateFrom && x.Date <= dateTo && x.Interest > 0).Count();
        }

        public List<Payment> GetInterestsForSummary(DateTime dateFrom, DateTime dateTo, int skipCounter)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            //var reader2D = Baza.Wczytaj2D("SELECT SKIP " + pozycjiNaStronie * _licznikStron + " Odsetki, Nazwisko, Imie FROM WPLATY JOIN FAKTURY ON WPLATY.ID_F = FAKTURY.ID JOIN OSOBY ON "
            //        + "FAKTURY.ID_N = OSOBY.ID WHERE WPLATY.DATA >= '" + _dataOd.ToString("dd.MM.yyyy") + "' AND WPLATY.DATA <= '" + _dataDo.ToString("dd.MM.yyyy")
            //        + "' AND ODSETKI <> 0 ORDER BY Grupa, Nazwisko, Imie");

            return context.Payments.Where(x => x.Date >= dateFrom && x.Date <= dateTo && x.Interest > 0)
                .OrderBy(x => x.Invoice.Customer.Group)
                .ThenBy(x => x.Invoice.Customer.LastName)
                .ThenBy(x => x.Invoice.Customer.FirstName)
                .Skip(skipCounter)
                .ToList();
        }

        public decimal GetInterestsSumForSummary(DateTime dateFrom, DateTime dateTo)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            var ret = context.Payments.Where(x => x.Date >= dateFrom && x.Date <= dateTo && x.Interest > 0);
            if (!ret.Any())
                return 0;
            return ret.Sum(x => x.Interest);
        }
    }
}
