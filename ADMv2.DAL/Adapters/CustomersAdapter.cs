﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using ADMv2.DAL.Entities;
using ADMv2.DAL.Enums;

namespace ADMv2.DAL.Adapters
{
    public class CustomersAdapter
    {
        public List<Customer> Get(string search, bool showHidden, bool showOnlyNotPrintedInvoices)
        {
            //TODO: Dodać ignorowaie wielkości liter

            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            //context.Configuration.ProxyCreationEnabled = false;
            //context.Configuration.LazyLoadingEnabled = false;
            IQueryable<Customer> searchedCustomers;

            if (string.IsNullOrEmpty(search))
            {
                searchedCustomers = context.Customers;
            }
            else
            {
                searchedCustomers = context.Customers.Where(x => x.LastName.Contains(search));
            }

            if (!showHidden)
            {
                searchedCustomers = searchedCustomers.Where(x => x.Group != CustomerGroup.Hidden);
            }

            if (showOnlyNotPrintedInvoices)
            {
                //TODO: Sprawdzić czy działa dobrze
                searchedCustomers = searchedCustomers.Where(x => x.Invoices.Any(y => y.Printed == false));
            }

            return searchedCustomers.OrderBy(x => x.Group).ThenBy(x => x.LastName).ThenBy(x => x.FirstName).ToList();
        }

        public Customer Get(int Id)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            return context.Customers.FirstOrDefault(x => x.Id == Id);
        }

        public Customer Get(string lastName, string firstName)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            return context.Customers.FirstOrDefault(x => x.FirstName == firstName && x.LastName == lastName);
        }

        //TODO Przekazywać int Id
        public Customer Get(SaleInvoice invoiceParam)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            var invoice = context.Invoices.FirstOrDefault(x => x.Id.Equals(invoiceParam.Id));
            return context.Customers.FirstOrDefault(x => x.Id == invoice.Customer.Id);
        }

        public Customer GetByPayment(int paymentId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            var payment = context.Payments.FirstOrDefault(x => x.Id.Equals(paymentId));
            var invoice = context.Invoices.FirstOrDefault(x => x.Id.Equals(payment.Invoice.Id));
            return context.Customers.FirstOrDefault(x => x.Id == invoice.Customer.Id);
        }

        public int AddOrUpdate(Customer customer)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            context.Customers.AddOrUpdate(customer);
            context.SaveChanges();

            var customerNew = context.Customers.FirstOrDefault(x => x.Id == customer.Id);

            //if (customerNew.Invoices?.Count == 0)
            //{
            //    context.Invoices.Add(new Invoice()
            //    {
            //        Number = 0,
            //        Date = DateTime.Today,
            //        Printed = true,
            //        Paid = 0,
            //        SumBrutto = 0,
            //        Customer = customerNew,
            //    });
            //}

            context.SaveChanges();

            return customer.Id;
        }

        public void AddRange(List<Customer> customers)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            context.Customers.AddRange(customers);
            context.SaveChanges();
        }

        public void Remove(int customerId)
        {
            using AdmContext context = ConnectionStringsManager.GetAdmContext();

            var custToDel = context.Customers.FirstOrDefault(x => x.Id == customerId);
            context.Customers.Remove(custToDel);
            context.SaveChanges();
        }

        public decimal GetBalanceOfCustomer(Customer customer)
        {
            if (customer?.Id == 0)
                return 0;

            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            var invoices = context.Invoices.Where(x => x.Customer.Id == customer.Id);

            if (invoices.Any())
            {
                decimal sumaB = invoices.Sum(x => x.SumBrutto);
                decimal paid = invoices.Sum(x => x.Paid);
                return paid - sumaB;
            }
            return 0;
        }

        public int GetMaxInvoiceMonth(Customer customer)
        {
            if (customer?.Id == 0)
                return 0;

            using AdmContext context = ConnectionStringsManager.GetAdmContext();
            var customerInvoices = context.Invoices.Where(x => x.Customer.Id == customer.Id);
            return customerInvoices.Any() ? customerInvoices?.Max(x => x.Date).Month ?? 0 : 0;
        }
    }
}
